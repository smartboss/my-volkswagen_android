package com.volkswagen.myvolkswagen.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.PatContent;

import java.util.ArrayList;
import java.util.List;

public class PatMaintenanceResultAdapter extends RecyclerView.Adapter<PatMaintenanceResultAdapter.PatMaintenanceResultHolder> {
    private List<PatContent.PatContentData> list = new ArrayList<>();

    @NonNull
    @Override
    public PatMaintenanceResultHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pat_maintenance, parent, false);
        return new PatMaintenanceResultHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PatMaintenanceResultHolder holder, int position) {
        PatContent.PatContentData data = list.get(position);
        holder.tvPredictDay.setText(data.predictedServiceDate);
        holder.tvPredictMiles.setText(String.format(holder.itemView.getContext().getString(R.string.car_maintenance_km), data.predictedServiceMileage));
        holder.tvServiceMust.setText(data.getAllService());
        holder.imgDivider.setVisibility(position == list.size() - 1 ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<PatContent.PatContentData> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class PatMaintenanceResultHolder extends RecyclerView.ViewHolder {
        private TextView tvPredictDay, tvPredictMiles, tvServiceMust;
        private ImageView imgDivider;

        public PatMaintenanceResultHolder(@NonNull View view) {
            super(view);

            tvPredictDay = view.findViewById(R.id.tv_predict_day);
            tvPredictMiles = view.findViewById(R.id.tv_predict_mile);
            tvServiceMust = view.findViewById(R.id.tv_service_must);
            imgDivider = view.findViewById(R.id.img_divider);
        }
    }
}
