package com.volkswagen.myvolkswagen;

import android.app.Application;

import com.volkswagen.myvolkswagen.controller.PreferenceManager;

public class MyVolkswagen extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.getInstance().init(this);
    }
}
