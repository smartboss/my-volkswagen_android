package com.volkswagen.myvolkswagen.view;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.EventBus.CancelAddCarEvent;
import com.volkswagen.myvolkswagen.model.EventBus.SucceedAddCarEvent;
import com.volkswagen.myvolkswagen.model.GetCarModelData;
import com.volkswagen.myvolkswagen.model.PendingAddCarData;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.VwFirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AddCarFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private static final String ARG_OWNER = "arg_owner";
    private static final String ARG_PENDING_DATA = "arg_pending_data";
    public static final String CAR_PLATE = "CAR_PLATE";
    public static final String CAR_MODEL = "CAR_MODEL";
    public static final String CAR_TYPE_ID ="CAR_TYPE_ID";
    private static final String PLATE_FORMAT = "%1$s-%2$s";

    private BottomSheetDialog mDialog;

    private ViewGroup mViewGroupStep1;
    private ViewGroup mViewGroupStep2;
    private ViewGroup mViewGroupStep3;
    private ViewGroup mViewGroupFindVin;

    private ApiService mApiService;
    private AddCarData mData;
    private PendingAddCarData mPendingData;
    private String mOwner;
    private String mCarType;
    private List<CarModel> mCarModels;
    private int mCurrentStep;

    public static AddCarFragment newInstance(String owner, PendingAddCarData data) {
        AddCarFragment fragment = new AddCarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_OWNER, owner);
        if (data != null) {
            bundle.putString(ARG_PENDING_DATA, new Gson().toJson(data));
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getTheme() {
        return R.style.AppTheme_BottomSheetDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApiService = ApiClientManager.getClient().create(ApiService.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // workaround to expand bottom sheet fully
        mDialog = (BottomSheetDialog) getDialog();
        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(mDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet));
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_HIDDEN);
                                break;
                            case BottomSheetBehavior.STATE_HIDDEN:
                                mDialog.cancel();
                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });

        final View v = inflater.inflate(R.layout.fragment_mycar_add_car, container, false);
        mData = new AddCarData();
        mOwner = getArguments().getString(ARG_OWNER);
        initView(v);
        String pendingData = getArguments().getString(ARG_PENDING_DATA);
        if (pendingData == null) {
            setupView(R.id.view_group_step1_add_car);
        } else {
            mPendingData = new Gson().fromJson(pendingData, PendingAddCarData.class);
            setupView(mPendingData);
        }
        return v;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (mCurrentStep == 1) {
            String owner = TextUtils.isEmpty(mOwner) ? ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_owner)).getText().toString() : mOwner;
            String plate1 = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate1)).getText().toString();
            String plate2 = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate2)).getText().toString();
            String vin = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_vin_abbr)).getText().toString();
            CancelAddCarEvent event = new CancelAddCarEvent(new PendingAddCarData(mCurrentStep, owner, plate1, plate2, vin, null, 0, null, 0));
            EventBus.getDefault().post(event);
        } else if (mCurrentStep == 2) {
            String[] plates = mData.plate.split("-");
            CancelAddCarEvent event = new CancelAddCarEvent(new PendingAddCarData(mCurrentStep, mData.name, plates[0], plates[1], mData.vin, mCarType, mData.typeId, mCarModels, mData.modelId));
            EventBus.getDefault().post(event);
        } else {
            EventBus.getDefault().post(new SucceedAddCarEvent());
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.add_car_step1_btn_next:
                validateData();
                break;
            case R.id.add_car_step2_btn_confirm:
                addCar();
                break;
            case R.id.add_car_step3_btn_complete:
                EventBus.getDefault().post(new SucceedAddCarEvent());
                dismiss();
                break;
            case R.id.add_car_step1_tv_find_vin:
                setupView(R.id.view_group_find_vin_add_car);
                break;
            case R.id.add_car_step2_tv_car_model:
                CharSequence[] items = getModelItems();
                if (items == null) {
                    return;
                }

                AlertUtils.selectItemDialog(getActivity(), null, items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CarModel model = mCarModels.get(which);
                        ((TextView) v).setText(model.name);
                        mData.modelId = model.id;
                    }
                });
                break;
            case R.id.add_car_find_vin_btn_back:
                setupView(R.id.view_group_step1_add_car);
                break;
        }
    }

    private void initView(View v) {
        mViewGroupStep1 = v.findViewById(R.id.view_group_step1_add_car);
        mViewGroupStep2 = v.findViewById(R.id.view_group_step2_add_car);
        mViewGroupStep3 = v.findViewById(R.id.view_group_step3_add_car);
        mViewGroupFindVin = v.findViewById(R.id.view_group_find_vin_add_car);

        if (TextUtils.isEmpty(mOwner)) {
            v.findViewById(R.id.add_car_step1_et_owner).setVisibility(View.VISIBLE);
            v.findViewById(R.id.add_car_step1_tv_owner).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.add_car_step1_et_owner).setVisibility(View.GONE);
            TextView tvOwner = v.findViewById(R.id.add_car_step1_tv_owner);
            tvOwner.setText(mOwner);
            tvOwner.setVisibility(View.VISIBLE);
        }

        v.findViewById(R.id.add_car_step1_btn_next).setOnClickListener(this);
        v.findViewById(R.id.add_car_step1_tv_find_vin).setOnClickListener(this);
        v.findViewById(R.id.add_car_step2_btn_confirm).setOnClickListener(this);
        v.findViewById(R.id.add_car_step2_tv_car_model).setOnClickListener(this);
        v.findViewById(R.id.add_car_step3_btn_complete).setOnClickListener(this);
        v.findViewById(R.id.add_car_find_vin_btn_back).setOnClickListener(this);
    }

    private void setupView(int id) {
        switch (id) {
            case R.id.view_group_step1_add_car:
                mCurrentStep = 1;
                mViewGroupStep1.setVisibility(View.VISIBLE);
                mViewGroupStep2.setVisibility(View.GONE);
                mViewGroupStep3.setVisibility(View.GONE);
                mViewGroupFindVin.setVisibility(View.GONE);
                break;
            case R.id.view_group_step2_add_car:
                mCurrentStep = 2;
                mViewGroupStep1.setVisibility(View.GONE);
                mViewGroupStep2.setVisibility(View.VISIBLE);
                mViewGroupStep3.setVisibility(View.GONE);
                mViewGroupFindVin.setVisibility(View.GONE);

                ((TextView) mViewGroupStep2.findViewById(R.id.add_car_step2_tv_car_type)).setText(mCarType);
                if (mCarModels != null && mCarModels.size() > 0) {
                    CarModel model = mCarModels.get(0);
                    if (mData.modelId > 0) {
                        for (CarModel m : mCarModels) {
                            if (m.id == mData.modelId) {
                                model = m;
                                break;
                            }
                        }
                    }
                    ((TextView) mViewGroupStep2.findViewById(R.id.add_car_step2_tv_car_model)).setText(model.name);
                    mData.modelId = model.id;
                }
                break;
            case R.id.view_group_step3_add_car:
                mCurrentStep = 3;
                mViewGroupStep1.setVisibility(View.GONE);
                mViewGroupStep2.setVisibility(View.GONE);
                mViewGroupStep3.setVisibility(View.VISIBLE);
                mViewGroupFindVin.setVisibility(View.GONE);
                break;
            case R.id.view_group_find_vin_add_car:
                mViewGroupStep1.setVisibility(View.GONE);
                mViewGroupStep2.setVisibility(View.GONE);
                mViewGroupStep3.setVisibility(View.GONE);
                mViewGroupFindVin.setVisibility(View.VISIBLE);
                mViewGroupFindVin.findViewById(R.id.add_car_find_vin_scroll_view).scrollTo(0,0);
                break;
        }
    }

    private void setupView(PendingAddCarData data) {
        if (data.step == 1) {
            if (TextUtils.isEmpty(mOwner)) {
                ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_owner)).setText(data.name);
            }
            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate1)).setText(data.plate1);
            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate2)).setText(data.plate2);
            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_vin_abbr)).setText(data.vin);
            setupView(R.id.view_group_step1_add_car);
        } else if (data.step == 2) {
            mData.name = mPendingData.name;
            mData.plate = String.format(PLATE_FORMAT, mPendingData.plate1, mPendingData.plate2);
            mData.vin = mPendingData.vin;
            mData.typeId = mPendingData.typeId;
            mData.modelId = mPendingData.modelId;
            mCarType = mPendingData.type;
            mCarModels = mPendingData.models;
            setupView(R.id.view_group_step2_add_car);
        }
    }

    private void validateData() {
        String owner = TextUtils.isEmpty(mOwner) ? ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_owner)).getText().toString() : mOwner;
        String plate1 = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate1)).getText().toString();
        String plate2 = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate2)).getText().toString();
        String vin = ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_vin_abbr)).getText().toString();

        if (TextUtils.isEmpty(owner) || TextUtils.isEmpty(plate1) || TextUtils.isEmpty(plate2) || TextUtils.isEmpty(vin)) {
            AlertUtils.showAlert(getActivity(), null, getString(R.string.add_car_step1_msg_fill_up), null, getString(R.string.ok_button), null);
        } else {
            String plate = String.format(PLATE_FORMAT, plate1, plate2);
            mData.name = owner;
            mData.plate = plate;
            mData.vin = vin;
            checkCarExist();
        }
    }

    private CharSequence[] getModelItems() {
        if (mCarModels == null || mCarModels.size() == 0) {
            return null;
        }

        CharSequence[] items = new CharSequence[mCarModels.size()];
        for (int i = 0; i < mCarModels.size(); i++) {
            items[i] = mCarModels.get(i).name;
        }
        return items;
    }

    private void checkCarExist() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        mApiService.getCarExist(mData, PreferenceManager.getInstance().getUserToken())
                .enqueue(new Callback<CarExistData>() {
            @Override
            public void onResponse(Call<CarExistData> call, retrofit2.Response<CarExistData> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to check car exist");
                    return;
                }

                CarExistData data = response.body();
                if (!data.result) {
                    Logger.d(this, "Failed to check car exist: code=" + data.errorCode);
                    TextView error = mViewGroupStep1.findViewById(R.id.add_car_step1_tv_error);
                    switch (data.errorCode) {
                        case 3001:
                            error.setText(R.string.add_car_step1_error_3001);
                            break;
                        case 3003:
                            error.setText(R.string.add_car_step1_error_3003);
                            break;
                        case 3004:
                            error.setText(R.string.add_car_step1_error_3004);
                            break;
                        case 3005:
                        case 3009:
                            error.setText(R.string.add_car_step1_error_3005_3009);
                            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_owner)).setText(null);
                            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate1)).setText(null);
                            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_plate2)).setText(null);
                            ((TextView) mViewGroupStep1.findViewById(R.id.add_car_step1_et_vin_abbr)).setText(null);
                            mData.clearAll();
                            break;
                        case 3010:
                            error.setText(R.string.add_car_step1_error_3010);
                            break;
                    }
                    return;
                }

                mCarType = data.name;
                mData.typeId = data.id;
                getCarModel();
            }

            @Override
            public void onFailure(Call<CarExistData> call, Throwable t) {
                Logger.d(this, "Failed to check car exist: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }

    private void getCarModel() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        mApiService.getCarModelData(new GetCarModelData(mData.typeId), PreferenceManager.getInstance().getUserToken())
                .enqueue(new Callback<CarModelData>() {
            @Override
            public void onResponse(Call<CarModelData> call, retrofit2.Response<CarModelData> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to get car model");
                    return;
                }

                CarModelData data = response.body();
                if (!data.result) {
                    Logger.d(this, "Failed to get car model: code=" + data.errorCode);
                    return;
                }

                mCarModels = data.models;
                setupView(R.id.view_group_step2_add_car);
            }

            @Override
            public void onFailure(Call<CarModelData> call, Throwable t) {
                Logger.d(this, "Failed to get car model: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }

    private void addCar() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        mApiService.addCar(mData, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to add car");
                    return;
                }

                Response data = response.body();
                if (!data.result) {
                    Logger.d(this, "Failed to add car: code=" + data.errorCode);
                    return;
                }
                setupView(R.id.view_group_step3_add_car);
                FirebaseAnalytics.getInstance(getContext()).logEvent(VwFirebaseAnalytics.Event.ADD_CAR_COMPLETE, null);
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                Logger.d(this, "Failed to add car: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }
}
