package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.model.MaintenancePlant;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.PatContent;
import com.volkswagen.myvolkswagen.utils.Const;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

// TODO: 2024/1/14 remove it
public class CarMaintenanceActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = CarMaintenanceActivity.class.getSimpleName();

    private ConstraintLayout layoutFirstOpen;
    private ImageButton btnNotice;

    public static void goCarMaintenance(final Activity activity, ArrayList<MyCarData.CarVinAndPlantData> list) {
        final Intent intent = new Intent(activity, CarMaintenanceActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Const.CAR_LIST, list);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_maintenance);
        EventBus.getDefault().register(this);

        if (getIntent().getExtras() != null) {
            goMaintenanceInputFragment(getIntent().getExtras());
        }

        initView();
    }

    private void initView() {
        //notice handle
        btnNotice = findViewById(R.id.btn_notice);
        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
        btnNotice.setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);

        //first open page
        layoutFirstOpen = findViewById(R.id.layout_first_open_maintenance);
    }

    private void goMaintenanceInputFragment(Bundle bundle) {
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, MaintenanceInputFragment.getInstance(bundle)).commitAllowingStateLoss();
    }

    public void goMaintenanceResultFragment(PatContent content) {
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, MaintenanceResultFragment.getInstance(content)).addToBackStack("").commitAllowingStateLoss();
    }

    public void goMaintenanceAreaFragment(MaintenancePlant maintenancePlant) {
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, CarMaintenanceAreaFragment.getInstance(maintenancePlant)).addToBackStack("").commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);
                if (fragment instanceof CarMaintenanceAreaFragment || fragment instanceof MaintenanceResultFragment) {
                }
                finish();
                break;
            case R.id.btn_notice:
                break;
            case R.id.layout_first_open_maintenance:
            case R.id.btn_first_open_maintenance_close:
                layoutFirstOpen.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
        btnNotice.setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }
}
