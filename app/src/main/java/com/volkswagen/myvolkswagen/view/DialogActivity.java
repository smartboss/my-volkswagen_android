package com.volkswagen.myvolkswagen.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.Logger;

public class DialogActivity extends BaseActivity implements DialogInterface.OnClickListener {

    public static final String EXTRA_IS_UPDATE = "extra_is_update";
    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_URL = "extra_url";

    public static void goDialog(final Context context, boolean isUpdate, String message, String url) {
        Intent intent = new Intent(context, DialogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(EXTRA_IS_UPDATE, isUpdate);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_URL, url);
        context.startActivity(intent);
    }

    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        Intent intent = getIntent();
        mUrl = intent.getStringExtra(EXTRA_URL);
        if (TextUtils.isEmpty(mUrl)) {
            Logger.d(DialogActivity.this, "Update url could not be empty");
            finish();
            return;
        }

        boolean isUpdate = intent.getBooleanExtra(EXTRA_IS_UPDATE, false);
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_update_available));

        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        }

        builder.setPositiveButton(getString(R.string.update_button), this);

        if (!isUpdate) {
            builder.setNegativeButton(getString(R.string.cancel_button), this);
        }
        builder.setCancelable(false);

        builder.create().show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl)));
                finish();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                finish();
                break;
        }
    }
}
