package com.volkswagen.myvolkswagen.view.addcar;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_MEMBER_INFO;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.Area;
import com.volkswagen.myvolkswagen.model.Plant;
import com.volkswagen.myvolkswagen.model.Profile;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class MemberInputFragment extends Fragment implements View.OnClickListener, AddCarControlListener, DatePickerDialog.OnDateSetListener {
    private static MemberInputFragment fragment;

    private EditText etNickname, etEmail, etPhone, etAddress;
    private TextView tvBirth;
    private Spinner spinnerGender, spinnerArea, spinnerPlant;

    private List<Area> areaList = new ArrayList<>();
    private List<Plant> plantList = new ArrayList<>();
    private ArrayAdapter<Area> areaAdapter;
    private ArrayAdapter<Plant> plantAdapter;

    public static MemberInputFragment getInstance() {
        if (fragment == null) {
            fragment = new MemberInputFragment();
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_input, container, false);
        initView(view);

        getMemberInfo();
        getMaintenancePlant();
        return view;
    }

    private void initView(View view) {
        tvBirth = view.findViewById(R.id.tv_birth);
        tvBirth.setOnClickListener(this);

        etNickname = view.findViewById(R.id.et_nickname);
        etEmail = view.findViewById(R.id.et_email);
        etPhone = view.findViewById(R.id.et_phone);
        etAddress = view.findViewById(R.id.et_address);

        spinnerGender = view.findViewById(R.id.spinner_gender);
        spinnerArea = view.findViewById(R.id.spinner_service_area);
        spinnerPlant = view.findViewById(R.id.spinner_service_plant);

        ArrayAdapter<CharSequence> genderAdapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, getResources().getTextArray(R.array.array_gender_options)) {
            @NonNull
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setPadding(20, 20, 20, 20);
                return textView;
            }
        };

        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);
        spinnerGender.setSelection(2);

        areaAdapter = new ArrayAdapter<Area>(getActivity(), android.R.layout.simple_spinner_item) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setTextColor(position == 0 ? getResources().getColor(R.color.grey400) : getResources().getColor(R.color.black2));

                return textView;
            }

            @NonNull
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setPadding(20, 20, 20, 20);
                return textView;
            }
        };
        areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerArea.setAdapter(areaAdapter);
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Area area = areaAdapter.getItem(position);

                if (area != null) {
                    plantList.clear();
                    Plant defaultPlant = new Plant();
                    defaultPlant.plantName = getString(R.string.profile_select_plant_hint);
                    plantList.add(defaultPlant);
                    plantList.addAll(area.plants);

                    plantAdapter.clear();
                    plantAdapter.addAll(plantList);
                    spinnerPlant.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        plantAdapter = new ArrayAdapter<Plant>(getActivity(), android.R.layout.simple_spinner_item) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setTextColor(position == 0 ? getResources().getColor(R.color.grey400) : getResources().getColor(R.color.black2));

                return textView;
            }

            @NonNull
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setPadding(20, 20, 20, 20);
                return textView;
            }
        };
        plantAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPlant.setAdapter(plantAdapter);
    }

    private void getMemberInfo() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().getMemberProfile(new Repository.OnApiCallback<Profile>() {
            @Override
            public void onSuccess(Profile profile) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (profile.nickname != null && !profile.nickname.isEmpty()) {
                    etNickname.setText(profile.nickname);
                }

                if (profile.mobile != null && !profile.mobile.isEmpty()) {
                    etPhone.setText(profile.mobile);
                    etPhone.setKeyListener(null);
                }

                if (profile.email != null && !profile.email.isEmpty()) {
                    etEmail.setText(profile.email);
                    etEmail.setKeyListener(null);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
            }
        });
    }

    private void getMaintenancePlant() {
        Repository.getInstance().getMaintenancePlant(new Repository.OnApiCallback<List<Area>>() {
            @Override
            public void onSuccess(List<Area> areas) {
                areaList.clear();
                areaAdapter.clear();
                plantAdapter.clear();

                Area defaultArea = new Area();
                defaultArea.areaName = getString(R.string.profile_select_area_hint);
                areaList.add(defaultArea);
                areaList.addAll(areas);

                Plant defaultPlant = new Plant();
                defaultPlant.plantName = getString(R.string.profile_select_plant_hint);
                plantList.add(defaultPlant);

                areaAdapter.addAll(areaList);
                plantAdapter.addAll(plantList);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_MEMBER_INFO);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }

    @Override
    public void onNextClick() {
        if (isInputValid()) {
            String nickname = etNickname.getText().toString().replaceAll(" ", "");
            String email = etEmail.getText().toString().replaceAll(" ", "");
            String phone = etPhone.getText().toString().replaceAll(" ", "");
            String address = etAddress.getText().toString().replaceAll(" ", "");
            String birth = tvBirth.getText().toString().replaceAll(" ", "");
            String gender = getGender();
            String areaId = ((Area) spinnerArea.getSelectedItem()).areaId;
            String plantId = ((Plant) spinnerPlant.getSelectedItem()).plantId;

            postUpdateMemberProfile(nickname, email, phone, address, birth, gender, areaId, plantId);
        }
    }

    public void postUpdateMemberProfile(String nickname, String email, String phone, String address, String birth, String gender, String areaId, String plantId) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().postUpdateMemberProfile(nickname, email, phone, address, birth, gender, areaId, plantId, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, AddCarBaseFragment.getInstance(), "").addToBackStack("").commitAllowingStateLoss();
                }

                PreferenceManager.getInstance().saveUserNickname(nickname);
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
            }
        });
    }

    private boolean isInputValid() {
        if (etNickname.getText().toString().replaceAll(" ", "").isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.profile_brief_hint), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etEmail.getText().toString().replaceAll(" ", "").isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.profile_email_hint), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Utils.isValidEmail(etEmail.getText().toString().replaceAll(" ", ""))) {
            Toast.makeText(getContext(), getString(R.string.profile_email_error), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etPhone.getText().toString().replaceAll(" ", "").isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.profile_mobile_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private String getGender() {
        int genderPosition = spinnerGender.getSelectedItemPosition();
        switch (genderPosition) {
            case 0:
                return "M";
            case 1:
                return "F";
            case 2:
            default:
                return "N";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_birth:
                if (getContext() != null) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, 1990, 12 - 1, 2);
                    datePickerDialog.show();
                }
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String birthday = "" + year + "/ " + (month + 1) + "/ " + day;
        tvBirth.setText(birthday);
    }
}
