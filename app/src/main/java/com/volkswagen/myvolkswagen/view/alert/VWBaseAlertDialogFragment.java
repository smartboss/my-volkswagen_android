package com.volkswagen.myvolkswagen.view.alert;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.Utils;

public class VWBaseAlertDialogFragment extends DialogFragment {
    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";
    private static final String ARG_POSITIVE = "positive";
    private static final String ARG_NEGATIVE = "negative";

    private VWDialogListener listener;

    public static VWBaseAlertDialogFragment newInstance(String title, String message, String positive, String negative) {
        VWBaseAlertDialogFragment fragment = new VWBaseAlertDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        args.putString(ARG_POSITIVE, positive);
        args.putString(ARG_NEGATIVE, negative);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_dialogbox, null);
            AlertDialog dialog = builder.setView(view).create();

            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }

            initView(view);
            return dialog;
        }

        return null;
    }

    private void initView(View view) {
        String title = "";
        String message = "";
        String positiveBtn = "";
        String negativeBtn = "";

        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE, "");
            message = getArguments().getString(ARG_MESSAGE, "");
            positiveBtn = getArguments().getString(ARG_POSITIVE, "");
            negativeBtn = getArguments().getString(ARG_NEGATIVE, "");
        }

        TextView tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setVisibility(title.isEmpty() ? View.GONE : View.VISIBLE);
        tvTitle.setText(title);

        TextView tvDescription = view.findViewById(R.id.tv_description);
        tvDescription.setText(message);

        TextView tvPositiveBtn = view.findViewById(R.id.btn_positive);
        tvPositiveBtn.setText(positiveBtn);
        tvPositiveBtn.setOnClickListener(view1 -> {
            dismiss();

            if (listener != null) {
                listener.onDialogPositiveClick();
            }
        });

        TextView tvNegativeBtn = view.findViewById(R.id.btn_negative);
        tvNegativeBtn.setVisibility(negativeBtn.isEmpty() ? View.GONE : View.VISIBLE);
        tvNegativeBtn.setText(negativeBtn);
        tvNegativeBtn.setOnClickListener(view13 -> {
            dismiss();

            if (listener != null) {
                listener.onDialogNegativeClick();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                WindowManager.LayoutParams layoutParams = window.getAttributes();
                layoutParams.width = Utils.dp2px(getResources(), 320);
                window.setAttributes(layoutParams);
            }
        }
    }

    public void addDialogListener(VWDialogListener listener) {
        this.listener = listener;
    }

    public interface VWDialogListener {
        void onDialogPositiveClick();

        void onDialogNegativeClick();
    }
}
