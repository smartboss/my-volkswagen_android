package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volkswagen.myvolkswagen.R;

public class VinHintBottomSheetDialog extends BottomSheetDialog {
    public VinHintBottomSheetDialog(@NonNull Context context) {
        super(context, R.style.BaseBottomSheetDialog);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_vin_hint, null);
        setContentView(view);
        ImageView imgClose = view.findViewById(R.id.img_close);
        imgClose.setOnClickListener(view1 -> dismiss());

        FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }
}