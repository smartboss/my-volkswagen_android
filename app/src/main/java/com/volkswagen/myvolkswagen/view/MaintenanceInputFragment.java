package com.volkswagen.myvolkswagen.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.controller.ValueCallback;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.PatContent;
import com.volkswagen.myvolkswagen.utils.Const;

import java.util.ArrayList;
import java.util.List;

// TODO: 2024/1/14 remove it
public class MaintenanceInputFragment extends Fragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, View.OnFocusChangeListener {
    public static final String TAG = MaintenanceInputFragment.class.getSimpleName();
    public static final int MIN_CURRENT_MILES = 2500;
    public static final int MAX_CURRENT_MILES = 1000000;
    public static final int DEFAULT_CURRENT_MILES = 20000;

    private TextView tvVin, tvCarId;
    private EditText etCurrentMiles;
    private SeekBar barCurrentMiles;
    private Button btnNext;

    private boolean inputCurrentMilesFromKeyboard = false;

    private List<MyCarData.CarVinAndPlantData> list = new ArrayList<>();
    private MyCarData.CarVinAndPlantData selectCarData = new MyCarData.CarVinAndPlantData();

    public static MaintenanceInputFragment getInstance(Bundle bundle) {
        MaintenanceInputFragment fragment = new MaintenanceInputFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private MaintenanceInputFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_input, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tvVin = view.findViewById(R.id.tv_vin);
        tvCarId = view.findViewById(R.id.tv_car_id);

        if (getArguments() != null && getArguments().containsKey(Const.CAR_LIST)) {
            list = getArguments().getParcelableArrayList(Const.CAR_LIST);
            if (list != null && list.size() > 0) {
                setSelectCarData(list.get(0));

                //if has more than one carVinAndPlant data, need show arrow UI to select
                tvCarId.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, list.size() > 1 ? R.drawable.bg_spinner : 0, 0);
                tvCarId.setOnClickListener(list.size() > 1 ? this : null);
            }
        }

        barCurrentMiles = view.findViewById(R.id.seek_bar_current_miles);
        barCurrentMiles.setMax(MAX_CURRENT_MILES - MIN_CURRENT_MILES);
        barCurrentMiles.setOnSeekBarChangeListener(this);

        etCurrentMiles = view.findViewById(R.id.et_miles_current);
        etCurrentMiles.setOnFocusChangeListener(this);

        btnNext = view.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);

        inputCurrentMilesFromKeyboard = true;

        barCurrentMiles.setProgress(DEFAULT_CURRENT_MILES - MIN_CURRENT_MILES);
        etCurrentMiles.setText(String.format(getString(R.string.car_maintenance_km), DEFAULT_CURRENT_MILES));
    }

    private void setSelectCarData(MyCarData.CarVinAndPlantData data) {
        selectCarData = data;
        tvCarId.setText(selectCarData.plant);
        tvVin.setText(selectCarData.vin);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                etCurrentMiles.clearFocus();

                int currentMiles = MIN_CURRENT_MILES + barCurrentMiles.getProgress();
                Repository.getInstance().getPatContent(getContext(), selectCarData.vin, selectCarData.plant, currentMiles, new ValueCallback<PatContent>() {
                    @Override
                    public void onSuccess(PatContent patContent) {
                        if (getActivity() != null) {
                            ((CarMaintenanceActivity) getActivity()).goMaintenanceResultFragment(patContent);
                        }
                    }
                });
                break;
            case R.id.tv_car_id:
                etCurrentMiles.clearFocus();

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
                for (MyCarData.CarVinAndPlantData data : list) {
                    arrayAdapter.add(data.plant);
                }

                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyCarData.CarVinAndPlantData data = list.get(which);
                        setSelectCarData(data);
                    }
                });

                builder.show();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (inputCurrentMilesFromKeyboard) {
            inputCurrentMilesFromKeyboard = false;
            return;
        }

        etCurrentMiles.clearFocus();
        setWording(etCurrentMiles, R.string.car_maintenance_km, MIN_CURRENT_MILES + progress);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        etCurrentMiles.setFilters(new InputFilter[]{new InputFilter.LengthFilter(hasFocus ? 6 : 10)});

        if (hasFocus) {
            String value = etCurrentMiles.getText().toString().split(" ")[0];
            etCurrentMiles.setText(value);
        } else {
            try {
                int value = Integer.parseInt(etCurrentMiles.getText().toString());
                if (value < MIN_CURRENT_MILES) {
                    setMinProgress();
                } else if (value > MAX_CURRENT_MILES) {
                    setMaxProgress();
                } else {
                    setWording(etCurrentMiles, R.string.car_maintenance_km, value);
                    barCurrentMiles.setProgress(value - MIN_CURRENT_MILES);
                }
            } catch (NumberFormatException e) {
                setMinProgress();
            }
        }
    }

    private void setMinProgress() {
        barCurrentMiles.setProgress(0);
        setWording(etCurrentMiles, R.string.car_maintenance_km, MIN_CURRENT_MILES);
    }

    private void setMaxProgress() {
        barCurrentMiles.setProgress(barCurrentMiles.getMax());
        setWording(etCurrentMiles, R.string.car_maintenance_km, MAX_CURRENT_MILES);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    private void setWording(EditText editText, int formatWording, int value) {
        String wording = String.format(getString(formatWording), value);
        editText.setText(wording);
        editText.setSelection(wording.length());
    }
}
