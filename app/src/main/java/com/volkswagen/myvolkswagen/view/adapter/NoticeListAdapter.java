package com.volkswagen.myvolkswagen.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.Notice;
import com.volkswagen.myvolkswagen.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NoticeListAdapter extends RecyclerView.Adapter<NoticeListAdapter.NoticeListViewHolder> {
    private List<Notice> list = new ArrayList<>();
    private NoticeControlListener listener;

    @NonNull
    @Override
    public NoticeListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_notice, parent, false);
        return new NoticeListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeListViewHolder holder, int position) {
        Notice data = list.get(position);

        holder.layout.setBackgroundResource(data.isRead ? 0 : R.color.lightblue50);
        holder.tvTitle.setText(data.content);
        if (data.description != null) {
            holder.tvContent.setText(data.description.replace("\n", ""));
        }

        holder.tvTime.setText(Utils.timeAgo(data.date, "yyyy/MM/dd HH:mm:ss"));

        holder.imgMore.setOnClickListener(view -> {
            if (listener != null) {
                listener.onClickMore(data);
            }
        });

        holder.itemView.setOnClickListener(view -> {
            data.isRead = true;
            notifyItemChanged(position);

            if (listener != null) {
                listener.onSelectNotice(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addNoticeControlListener(NoticeControlListener listener) {
        this.listener = listener;
    }

    public void setData(List<Notice> noticeList) {
        list.clear();
        list.addAll(noticeList);
        notifyDataSetChanged();
    }

    public static class NoticeListViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout layout;
        private TextView tvTitle, tvContent, tvTime;
        private ImageView imgMore;

        public NoticeListViewHolder(@NonNull View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.layout_view_holder);
            tvTitle = itemView.findViewById(R.id.tv_view_holder_notice_title);
            tvContent = itemView.findViewById(R.id.tv_view_holder_content);
            tvTime = itemView.findViewById(R.id.tv_view_holder_time);
            imgMore = itemView.findViewById(R.id.img_view_holder_more);
        }
    }

    public interface NoticeControlListener {
        void onClickMore(Notice notice);

        void onSelectNotice(Notice notice);
    }
}
