package com.volkswagen.myvolkswagen.view;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.ServiceReminder;
import com.volkswagen.myvolkswagen.model.ServiceReminderResponseData;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewReminderPagerAdapter extends RecyclerView.Adapter<ViewReminderPagerAdapter.ViewHolder>
{
    private ServiceReminderResponseData reminderResData;
    private ViewReminderPagerAdapter.OnButtonClickListener listener;
    public ViewReminderPagerAdapter(ServiceReminderResponseData reminderResData, ViewReminderPagerAdapter.OnButtonClickListener listener) {
        this.reminderResData = reminderResData;
        this.listener = listener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_maintenance_info, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ServiceReminder reminder = this.reminderResData.serviceReminderList.get(position);
        holder.text_exclamation.setText(reminder.description);
        holder.ButtonLink = reminder.btnUrl;
        holder.btn_alarm.setText(reminder.btnName);
    }
    @Override
    public int getItemCount() {
        return reminderResData.serviceReminderList.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_exclamation;
        ImageView alarmImage;
        Button btn_alarm;
        String ButtonLink;

        public ViewHolder(View itemView) {
            super(itemView);
            text_exclamation = itemView.findViewById(R.id.text_exclamation);
            alarmImage = itemView.findViewById(R.id.img_exclamation);
            btn_alarm = itemView.findViewById(R.id.btn_alarm);
            btn_alarm.setOnClickListener(v -> {
                String text_title = text_exclamation.getText().toString();
                listener.onButtonClick(text_title, ButtonLink);
            });
        }
    }

    public interface OnButtonClickListener {
        void onButtonClick(String title, String url);
    }
}
