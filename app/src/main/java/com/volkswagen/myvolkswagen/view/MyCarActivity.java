package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.view.adapter.CarListAdapter;
import com.volkswagen.myvolkswagen.view.alert.VWBaseAlertDialogFragment;
import com.volkswagen.myvolkswagen.view.bottomsheet.CarManagerBottomSheetDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class MyCarActivity extends BaseActivity implements CarListAdapter.OnCarSelectListener, CarManagerBottomSheetDialog.CarManagerListener {
    private CarListAdapter adapter;

    public static void goMyCar(final Activity activity) {
        final Intent intent = new Intent(activity, MyCarActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycar);
        EventBus.getDefault().register(this);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_show_car_mycar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CarListAdapter();
        adapter.addOnSelectListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
        getCarList();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "上一頁", null, EventLog.EVENT_CATEGORY_BUTTON);
                onBackPressed();
                break;
            case R.id.tv_add_car:
                goAddCar();
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "新增愛車", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
        }
    }

    public void getCarList() {
        showLoading();
        Repository.getInstance().getMyCarDataList(new Repository.OnApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> carDataList) {
                hideLoading();
                if (carDataList.size() <= 0) {
                    //user has no car, finish this view
                    finish();
                } else {
                    adapter.setData(carDataList);

                    for (CarData carData : carDataList) {
                        Repository.getInstance().getDongleBinding(carData.plate, new Repository.OnApiCallback<DongleBindingCarData>() {
                            @Override
                            public void onSuccess(DongleBindingCarData dongleBindingCarData) {
                                adapter.setDingleStateByCarPlate(carData.plate, dongleBindingCarData.state);
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                adapter.setDingleStateByCarPlate(carData.plate, CarData.DONGLE_STATUS_NONE);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    private void removeCar(String carUserName, String carPlate, String carVin) {
        showLoading();
        Repository.getInstance().removeCar(carUserName, carPlate, carVin, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                hideLoading();
                getCarList();
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    private void removeCarDongle(String carPlateNo) {
        showLoading();
        Repository.getInstance().removeCarDongle(carPlateNo, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                hideLoading();
                getCarList();
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    private void removeCarDongleAndRemoveCar(String carUserName, String carPlate, String carVin) {
        showLoading();
        Repository.getInstance().removeCarDongle(carPlate, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                removeCar(carUserName, carPlate, carVin);
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    @Override
    public void onSelect(CarData carData) {
        PreferenceManager.getInstance().saveMainCar(carData.name, carData.plate, carData.vin);
        onBackPressed();
    }

    @Override
    public void onClickSetting(CarData carData) {
        CarManagerBottomSheetDialog dialog = new CarManagerBottomSheetDialog(this, carData, this);
        dialog.show();

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "管理車輛", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onCarBindingAssistant(CarData carData) {
        goDongleBinding(carData.plate, carData.model);

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "綁定行車助理", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onCarUnBindingAssistant(CarData carData) {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.dialog_remove_dongle_title), getString(R.string.dialog_remove_dongle_content), getString(R.string.confirm_button), getString(R.string.cancel_button));
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                removeCarDongle(carData.plate);
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "解綁行車助理", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onCarRemove(CarData carData) {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.dialog_remove_car_title), String.format(getString(R.string.dialog_remove_car_content), carData.plate), getString(R.string.confirm_button), getString(R.string.cancel_button));
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                if (carData.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_USER_MAPPED)) {
                    //if car has dongle, need remove dongle first
                    removeCarDongleAndRemoveCar(carData.name, carData.plate, carData.vin);
                } else {
                    removeCar(carData.name, carData.plate, carData.vin);
                }
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "解綁愛車", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onCarIdentification(CarData carData) {
        goCarIdentification(carData.plate, carData.vin, "");

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_VEHICLE, "愛車鑑價", null, EventLog.EVENT_CATEGORY_BUTTON);
    }
}
