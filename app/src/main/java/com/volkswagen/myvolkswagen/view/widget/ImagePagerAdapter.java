package com.volkswagen.myvolkswagen.view.widget;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImagePagerAdapter extends PagerAdapter {
    private List<String> imageList = new ArrayList<>();
    private ImageView firstView;

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(container.getContext());
        if (position == 0) {
            firstView = imageView;
        }
        Picasso.get().load(imageList.get(position)).fit().centerInside().into(imageView);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    public void setList(List<String> imageList) {
        this.imageList = imageList;
        notifyDataSetChanged();
    }

    public ImageView getFirstImageView() {
        return firstView;
    }
}
