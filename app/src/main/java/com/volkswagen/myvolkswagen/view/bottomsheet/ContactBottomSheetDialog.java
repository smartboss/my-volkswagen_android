package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volkswagen.myvolkswagen.R;

public class ContactBottomSheetDialog extends BottomSheetDialog {
    private final ContactListener contactListener;

    public ContactBottomSheetDialog(@NonNull Context context, ContactListener contactListener) {
        super(context, R.style.BaseBottomSheetDialog);
        this.contactListener = contactListener;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_contact, null);
        setContentView(view);

        TextView tvContactOnline = view.findViewById(R.id.tv_contact_online);
        TextView tvContactEmail = view.findViewById(R.id.tv_contact_email);
        TextView tvContactPhone = view.findViewById(R.id.tv_contact_phone);

        tvContactOnline.setOnClickListener(view1 -> {
            if (contactListener != null) {
                contactListener.onContactOnline();
            }

            dismiss();
        });

        tvContactEmail.setOnClickListener(view1 -> {
            if (contactListener != null) {
                contactListener.onContactByEmail();
            }

            dismiss();
        });

        tvContactPhone.setOnClickListener(view1 -> {
            if (contactListener != null) {
                contactListener.onContactByPhone();
            }

            dismiss();
        });
    }

    public interface ContactListener {
        void onContactOnline();

        void onContactByEmail();

        void onContactByPhone();
    }
}
