package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.setting.SettingsActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MemberWebViewNewFragment extends Fragment {
    public static final String ARG_INITIAL_VIEW = "arg_initial_view";
    private PermissionRequest webViewPermissionRequest;
    private WebView webView;

    private Uri mCallUri;

    public static MemberWebViewNewFragment newInstance(int initialView) {
        MemberWebViewNewFragment fragment = new MemberWebViewNewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_INITIAL_VIEW, initialView);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_new, container, false);
        webView = view.findViewById(R.id.web_view);

        //webView
        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
        String accountId = PreferenceManager.getInstance().getAccountId();
        String plate = "";
        String vin = "";
        if (carData != null) {
            plate = carData.plate;
            vin = carData.vin;
        }
        String myCarUrl = Uri.parse(Config.VW_MEMBER_NEW_URL).buildUpon()
                .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                .appendQueryParameter("LicensePlateNumber", plate)
                .appendQueryParameter("VIN", vin)
                .appendQueryParameter("AccountId", accountId)
                .build()
                .toString();


        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                if (url.startsWith("tel:")) {
//                    mCallUri = Uri.parse(url);
//                    call();
//                    return true;
//                }

                String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
                if (url.startsWith(appScheme)) {
                    mCallUri = Uri.parse(url);
                    Logger.d(this, "deeplink :" + url + ",schema:" + appScheme);
                    String host = mCallUri.getHost();
                    String path = mCallUri.getPath();

                    call(host, path);


                    return true;
                }


                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }
        });

        String finalMyCarUrl = myCarUrl;
        webView.loadUrl(finalMyCarUrl);

//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                webView.loadUrl(finalMyCarUrl);
//            }
//        });

        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
//        btnNotice.setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }

    public WebView getWebView() {
        return webView;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length <= 0) {
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissions.length > 0 && permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                }
            } else {
            }
        }
    }

    private void call(String host, String path) {
        if (path.contains(getString(R.string.app_deeplink_path_setting))) {
            SettingsActivity.goSettings(getActivity());
        }
        if (host.equals(getString(R.string.app_deeplink_host_member))) {
            //myvolkswagen://member/
            //myvolkswagendev://member
//                goMemberWithDeeplink(intentPath);
            if (path.equals(getString(R.string.app_deeplink_path_coupon))) {
                WebViewActivity.goVWMemberCoupon(this.getActivity());
            } else if (path.equals(getString(R.string.app_deeplink_path_profile))) {
                WebViewActivity.goVWMemberProfile(this.getActivity());
            } else if (path.equals(getString(R.string.app_deeplink_path_benefits))) {
                WebViewActivity.goVWMemberBenefits(this.getActivity());
            }

            if (host.equals(getString(R.string.app_deeplink_host_car))) {
                if (path.equals(getString(R.string.app_deeplink_path_mygarage))) {
                    MyCarActivity.goMyCar(getActivity());
                }
            }
        }
    }
}
//        Logger.d(MainActivity.this, "got intent path:" + intentPath);
//        if (host == null || path == null) {
//            return;
//        }
//
////            deeplink myvolkswagen://member/coupon
////            uri.getScheme():myvolkswagen
////            uri.getHost():member
////            uri.getPath: /coupon
//            Logger.d(MainActivity.this, "got intent scheme: "+intent.getData().getScheme());
//            String intentHost = intent.getData().getHost();
//            String intentPath = intent.getData().getPath();
//            if (intentHost == null || intentPath == null) {
//                return;
//            }
//
//            if (intentHost.equals(getString(R.string.app_deeplink_host_car))) {
//                //myvolkswagen://car
//                //myvolkswagen://car/maintain
////                mMyCarInitialView = intentPath.equals(getString(R.string.app_deeplink_path_maintain)) ? MyCarFragment.INITIAL_MAINTAIN_INFO : MyCarFragment.INITIAL_NORMAL;
////                selectTab(R.id.tab_button_mycar);
//            } else if (intentHost.equals(getString(R.string.app_deeplink_host_maintenance))) {
//                //myvolkswagen://maintenance
//                if ( intentPath.equals(getString(R.string.app_deeplink_path_records))) {
//                    WebViewActivity.goMaintenanceRecords(this, Config.VW_MAINTENANCE_RECORDS_URL);
//                } else if(intentPath.equals(getString(R.string.app_deeplink_path_warranty))) {
//                    WebViewActivity.goMaintenanceWarranty(this);
//                } else {
//                    selectTab(R.id.tab_button_mycar);
////                    OnDashboardView(TAB_MYCAR);
//                    Logger.d(MainActivity.this, "got to maintenance");
//                }
//
//
//            } else if (intentHost.equals(getString(R.string.app_deeplink_host_main))) {
//                //myvolkswagen://main/
//                //myvolkswagendev://main
////                goMemberWithDeeplink(intentPath);
//                OnDashboardView(TAB_COMMUNITY);
//
//                Logger.d(MainActivity.this, "got to dashboard");
//            } else {
//                //do nothing
//            }
//        } else if (intent.getExtras() != null) {
//            try {
//                if (intent.getExtras().containsKey("news_id")) {
////                    int newsId = Integer.valueOf(intent.getStringExtra("news_id"));
////                    if (newsId > 0) {
////                        PostActivity.goPost(this, newsId, false);
////                    }
//                } else if (intent.getExtras().containsKey("inbox_id")) {
//                    int newsId = Integer.valueOf(intent.getStringExtra("inbox_id"));
//                    if (newsId > 0) {
//                        OwnershipActivity.goOwnerShipActivity(this, newsId);
//                    }
//                }
//            } catch (NumberFormatException e) {
//            }
//        }


