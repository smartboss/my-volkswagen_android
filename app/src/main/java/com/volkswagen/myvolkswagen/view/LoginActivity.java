package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.installations.FirebaseInstallations;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.LoginData_ID;
import com.volkswagen.myvolkswagen.model.User;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    public static void goLogin(final Activity activity) {
        final Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        EventBus.getDefault().removeAllStickyEvents();

        PreferenceManager.getInstance().deleteUserData();
        PreferenceManager.getInstance().deleteUserNickName();
        PreferenceManager.getInstance().deleteRecentSearch();
        PreferenceManager.getInstance().deleteMyCars();
        PreferenceManager.getInstance().deleteMainCar();
        Utils.deleteWebViewCookiesForDomain(null);

        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new SplashFragment()).commitAllowingStateLoss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.REQ_LOGIN && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                String fragment = uri.getFragment();
                if (fragment == null) {
                    return;
                }

                Map<String, String> params = parseParams(fragment);
                String state = params.get("state");
                final String code = params.get("code");
                final String token = params.get("id_token");
                if (state == null || !state.equals(PreferenceManager.getInstance().getState()) || token == null || code == null) {
                    loginFailed();
                    return;
                }

                FirebaseInstallations.getInstance().getId().addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    processLoginData(new LoginData_ID(code, token, task.getResult()));
                });
            }
        }
    }

    public void goVWLogin() {
        WebViewActivity.goVWLogin(this, Utils.REQ_LOGIN);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, null);
    }

    private void processLoginData(LoginData_ID data) {
        ApiService service = ApiClientManager.getClient().create(ApiService.class);
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        service.loginById(data).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    User user = response.body();
                    PreferenceManager.getInstance().saveUserData(user.token, user.data.accountId, user.data.email, user.data.mobile, user.data.level, user.data.levelName);
                    handleResult(user);
                } else {
                    loginFailed();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hud.dismiss();
                loginFailed();
            }
        });
    }

    public void handleResult(User user) {
        if (user != null && user.result) {
            if (user.data.showProfile) {
                MemberInputActivity.goMemberInput(LoginActivity.this);
            } else {
                MainNewActivity.goMain(LoginActivity.this);
            }

            finish();
        } else {
            loginFailed();
        }
    }

    public void loginFailed() {
        AlertUtils.showAlert(this, null, getString(R.string.msg_login_failed), null, getString(R.string.ok_button), null);
    }

    private Map<String, String> parseParams(String str) {
        Map<String, String> map = new HashMap<>();
        String[] arr = str.split("&");
        for (String s : arr) {
            String[] a = s.split("=");
            map.put(a[0], a[1]);
        }
        return map;
    }
}
