package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class MyCarFragment extends Fragment {
    public static final String ARG_INITIAL_VIEW = "arg_initial_view";

    public static final int INITIAL_NORMAL = 0;
    public static final int INITIAL_CAR_INFO = 1;
    public static final int INITIAL_MAINTAIN_INFO = 2;

    private PermissionRequest webViewPermissionRequest;

    private TextView tvTitle;
    private ImageButton btnNotice;
    private ImageView btnClose;
    private WebView webView;

    private Uri mCallUri;

    public static MyCarFragment newInstance(int initialView) {
        MyCarFragment fragment = new MyCarFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_INITIAL_VIEW, initialView);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycar, container, false);
        tvTitle = view.findViewById(R.id.tv_title_mycar);
        btnNotice = view.findViewById(R.id.btn_notice);
        btnClose = view.findViewById(R.id.btn_close);
        webView = view.findViewById(R.id.web_view);

        //notice button
        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
        btnNotice.setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);

        //webView
        int initialView = INITIAL_NORMAL;
        if (getArguments() != null && getArguments().containsKey(ARG_INITIAL_VIEW)) {
            initialView = getArguments().getInt(ARG_INITIAL_VIEW, INITIAL_NORMAL);
        }

        String myCarUrl = Uri.parse(Config.VW_MY_CAR_URL).buildUpon()
                .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                .build()
                .toString();

        if (initialView == INITIAL_MAINTAIN_INFO) {
            myCarUrl = Uri.parse(myCarUrl).buildUpon()
                    .appendQueryParameter("tabType", "CarMaintain")
                    .build()
                    .toString();
        }

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.contains("myCar.html")) {
                    tvTitle.setText(getString(R.string.mycar_title));
                    btnClose.setVisibility(View.GONE);
                } else if (url.contains("service.html")) {
                    tvTitle.setText(getString(R.string.mycar_service_title));
                    btnClose.setVisibility(View.GONE);
                } else if (url.contains("addCar.html")) {
                    tvTitle.setText(getString(R.string.add_car_title));
                    btnClose.setVisibility(View.VISIBLE);
                } else if (url.contains("#PAT")) {
                    tvTitle.setText(getString(R.string.car_maintenance_title));
                    btnClose.setVisibility(View.VISIBLE);
                } else if (url.contains("#dccsearch")) {
                    tvTitle.setText(getString(R.string.maintenance_reservation));
                    btnClose.setVisibility(View.VISIBLE);
                } else if (url.contains(Config.VW_SUGGEST_MAINTAIN_CAR)) {
                    tvTitle.setText(getString(R.string.web_view_title_suggest_maintain));
                    btnClose.setVisibility(View.VISIBLE);
                } else if (url.contains("#")) {
                    String customTitle = url.split("#")[1];
                    try {
                        tvTitle.setText(URLDecoder.decode(customTitle, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    btnClose.setVisibility(View.VISIBLE);
                } else {
                    btnClose.setVisibility(View.VISIBLE);
                }

                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")) {
                    mCallUri = Uri.parse(url);
                    call();
                    return true;
                }

                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }
        });

        String finalMyCarUrl = myCarUrl;
        webView.loadUrl(finalMyCarUrl);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.loadUrl(finalMyCarUrl);
            }
        });

        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
        btnNotice.setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }

    public WebView getWebView() {
        return webView;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length <= 0) {
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissions.length > 0 && permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                }
            } else {
            }
        }
    }

    private void call() {
        startActivity(new Intent(Intent.ACTION_DIAL, mCallUri));
    }
}

