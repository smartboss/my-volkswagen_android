package com.volkswagen.myvolkswagen.view;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.MaintenancePlant;

// TODO: 2024/1/14 remove it
public class CarMaintenanceAreaFragment extends Fragment implements View.OnClickListener {
    private MaintenancePlant maintenancePlant;

    private TextView tvArea, tvAddress, tvPhone;
    private Button btnLastStep, btnOtherArea;

    public static CarMaintenanceAreaFragment getInstance(MaintenancePlant maintenancePlant) {
        CarMaintenanceAreaFragment fragment = new CarMaintenanceAreaFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MaintenancePlant.class.getSimpleName(), maintenancePlant);
        fragment.setArguments(bundle);

        return fragment;
    }

    private CarMaintenanceAreaFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_maintenance_area, container, false);
        if (getArguments() != null) {
            if (getArguments().containsKey(MaintenancePlant.class.getSimpleName())) {
                maintenancePlant = getArguments().getParcelable(MaintenancePlant.class.getSimpleName());
            }
        }

        initView(view);
        return view;
    }

    private void initView(View view) {
        tvArea = view.findViewById(R.id.tv_maintenance_area);
        tvAddress = view.findViewById(R.id.tv_maintenance_address);
        tvPhone = view.findViewById(R.id.tv_maintenance_phone);
        tvPhone.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvPhone.getPaint().setAntiAlias(true);
        tvPhone.setOnClickListener(this);
        btnLastStep = view.findViewById(R.id.btn_last);
        btnLastStep.setOnClickListener(this);
        btnOtherArea = view.findViewById(R.id.btn_other_area);
        btnOtherArea.setOnClickListener(this);

        if (maintenancePlant != null) {
            tvArea.setText(maintenancePlant.plantsName);
            tvAddress.setText(maintenancePlant.address);
            tvPhone.setText(maintenancePlant.phone);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_last:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
            case R.id.btn_other_area:
                if (maintenancePlant != null) {
                    WebViewActivity.goVWCarMaintenance(getActivity(), maintenancePlant.url);
                }
                break;
            case R.id.tv_maintenance_phone:
                String phone = maintenancePlant.phone;
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
        }
    }
}
