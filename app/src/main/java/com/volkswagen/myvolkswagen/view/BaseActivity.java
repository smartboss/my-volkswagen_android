package com.volkswagen.myvolkswagen.view;

import static com.volkswagen.myvolkswagen.view.base.BaseWebViewActivity.TYPE_ANY;
import static com.volkswagen.myvolkswagen.view.base.BaseWebViewActivity.TYPE_CAR_IDENTIFICATION;
import static com.volkswagen.myvolkswagen.view.base.BaseWebViewActivity.TYPE_OPEN_HUB;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.ExtRedirectUrlInput;
import com.volkswagen.myvolkswagen.model.Profile;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.view.alert.VWBaseAlertDialogFragment;
import com.volkswagen.myvolkswagen.view.base.BaseWebViewActivity;
import com.volkswagen.myvolkswagen.view.bottomsheet.AddCarBottomSheetDialogFragment;
import com.volkswagen.myvolkswagen.view.bottomsheet.ContactBottomSheetDialog;
import com.volkswagen.myvolkswagen.view.bottomsheet.DongleBindingBottomSheetDialogFragment;
import com.volkswagen.myvolkswagen.view.bottomsheet.ServiceCenterBottomSheetDialog;
import com.volkswagen.myvolkswagen.view.notice.NoticeActivity;
import com.volkswagen.myvolkswagen.view.setting.SettingsActivity;

import java.util.Set;

public class BaseActivity extends AppCompatActivity implements ContactBottomSheetDialog.ContactListener, ServiceCenterBottomSheetDialog.ServiceCenterListener {
    private Dialog loadingProgress;
    public static final int REQUEST_CALL_PERMISSION = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        loadingProgress = loadingDialog();

    }

    private Dialog loadingDialog() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_loading);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        return dialog;
    }

    @Override
    protected void onDestroy() {
        if (loadingProgress != null) {
            loadingProgress.dismiss();
            loadingProgress = null;
        }

        super.onDestroy();
    }

    public void handleDeeplink(String url) {
        String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);

        Uri uri = Uri.parse(url);
        String host = uri.getHost();
        String path = uri.getPath();
        String query = uri.getQuery();

        if (url.startsWith(appScheme)) {
            if (host == null) {
                return;
            }

            //myvolkswagendev://main
            if (host.equals(getString(R.string.app_deeplink_host_main))) {
                goMain();
                return;
            }

            //myvolkswagendev://member
            if (host.equals(getString(R.string.app_deeplink_host_member))) {
                //myvolkswagendev://member/setting
                if (path != null && path.contains(getString(R.string.app_deeplink_path_setting))) {
                    goSetting();
                    return;
                }

                //myvolkswagendev://member/profile
                if (path != null && path.contains(getString(R.string.app_deeplink_path_profile))) {
                    goServiceSetting(query);
                    return;
                }

                //myvolkswagendev://member/benefits
                if (path != null && path.contains(getString(R.string.app_deeplink_path_benefits))) {
                    goBenefits(query);
                    return;
                }

                //myvolkswagendev://member/coupon
                if (path != null && path.contains(getString(R.string.app_deeplink_path_coupon))) {
                    goCoupon(0, query);
                    return;
                }

                //myvolkswagendev://member/privacy
                if (path != null && path.contains(getString(R.string.app_deeplink_path_privacy))) {
                    goPrivacy(query);
                    return;
                }

                //myvolkswagendev://member/terms
                if (path != null && path.contains(getString(R.string.app_deeplink_path_terms))) {
                    goService(query);
                    return;
                }
            }

            //myvolkswagendev://maintenance
            if (host.equals(getString(R.string.app_deeplink_host_maintenance))) {
                if (path == null || path.isEmpty()) {
                    goMainCarMaintenance();
                    return;
                }

                //myvolkswagendev://maintenance/reservation
                if (path.contains(getString(R.string.app_deeplink_path_reservation))) {
                    goCarReservation(query);
                    return;
                }
                //myvolkswagendev://maintenance/records
                if (path.contains(getString(R.string.app_deeplink_path_records))) {
                    goCarRecords(query);
                    return;
                }
                //myvolkswagendev://maintenance/pat
                if (path.contains(getString(R.string.app_deeplink_path_pat))) {
                    goCarSuggestion(query);
                    return;
                }
                //myvolkswagendev://maintenance/bodyandpaint
                if (path.contains(getString(R.string.app_deeplink_path_bodyandpaint))) {
                    goCarEstimate(query);
                    return;
                }
                //myvolkswagendev://maintenance/warranty
                if (path.contains(getString(R.string.app_deeplink_path_warranty))) {
                    goCarInsurance(query);
                    return;
                }
                //myvolkswagendev://maintenance/recalls
                if (path.contains(getString(R.string.app_deeplink_path_recalls))) {
                    goCarRecall(query);
                    return;
                }
                //myvolkswagendev://maintenance/additionalquotation
                if (path.contains(getString(R.string.app_deeplink_path_additionalquotation))) {
                    goCarAdditionalQuotation(query);
                    return;
                }
            }

            //myvolkswagendev://car
            if (host.equals(getString(R.string.app_deeplink_host_car))) {
                //myvolkswagendev://car/mygarage
                if (path != null && path.contains(getString(R.string.app_deeplink_path_mygarage))) {
                    goMyCar();
                    return;
                }
                //myvolkswagendev://car/addcar
                if (path != null && path.contains(getString(R.string.app_deeplink_path_add_car))) {
                    goAddCar();
                    return;
                }
            }

            //myvolkswagendev://contact
            if (host.equals(getString(R.string.app_deeplink_host_notification))) {
                goNotice(NoticeActivity.TYPE_NOTICE);
                return;
            }

            //myvolkswagendev://drivingassistant
            if (host.equals(getString(R.string.app_deeplink_host_driving_assistant))) {
                goMainCarDongle();
                return;
            }

            //myvolkswagendev://servicecenter
            if (host.equals(getString(R.string.app_deeplink_host_servicecenter))) {
                showServiceCenterBottomSheetDialog();
                return;
            }

            //myvolkswagendev://contact
            if (host.equals(getString(R.string.app_deeplink_host_contact))) {
                showContactUsBottomSheetDialog();
                return;
            }

            //myvolkswagendev://cpo
            if (host.equals(getString(R.string.app_deeplink_host_car_identification))) {
                String carPlate = PreferenceManager.getInstance().getMainCarPlate();
                String carVin = PreferenceManager.getInstance().getMainCarVin();

                if (!carPlate.isEmpty() && !carVin.isEmpty()) {
                    goCarIdentification(carPlate, carVin, query);
                }

                return;
            }

            //myvolkswagendev://openhub
            if (host.equals(getString(R.string.app_deeplink_host_open_hub))) {
                checkMemberPhoneAndGoOpenHub();
                return;
            }

            //myvolkswagendev://webview?url={{網址與參數}}&header={{標題}}
            if (host.equals(getString(R.string.app_deeplink_host_web_view)) && url.contains("url=")) {
                String customUrl = url.split("url=")[1];
                String customTitle = uri.getQueryParameter("header");
                String webViewUrl = customUrl.split("&")[0];
                Set<String> stringSet = uri.getQueryParameterNames();
                for (String parameterName : stringSet) {
                    if (parameterName.equalsIgnoreCase("url") || parameterName.equalsIgnoreCase("header")) {
                        continue;
                    }

                    String parameterValue = uri.getQueryParameter(parameterName);
                    webViewUrl = Uri.parse(webViewUrl).buildUpon().appendQueryParameter(parameterName, parameterValue).build().toString();
                }

                goCustomWebView(customTitle, webViewUrl);
                return;
            }

            //myvolkswagen://register
            if (host.equals(getString(R.string.app_deeplink_host_register))) {
                //myvolkswagen://register/line/success
                if (path != null && path.contains(getString(R.string.app_deeplink_path_line_success))) {
                    showBindLineSuccessDialog();
                    return;
                }

                //myvolkswagen://register/line/fail
                if (path != null && path.contains(getString(R.string.app_deeplink_path_line_fail))) {
                    return;
                }
            }
        }
    }

    public void goMain() {
        MainNewActivity.goMain(this);
    }

    public void goMainCarMaintenance() {
        MainNewActivity.goMain(this, MainNewActivity.TAB_MAINTENANCE);
    }

    public void goMainCarDongle() {
        MainNewActivity.goMain(this, MainNewActivity.TAB_ASSISTANT);
    }

    public void goSetting() {
        SettingsActivity.goSettings(this);
    }

    public void goMyCar() {
        MyCarActivity.goMyCar(this);
    }

    public void goAddCar() {
        AddCarBottomSheetDialogFragment.getInstance().show(getSupportFragmentManager(), "");
    }

    public void goDongleBinding(String carPlate, String model) {
        DongleBindingBottomSheetDialogFragment dialogFragment = DongleBindingBottomSheetDialogFragment.getInstance(carPlate, model);
        dialogFragment.show(getSupportFragmentManager(), "");
    }

    public void goNotice(int type) {
        NoticeActivity.goNotice(this, type);
    }

    public void goCarIdentification(String carPlate, String carVin, String queryString) {
        String query = "";
        if (queryString != null && !queryString.isEmpty()) {
            query = queryString;
        } else {
            query = "token=" + PreferenceManager.getInstance().getUserToken() +
                    "&AccountId=" + PreferenceManager.getInstance().getAccountId() +
                    "&LicensePlateNumber=" + carPlate +
                    "&VIN=" + carVin;
        }

        BaseWebViewActivity.goWebView(this, TYPE_CAR_IDENTIFICATION, query);
    }

    public void goMyServiceNearby(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_SERVICE_NEARBY, queryString);
    }

    public void goServiceSetting(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_SERVICE_SETTING, queryString);
    }

    public void goBenefits(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_MEMBER_BENEFITS, queryString);
    }

    public void goCoupon(int type, String queryString) {
        String query = "";
        if (queryString != null && !queryString.replaceAll(" ", "").isEmpty()) {
            query = queryString.contains("type") ? queryString : queryString + "&type=" + type;
        } else {
            query = "type=" + type;
        }

        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_MEMBER_COUPON, query);
    }

    public void goPrivacy(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_PRIVACY, queryString);
    }

    public void goService(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_TERMS, queryString);
    }

    public void goCarReservation(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_RESERVATION, queryString);
    }

    public void goCarRecords(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_RECORDS, queryString);
    }

    public void goCarSuggestion(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_SUGGESTION, queryString);
    }

    public void goCarEstimate(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_ESTIMATE, queryString);
    }

    public void goCarInsurance(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_INSURANCE, queryString);
    }

    public void goCarRecall(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_RECALL, queryString);
    }

    public void goCarAdditionalQuotation(String queryString) {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_CAR_ADDITIONAL_QUOTATION, queryString);
    }

    public void goLineBinding() {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_LINE_BINDING);
    }

    public void goCustomWebView(String title, String url) {
        BaseWebViewActivity.goWebView(this, TYPE_ANY, title, url);
    }

    public void checkMemberPhoneAndGoOpenHub() {
        showLoading();
        Repository.getInstance().getMemberProfile(new Repository.OnApiCallback<Profile>() {
            @Override
            public void onSuccess(Profile profile) {
                if (profile != null && profile.mobile != null && !profile.mobile.trim().replaceAll(" ", "").isEmpty()) {
                    //has cellphone, call get redirect url api
                    Repository.getInstance().postGetExtRedirectUrl(ExtRedirectUrlInput.OPEN_HUB, new Repository.OnApiCallback<String>() {
                        @Override
                        public void onSuccess(String s) {
                            hideLoading();
                            BaseWebViewActivity.goWebView(BaseActivity.this, TYPE_OPEN_HUB, "", s);
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            hideLoading();
                        }
                    });

                } else {
                    //no cellphone
                    hideLoading();
                    showNoMobileDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    public void showBindLineSuccessDialog() {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.title_bind_line_success), getString(R.string.msg_bind_line_success), getString(R.string.confirm_button), "");
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                goMain();
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });
    }

    public void showServiceCenterBottomSheetDialog() {
        ServiceCenterBottomSheetDialog dialog = new ServiceCenterBottomSheetDialog(this, this);
        dialog.show();
    }

    public void showNoMobileDialog() {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.dialog_no_mobile_title), getString(R.string.dialog_no_mobile_content), getString(R.string.confirm_button), "");
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                goServiceSetting("");
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });
    }

    @Override
    public void onSettingServiceCenterClick() {
        goServiceSetting("");
    }

    public void showContactUsBottomSheetDialog() {
        ContactBottomSheetDialog dialog = new ContactBottomSheetDialog(this, this);
        dialog.show();
    }

    @Override
    public void onContactOnline() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Config.VW_CONTACT_US_URL));
        startActivity(browserIntent);

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "專屬線上顧問", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onContactByEmail() {
        BaseWebViewActivity.goWebView(this, BaseWebViewActivity.TYPE_VW_EMAIL_SERVICE);
        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "客服信箱", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onContactByPhone() {
        makePhoneCall();
        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "撥打客服電話", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    public void showLoading() {
        if (loadingProgress != null) {
            loadingProgress.show();
        }
    }

    public void hideLoading() {
        if (loadingProgress != null) {
            loadingProgress.dismiss();
        }
    }

    private void makePhoneCall() {
        String dial = "tel:0800828818";
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall();
            } else {
                Toast.makeText(this, getString(R.string.contect_us_permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
