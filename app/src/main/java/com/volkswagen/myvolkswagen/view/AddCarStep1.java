package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.GetCarModelData;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

public class AddCarStep1 extends Fragment implements View.OnClickListener{

    private Button add_car_step1_btn;
    private Button add_car_step1_btn_skip;
    private ImageButton btn_back;
    final String PLATE_FORMAT = "%1$s-%2$s";
    private String mOwner;
    private AddCarData mData;
    private AddCarActivity baseActivity;
    private EditText ownerEdit;
    private EditText plate1Edit;
    private EditText plate2Edit;
    private EditText vinEdit;
    private View view;
    private List<CarModel> mCarModels;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycar_add_car_step1, container, false);
        initView(view);
        baseActivity = (AddCarActivity) getActivity();
        return view;
    }
    private void initView(View v) {
        view =v;
        add_car_step1_btn = v.findViewById(R.id.add_car_step1_btn_next);
        add_car_step1_btn.setOnClickListener(this);
        add_car_step1_btn_skip= v.findViewById(R.id.add_car_step1_btn_skip);
        add_car_step1_btn_skip.setOnClickListener(this);

        btn_back = v.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        v.findViewById(R.id.add_car_step1_tv_find_vin).setOnClickListener(this);

        ownerEdit = v.findViewById(R.id.add_car_step1_et_owner);
        ownerEdit.setOnClickListener(this);
        plate1Edit = v.findViewById(R.id.add_car_step1_et_plate1);
        plate1Edit.setOnClickListener(this);
        plate2Edit = v.findViewById(R.id.add_car_step1_et_plate2);
        plate2Edit.setOnClickListener(this);
        vinEdit = v.findViewById(R.id.add_car_step1_et_vin_abbr);
        vinEdit.setOnClickListener(this);

        mOwner = getActivity().getIntent().getStringExtra(AddCarActivity.KEY_NICKNAME);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step1_btn_next:
                validateData(v);
                //getCarModel();
                break;
            case  R.id.add_car_step1_btn_skip:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep5()).commitAllowingStateLoss();
                break;
            case R.id.btn_back:
                if(getActivity().getIntent().getBooleanExtra(AddCarActivity.BACK_MY_CAR_VIEW, false)) {
                    getActivity().finish();
                }
//                else {
//                    ProfileInputActivity.goProfile(getActivity(), mOwner, null);
//                }
                break;
        }
    }
    private void validateData(View v) {
        String owner = ownerEdit.getText().toString();
        String plate1 = plate1Edit.getText().toString();
        String plate2 = plate2Edit.getText().toString();
        String vin = vinEdit.getText().toString();

        if (TextUtils.isEmpty(owner) || TextUtils.isEmpty(plate1) || TextUtils.isEmpty(plate2) || TextUtils.isEmpty(vin)) {
            AlertUtils.showAlert(getActivity(), null, getString(R.string.add_car_step1_msg_fill_up), null, getString(R.string.ok_button), null);
        } else {
            String plate = String.format(PLATE_FORMAT, plate1, plate2);
            mData = baseActivity.GetCarData();
            mData.name = owner;
            mData.plate = plate;
            mData.vin = vin;
            checkCarExist();
        }
    }
    private void checkCarExist() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        baseActivity.GetApiService().getCarExist(mData, PreferenceManager.getInstance().getUserToken())
                .enqueue(new Callback<CarExistData>() {
                    @Override
                    public void onResponse(Call<CarExistData> call, retrofit2.Response<CarExistData> response) {
                        hud.dismiss();
                        if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                            return;
                        }

                        if (!response.isSuccessful()) {
                            Logger.d(this, "Failed to check car exist");
                            return;
                        }

                        CarExistData data = response.body();
                        if (!data.result) {
                            Logger.d(this, "Failed to check car exist: code=" + data.errorCode);
                            TextView error = view.findViewById(R.id.add_car_step1_tv_error);
                            switch (data.errorCode) {
                                case 3001:
                                    error.setText(R.string.add_car_step1_error_3001);
                                    break;
                                case 3003:
                                    error.setText(R.string.add_car_step1_error_3003);
                                    break;
                                case 3004:
                                    error.setText(R.string.add_car_step1_error_3004);
                                    break;
                                case 3005:
                                case 3009:
                                    error.setText(R.string.add_car_step1_error_3005_3009);
                                    ((TextView) view.findViewById(R.id.add_car_step1_et_owner)).setText(null);
                                    ((TextView) view.findViewById(R.id.add_car_step1_et_plate1)).setText(null);
                                    ((TextView) view.findViewById(R.id.add_car_step1_et_plate2)).setText(null);
                                    ((TextView) view.findViewById(R.id.add_car_step1_et_vin_abbr)).setText(null);
                                    mData.clearAll();
                                    break;
                                case 3010:
                                    error.setText(R.string.add_car_step1_error_3010);
                                    break;
                            }
                            return;
                        }

                        mData.typeId = data.id;
                        getCarModel();
                    }

                    @Override
                    public void onFailure(Call<CarExistData> call, Throwable t) {

                    }
                });
    }
    private void getCarModel() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        baseActivity.GetApiService().getCarModelData(new GetCarModelData(2), PreferenceManager.getInstance().getUserToken())
                .enqueue(new Callback<CarModelData>() {
                    @Override
                    public void onResponse(Call<CarModelData> call, retrofit2.Response<CarModelData> response) {
                        hud.dismiss();
                        if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                            return;
                        }

                        if (!response.isSuccessful()) {
                            Logger.d(this, "Failed to get car model");
                            return;
                        }

                        CarModelData data = response.body();
                        if (!data.result) {
                            Logger.d(this, "Failed to get car model: code=" + data.errorCode);
                            return;
                        }

                        mCarModels = data.models;
                        goAddCarStep2();
                    }

                    @Override
                    public void onFailure(Call<CarModelData> call, Throwable t) {
                        Logger.d(this, "Failed to get car model: " + t.getLocalizedMessage());
                        hud.dismiss();
                    }
                });
    }

    private void goAddCarStep2(){
        AddCarStep2 fragment = new AddCarStep2();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AddCarFragment.CAR_MODEL, (Serializable) mCarModels);
        bundle.putString(AddCarFragment.CAR_PLATE, mData.plate);
        bundle.putInt(AddCarFragment.CAR_TYPE_ID, mData.typeId);
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, fragment).commitAllowingStateLoss();
    }
}
