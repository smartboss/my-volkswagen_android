package com.volkswagen.myvolkswagen.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

import com.volkswagen.myvolkswagen.R;

import java.io.InputStream;

public class GifView extends View {
    private Movie gifMovie;
    private long movieStart;
    private int gifId = -1;

    public GifView(Context context) {
        super(context);
    }

    public GifView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.GifView, 0, 0);
            gifId = a.getResourceId(R.styleable.GifView_gifSrc, -1);
            a.recycle();
        }

        if (gifId != -1) {
            InputStream gifInputStream = context.getResources().openRawResource(gifId);
            gifMovie = Movie.decodeStream(gifInputStream);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        long now = android.os.SystemClock.uptimeMillis();
        if (movieStart == 0) {
            movieStart = now;
        }

        if (gifMovie != null) {
            int dur = gifMovie.duration();
            if (dur == 0) {
                dur = 1000;
            }
            int relTime = (int) ((now - movieStart) % dur);
            gifMovie.setTime(relTime);

            float scaleX = (float) getWidth() / (float) gifMovie.width();
            float scaleY = (float) getHeight() / (float) gifMovie.height();
            canvas.save();
            canvas.scale(scaleX, scaleY);
            gifMovie.draw(canvas, 0, 0);
            canvas.restore();

            invalidate();
        }
    }
}