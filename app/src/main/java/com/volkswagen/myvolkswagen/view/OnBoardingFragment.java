package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.volkswagen.myvolkswagen.R;

public class OnBoardingFragment extends Fragment implements View.OnClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        ViewPager viewPager = view.findViewById(R.id.viewPager);
        OnBoardingAdapter adapter = new OnBoardingAdapter(getContext());
        viewPager.setAdapter(adapter);

        TabLayout indicator = view.findViewById(R.id.layout_indicator);
        indicator.setupWithViewPager(viewPager);

        TextView tvSkip = view.findViewById(R.id.btn_skip);
        tvSkip.setOnClickListener(this);

        TextView tvStart = view.findViewById(R.id.btn_start);
        tvStart.setOnClickListener(this);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                tvStart.setVisibility((position == adapter.getCount() - 1) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new LoginFragment()).addToBackStack("").commitAllowingStateLoss();
        }
    }
}
