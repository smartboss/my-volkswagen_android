package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;

public class DongleUnbindFragment extends Fragment implements View.OnClickListener {
    private static DongleUnbindFragment fragment;
    public static final String CONST_MODEL = "model";
    public static final String CONST_DONGLE_STATE = "dongle_state";

    public static DongleUnbindFragment newInstance(String model, String dongleState) {
        fragment = new DongleUnbindFragment();

        Bundle bundle = new Bundle();
        bundle.putString(CONST_MODEL, model);
        bundle.putString(CONST_DONGLE_STATE, dongleState);
        fragment.setArguments(bundle);

        return fragment;
    }

    private TextView tvTitle, tvContent, tvNoDongleServiceHint, btnAddCar, tvBindDongle;
    private Group groupBottomButton;
    private String model = "";
    private String dongleState = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dongle_unbind, container, false);

        if (getArguments() != null && getArguments().containsKey(CONST_MODEL)) {
            model = getArguments().getString(CONST_MODEL, "");
        }

        if (getArguments() != null && getArguments().containsKey(CONST_DONGLE_STATE)) {
            dongleState = getArguments().getString(CONST_DONGLE_STATE, "");
        }

        initView(view);
        return view;
    }

    private void initView(View view) {
        tvTitle = view.findViewById(R.id.tv_title);
        tvContent = view.findViewById(R.id.tv_content);
        tvNoDongleServiceHint = view.findViewById(R.id.tv_no_dingle_hint);

        btnAddCar = view.findViewById(R.id.btn_add_car);
        btnAddCar.setOnClickListener(this);

        tvBindDongle = view.findViewById(R.id.tv_bind_now);
        tvBindDongle.setOnClickListener(this);

        groupBottomButton = view.findViewById(R.id.group_bottom_button);

        String carPlateNo = PreferenceManager.getInstance().getMainCarPlate();
        if (!carPlateNo.isEmpty()) {
            btnAddCar.setVisibility(View.GONE);
            tvNoDongleServiceHint.setVisibility(View.VISIBLE);
            tvTitle.setText(getString(R.string.assistant_unbinding_assistant));
            tvContent.setText(getString(R.string.assistant_unbinding_desc));
        } else {
            btnAddCar.setVisibility(View.VISIBLE);
            tvNoDongleServiceHint.setVisibility(View.GONE);
            tvTitle.setText(getString(R.string.assistant_not_owner));
            tvContent.setText(getString(R.string.assistant_not_owner_desc));
        }

        if (!carPlateNo.isEmpty() && !dongleState.isEmpty() && dongleState.equalsIgnoreCase(CarData.DONGLE_STATUS_VIN_MAPPED)) {
            groupBottomButton.setVisibility(View.VISIBLE);
        } else {
            groupBottomButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_car:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goAddCar();
                }
                break;
            case R.id.tv_bind_now:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    String plateNo = PreferenceManager.getInstance().getMainCarPlate();
                    ((BaseActivity) getActivity()).goDongleBinding(plateNo, model);
                }
                break;
        }
    }
}
