package com.volkswagen.myvolkswagen.view.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.alert.VWBaseAlertDialogFragment;

public class SettingFragment extends Fragment implements View.OnClickListener {
    private String CHANNEL_ID = "volks_channel";

    private static SettingFragment fragment;
    private SwitchCompat switchBtn;

    public static SettingFragment newInstance() {
        if (fragment == null) {
            fragment = new SettingFragment();
        }

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        TextView tvPrivacy = view.findViewById(R.id.tv_privacy_title);
        tvPrivacy.setOnClickListener(this);

        TextView tvService = view.findViewById(R.id.tv_service_title);
        tvService.setOnClickListener(this);

        TextView tvVersion = view.findViewById(R.id.tv_version);
        tvVersion.setText(String.format(getString(R.string.settings_version), BuildConfig.VERSION_NAME));

        TextView tvDeleteAccount = view.findViewById(R.id.tv_delete_account_title);
        tvDeleteAccount.setOnClickListener(this);

        TextView tvLogout = view.findViewById(R.id.tv_logout);
        tvLogout.setOnClickListener(this);

        switchBtn = view.findViewById(R.id.settings_push_btn);
        switchBtn.setOnClickListener(view1 -> {
            boolean isChecked = switchBtn.isChecked();
            if (isChecked) {
                openNotificationSettingsForApp();
            } else {
                VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.settings_open_notification_title), getString(R.string.settings_open_notification_msg), getString(R.string.confirm_button), getString(R.string.cancel_button));
                dialogFragment.show(getChildFragmentManager(), null);
                dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
                    @Override
                    public void onDialogPositiveClick() {
                        openNotificationSettingsForApp();
                    }

                    @Override
                    public void onDialogNegativeClick() {
                        switchBtn.setChecked(true);
                    }
                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getContext() != null) {
            boolean pushStatus = NotificationManagerCompat.from(getContext()).areNotificationsEnabled();
            switchBtn.setChecked(pushStatus);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_privacy_title:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goPrivacy("");
                }
                break;
            case R.id.tv_service_title:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goService("");
                }
                break;
            case R.id.tv_delete_account_title:
                if (getActivity() != null && getActivity() instanceof SettingsActivity) {
                    ((SettingsActivity) getActivity()).deleteUserAccount();
                }
                break;
            case R.id.tv_logout:
                if (getActivity() != null && getActivity() instanceof SettingsActivity) {
                    ((SettingsActivity) getActivity()).logout();
                }
                break;
        }
    }

    private void openNotificationSettingsForApp() {
        if (getActivity() == null) {
            return;
        }

        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, CHANNEL_ID);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, getActivity().getPackageName());
        } else {
            intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
            intent.setData(uri);
        }
        startActivity(intent);
    }
}
