package com.volkswagen.myvolkswagen.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.ServiceReminder;

import java.util.ArrayList;
import java.util.List;

public class CarMaintenanceAdapter extends PagerAdapter {

    private Context context;
    private List<ServiceReminder> serviceReminders = new ArrayList<>();
    private OnMaintenanceItemClick onMaintenanceItemClick;

    public CarMaintenanceAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_car_maintenance, container, false);

        ServiceReminder reminder = serviceReminders.get(position);

        View bgCarMaintenance = layout.findViewById(R.id.bg_car_maintenance);
        TextView tvContent = layout.findViewById(R.id.tv_car_maintenance_content);
        TextView btnMaintenance = layout.findViewById(R.id.btn_car_maintenance);
        ImageView imgNotice = layout.findViewById(R.id.img_car_maintenance_notice);

        tvContent.setText(reminder.description);
        btnMaintenance.setText(reminder.btnName);
        btnMaintenance.setOnClickListener(view -> {
            if (onMaintenanceItemClick != null) {
                onMaintenanceItemClick.onButtonClick(reminder);
            }
        });

        boolean isAlert = reminder.type.equals("alert");
        imgNotice.setVisibility(isAlert ? View.VISIBLE : View.GONE);
        bgCarMaintenance.setBackgroundResource(isAlert ? R.drawable.rec_white_corner_with_red_stroke_8 : R.drawable.rec_white_corner_8);

        container.addView(layout);
        return layout;
    }

    public void setOnMaintenanceItemClick(OnMaintenanceItemClick onMaintenanceItemClick) {
        this.onMaintenanceItemClick = onMaintenanceItemClick;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return serviceReminders.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setData(List<ServiceReminder> serviceReminders) {
        this.serviceReminders = serviceReminders;
        notifyDataSetChanged();
    }

    public interface OnMaintenanceItemClick {
        void onButtonClick(ServiceReminder reminder);
    }
}