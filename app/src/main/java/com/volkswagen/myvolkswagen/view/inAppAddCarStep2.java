package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.VwFirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

public class inAppAddCarStep2 extends Fragment implements View.OnClickListener{

    private Button add_car_step2_btn;
    private Spinner spinner_car_model;
    private TextView carTypeText , carPlateText;
    final String PLATE_FORMAT = "%1$s-%2$s";
    private List<CarModel> mCarModels;
    private List<String> mCarImgs;
    private List<Integer> mCarIds;
    private ImageView img_add_car, view_holder_car_info_image;
    private inAppAddCarActivity baseActivity;
    private ImageButton btn_back, btn_close;
    private int SelectModelID;

    TextView errorText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inapp_add_car_step2, container, false);

        initView(view);
        return view;
    }
    private void initView(View v) {
        baseActivity = (inAppAddCarActivity) getActivity();

        add_car_step2_btn = v.findViewById(R.id.add_car_step2_btn_next);
        add_car_step2_btn.setOnClickListener(this);

        btn_back = v.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        btn_close = v.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(this);

        carTypeText = v.findViewById(R.id.add_car_step2_tv_car_type);
        carPlateText = v.findViewById(R.id.add_car_step2_tv_car_plate);
        carPlateText.setText(getArguments().getString(AddCarFragment.CAR_PLATE));

        errorText =v.findViewById(R.id.add_car_step2_tv_error);

        spinner_car_model = v.findViewById(R.id.spinner_car_model);

        img_add_car = v.findViewById(R.id.img_add_car);
        view_holder_car_info_image = v.findViewById(R.id.view_holder_car_info_image);

        loadImageWithPicasso("https://imgd.aeplcdn.com/664x374/n/cw/ec/144681/virtus-exterior-right-front-three-quarter-7.jpeg");

        GetCarModel();
    }

    private void GetCarModel() {
        mCarModels = (List<CarModel>) getArguments().getSerializable(AddCarFragment.CAR_MODEL);
        mCarImgs = new ArrayList<String>();
        mCarIds = new ArrayList<Integer>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
        for(CarModel carModel : mCarModels)
        {
            mCarIds.add(carModel.id);
            mCarImgs.add(carModel.image);
            adapter.add(carModel.name);
        }
        spinner_car_model.setAdapter(adapter);

        spinner_car_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // 您可以在这里处理Spinner项的选择更改
                SelectModelID = mCarIds.indexOf(position);
                String modelName = parentView.getItemAtPosition(position).toString();
                carTypeText.setText(modelName);
                // 如有必要，可以使用selectedValue或position
                loadImageWithPicasso(mCarImgs.get(position));
                baseActivity.GetCarData().modelId = GetTypeIdByCarModel(modelName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // 如果用户未选择任何项，您可以在这里处理
            }
        });
    }

    private void loadImageWithPicasso(String url) {
        Picasso.get().
                load(url).
                fit().
                centerCrop().
                into(img_add_car);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step2_btn_next:
                AddCarData addCarData = baseActivity.GetCarData();
                addCar(addCarData);
                break;
            case R.id.btn_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new inAppAddCarStep1()).commitAllowingStateLoss();
                break;
            case R.id.btn_close:
                getActivity().finish();
                break;
        }
    }

    private int GetTypeIdByCarModel(String _modelName)
    {
        for (CarModel carModel: mCarModels
             ) {
            if(carModel.name.equals(_modelName))
                return carModel.id;
        }
        return 0;
    }

    private void addCar(AddCarData addCarData) {
        Logger.d(this, "baseActivity.GetApiService addCar");
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        baseActivity.GetApiService().addCar(addCarData, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to add car");
                    errorText.setText("Failed to add car");
                    return;
                }

                Response data = response.body();
                if (!data.result) {
                    Logger.d(this, "Failed to add car: code=" + data.errorCode);
                    switch (data.errorCode)
                    {
                        case 3001:
                            errorText.setText(R.string.addcar_error_code_3001);
                            break;
                        case 3003:
                            errorText.setText(R.string.addcar_error_code_3003);
                            break;
                        case 3004:
                            errorText.setText(R.string.addcar_error_code_3004);
                            break;
                        case 3005:
                            errorText.setText(R.string.addcar_error_code_3005);
                            break;
                        case 3008:
                            errorText.setText(R.string.addcar_error_code_3008);
                            break;
                        case 3009:
                            errorText.setText(R.string.addcar_error_code_3009);
                            break;
                        case 3010:
                            errorText.setText(R.string.addcar_error_code_3010);
                            break;
                    }
                    return;
                }
                FirebaseAnalytics.getInstance(getContext()).logEvent(VwFirebaseAnalytics.Event.ADD_CAR_COMPLETE, null);
                ReGetCarDatas();
                baseActivity.finish();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Logger.d(this, "Failed to add car: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }

    private void ReGetCarDatas(){
        baseActivity.GetApiService().getMyCarData(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<MyCarData>() {
            @Override
            public void onResponse(Call<MyCarData> call, retrofit2.Response<MyCarData> response) {
                if (ApiClientManager.handleTokenResponse(baseActivity, response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to get my car data");
                    return;
                }

                MyCarData data = response.body();
                Logger.d(this, "\n" + data.toString());
                if (!data.result) {
                    Logger.d(this, "Failed to get my car data: code=" + data.errorCode);
                    return;
                }

                // update mycardata to PreferenceManager
                PreferenceManager.getInstance().saveMyCars(data);
                Logger.d(this, "Car data size=" + data.cars.size());
            }

            @Override
            public void onFailure(Call<MyCarData> call, Throwable t) {
                Logger.d(this, "Failed to get my car data: " + t.getLocalizedMessage());
            }
        });
    }
}
