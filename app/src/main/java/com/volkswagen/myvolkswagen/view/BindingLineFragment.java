package com.volkswagen.myvolkswagen.view;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_BIND_LINE;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BindingLineFragment extends Fragment {

    private static BindingLineFragment fragment;

    public static BindingLineFragment getInstance() {
        if (fragment == null) {
            fragment = new BindingLineFragment();
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_binding_line, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_BIND_LINE);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }
}
