package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.Utils;

import java.net.URISyntaxException;

public class BaseWebViewFragment extends Fragment {
    private static final String ARG_URL = "url";

    private WebView webView = null;
    private PermissionRequest webViewPermissionRequest;
    private GeolocationPermissions.Callback gpsCallback;
    private String gpsOrigin = "";

    public static BaseWebViewFragment newInstance(String url) {
        BaseWebViewFragment fragment = new BaseWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        fragment.setArguments(bundle);

        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview_base, container, false);
        String url = "";
        if (getArguments() != null && getArguments().containsKey(ARG_URL)) {
            url = getArguments().getString(ARG_URL, "");
        }

        webView = view.findViewById(R.id.webView);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null) {
                    return false;
                }

                if (!isAdded() || getActivity() == null) {
                    return true;
                }

                if (url.contains("tel:")) {
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(url)));
                    return true;
                }

                if (url.contains("www.google.com/maps")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                }

                String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
                if (url.startsWith(appScheme)) {
                    activityHandleDeeplink(url);
                    return url.startsWith(appScheme);
                } else if (url.startsWith("intent://")) {
                    try {
                        Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                        if (fallbackUrl != null) {
                            view.loadUrl(fallbackUrl);
                        }
                        return true;
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    return false;
                } else {
                    return false;
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                if (getActivity() != null) {
                    int permissionCheckFineLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permissionCheckFineLocation != PackageManager.PERMISSION_GRANTED) {
                        gpsCallback = callback;
                        gpsOrigin = origin;

                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Utils.REQUEST_PERMISSIONS_CODE);
                    } else {
                        callback.invoke(origin, true, false);
                    }
                }

            }
        });

        webView.loadUrl(url);

        return view;
    }

    public void activityHandleDeeplink(String url) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).handleDeeplink(url);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                }

                if (permissions[0].equalsIgnoreCase(Manifest.permission.ACCESS_FINE_LOCATION) && gpsCallback != null && gpsOrigin != null) {
                    gpsCallback.invoke(gpsOrigin, true, false);
                }
            }
        }
    }

    public WebView getWebView() {
        return webView;
    }
}
