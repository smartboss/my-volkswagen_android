package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.view.addcar.AddCarBaseFragment;

public class AddCarBottomSheetDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private AddCarBaseFragment addCarBaseFragment;

    public static AddCarBottomSheetDialogFragment getInstance() {
        return new AddCarBottomSheetDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BaseBottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheetdialog_add_car, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        ImageView imgBack = view.findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);

        ImageView imgClose = view.findViewById(R.id.img_close);
        imgClose.setOnClickListener(this);

        Button button = view.findViewById(R.id.btn_next);
        button.setOnClickListener(this);

        addCarBaseFragment = AddCarBaseFragment.getInstance();
        addCarBaseFragment.setRootBottomSheetDialog(this);
        getChildFragmentManager().beginTransaction().replace(R.id.layout_frame, addCarBaseFragment, "").commitAllowingStateLoss();
    }

    @Override
    public void onStart() {
        super.onStart();
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    @Override
    public void onDestroyView() {
        addCarBaseFragment = null;
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (addCarBaseFragment != null) {
                    addCarBaseFragment.goBack();
                }
                break;
            case R.id.img_close:
                dismiss();
                break;
            case R.id.btn_next:
                if (addCarBaseFragment != null) {
                    addCarBaseFragment.onNextClick();
                }
                break;
        }
    }
}