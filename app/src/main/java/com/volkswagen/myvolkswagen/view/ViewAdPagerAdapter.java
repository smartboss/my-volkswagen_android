package com.volkswagen.myvolkswagen.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.NewsData;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class ViewAdPagerAdapter extends RecyclerView.Adapter<ViewAdPagerAdapter.ViewHolder> {

    private AppNews appNews;
    private OnAdClickListener listener;
    public ViewAdPagerAdapter(AppNews newsList, OnAdClickListener listener) {
        this.appNews = newsList;
        this.listener = listener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_new_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewsData newsData = appNews.newsDataList.get(position);
        holder.text_car_ad_context.setText(newsData.title);
        holder.text_ad_date.setText(newsData.published_at);
        if(newsData.cover_image == null || newsData.cover_image.length() == 0)
        {
            return;
        }
        Picasso.get().load(newsData.cover_image).placeholder(R.drawable.large).fit().
                centerCrop().into(holder.img_dashboard_ad);
        holder.newsLink = newsData.link;
    }

    @Override
    public int getItemCount() {
        return appNews.newsDataList.toArray().length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_car_ad_context;
        TextView text_ad_date;
        ImageView img_dashboard_ad;
        String newsLink;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_car_ad_context = itemView.findViewById(R.id.text_car_ad_context);
            text_ad_date = itemView.findViewById(R.id.text_ad_date);
            img_dashboard_ad = itemView.findViewById(R.id.img_dashboard_ad);

            // 為 img_dashboard_ad 設置 OnClickListener
            img_dashboard_ad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text_title = text_car_ad_context.getText().toString();
                    listener.onAdClick(text_title, newsLink);
                }
            });
        }
    }

    public interface OnAdClickListener {
        void onAdClick(String title, String url);
    }
}
