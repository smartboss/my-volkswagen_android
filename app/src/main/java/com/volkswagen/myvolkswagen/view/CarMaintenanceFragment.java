package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class CarMaintenanceFragment extends Fragment implements View.OnClickListener{
    public static final String ARG_INITIAL_VIEW = "arg_initial_view";

    public static final int INITIAL_NORMAL = 0;
    public static final int INITIAL_CAR_INFO = 1;
    public static final int INITIAL_MAINTAIN_INFO = 2;

    private PermissionRequest webViewPermissionRequest;

//    private TextView tvTitle;
    private Button btn_myCar;
    private ImageButton btnNotice;
    private ImageView btnClose;
    private WebView webView;

    private Uri mCallUri;

    public static CarMaintenanceFragment newInstance(int initialView) {
        CarMaintenanceFragment fragment = new CarMaintenanceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_INITIAL_VIEW, initialView);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_maintenance, container, false);
//        tvTitle = view.findViewById(R.id.tv_title);
        btnNotice = view.findViewById(R.id.btn_notice);
        btnClose = view.findViewById(R.id.btn_close);
        webView = view.findViewById(R.id.web_view);
        btn_myCar = view.findViewById(R.id.btn_mycar);
        btn_myCar.setOnClickListener(this);

        //notice button
        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
        btnNotice.setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);

        //webView
        int initialView = INITIAL_NORMAL;
        if (getArguments() != null && getArguments().containsKey(ARG_INITIAL_VIEW)) {
            initialView = getArguments().getInt(ARG_INITIAL_VIEW, INITIAL_NORMAL);
        }

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")) {
                    mCallUri = Uri.parse(url);
                    callDial();
                    return true;
                }

                String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
                if( url.startsWith(appScheme)){
                    mCallUri = Uri.parse(url);
                    Logger.d(this, "deeplink :" + url + ",schema:" + appScheme);
                    String host = mCallUri.getHost();
                    String path = mCallUri.getPath();

                    call(host, path);
                    return true;
                }


                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }
        });

        String accountId = PreferenceManager.getInstance().getAccountId();
        String plate = "";
        String vin = "";
        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();

        if( carData != null ) {
            plate = carData.plate;
            vin = carData.vin;
            if( plate != null) {
                String myCarUrl = Uri.parse(Config.VW_MAINTENANCE_URL).buildUpon()
                        .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                        .appendQueryParameter("LicensePlateNumber", plate)
                        .appendQueryParameter("VIN", vin)
                        .appendQueryParameter("AccountId", accountId )
                        .build()
                        .toString();

                String finalMyCarUrl = myCarUrl;
                webView.loadUrl(finalMyCarUrl);
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        webView.loadUrl(finalMyCarUrl);
                    }
                });
            }

            if( btn_myCar != null && carData != null) {
                btn_myCar.setText(carData.plate);
            }
        } else {
            btn_myCar.setText("");
        }


        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
//        btnNotice.setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }

    public WebView getWebView() {
        return webView;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length <= 0) {
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissions.length > 0 && permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                }
            } else {
            }
        }
    }

    private void call(String intentHost, String intentPath) {
//        startActivity(new Intent(Intent.ACTION_DIAL, mCallUri));
            if (intentHost.equals(getString(R.string.app_deeplink_host_maintenance))) {
                //myvolkswagen://maintenance
                if ( intentPath.equals(getString(R.string.app_deeplink_path_records))) {
                    WebViewActivity.goMaintenanceRecords(this.getActivity(), Config.VW_MAINTENANCE_RECORDS_URL);
                } else if(intentPath.equals(getString(R.string.app_deeplink_path_warranty))) {
                    WebViewActivity.goMaintenanceWarranty(this.getActivity());
                } else if(intentPath.equals(getString(R.string.app_deeplink_path_pat))) {
                    WebViewActivity.goMaintenancePat(this.getActivity());
                } else if(intentPath.equals(getString(R.string.app_deeplink_path_recalls))) {
                    WebViewActivity.goMaintenanceRecalls(this.getActivity());
                } else if(intentPath.equals(getString(R.string.app_deeplink_path_bodyandpaint))) {
                    WebViewActivity.goMaintenanceBodyAndPaint(this.getActivity());
                }
            } else if (intentHost.equals(getString(R.string.app_deeplink_host_contact))) {
//                WebViewActivity.goMaintenanceContact(this.getActivity());
            }

    }

    private void callDial() {
        startActivity(new Intent(Intent.ACTION_DIAL, mCallUri));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_mycar:
                MyCarActivity.goMyCar(getActivity());
                break;
        }
    }

}

