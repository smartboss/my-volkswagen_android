package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.CruisysApiClientManager;
import com.volkswagen.myvolkswagen.controller.CruisysApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.controller.ValueCallback;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarSummary;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.DongleBindingResponseData;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.model.Notice;
import com.volkswagen.myvolkswagen.model.RecallCheckCampaignResponse;
import com.volkswagen.myvolkswagen.model.ServiceReminder;
import com.volkswagen.myvolkswagen.model.ServiceReminderResponseData;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.view.notice.NoticeActivity;
import com.volkswagen.myvolkswagen.view.widget.ImagePagerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import retrofit2.Call;
import retrofit2.Callback;

public class DashboardFragment extends Fragment implements View.OnClickListener {
    public static boolean showRecallPopupCampaign = true;
    private static DashboardFragment instance;
    private NoticeAdapter mAdapter;
    private ReminderAdapter rAdapter;
    private Button spinner_mycar;

    public static void SetDashboardViewState(DashboardViewState state) {
        if (instance == null) {
            instance = new DashboardFragment();
        }

        DashboardFragment.mViewState = state;
    }

    public static final int TYPE_ALL = 0;

    public static DashboardViewState mViewState;

    public static AppNews AppNews;

    public static void SetAppNews(AppNews news) {
        AppNews = news;
    }

    private TextView tvNickname, text_car_mileage, text_car_battery, text_car_oil;

    private TextView tvCarType, tvCarModelName, tvCarPlat, tvCarOwnerDate, tvCarExpiration_date,
            text_my_service_center;
    private ImageView ivMembership, ivCarType;

    private Button dashboard_add_car_banner_btn, btn_contect_us, btn_no_assistant_tip, btnIdentityCar, btnWarranty, btnService, btnMoreNews;

    private ConstraintLayout layout_car_state_banner, layout_OwnerBanner, layout_nonOwnerBanner,
            layout_maintenance_info, layout_car_info;
    private FrameLayout layout_carBoundNoAssistant, fl_car_state_info;
    private LayoutInflater minflater;
    private CarData carData;
    private int spinnerFirstInteraction  = 0;

    enum DashboardViewState {
        nonOwner,
        carBoundNoAssistant,
        carBoundNoAssistant_notify,
        carAndAssistantBound
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        instance = this;
        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
        ((ImageButton) v.findViewById(R.id.btn_notice)).setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
        ((ImageButton) v.findViewById(R.id.btn_notice)).setOnClickListener(this);
        minflater = inflater;
        EventBus.getDefault().register(this);

        initView(v);
//        setView();

        return v;
    }

    private void initView(View v) {
        tvNickname = v.findViewById(R.id.tv_title_nickname);
        ivMembership = v.findViewById(R.id.img_membership);
        ivCarType = v.findViewById(R.id.img_car_type);

        spinner_mycar = v.findViewById(R.id.Spinner_mycar);
        spinner_mycar.setOnClickListener(this);

        dashboard_add_car_banner_btn = v.findViewById(R.id.dashboard_add_car_banner_btn);
        dashboard_add_car_banner_btn.setOnClickListener(this);

        btn_contect_us = v.findViewById(R.id.btn_contect_us);
        btn_contect_us.setOnClickListener(this);

        btn_no_assistant_tip = v.findViewById(R.id.btn_no_assistant_tip);
        btn_no_assistant_tip.setOnClickListener(this);

        text_car_mileage = v.findViewById(R.id.text_car_mileage);
        text_car_oil = v.findViewById(R.id.text_oil);
        text_car_battery = v.findViewById(R.id.text_battery);

        //car layout
        layout_OwnerBanner = v.findViewById(R.id.layout_OwnerBanner);
        //non owner layout
        layout_nonOwnerBanner = v.findViewById(R.id.layout_nonOwnerBanner);

        fl_car_state_info = v.findViewById(R.id.fl_car_state_info);

        layout_maintenance_info = v.findViewById(R.id.layout_maintenance_info);

        //car info
        layout_car_info = v.findViewById(R.id.layout_car_info);
        tvCarType = v.findViewById(R.id.text_car_type);
        tvCarModelName = v.findViewById(R.id.text_car_model_name);
        tvCarPlat = v.findViewById(R.id.text_car_plat);
        tvCarOwnerDate = v.findViewById(R.id.text_car_owner_date);
        tvCarExpiration_date = v.findViewById(R.id.text_warranty_date);
        text_my_service_center = v.findViewById(R.id.text_my_service_center);
        btnWarranty = v.findViewById(R.id.btn_add_warranty);
        btnWarranty.setOnClickListener(this);
        btnIdentityCar = v.findViewById(R.id.btn_identify_car);
        btnIdentityCar.setOnClickListener(this);
        btnService = v.findViewById(R.id.btn_other_services);
        btnService.setOnClickListener(this);

        //car state banner
        layout_car_state_banner = v.findViewById(R.id.layout_car_state_banner);
        //car no assistant tip
        layout_carBoundNoAssistant = v.findViewById(R.id.layout_carBoundNoAssistant);

        //test code
        //DashboardFragment.mViewState = mViewState.carAndAssistantBound;

        if(DashboardFragment.mViewState != null ) {
            switch (DashboardFragment.mViewState) {
                //非車主
                case nonOwner:
                    spinner_mycar.setVisibility(View.GONE);
                    ivMembership.setVisibility(View.GONE);
                    layout_OwnerBanner.setVisibility(View.GONE);
                    layout_maintenance_info.setVisibility(View.GONE);
                    layout_car_info.setVisibility(View.GONE);
                    break;
                //綁車不綁助理(提醒)
                case carBoundNoAssistant_notify:
                    layout_car_state_banner.setVisibility(View.GONE);
                    layout_nonOwnerBanner.setVisibility(View.GONE);
                    break;
                //綁車不綁助理
                case carBoundNoAssistant:
                    layout_car_state_banner.setVisibility(View.GONE);
                    layout_nonOwnerBanner.setVisibility(View.GONE);
                    layout_maintenance_info.setVisibility(View.GONE);
                    break;
                //綁車綁助理
                case carAndAssistantBound:
                    layout_car_state_banner.setVisibility(View.VISIBLE);
                    layout_carBoundNoAssistant.setVisibility(View.GONE);
                    layout_nonOwnerBanner.setVisibility(View.GONE);
                default:
                    break;
            }
        }

        // app news
        btnMoreNews = v.findViewById(R.id.notice_more_news);
        btnMoreNews.setOnClickListener(this);

        ViewPager2 viewPager2_news = v.findViewById(R.id.viewPager_news);
        if (AppNews == null || AppNews.newsDataList == null) {
            AppNews = new AppNews();
            AppNews.newsDataList = new ArrayList<NewsData>();
        }
        ViewAdPagerAdapter adapter = new ViewAdPagerAdapter(AppNews, new ViewAdPagerAdapter.OnAdClickListener() {
            @Override
            public void onAdClick(String title, String url) {
//                WebViewActivity.goNewsLink(getActivity(), title, url);
            }
        });

        viewPager2_news.setAdapter(adapter);
        viewPager2_news.setAdapter(mAdapter);
        viewPager2_news.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPager2_news.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int position = parent.getChildAdapterPosition(view);
                int itemCount = state.getItemCount();
                outRect.right = position == itemCount - 1 ? 16 : 0;
                outRect.left = 16;
            }
        });

        viewPager2_news.setPageTransformer((page, position) -> {
            page.setTranslationX(page.getWidth() * -position);
            float translation = page.getWidth() * -0.3f * position;

            if (viewPager2_news.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
                page.setTranslationX(translation);
            } else {
                page.setTranslationY(translation);
            }
        });
        mAdapter = new NoticeAdapter();
        RecyclerView recyclerView = v.findViewById(R.id.news_recycler_view);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(mAdapter);

        //getReminder list

        ViewPager2 viewPager2_reminder = v.findViewById(R.id.viewPager_maintenance_info);
        ServiceReminderResponseData reminderResData = PreferenceManager.getInstance().getReminder();
        if(reminderResData == null)
        {
            reminderResData = new ServiceReminderResponseData();
            reminderResData.serviceReminderList = new ArrayList<ServiceReminder>();
        }
        ViewReminderPagerAdapter reminder_adapter = new ViewReminderPagerAdapter(reminderResData, new ViewReminderPagerAdapter.OnButtonClickListener() {
            @Override
            public void onButtonClick(String title, String url2) {
                CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
                String accessToken = PreferenceManager.getInstance().getCruisysAccessToken();

                String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
                if( url2.startsWith(appScheme)) {
                    Uri uri = Uri.parse(url2);
                    String host = uri.getHost();
                    String path = uri.getPath();

                    // deep link : myvolkswagendev://maintenance/records
                    if(host.contains(getString(R.string.app_deeplink_host_maintenance))) {
                        if(path.contains(getString(R.string.app_deeplink_path_records))) {
                            // 保養維修紀錄
                            WebViewActivity.goMaintenanceRecords(getActivity(), Config.VW_MAINTENANCE_RECORDS_URL);
                        }
                    }

                } else {
                    // 預約保養
                    if(carData != null && carData.plate != null ) {
                        String queryParams = "AccountId=" + PreferenceManager.getInstance().getAccountId() + "&PlateNum=" + carData.plate + "&ServiceType=health_check&issueDescription=我想預約保養，請協助安排";
                        String url = Config.VW_FAB_AST_CAR + "?" + Base64.encodeToString(queryParams.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);
                        WebViewActivity.goVWAstCar(getActivity(), url);
                    }

                }

            }
        });
        viewPager2_reminder.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPager2_reminder.setAdapter(reminder_adapter);

        viewPager2_reminder.setPageTransformer((page, position) -> {
            page.setTranslationX(page.getWidth() * -position);
            float translation = page.getWidth() * -0.3f * position;

            if (viewPager2_reminder.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
                page.setTranslationX(translation);
            } else {
                page.setTranslationY(translation);
            }
        });

        RecyclerView reminderRecyclerView = v.findViewById(R.id.reminder_recycler_view);
        LinearLayoutManager reminderHorizontalLayoutManagaer = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false);
        reminderRecyclerView.setLayoutManager(reminderHorizontalLayoutManagaer);
        reminderRecyclerView.setAdapter(rAdapter);

    }

    private void setView() {
        carData = PreferenceManager.getInstance().tryGetFirstCarData();

        CarSummaryResponseData carSummaryResponseData = PreferenceManager.getInstance().getCarSummary();

        CarSummary carSummary = null;
        if (carSummaryResponseData != null) {
            if (carSummaryResponseData.carSummaryDataList != null && carSummaryResponseData.carSummaryDataList.size() > 0) {
                carSummary = carSummaryResponseData.carSummaryDataList.get(0);
            }
        }
        if (tvNickname != null) {
            tvNickname.setText("Hi " + PreferenceManager.getInstance().getUserNickname());
        }

        if (ivMembership != null) {
            int level = 1;
            Logger.d(DashboardFragment.this, "profile level => " + level);
            switch (level) {
                // blue
                case 1:
                    ivMembership.setImageResource(R.drawable.ic_new_membership_blue);
                    break;
                // sliver
                case 2:
                    ivMembership.setImageResource(R.drawable.ic_new_membership_sliver);
                    break;
                // gold
                case 3:
                    ivMembership.setImageResource(R.drawable.ic_new_membership_golden);
                    break;

                // platinum
                case 4:
                    ivMembership.setImageResource(R.drawable.ic_new_membership_platinum);
                    break;
            }
        }

        if (carData == null) {
            return;
        }

        if (ivCarType != null) {
            ArrayList<String> carPlatesArrayList = PreferenceManager.getInstance().getCarPlateList();
            carPlatesArrayList.add(0,"請選擇");
            if(carPlatesArrayList.contains(carData.plate))
            {
                carPlatesArrayList.remove(carData.plate);
                carPlatesArrayList.add(1, carData.plate);
            }
            String[] carPlates = carPlatesArrayList.toArray(new String[carPlatesArrayList.size()]);
            String imgUrl = carData.image;
            Logger.d(DashboardFragment.this, "loadImageWithPicasso => " + imgUrl);
            Picasso.get().load(imgUrl).placeholder(R.drawable.img_golf_rline).fit().centerInside().into(ivCarType);

            if (spinner_mycar != null) {
                spinner_mycar.setText(carData.plate);
                /*
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, carPlates);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_mycar.setAdapter(adapter);
                if(spinner_mycar.getCount() > 1)
                {
                    spinner_mycar.setSelection(1);
                }
                spinner_mycar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        if(++spinnerFirstInteraction > 1 && position > 0) {
                            MyCarActivity.goMyCar(getActivity());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // 如果用户未选择任何项，您可以在这里处理
                        if(++spinnerFirstInteraction > 1) {
                            MyCarActivity.goMyCar(getActivity());
                        }
                    }
                });
                */
            }
        }

        //mileage
        if (text_car_mileage != null) {
            //String mileageText = String.format(getResources().getString(R.string.dashboard_car_mileage_text), carData.mileage);
            if (carSummary != null && carSummary.odometer != null) {
                String mileageText = "總里程 "+carSummary.odometer.value + " " + carSummary.odometer.unit;
                text_car_mileage.setText(mileageText);
            }
        }

        //oil
        if (text_car_oil != null) {
            if (carSummary != null && carSummary.fuelLevel != null) {
                String odometerStr = carSummary.fuelLevel.value + " %";
                text_car_oil.setText(odometerStr);
                if(carSummary.fuelLevel.value <= 15.0)
                {
                    fl_car_state_info.setVisibility(View.VISIBLE);
                }
                else {
                    fl_car_state_info.setVisibility(View.GONE);
                }
            }
        }

        if (text_car_battery != null) {
            if (carSummary != null && carSummary.controlModuleVoltage != null) {
                String batteryStr = carSummary.controlModuleVoltage.value + " " + carSummary.controlModuleVoltage.unit;
                text_car_battery.setText(batteryStr);
            }
        }

        if (tvCarType != null) {
            tvCarType.setText(carData.type);
        }

        if (tvCarModelName != null) {
            tvCarModelName.setText(carData.model);
        }

        if (tvCarPlat != null) {
//            tvCarPlat.setText(carData.plate);
            tvCarPlat.setText(carData.vin);
        }

        if (tvCarOwnerDate != null) {
            tvCarOwnerDate.setText(carData.ownerDate);
        }

        if (text_my_service_center != null) {
            text_my_service_center.setText(carData.dealerPlantName);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        setView();
        // check dongle_binding status
        CruisysApiService service = CruisysApiClientManager.getClient().create(CruisysApiService.class);
        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
        String accessToken = PreferenceManager.getInstance().getCruisysAccessToken();
        if(carData != null && carData.plate != null ) {
            String Dongle_bindingUrl = CruisysApiClientManager.GET_ServiceDongle_bindingUrl(carData.plate);
            service.service_get_dongle_bind(Dongle_bindingUrl, "Bearer " + accessToken).enqueue(new Callback<DongleBindingResponseData>(){
                @Override
                public void onResponse(Call<DongleBindingResponseData> call, retrofit2.Response<DongleBindingResponseData> response) {
                    Logger.v(this,"GET Donglebind12345: " +  response.toString());
                    if( response.code() == 200) {
                        PreferenceManager.getInstance().saveDongle_binding(response.body());
                        PreferenceManager.getInstance().neverShowDongleHint();
                        DashboardFragment.SetDashboardViewState(DashboardViewState.carAndAssistantBound);
                    } else {
                        PreferenceManager.getInstance().neverShowDongleHint();
                        DashboardFragment.SetDashboardViewState(DashboardViewState.carBoundNoAssistant);

                    }
                    updateView();
                }
                @Override
                public void onFailure(Call<DongleBindingResponseData> call, Throwable t) {
                    Logger.e(this,"GET Donglebind error: " +  t.toString());
//                incrementAndCheck(id);
                }
            });

            if (showRecallPopupCampaign) {
                showRecallPopupCampaign = false;
                Repository.getInstance().checkCarHasRecallCampaign(getContext(), new ValueCallback<RecallCheckCampaignResponse.RecallCheckCampaignData>() {
                    @Override
                    public void onSuccess(RecallCheckCampaignResponse.RecallCheckCampaignData data) {
                        if (data.showNoticeBoard && getActivity() != null) {
                            RecallPopupFragment fragment = RecallPopupFragment.getInstance(data.recallUrl);
                            ((MainActivity) getActivity()).showPopupFragment(fragment);
                        }
                    }
                });
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
        ((ImageButton) getView().findViewById(R.id.btn_notice)).setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Spinner_mycar:
                MyCarActivity.goMyCar(getActivity());
                break;
            case R.id.btn_notice:
                NoticeActivity.goNotice(getActivity(), NoticeActivity.TYPE_NOTICE);
                break;
            case R.id.dashboard_add_car_banner_btn:
                //AddCarActivity.goAddCar(getActivity(), null, true);
                inAppAddCarActivity.goAddCar(getActivity(), PreferenceManager.getInstance().getUserNickname());
                break;
            case R.id.btn_contect_us:
                BottomSheetDialog ContectUsSheetDialog = new BottomSheetDialog(getActivity());
                View bottomSheetView = minflater.inflate(R.layout.sheetdalog_contect_us, null);
                ContectUsSheetDialog.setContentView(bottomSheetView);

                Button btn_contect_us_online_service = bottomSheetView.findViewById(R.id.btn_contect_us_online_service);
                Button btn_contect_us_email_service = bottomSheetView.findViewById(R.id.btn_contect_us_email_service);
                Button btn_contect_us_phone_service = bottomSheetView.findViewById(R.id.btn_contect_us_phone_service);
                btn_contect_us_online_service.setOnClickListener(this);
                btn_contect_us_email_service.setOnClickListener(this);
                btn_contect_us_phone_service.setOnClickListener(this);
                ContectUsSheetDialog.show();
                break;
            case R.id.btn_no_assistant_tip:
                PreferenceManager.getInstance().neverShowDongleHint();
                break;
            case R.id.btn_identify_car:
                WebViewActivity.goVWOnlineView(getActivity());
                break;
            case R.id.btn_add_warranty:
                String accountId = PreferenceManager.getInstance().getAccountId();
                CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
                String accessToken = PreferenceManager.getInstance().getCruisysAccessToken();
                if(carData != null && carData.plate != null ) {
                    String queryParams = "token=" + accessToken + "&AccountId=" + accountId + "&LicensePlateNumber=" + carData.plate + "&VIN=" + carData.vin;
                    String url = Config.VW_MAINTENANCE_WARRANTY_URL + "?" + queryParams;
                    WebViewActivity.goAddWarranty(getActivity(), url);
                }
                break;
            case R.id.btn_other_services:
                WebViewActivity.goVWService(getActivity());
                break;

                // 線上顧問
            case R.id.btn_contect_us_online_service:
                // open link by browser
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://consulting-service-pre.volkswagentaiwan.com.tw/join?category_id=3"));
                startActivity(browserIntent);
                break;
            case R.id.btn_contect_us_email_service:
//                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
//                emailIntent.setData(Uri.parse("mailto:"));
//                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"customercare@volkswagen.de"});
//                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
//                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
//                try {
//                    startActivity(emailIntent);
//                } catch (ActivityNotFoundException e) {
//                    Toast.makeText(getContext(), "No email client installed.", Toast.LENGTH_SHORT).show();
//                }
                WebViewActivity.goVWEmailService(getActivity());
                break;
            case R.id.btn_contect_us_phone_service:
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:1234567890"));
//                startActivity(dialIntent);
                if (ContextCompat.checkSelfPermission(MainActivity.getInstance(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 3);
                } else {
                    startActivity(dialIntent);
                }
                break;

            case R.id.notice_more_news:
//                NoticeNewsActivity.goNews(getActivity());
                break;
        }
    }

    private class NoticeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_HEADER = 1;
        private static final int VIEW_TYPE_CONTENT = 2;

        private SimpleDateFormat mSdf;

        public NoticeAdapter() {
            mSdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            mSdf.setTimeZone(TimeZone.getTimeZone(Config.API_DEFAUL_TIMEZONE));
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            if (viewType == VIEW_TYPE_HEADER) {
//                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_notice_header, parent, false);
//                return new HeaderViewHolder(v);
//            } else {
//                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_notice_content, parent, false);
//                return new ContentViewHolder(v);
//            }
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_news_content, parent, false);
            return new ContentViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof HeaderViewHolder) {
                ((HeaderViewHolder) holder).title.setText(position == 0 ? R.string.notice_header_recent : R.string.notice_header_past);
            } else {
//                bindContent((ContentViewHolder) holder, position);
                bindContentNew((ContentViewHolder) holder, position);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_CONTENT;
        }

        @Override
        public int getItemCount() {

            if( AppNews.newsDataList.size() > 6 ) {
                return 6;
            }
            return AppNews.newsDataList.size();

        }


        private void bindContentNew(ContentViewHolder holder, int position) {
            final Notice notice;
            final NewsData newsData;
            newsData = AppNews.newsDataList.get(position);
            holder.itemView.setOnClickListener(null);

            Logger.d(this, "bindContentNew Position2 " + position + ":\n" + newsData.toString());

            holder.itemView.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.bg_qr_code));

            holder.title.setText(newsData.title);
            holder.newsDate.setVisibility(View.VISIBLE);
            holder.newsDate.setText(newsData.published_at);

            if (newsData.thumb_cover_image.length() > 0) {
                ImagePagerAdapter pagerAdapter = new ImagePagerAdapter();
                holder.viewPager.setAdapter(pagerAdapter);
                List<String> coverList = new ArrayList<>();
                coverList.add(newsData.thumb_cover_image);
                pagerAdapter.setList(coverList);
                holder.tabLayout.setupWithViewPager(holder.viewPager, true);
                holder.viewPager.setVisibility(View.VISIBLE);
            } else {
                holder.viewPager.setVisibility(View.GONE);
            }

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                        OwnershipActivity.goOwnerShipActivity(NoticeNewsActivity.this, notice.newsId, notice.kind == KIND_SV_NORMAL_LINK, notice.kind == KIND_SV_NEW_CAR_LINK);
                    Logger.d(this, "DashboardFragment " + position + ":\n" + newsData.toString());

                    if( newsData.is_link > 0 ) {
                        WebViewActivity.goNewsLink(getActivity(), newsData.title, newsData.link);
                    } else {
                        Gson gson = new Gson();
                        String json = gson.toJson(newsData);
//                        LatestnewsActivity.goLatestnewsActivity(getActivity(), json );
                        Logger.d(getActivity(), "LatestnewsActivity intent:" + json);
                    }
                }
            };
            holder.itemView.setOnClickListener(listener);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView title;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.view_holder_notice_header_title);
        }
    }

    private class ContentViewHolder extends RecyclerView.ViewHolder {
        private TextView title, newsDate;
        private ViewPager viewPager;
        private TabLayout tabLayout;

        public ContentViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.view_holder_notice_title);
            newsDate = itemView.findViewById(R.id.view_holder_news_content_date);
            viewPager = itemView.findViewById(R.id.pager_photo);
            tabLayout = itemView.findViewById(R.id.indicator_tab);
        }
    }

    private class ReminderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }

    private void updateView() {
        switch (DashboardFragment.mViewState) {
            //非車主
            case nonOwner:
                spinner_mycar.setVisibility(View.GONE);
                ivMembership.setVisibility(View.GONE);
                layout_OwnerBanner.setVisibility(View.GONE);
                layout_maintenance_info.setVisibility(View.GONE);
                layout_car_info.setVisibility(View.GONE);
                break;
            //綁車不綁助理(提醒)
            case carBoundNoAssistant_notify:
                layout_car_state_banner.setVisibility(View.GONE);
                layout_nonOwnerBanner.setVisibility(View.GONE);
                break;
            //綁車不綁助理
            case carBoundNoAssistant:
                layout_car_state_banner.setVisibility(View.GONE);
                layout_nonOwnerBanner.setVisibility(View.GONE);
                layout_maintenance_info.setVisibility(View.GONE);
                break;
            //綁車綁助理
            case carAndAssistantBound:
                layout_car_state_banner.setVisibility(View.VISIBLE);
                layout_carBoundNoAssistant.setVisibility(View.GONE);
                layout_nonOwnerBanner.setVisibility(View.GONE);
            default:
                break;
        }


    }
}


