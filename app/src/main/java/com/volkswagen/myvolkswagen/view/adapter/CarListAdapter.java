package com.volkswagen.myvolkswagen.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;

import java.util.ArrayList;
import java.util.List;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CarListViewHolder> {
    private List<CarData> list = new ArrayList<>();
    private OnCarSelectListener onCarSelectListener;

    @NonNull
    @Override
    public CarListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_car_info_new, parent, false);
        return new CarListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CarListViewHolder holder, int position) {
        CarData data = list.get(position);
        String mainCarPlate = PreferenceManager.getInstance().getMainCarPlate();

        boolean isDongleBinding = data.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_USER_MAPPED);
        boolean hasDongle = data.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_USER_MAPPED) || data.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_VIN_MAPPED);

        Picasso.get().load(data.image).fit().centerInside().into(holder.imgCar);
        holder.tvType.setText(data.type);
        holder.tvPlate.setText(data.plate);
        holder.tvDongleStatus.setText(isDongleBinding ? holder.itemView.getContext().getString(R.string.msg_bind_vw_dongle) : holder.itemView.getContext().getString(R.string.msg_unbind_vw_dongle));
        holder.tvDongleStatus.setVisibility(hasDongle ? View.VISIBLE : View.INVISIBLE);
        holder.selectView.setVisibility(mainCarPlate.equals(data.plate) ? View.VISIBLE : View.GONE);
        holder.btnSetting.setOnClickListener(view -> {
            if (onCarSelectListener != null) {
                onCarSelectListener.onClickSetting(data);
            }
        });

        holder.itemView.setOnClickListener(view -> {
            if (onCarSelectListener != null) {
                onCarSelectListener.onSelect(data);
            }
        });
    }

    public void setDingleStateByCarPlate(String carPlate, String dongleStatus) {
        int position = 0;
        for (int i = 0; i < list.size(); i++) {
            CarData carData = list.get(i);
            if (carData.plate.equalsIgnoreCase(carPlate)) {
                carData.dongleStatus = dongleStatus;
                position = i;
                break;
            }
        }

        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<CarData> carDataList) {
        list.clear();
        list.addAll(carDataList);
        notifyDataSetChanged();
    }

    public void addOnSelectListener(OnCarSelectListener onCarSelectListener) {
        this.onCarSelectListener = onCarSelectListener;
    }

    public static class CarListViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgCar;
        private TextView tvType, tvPlate, tvDongleStatus;
        private ImageButton btnSetting;
        private View selectView;

        public CarListViewHolder(@NonNull View itemView) {
            super(itemView);

            imgCar = itemView.findViewById(R.id.view_holder_car_info_image);
            tvType = itemView.findViewById(R.id.view_holder_car_info_type);
            tvPlate = itemView.findViewById(R.id.view_holder_car_info_plate);
            tvDongleStatus = itemView.findViewById(R.id.view_holder_car_binding_status);
            btnSetting = itemView.findViewById(R.id.view_holder_btn_settings);
            selectView = itemView.findViewById(R.id.view_holder_select);
        }
    }

    public interface OnCarSelectListener {
        void onSelect(CarData carData);

        void onClickSetting(CarData carData);
    }
}
