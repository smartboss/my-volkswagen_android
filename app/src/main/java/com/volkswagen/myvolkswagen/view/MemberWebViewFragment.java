package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MemberWebViewFragment extends Fragment {
    public static final String DEEPLINK_PATH = "deeplink_path";

    private PermissionRequest webViewPermissionRequest;

    private ImageButton btnNotice;
    private ImageView btnClose;
    private WebView webView;

    private Uri mCallUri;

    public static MemberWebViewFragment newInstance(String path) {
        MemberWebViewFragment fragment = new MemberWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(DEEPLINK_PATH, path);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_webview, container, false);
        btnNotice = view.findViewById(R.id.btn_notice);
        btnClose = view.findViewById(R.id.btn_close);
        webView = view.findViewById(R.id.web_view);

        //notice button
        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
        btnNotice.setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);

        //webView
        final String rootUrl = Uri.parse(Config.VW_MEMBER_URL).buildUpon()
                .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                .build()
                .toString();

        String url = "";

        if (getArguments() != null && getArguments().containsKey(DEEPLINK_PATH)) {
            String deeplinkPath = getArguments().getString(DEEPLINK_PATH, "");
            if (deeplinkPath.equals(getString(R.string.app_deeplink_path_coupon))) {
                url = Uri.parse(Config.VW_MEMBER_COUPON_URL).buildUpon()
                        .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                        .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                        .build()
                        .toString();
            }
        }

        if (url.length() <= 0) {
            url = rootUrl;
        }

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.contains("member.html")) {
                    btnClose.setVisibility(View.GONE);
                } else {
                    btnClose.setVisibility(View.VISIBLE);
                }

                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")) {
                    mCallUri = Uri.parse(url);
                    call();
                    return true;
                }

                return false;
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }
        });

        webView.loadUrl(url);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.loadUrl(rootUrl);
            }
        });

        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
    }

    @Subscribe
    public void onNoticeUpdate(UpdateNoticeEvent event) {
        btnNotice.setImageResource(event.hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                if (permissions.length > 0 && permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                }
            } else {
                // Permission denied.
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void call() {
        startActivity(new Intent(Intent.ACTION_DIAL, mCallUri));
    }
}

