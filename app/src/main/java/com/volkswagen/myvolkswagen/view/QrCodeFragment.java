package com.volkswagen.myvolkswagen.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.QrCode;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QrCodeFragment extends DialogFragment {

    private View mContent;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContent = getActivity().getLayoutInflater().inflate(R.layout.fragment_qr_code, null);
        mContent.findViewById(R.id.qr_code_leave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(mContent);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_qr_code);
        dialog.getWindow().setDimAmount(0.85f);
        getQrCode();
        return dialog;
    }

    private void getQrCode() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        ApiService service = ApiClientManager.getClient().create(ApiService.class);
        service.getQrCode(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<QrCode>() {
            @Override
            public void onResponse(Call<QrCode> call, Response<QrCode> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(QrCodeFragment.this, "Failed to get qr code");
                    return;
                }

                QrCode data = response.body();
                if (!data.result) {
                    Logger.d(QrCodeFragment.this, "Failed to get qr code: code=" + data.errorCode);
                    return;
                }

                try {
                    String imageStr = data.image.substring(data.image.indexOf(',') + 1);
                    byte[] imageData = Base64.decode(imageStr, Base64.DEFAULT);
                    Bitmap qrCode = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                    ((ImageView) mContent.findViewById(R.id.qr_code_image)).setImageBitmap(qrCode);
                } catch (Exception e) {
                    Logger.d(QrCodeFragment.this, "Failed to decode image string: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<QrCode> call, Throwable t) {
                Logger.d(QrCodeFragment.this, "Failed to get qr code: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }
}
