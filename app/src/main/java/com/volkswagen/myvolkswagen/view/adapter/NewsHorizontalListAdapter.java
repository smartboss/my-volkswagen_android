package com.volkswagen.myvolkswagen.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NewsHorizontalListAdapter extends RecyclerView.Adapter<NewsHorizontalListAdapter.NewsHorizontalListViewHolder> {
    private List<NewsData> newsList = new ArrayList<>();
    private NewsListAdapter.OnNewsClickListener onNewsClickListener;

    @NonNull
    @Override
    public NewsHorizontalListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_news_content, parent, false);
        return new NewsHorizontalListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHorizontalListViewHolder holder, int position) {
        NewsData data = newsList.get(position);

        holder.tvTitle.setText(data.title);
        holder.tvDate.setText(Utils.formatVWDate(data.published_at));
        if (!data.thumb_cover_image.isEmpty()) {
            Picasso.get().load(data.thumb_cover_image).fit().centerInside().into(holder.imgNews);
        }

        holder.itemView.setOnClickListener(view -> {
            if (onNewsClickListener != null) {
                onNewsClickListener.onClickNews(data);
            }
        });
    }

    public void addNewsClickListener(NewsListAdapter.OnNewsClickListener onNewsClickListener) {
        this.onNewsClickListener = onNewsClickListener;
    }

    public void setData(List<NewsData> list) {
        newsList.clear();
        newsList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Math.min(newsList.size(), 6);
    }

    static class NewsHorizontalListViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvDate;
        private ImageView imgNews;

        public NewsHorizontalListViewHolder(View itemView) {
            super(itemView);
            imgNews = itemView.findViewById(R.id.view_holder_img_news);
            tvTitle = itemView.findViewById(R.id.view_holder_news_title);
            tvDate = itemView.findViewById(R.id.view_holder_news_date);
        }
    }
}
