package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.VwFirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

public class AddCarStep3 extends Fragment implements View.OnClickListener{

    private Button add_car_step3_btn;
    private Button add_car_step3_btn_skip;
    private AddCarActivity baseActivity;
    private ImageButton btn_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycar_add_car_step3, container, false);
        initView(view);
        return view;
    }
    private void initView(View v) {
        baseActivity = (AddCarActivity) getActivity();

        btn_back = v.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        add_car_step3_btn = v.findViewById(R.id.add_car_step3_btn_next);
        add_car_step3_btn.setOnClickListener(this);
        add_car_step3_btn_skip = v.findViewById(R.id.add_car_step3_btn_skip);
        add_car_step3_btn_skip.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step3_btn_next:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep4()).commitAllowingStateLoss();
                break;
            case  R.id.add_car_step3_btn_skip:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep5()).commitAllowingStateLoss();
                break;
            case R.id.btn_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep2()).commitAllowingStateLoss();
                break;
        }
    }
}
