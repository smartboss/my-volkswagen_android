package com.volkswagen.myvolkswagen.view.addcar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.BindingLineFragment;
import com.volkswagen.myvolkswagen.view.MainFragment;
import com.volkswagen.myvolkswagen.view.MyCarActivity;
import com.volkswagen.myvolkswagen.view.bottomsheet.AddCarBottomSheetDialogFragment;

public class AddCarBaseFragment extends Fragment implements AddCarControlListener {
    private AddCarBottomSheetDialogFragment dialogFragment;
    private AddCarFragment addCarFragment;

    public static AddCarBaseFragment getInstance() {
        return new AddCarBaseFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_car_base, container, false);

        addCarFragment = AddCarFragment.getInstance(dialogFragment == null);
        getChildFragmentManager().beginTransaction().replace(R.id.layout_child_frame, addCarFragment, "").commitAllowingStateLoss();
        return view;
    }

    public void setRootBottomSheetDialog(AddCarBottomSheetDialogFragment dialogFragment) {
        this.dialogFragment = dialogFragment;
    }

    @Override
    public void onDestroyView() {
        dialogFragment = null;
        addCarFragment = null;
        super.onDestroyView();
    }

    public void goBack() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.layout_child_frame);
        if (fragment instanceof AddCarTypeFragment) {
            getChildFragmentManager().popBackStack();
        } else {
            dialogFragment.dismiss();
        }
    }

    @Override
    public void onNextClick() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.layout_child_frame);
        if (fragment instanceof AddCarControlListener) {
            ((AddCarControlListener) fragment).onNextClick();
        }
    }

    public void finishAddCar(String plateNo) {
        if (dialogFragment != null) {
            dialogFragment.dismiss();
            if (getActivity() != null) {
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                //add car from Main
                if (fragment instanceof MainFragment) {
                    ((MainFragment) fragment).startCallStep1Api();
                }

                //add car from MyCarActivity
                if (getActivity() instanceof MyCarActivity) {
                    ((MyCarActivity) getActivity()).getCarList();
                }
            }
        } else {
            //add car from Login
            callDongleBindingApi(plateNo);
        }
    }

    private void callDongleBindingApi(String plateNo) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().getDongleBinding(plateNo, new Repository.OnApiCallback<DongleBindingCarData>() {
            @Override
            public void onSuccess(DongleBindingCarData data) {
                PreferenceManager.getInstance().saveMainCar(data.firstName + data.lastName, plateNo, data.vin);

                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (getActivity() != null) {
                    if (data.state.equalsIgnoreCase(CarData.DONGLE_STATUS_VIN_MAPPED)) {
                        getActivity()
                                .getSupportFragmentManager()
                                .beginTransaction().replace(R.id.layout_frame, DongleBindingFragment.getInstance(plateNo, data.model), "")
                                .addToBackStack("")
                                .commitAllowingStateLoss();
                    } else {
                        getActivity()
                                .getSupportFragmentManager()
                                .beginTransaction().replace(R.id.layout_frame, NoDongleFragment.getInstance())
                                .addToBackStack("")
                                .commitAllowingStateLoss();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction().replace(R.id.layout_frame, NoDongleFragment.getInstance())
                            .addToBackStack("")
                            .commitAllowingStateLoss();
                }
            }
        });
    }
}
