package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.AlertUtils;

public class HelpActivity extends BaseActivity implements View.OnClickListener {

    private final int CALL_POLICE = 0;
    private final int CALL_EMERGENCY = 1;

    private int mCallType = CALL_POLICE;

    public static void goHelp(final Activity activity) {
        final Intent intent = new Intent(activity, HelpActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Spannable spanText;
        ClickableSpan clickableSpan;
        SpannableStringBuilder strBuilder;

        spanText = new SpannableString(getString(R.string.tools_call_number));
        clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View view) {
                mCallType = CALL_EMERGENCY;
                callForHelp();
            }
        };
        spanText.setSpan(clickableSpan, 0, spanText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pinky_red)), 0, spanText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(new AbsoluteSizeSpan(16, true), 0, spanText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        strBuilder = new SpannableStringBuilder(getString(R.string.help_7_dail));
        strBuilder.append(spanText);
        strBuilder.append(getString(R.string.help_7_tow));
        TextView text_help_7_detail = findViewById(R.id.help_7_detail);

        text_help_7_detail.setMovementMethod(LinkMovementMethod.getInstance());

        text_help_7_detail.setText(strBuilder);
        text_help_7_detail.setHighlightColor(Color.TRANSPARENT);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.help_back:
                onBackPressed();
                break;

            case R.id.help_6_police:
                mCallType = CALL_POLICE;
                callForHelp();
                break;
        }
    }

    private void callForHelp() {
        int title_resid;
        final int dial_num_resid;

        if (mCallType == CALL_POLICE) {
            title_resid = R.string.help_6_call;
            dial_num_resid = R.string.help_6_number;
        } else {
            title_resid = R.string.tools_call;
            dial_num_resid = R.string.tools_call_number;
        }

        AlertUtils.showAlert(
                this, getString(title_resid),
                getString(dial_num_resid),
                getString(R.string.forget_it_button),
                getString(R.string.confirm_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(dial_num_resid)));
                            startActivity(intent);
                        }
                    }
                });
    }
}
