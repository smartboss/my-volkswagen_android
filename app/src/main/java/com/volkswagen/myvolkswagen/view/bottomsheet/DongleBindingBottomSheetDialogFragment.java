package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.view.MainNewActivity;
import com.volkswagen.myvolkswagen.view.MyCarActivity;
import com.volkswagen.myvolkswagen.view.addcar.DongleBindingFragment;

public class DongleBindingBottomSheetDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener, DongleBindingFragment.OnDongleBindingListener {
    public static final String CONST_PLATE_NO = "plateNo";
    public static final String CONST_MODEL = "model";

    private DongleBindingFragment dongleBindingFragment;

    public static DongleBindingBottomSheetDialogFragment getInstance(String plateNo, String model) {
        DongleBindingBottomSheetDialogFragment dialogFragment = new DongleBindingBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CONST_PLATE_NO, plateNo);
        bundle.putString(CONST_MODEL, model);
        dialogFragment.setArguments(bundle);

        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BaseBottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheetdialog_dongle_binding, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        ImageView imgBack = view.findViewById(R.id.img_back);
        imgBack.setOnClickListener(this);

        ImageView imgClose = view.findViewById(R.id.img_close);
        imgClose.setOnClickListener(this);

        Button button = view.findViewById(R.id.btn_confirm);
        button.setOnClickListener(this);

        String plateNo = "";
        String model = "";
        if (getArguments() != null && getArguments().containsKey(CONST_PLATE_NO)) {
            plateNo = getArguments().getString(CONST_PLATE_NO, "");
        }

        if (getArguments() != null && getArguments().containsKey(CONST_MODEL)) {
            model = getArguments().getString(CONST_MODEL, "");
        }

        dongleBindingFragment = DongleBindingFragment.getInstance(plateNo, model);
        dongleBindingFragment.setDongleBindingListener(this);

        getChildFragmentManager().beginTransaction().replace(R.id.layout_frame, dongleBindingFragment, "").commitAllowingStateLoss();
    }

    @Override
    public void onStart() {
        super.onStart();
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    @Override
    public void onDestroyView() {
        dongleBindingFragment = null;
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
            case R.id.img_close:
                dismiss();
                break;
            case R.id.btn_confirm:
                if (dongleBindingFragment != null) {
                    dongleBindingFragment.onNextClick();
                }
                break;
        }
    }

    @Override
    public void onDongleBinding() {
        if (getActivity() != null && getActivity() instanceof MyCarActivity) {
            dismiss();
            ((MyCarActivity) getActivity()).getCarList();
        }

        if (getActivity() != null && getActivity() instanceof MainNewActivity) {
            dismiss();
            ((MainNewActivity) getActivity()).getCarDongleBinding();
        }
    }
}