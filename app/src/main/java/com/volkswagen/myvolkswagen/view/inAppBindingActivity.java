package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.GetCarModelData;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

public class inAppBindingActivity extends BaseActivity implements View.OnClickListener{

    private Button carBingingBtn;
    private ImageButton btn_back, btn_close;
    final String PLATE_FORMAT = "%1$s-%2$s";
    private String mOwner;
    private AddCarData mData;
    private inAppAddCarActivity baseActivity;
    private EditText ownerEdit;
    private EditText phoneEdit;
    private EditText emailEdit;
    private View view;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_inapp_binding);
        carBingingBtn = findViewById(R.id.add_car_step1_btn_next);
        carBingingBtn.setOnClickListener(this);

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(this);

        ownerEdit = findViewById(R.id.et_profile_nickname_edit);
        ownerEdit.setOnClickListener(this);
        phoneEdit = findViewById(R.id.et_profile_phone_edit);
        phoneEdit.setOnClickListener(this);
        emailEdit = findViewById(R.id.et_profile_mail_edit);
        emailEdit.setOnClickListener(this);

//        mOwner = getActivity().getIntent().getStringExtra(AddCarActivity.KEY_NICKNAME);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step1_btn_next:
                validateData(v);
                break;
            case R.id.btn_back:
            case R.id.btn_close:
                finish();
                break;
        }
    }
    private void validateData(View v) {
        String owner = ownerEdit.getText().toString();
        String phone = phoneEdit.getText().toString();
        String email = emailEdit.getText().toString();

        if (TextUtils.isEmpty(owner) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email) ) {
            AlertUtils.showAlert(this, null, getString(R.string.add_car_step1_msg_fill_up), null, getString(R.string.ok_button), null);
        } else {
            String plate = String.format(PLATE_FORMAT, phone, email);
            mData = baseActivity.GetCarData();
            mData.name = owner;
            mData.plate = plate;
//            checkCarExist();
        }
    }


    public final static void goBindingCar(final Activity activity) {
        final Intent intent = new Intent(activity, inAppBindingActivity.class);
        activity.startActivity(intent);
    }


}
