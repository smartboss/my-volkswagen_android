package com.volkswagen.myvolkswagen.view.addcar;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_ADD_CAR_TYPE;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.view.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class AddCarTypeFragment extends Fragment implements AddCarControlListener {
    private static final String NAME = "name";
    private static final String PLATE = "plate";
    private static final String VIN = "vin";
    private static final String CAR_TYPE_ID = "car_type_id";
    private static final String CAR_TYPE_NAME = "car_type_name";

    private String name = "";
    private String plate = "";
    private String vin = "";
    private int carTypeId = 0;
    private String carTypeName = "";

    private ImageView imgCar;
    private TextView tvCarType, tvCarPlate;
    private Spinner spinnerCarType;
    private ArrayAdapter<String> adapter;
    private List<CarModel> carModelList = new ArrayList<>();

    public static AddCarTypeFragment getInstance(String name, String plate, String vin, int carTypeId, String carTypeName) {
        AddCarTypeFragment fragment = new AddCarTypeFragment();

        Bundle bundle = new Bundle();
        bundle.putString(NAME, name);
        bundle.putString(PLATE, plate);
        bundle.putString(VIN, vin);
        bundle.putInt(CAR_TYPE_ID, carTypeId);
        bundle.putString(CAR_TYPE_NAME, carTypeName);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_car_type, container, false);

        if (getArguments() != null) {
            if (getArguments().containsKey(NAME)) {
                name = getArguments().getString(NAME, "");
            }
            if (getArguments().containsKey(PLATE)) {
                plate = getArguments().getString(PLATE, "");
            }
            if (getArguments().containsKey(VIN)) {
                vin = getArguments().getString(VIN, "");
            }
            if (getArguments().containsKey(CAR_TYPE_ID)) {
                carTypeId = getArguments().getInt(CAR_TYPE_ID, 0);
            }
            if (getArguments().containsKey(CAR_TYPE_NAME)) {
                carTypeName = getArguments().getString(CAR_TYPE_NAME, "");
            }
        }

        initView(view);
        getCarModelData();
        return view;
    }

    private void initView(View view) {
        imgCar = view.findViewById(R.id.img_car);

        tvCarType = view.findViewById(R.id.tv_car_type);
        tvCarType.setText(carTypeName);
        tvCarPlate = view.findViewById(R.id.tv_car_plate);
        tvCarPlate.setText(plate);

        spinnerCarType = view.findViewById(R.id.spinner_car_type);
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item) {
            @NonNull
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setPadding(20, 20, 20, 20);
                return textView;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCarType.setAdapter(adapter);
        spinnerCarType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                CarModel carModel = carModelList.get(position);

                if (carModel != null) {
                    Picasso.get().load(carModel.image).fit().centerCrop().into(imgCar);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getCarModelData() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().getCarModelData(carTypeId, new Repository.OnApiCallback<CarModelData>() {
            @Override
            public void onSuccess(CarModelData carModelData) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                adapter.clear();
                carModelList.clear();
                carModelList.addAll(carModelData.models);

                for (CarModel carModel : carModelData.models) {
                    adapter.add(carModel.name);
                }

                spinnerCarType.setAdapter(adapter);
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_ADD_CAR_TYPE);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }

    @Override
    public void onNextClick() {
        int carModelId = carModelList.get(spinnerCarType.getSelectedItemPosition()).id;
        postAddCar(carModelId);
    }

    private void postAddCar(int carModelId) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().postAddCar(name, plate, vin, carTypeId, carModelId, new Repository.OnApiCallback<Response>() {
            @Override
            public void onSuccess(Response response) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (response.result) {
                    if (getParentFragment() instanceof AddCarBaseFragment) {
                        ((AddCarBaseFragment) getParentFragment()).finishAddCar(plate);
                    }
                } else {
                    switch (response.errorCode) {
                        case 3001:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3001), Toast.LENGTH_SHORT).show();
                            break;
                        case 3003:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3003), Toast.LENGTH_SHORT).show();
                            break;
                        case 3004:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3004), Toast.LENGTH_SHORT).show();
                            break;
                        case 3005:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3005), Toast.LENGTH_SHORT).show();
                            break;
                        case 3008:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3008), Toast.LENGTH_SHORT).show();
                            break;
                        case 3009:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3009), Toast.LENGTH_SHORT).show();
                            break;
                        case 3010:
                            Toast.makeText(getContext(), getString(R.string.addcar_error_code_3010), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
