package com.volkswagen.myvolkswagen.view;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.RedeemData;

public class LineRedeemBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String TAG = LineRedeemBottomSheet.class.getSimpleName();

    public static final int TYPE_EXCHANGE = 1000;
    public static final int TYPE_COPY_CODE = 1001;

    private LineRedeemPresenter presenter;
    private BottomSheetDialog mDialog;
    private ImageButton btnCopy, btnGoLine;

    private RedeemData redeemData = new RedeemData();
    private String lineCode = "";
    private boolean isCopied = false;

    @Override
    public int getTheme() {
        return R.style.AppTheme_BottomSheetDialog;
    }

    public static LineRedeemBottomSheet newInstance(int type) {
        LineRedeemBottomSheet fragment = new LineRedeemBottomSheet();
        Bundle bundle = new Bundle();
        bundle.putInt(TAG, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialog = (BottomSheetDialog) getDialog();
        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(mDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet));
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_HIDDEN);
                                break;
                            case BottomSheetBehavior.STATE_HIDDEN:
                                mDialog.cancel();
                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });

        final View v = inflater.inflate(R.layout.bottom_sheet_line_redeem, container, false);

        if (getArguments() != null) {
            int type = getArguments().getInt(TAG, TYPE_EXCHANGE);
            setView(v, type);
        }
        return v;
    }

    private void setView(View view, int type) {
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvTaskName = view.findViewById(R.id.tv_task_name);
        TextView tvRedeemName = view.findViewById(R.id.tv_redeem_name);
        TextView tvRedeemPoint = view.findViewById(R.id.tv_redeem_point);
        TextView tvRedeemCode = view.findViewById(R.id.tv_redeem_code);
        ImageButton btnExchange = view.findViewById(R.id.btn_exchange);
        btnExchange.setOnClickListener(this);
        btnCopy = view.findViewById(R.id.btn_copy_code);
        btnCopy.setOnClickListener(this);
        btnGoLine = view.findViewById(R.id.btn_open_line);
        btnGoLine.setOnClickListener(this);

        Group groupExchange = view.findViewById(R.id.group_exchange);
        Group groupRedeem = view.findViewById(R.id.group_redeem);

        switch (type) {
            case TYPE_EXCHANGE:
                tvTitle.setText(R.string.line_title_exchange);
                tvTaskName.setText(redeemData.name);
                groupExchange.setVisibility(View.VISIBLE);
                groupRedeem.setVisibility(View.GONE);
                break;
            case TYPE_COPY_CODE:
                tvTitle.setText(R.string.line_title_get_point);
                tvRedeemName.setText(redeemData.name);
                tvRedeemPoint.setText(redeemData.point);
                tvRedeemCode.setText(lineCode);
                groupExchange.setVisibility(View.GONE);
                groupRedeem.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_exchange:
                presenter.onExchange(redeemData);
                break;
            case R.id.btn_copy_code:
                isCopied = true;
                presenter.onCopy(redeemData, lineCode);
                btnCopy.setImageResource(R.drawable.btn_copy_code);
                btnGoLine.setImageResource(R.drawable.btn_open_line);
                break;
            case R.id.btn_open_line:
                if (isCopied) {
                    presenter.goLine();
                }
                break;
        }
    }

    public void addPresenter(LineRedeemPresenter presenter) {
        this.presenter = presenter;
    }

    public void setRedeemData(RedeemData redeemData) {
        this.redeemData = redeemData;
    }

    public void setRedeemData(RedeemData redeemData, String lineCode) {
        this.redeemData = redeemData;
        this.lineCode = lineCode;
    }

    interface LineRedeemPresenter {
        void onExchange(RedeemData redeemData);

        void onCopy(RedeemData redeemData, String code);

        void goLine();
    }
}
