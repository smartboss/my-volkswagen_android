package com.volkswagen.myvolkswagen.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.volkswagen.myvolkswagen.R;

import androidx.viewpager.widget.PagerAdapter;

public class OnBoardingAdapter extends PagerAdapter {
    private Context context;
    private int[] images = {
            R.drawable.onboarding_screens1,
            R.drawable.onboarding_screens2,
            R.drawable.onboarding_screens3,
            R.drawable.onboarding_screens4,
            R.drawable.onboarding_screens5
    };

    public OnBoardingAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.onboarding_slide, null);
        ImageView slideImage = view.findViewById(R.id.slide_img);
        slideImage.setImageResource(images[position]);

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}