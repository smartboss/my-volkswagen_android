package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.CarData;

public class CarManagerBottomSheetDialog extends BottomSheetDialog {
    private CarData carData;
    private final CarManagerListener carManagerListener;

    public CarManagerBottomSheetDialog(@NonNull Context context, CarData carData, CarManagerListener carManagerListener) {
        super(context, R.style.BaseBottomSheetDialog);
        this.carData = carData;
        this.carManagerListener = carManagerListener;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_mycar_setting, null);
        setContentView(view);

        boolean isDongleBinding = carData.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_USER_MAPPED);
        boolean hasDongle = carData.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_USER_MAPPED) || carData.dongleStatus.equalsIgnoreCase(CarData.DONGLE_STATUS_VIN_MAPPED);

        TextView tvBindingAssistant = view.findViewById(R.id.btn_mycar_binding_assistant);
        tvBindingAssistant.setText(isDongleBinding ? getContext().getString(R.string.msg_unbinding_check_title) : getContext().getString(R.string.msg_binding_check_title));
        tvBindingAssistant.setVisibility(hasDongle ? View.VISIBLE : View.GONE);

        TextView tvRemoveCar = view.findViewById(R.id.btn_mycar_car_remove);
        TextView tvCarIdentification = view.findViewById(R.id.btn_mycar_identification);

        tvBindingAssistant.setOnClickListener(view1 -> {
            if (carManagerListener != null) {
                if (isDongleBinding) {
                    carManagerListener.onCarUnBindingAssistant(carData);
                } else {
                    carManagerListener.onCarBindingAssistant(carData);
                }
            }

            dismiss();
        });

        tvRemoveCar.setOnClickListener(view1 -> {
            if (carManagerListener != null) {
                carManagerListener.onCarRemove(carData);
            }

            dismiss();
        });

        tvCarIdentification.setOnClickListener(view1 -> {
            if (carManagerListener != null) {
                carManagerListener.onCarIdentification(carData);
            }

            dismiss();
        });
    }

    public interface CarManagerListener {
        void onCarBindingAssistant(CarData carData);

        void onCarUnBindingAssistant(CarData carData);

        void onCarRemove(CarData carData);

        void onCarIdentification(CarData carData);
    }
}
