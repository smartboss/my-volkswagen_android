package com.volkswagen.myvolkswagen.view.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.LoginActivity;
import com.volkswagen.myvolkswagen.view.alert.VWBaseAlertDialogFragment;

import java.util.List;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {
    public static void goSettings(final Activity activity) {
        Intent intent = new Intent(activity, SettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_back);
        TextView tvTitle = findViewById(R.id.tv_view_title);
        tvTitle.setText(R.string.settings_title);

        ImageButton btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, SettingFragment.newInstance(), null).commitAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
        }
    }

    public void deleteUserAccount() {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.settings_delete_id), getString(R.string.settings_delete_id_message), getString(R.string.confirm_button), getString(R.string.cancel_button));
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                showLoading();

                //need remove all car dongle first
                getCarList();
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });
    }

    public void logout() {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.settings_logout), getString(R.string.settings_msg_logout), getString(R.string.confirm_button), getString(R.string.cancel_button));
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                Repository.getInstance().logout(SettingsActivity.this, new Repository.OnApiCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        LoginActivity.goLogin(SettingsActivity.this);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        LoginActivity.goLogin(SettingsActivity.this);
                    }
                });
            }

            @Override
            public void onDialogNegativeClick() {

            }
        });
    }

    private void getCarList() {
        Repository.getInstance().getMyCarDataList(new Repository.OnApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> carDataList) {
                if (!carDataList.isEmpty()) {
                    for (CarData carData : carDataList) {
                        Repository.getInstance().removeCarDongle(carData.plate, null);
                    }
                }

                unBindLine();
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    public void unBindLine() {
        Repository.getInstance().postUnbindLine(new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                deleteAccount();
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }

    private void deleteAccount() {
        Repository.getInstance().postDeleteAccount(new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                hideLoading();
                Toast.makeText(SettingsActivity.this, getString(R.string.setting_delete_account_finish), Toast.LENGTH_SHORT).show();
                LoginActivity.goLogin(SettingsActivity.this);
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
            }
        });
    }
}
