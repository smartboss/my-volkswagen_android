package com.volkswagen.myvolkswagen.view.notice;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.WebViewActivity;
import com.volkswagen.myvolkswagen.view.adapter.ImageViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NoticeDetailFragment extends Fragment {
    private static NoticeDetailFragment fragment;
    private static String TYPE = "type";
    private static String KIND = "kind";
    private static String LIST = "list";
    private static String TITLE = "title";
    private static String CONTENT = "content";
    private static String DATE = "date";
    private static String ACCOUNT_ID = "accountId";

    private String title = "";
    private String content = "";
    private String date = "";
    private String accountId = "";
    private int type = NoticeActivity.TYPE_NOTICE;
    private int kind = -1;
    private List<String> imageList = new ArrayList<>();
    private boolean hasImage = false;

    public static NoticeDetailFragment newInstance(int type, int kind, ArrayList<String> imageList, String title, String content, String date, String accountId) {
        if (fragment == null) {
            fragment = new NoticeDetailFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        bundle.putInt(KIND, kind);
        bundle.putStringArrayList(LIST, imageList);
        bundle.putString(TITLE, title);
        bundle.putString(CONTENT, content);
        bundle.putString(DATE, date);
        bundle.putString(ACCOUNT_ID, accountId);
        fragment.setArguments(bundle);

        return fragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            if (getArguments().containsKey(TYPE)) {
                type = getArguments().getInt(TYPE, NoticeActivity.TYPE_NOTICE);
            }

            if (getArguments().containsKey(KIND)) {
                kind = getArguments().getInt(KIND, -1);
            }

            if (getArguments().containsKey(LIST)) {
                imageList = getArguments().getStringArrayList(LIST);
            }

            if (getArguments().containsKey(TITLE)) {
                title = getArguments().getString(TITLE, "");
            }

            if (getArguments().containsKey(CONTENT)) {
                content = getArguments().getString(CONTENT, "");
            }

            if (getArguments().containsKey(DATE)) {
                date = getArguments().getString(DATE, "");
            }

            if (getArguments().containsKey(ACCOUNT_ID)) {
                accountId = getArguments().getString(ACCOUNT_ID, "");
            }
        }

        hasImage = imageList != null && imageList.size() > 0 && !imageList.get(0).contains("empty");
        View view = inflater.inflate(hasImage ? R.layout.fragment_notice_detail : R.layout.fragment_notice_detail_with_no_image, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        switch (type) {
            case NoticeActivity.TYPE_NOTICE:
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE_DETAIL, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
                break;
            case NoticeActivity.TYPE_NEWS:
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NEWS_DETAIL, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
                break;
        }

        TextView tvDate = view.findViewById(R.id.tv_date);
        tvDate.setText(Utils.formatVWDate(date));

        TextView tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setText(title);

        TextView tvContent = view.findViewById(R.id.tv_content);

        if (kind == 34 || kind == 35) {
            Pattern pattern = Pattern.compile("https?://\\S+");
            Matcher matcher = pattern.matcher(content);

            if (matcher.find()) {
                String url = matcher.group();
                String newUrl = url + "?AccountId=" + accountId;

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setUnderlineText(false);    // this remove the underline
                    }

                    @Override
                    public void onClick(View view) {
                        WebViewActivity.goVWSurveyCake(getActivity(), newUrl, true, false);
                    }
                };

                int urlStartIndex = content.indexOf(url);
                int urlEndIndex = urlStartIndex + url.length();

                Spannable spanText = new SpannableString(content);
                spanText.setSpan(clickableSpan, urlStartIndex, urlEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pinky_red)), urlStartIndex, urlEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableStringBuilder strBuilder = new SpannableStringBuilder(spanText);

                tvContent.setText(strBuilder);
                tvContent.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                tvContent.setText(content);
            }
        } else {
            String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
            Pattern pattern = null;

            if (content.charAt(content.length() - 1) == '=') {
                pattern = Pattern.compile("(" + appScheme + "://[^?]+)(?:\\?\\w+=+)?");
            } else {
                pattern = Pattern.compile("(" + appScheme + "://[^?]+)(?:\\?\\w+=\\w+(?:&\\w+=\\w+)*)?");
            }

            Matcher matcher = pattern.matcher(content);

            if (matcher.find()) {
                String deeplink = matcher.group();
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setUnderlineText(false);    // this remove the underline
                    }

                    @Override
                    public void onClick(View view) {
                        if (getActivity() != null && getActivity() instanceof BaseActivity) {
                            ((BaseActivity) getActivity()).handleDeeplink(deeplink);
                        }
                    }
                };

                int urlStartIndex = content.indexOf(deeplink);
                int urlEndIndex = urlStartIndex + deeplink.length();

                Spannable spanText = new SpannableString(content);
                spanText.setSpan(clickableSpan, urlStartIndex, urlEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pinky_red)), urlStartIndex, urlEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableStringBuilder strBuilder = new SpannableStringBuilder(spanText);

                tvContent.setText(strBuilder);
                tvContent.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                tvContent.setText(content);
            }
        }

        if (hasImage) {
            ViewPager viewPager = view.findViewById(R.id.view_pager_image);
            ImageViewPagerAdapter adapter = new ImageViewPagerAdapter(getContext());
            viewPager.setAdapter(adapter);
            adapter.setList(imageList);

            TabLayout indicator = view.findViewById(R.id.layout_indicator);
            indicator.setupWithViewPager(viewPager);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof NoticeActivity) {
            switch (type) {
                case NoticeActivity.TYPE_NOTICE:
                    ((NoticeActivity) getActivity()).setTitle(getString(R.string.notice_tab_owner_info));
                    break;
                case NoticeActivity.TYPE_NEWS:
                    ((NoticeActivity) getActivity()).setTitle(getString(R.string.notice_tab_news_info));
                    break;
            }
        }
    }

    public int getCurrentType() {
        return type;
    }
}
