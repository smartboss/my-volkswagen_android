package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.utils.Utils;

public class LoginFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private Button btnNext, btnLogin;
    private TextView tvPhoneError;
    private EditText etMobile;
    private Spinner spinner;
    private ImageButton btnBack;

    private String phone = "";
    private String countryCode = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tvPhoneError = view.findViewById(R.id.tv_phone_error);

        btnNext = view.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
        btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnLogin = view.findViewById(R.id.btn_login_by_id);
        btnLogin.setOnClickListener(this);

        etMobile = view.findViewById(R.id.et_phone);
        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phone = etMobile.getText().toString();
                if (Utils.isValidMobile(phone)) {
                    btnNext.setEnabled(true);
                    btnNext.getBackground().setTint(getResources().getColor(R.color.lightblue400));
                    btnNext.setTextColor(getResources().getColor(R.color.white2));
                } else {
                    if (btnNext.isEnabled()) {
                        btnNext.setEnabled(false);
                        btnNext.getBackground().setTint(getResources().getColor(R.color.grey300));
                        btnNext.setTextColor(getResources().getColor(R.color.grey400));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        spinner = view.findViewById(R.id.spinner_country);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(getContext(), android.R.layout.simple_spinner_item, getResources().getTextArray(R.array.country_code_array)) {
            @NonNull
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                textView.setPadding(20, 20, 20, 20);
                return textView;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(68, false);
        countryCode = adapter.getItem(68).toString();
        spinner.setOnItemSelectedListener(this);
    }

    private void getOtpCode(String countryCode, String phone) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().genOtpCode(countryCode, phone, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (getActivity() != null) {
                    etMobile.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
                    tvPhoneError.setVisibility(View.GONE);
                    tvPhoneError.setText("");

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, VerifyFragment.newInstance(countryCode, phone)).addToBackStack(null).commitAllowingStateLoss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                String message = t.getMessage();
                etMobile.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
                tvPhoneError.setVisibility(View.VISIBLE);
                tvPhoneError.setText(message != null ? message : getString(R.string.unknown_error));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                phone = etMobile.getText().toString();
                getOtpCode(countryCode, phone);
                break;
            case R.id.btn_login_by_id:
                if (getActivity() != null && getActivity() instanceof LoginActivity) {
                    ((LoginActivity) getActivity()).goVWLogin();
                }
                break;
            case R.id.btn_back:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        countryCode = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
