package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.Version;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.notice.NoticeActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class MainNewActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = MainNewActivity.class.getSimpleName();

    public static final String KEY_TAB = "tab";
    public static final int TAB_HOME = 0;
    public static final int TAB_MAINTENANCE = 1;
    public static final int TAB_ASSISTANT = 2;
    public static final int TAB_OPEN_HUB = 3;
    public static final int TAB_STORE = 4;
    public static final int TAB_MY = 5;

    private int buttonTabKey = TAB_HOME;
    private TextView tvNickName, tvCarPlate;
    private ImageView imgNotice;

    public static void goMain(final Activity activity) {
        final Intent intent = new Intent(activity, MainNewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void goMain(final Activity activity, int tab) {
        final Intent intent = new Intent(activity, MainNewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(KEY_TAB, tab);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);
        Repository.getInstance().requestFirebaseToken();
        checkAppVersion();
    }

    //for MyCar change car, refresh view
    @Override
    protected void onResume() {
        super.onResume();

        switch (buttonTabKey) {
            case TAB_MAINTENANCE:
                selectTab(R.id.tab_maintain);
                break;
            case TAB_ASSISTANT:
                selectTab(R.id.tab_assistant);
            case TAB_STORE:
            case TAB_OPEN_HUB:
            case TAB_MY:
            case TAB_HOME:
            default:
                break;
        }
    }

    private void initView() {
        tvNickName = findViewById(R.id.tv_nickname);
        tvCarPlate = findViewById(R.id.tv_car_plate);
        tvCarPlate.setOnClickListener(this);
        imgNotice = findViewById(R.id.img_notice);
        imgNotice.setOnClickListener(this);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(KEY_TAB)) {
            int key = getIntent().getExtras().getInt(KEY_TAB, TAB_HOME);
            selectTabByKey(key);
        } else {
            selectTab(R.id.tab_home);
        }
    }

    private void checkAppVersion() {
        Repository.getInstance().getVersion(new Repository.OnApiCallback<Version>() {
            @Override
            public void onSuccess(Version version) {
                if (BuildConfig.VERSION_NAME.compareTo(version.version) < 0) {
                    showUpdateAlertDialog(version);
                } else {
                    doNormalFlow();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private void showUpdateAlertDialog(Version version) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_update_available));
        builder.setMessage(version.message);
        builder.setPositiveButton(getString(R.string.update_button), (dialogInterface, i) -> {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(version.url)));
            finish();
        });
        if (!version.isUpdate) {
            builder.setNegativeButton(getString(R.string.cancel_button), (dialogInterface, i) -> doNormalFlow());
        }

        builder.setCancelable(false);
        builder.create().show();
    }

    private void doNormalFlow() {
        if (PreferenceManager.getInstance().getUserToken() == null) {
            LoginActivity.goLogin(this);
            finish();
            return;
        }

        initView();
        autoLogin();
    }

    private void autoLogin() {
        Repository.getInstance().autoLogin(new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (getIntent() != null && getIntent().getData() != null) {
                    handleDeeplink(getIntent().getData().toString());
                }
                EventBus.getDefault().postSticky(new TokenReadyEvent());
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    private void selectTabByKey(int key) {
        switch (key) {
            case TAB_MAINTENANCE:
                selectTab(R.id.tab_maintain);
                break;
            case TAB_ASSISTANT:
                selectTab(R.id.tab_assistant);
                break;
            case TAB_OPEN_HUB:
                selectTab(R.id.tab_open_hub);
                break;
            case TAB_STORE:
                selectTab(R.id.tab_store);
                break;
            case TAB_MY:
                selectTab(R.id.tab_vw);
                break;
            case TAB_HOME:
            default:
                selectTab(R.id.tab_home);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        if (!view.isSelected()) {
            selectTab(view.getId());

            switch (view.getId()) {
                case R.id.tv_car_plate:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "我的車庫", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.img_notice:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_HEADER_MENU, "首頁小鈴鐺", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.tab_home:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_FOOTER_MENU, "首頁", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.tab_maintain:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_FOOTER_MENU, "保養維修", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.tab_assistant:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_FOOTER_MENU, "行車助理", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.tab_open_hub:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_FOOTER_MENU, "充電地圖", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
                case R.id.tab_store:
                    break;
                case R.id.tab_vw:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_FOOTER_MENU, "福斯人", null, EventLog.EVENT_CATEGORY_BUTTON);
                    break;
            }
        }
    }

    private void selectTab(int id) {
        setTabView(id);

        String url = "";
        switch (id) {
            case R.id.tv_car_plate:
                goMyCar();
                break;
            case R.id.img_notice:
                goNotice(NoticeActivity.TYPE_NOTICE);
                break;
            case R.id.tab_home:
                buttonTabKey = TAB_HOME;
                showNameAndNotice();

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, MainFragment.newInstance(), null).commitAllowingStateLoss();
                break;
            case R.id.tab_maintain:
                buttonTabKey = TAB_MAINTENANCE;
                showNameAndNotice();

                url = Utils.addDefaultParamsToUrl(Config.VW_MAINTENANCE_URL);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, BaseWebViewFragment.newInstance(url), null).commitAllowingStateLoss();
                break;
            case R.id.tab_assistant:
                buttonTabKey = TAB_ASSISTANT;
                showNameAndNotice();

                if (!PreferenceManager.getInstance().getMainCarPlate().isEmpty()) {
                    getCarDongleBinding();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, DongleUnbindFragment.newInstance("", ""), null).commitAllowingStateLoss();
                }
                break;
            case R.id.tab_open_hub:
                buttonTabKey = TAB_OPEN_HUB;
                checkMemberPhoneAndGoOpenHub();
                break;
            case R.id.tab_store:
                buttonTabKey = TAB_STORE;
                hideNameAndNotice();
                break;
            case R.id.tab_vw:
                buttonTabKey = TAB_MY;
                hideNameAndNotice();

                String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                url = Utils.addDefaultParamsToUrl(Config.VW_MEMBER_NEW_URL) + "&device_id=" + androidId;
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, BaseWebViewFragment.newInstance(url), null).commitAllowingStateLoss();
                break;
        }
    }

    private void setTabView(int id) {
        List<Integer> tabList = new ArrayList<>();
        tabList.add(R.id.tab_home);
        tabList.add(R.id.tab_maintain);
        tabList.add(R.id.tab_assistant);
        tabList.add(R.id.tab_store);
        tabList.add(R.id.tab_vw);

        for (Integer i : tabList) {
            ImageButton button = findViewById(i);
            if (button != null) {
                button.setSelected(i == id);
            }
        }
    }

    public void showNameAndNotice() {
        if (!PreferenceManager.getInstance().getMainCarPlate().isEmpty()) {
            tvCarPlate.setVisibility(View.VISIBLE);
            tvCarPlate.setText(PreferenceManager.getInstance().getMainCarPlate());

            tvNickName.setVisibility(View.GONE);
            tvNickName.setText("");
        } else {
            tvNickName.setVisibility(View.VISIBLE);
            tvNickName.setText(String.format(getString(R.string.dashboard_welcome), PreferenceManager.getInstance().getUserNickname()));

            tvCarPlate.setVisibility(View.GONE);
            tvCarPlate.setText("");
        }

        imgNotice.setVisibility(View.VISIBLE);
    }

    private void hideNameAndNotice() {
        tvNickName.setVisibility(View.GONE);
        tvCarPlate.setVisibility(View.GONE);
        imgNotice.setVisibility(View.GONE);
    }

    public void setNotice(boolean hasNewNotice) {
        imgNotice.setImageResource(hasNewNotice ? R.drawable.ic_notice_with_dot : R.drawable.ic_notice);
    }

    public void getCarDongleBinding() {
        String carPlateNo = PreferenceManager.getInstance().getMainCarPlate();
        showLoading();
        Repository.getInstance().getDongleBinding(carPlateNo, new Repository.OnApiCallback<DongleBindingCarData>() {
            @Override
            public void onSuccess(DongleBindingCarData data) {
                hideLoading();

                if (data.redirectUrl == null || data.redirectUrl.isEmpty()) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, DongleUnbindFragment.newInstance(data.model, data.state), null).commitAllowingStateLoss();
                } else {
                    String url = Utils.addDefaultParamsToUrl(data.redirectUrl);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, BaseWebViewFragment.newInstance(url), null).commitAllowingStateLoss();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoading();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, DongleUnbindFragment.newInstance("", ""), null).commitAllowingStateLoss();
            }
        });
    }
}
