package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;

import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.EncryptUtils;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.utils.VwFirebaseAnalytics;

import java.net.URISyntaxException;
import java.util.UUID;

public class WebViewActivity extends BaseActivity {
    public static final String KEY_SV_NORMAL_LINK = "sv_normal_link";
    public static final String KEY_SV_NEW_CAR_LINK = "sv_new_car_link";

    public static final String EXTRA_TYPE = "extra_type";
    public static final String EXTRA_KEY_URL = "url";

    public static final String NEWS_TITLE = "NEWS_TITLE";
    public static final int TYPE_NONE = 0;
    public static final int TYPE_VW_LOGIN = 1;
    public static final int TYPE_VW_SERVICE = 2;
    public static final int TYPE_VW_CAR_MAINTENANCE = 3;
    public static final int TYPE_ETAG = 4;
    public static final int TYPE_GOV = 5;
    public static final int TYPE_TEST_DRIVE = 6;
    public static final int TYPE_VW_SALES_CHANNEL = 7;
    public static final int TYPE_VW_WEBSITE = 8;
    public static final int TYPE_VW_ONLINE_VIEW = 9;
    public static final int TYPE_VW_TERMS = 10;
    public static final int TYPE_VW_PRIVACY = 11;
    public static final int TYPE_VW_MEMBER_LEVEL = 12;
    public static final int TYPE_VW_MEMBER_FAQ = 13;
    public static final int TYPE_VW_LINE_POINT_INFO = 14;
    public static final int TYPE_VW_SURVEY_CAKE = 15;
    public static final int TYPE_VW_MEMBER_COUPON = 16;
    public static final int TYPE_VW_FAB_AST_CAR = 17;
    public static final int TYPE_VW_CAR_ESTIMATE = 18;
    public static final int TYPE_VW_SUGGEST_MAINTAIN_CAR = 19;
    public static final int TYPE_VW_NEWS = 20;
    public static final int TYPE_VW_WARRANTY = 21;
    public static final int TYPE_MAINTENANCE_RECORDS = 22;
    public static final int TYPE_VW_EMAIL_SERVICE = 23;
    public static final int TYPE_VW_MEMBER_BENEFITS = 24;
    public static final int TYPE_VW_MEMBER_PROFILE = 25;
    public static final int TYPE_MAINTENANCE_WARRANTY = 26;
    public static final int TYPE_MAINTENANCE_PAT = 27;
    public static final int TYPE_MAINTENANCE_RECALLS = 28;
    public static final int TYPE_MAINTENANCE_BODYANDPAINT = 29;
    public static final int TYPE_MAINTENANCE_ADDITIONALQUOTATION = 30;
    public static final int TYPE_VW_MEMBER_COUPON_DEEPLINK = 31;


    public static void goVWLogin(final Activity activity, int reqCode) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_LOGIN);
        activity.startActivityForResult(intent, reqCode);
    }

    public static void goVWService(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_SERVICE);
        activity.startActivity(intent);
    }

    public static void goVWCarMaintenance(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_CAR_MAINTENANCE);
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goVWWebsite(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_WEBSITE);
        activity.startActivity(intent);
    }

    public static void goVWOnlineView(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_ONLINE_VIEW);
        activity.startActivity(intent);
    }

    public static void goEtag(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_ETAG);
        activity.startActivity(intent);
    }

    public static void goGov(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_GOV);
        activity.startActivity(intent);
    }

    public static void goTestDrive(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_TEST_DRIVE);
        activity.startActivity(intent);
    }

    public static void goVWSalesChannel(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_SALES_CHANNEL);
        activity.startActivity(intent);
    }

    public static void goVWTerms(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_TERMS);
        activity.startActivity(intent);
    }

    public static void goVWPrivacy(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_PRIVACY);
        activity.startActivity(intent);
    }

    public static void goVWMemberLevel(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_LEVEL);
        activity.startActivity(intent);
    }

    public static void goVWMemberFaq(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_FAQ);
        activity.startActivity(intent);
    }

    public static void goVWMemberProfile(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_PROFILE);
        activity.startActivity(intent);
    }

    public static void goVWMemberCoupon(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_COUPON);
        activity.startActivity(intent);
    }

    public static void goVWMemberCoupon(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_COUPON_DEEPLINK);
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goVWMemberBenefits(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_MEMBER_BENEFITS);
        activity.startActivity(intent);
    }

    public static void goVWLinePointInfo(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_LINE_POINT_INFO);
        activity.startActivity(intent);
    }

    public static void goVWSurveyCake(final Activity activity, String url, boolean isNormalLink, boolean isNewCarLink) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_SURVEY_CAKE);
        intent.putExtra(KEY_SV_NORMAL_LINK, isNormalLink);
        intent.putExtra(KEY_SV_NEW_CAR_LINK, isNewCarLink);
        intent.putExtra(WebViewActivity.class.getSimpleName(), url);
        activity.startActivity(intent);
    }


    public static void goVWAstCar(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_FAB_AST_CAR);
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goVWCarEstimate(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_CAR_ESTIMATE);
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goVWSuggestMaintainCar(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_SUGGEST_MAINTAIN_CAR);
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goNewsLink(final Activity activity, String title, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_NEWS);
        intent.putExtra(EXTRA_KEY_URL, url);
        if( title != null) {
            intent.putExtra(NEWS_TITLE, title);
        } else {
            intent.putExtra(NEWS_TITLE, "");
        }
        activity.startActivity(intent);
    }

    public static void goAddWarranty(final  Activity activity, String url ) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_WARRANTY );
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goMaintenanceRecords(final Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_MAINTENANCE_RECORDS );
        intent.putExtra(EXTRA_KEY_URL, url);
        activity.startActivity(intent);
    }

    public static void goMaintenanceWarranty(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_MAINTENANCE_WARRANTY);
        activity.startActivity(intent);
    }

    public static void goVWEmailService(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_VW_EMAIL_SERVICE);
        activity.startActivity(intent);
    }

    public static void goMaintenancePat(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_MAINTENANCE_PAT);
        activity.startActivity(intent);
    }

    public static void goMaintenanceRecalls(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_MAINTENANCE_RECALLS);
        activity.startActivity(intent);
    }

    public static void goMaintenanceBodyAndPaint(final Activity activity) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(EXTRA_TYPE, TYPE_MAINTENANCE_BODYANDPAINT);
        activity.startActivity(intent);
    }

    private TextView mTitle;
    private WebView mWebView;
    private int mType;
    private Uri mCallUri;
    private PermissionRequest webViewPermissionRequest;

    private boolean mIsNormalLink = false;
    private boolean mIsNewCarLink = false;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        mTitle = findViewById(R.id.tv_view_title);
        mWebView = findViewById(R.id.web_view_content);

        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setSaveFormData(false);
        mWebView.getSettings().setAllowFileAccess(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null) {
                    return false;
                }

                if (url.startsWith(BuildConfig.DEBUG ? getString(R.string.app_scheme_dev) : getString(R.string.app_scheme_production))) {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent();
                    intent.setData(uri);
                    setResult(RESULT_OK, intent);
                    finish();
                    return true;
                } else if (url.startsWith("tel:")) {
                    mCallUri = Uri.parse(url);
                    call();
                    return true;
                } else if (url.startsWith("intent://")) {
                    try {
                        Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                        view.loadUrl(fallbackUrl);
                        return true;
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    return false;
                } else if (mIsNewCarLink) {
                    //surveyCake new Car link url is a shortcut url, need add params after url translate to real url
                    Uri uri = Uri.parse(url)
                            .buildUpon()
                            .appendQueryParameter(Config.VW_SV_NEW_CAR_LINK_QUERY_PARAMS, PreferenceManager.getInstance().getAccountId())
                            .build();
                    view.loadUrl(uri.toString());
                    return true;
                } else {
                    return false;
                }
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                webViewPermissionRequest = request;

                if (request.getResources().length > 0 && request.getResources()[0].equalsIgnoreCase(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CODE);
                }
            }
        });

        String url = null;
        String title = null;
        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
        mType = getIntent().getIntExtra(EXTRA_TYPE, TYPE_NONE);
        switch (mType) {
            case TYPE_VW_LOGIN:
                url = String.format(Config.VWID_LOGIN_URL, BuildConfig.DEBUG ? getString(R.string.app_scheme_dev) : getString(R.string.app_scheme_production), generateState());
                break;
            case TYPE_VW_SERVICE:
                title = getString(R.string.web_view_title_vw_service);
                url = Config.VW_STORES_URL;
                break;
            case TYPE_VW_CAR_MAINTENANCE:
                title = getString(R.string.web_view_title_vw_service);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_SALES_CHANNEL:
                title = getString(R.string.web_view_title_vw_sales_channel);
                url = Config.VW_STORES_URL;
                break;
            case TYPE_ETAG:
                title = getString(R.string.web_view_title_etag);
                url = Config.ETAG_URL;
                Utils.deleteWebViewCookiesForDomain(url);
                break;
            case TYPE_GOV:
                title = getString(R.string.web_view_title_gov);
                url = Config.GOV_URL;
                break;
            case TYPE_TEST_DRIVE:
                title = getString(R.string.web_view_title_test_drive);
                url = Config.TEST_DRIVE_URL;
                break;
            case TYPE_VW_WEBSITE:
                title = getString(R.string.web_view_title_vw_website);
                url = Config.VW_WEBSITE_URL;
                break;
            case TYPE_VW_ONLINE_VIEW:
                title = getString(R.string.title_online_view);
                url = Config.VW_IDENTITY_CAR_URL;
                break;
            case TYPE_VW_TERMS:
                title = getString(R.string.settings_terms_of_service);
//                url = Config.VW_TERMS_URL;
                if( carData != null ) {
                    url = Uri.parse(Config.VW_TERMS_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate )
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }

                break;
            case TYPE_VW_PRIVACY:
                title = getString(R.string.settings_privacy);
//                url = Config.VW_PRIVACY_URL;

                if( carData != null ) {
                    url = Uri.parse(Config.VW_PRIVACY_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate )
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();

                }

                break;
            case TYPE_VW_MEMBER_LEVEL:
                title = getString(R.string.web_view_title_vw_member_right);
                url = Config.VW_MEMBER_LEVEL;
                break;
            case TYPE_VW_MEMBER_FAQ:
                title = getString(R.string.web_view_title_vw_member_right);
                url = Config.VW_MEMBER_FAQ;
                break;
            case TYPE_VW_LINE_POINT_INFO:
                title = getString(R.string.web_view_title_vw_line_point_info);
                url = Config.VW_LINE_POINT_INFO;
                break;
            case TYPE_VW_SURVEY_CAKE:
                title = getString(R.string.web_view_title_vw_survey_cake);

                mIsNormalLink = getIntent().getBooleanExtra(KEY_SV_NORMAL_LINK, false);
                mIsNewCarLink = getIntent().getBooleanExtra(KEY_SV_NEW_CAR_LINK, false);

                if (getIntent().hasExtra(WebViewActivity.class.getSimpleName())) {
                    url = getIntent().getStringExtra(WebViewActivity.class.getSimpleName());
                    if (mIsNormalLink) {
                        //surveyCake normal link url is a normal url, need add params before load url
                        Uri uri = Uri.parse(url)
                                .buildUpon()
                                .appendQueryParameter(Config.VW_SV_NORMAL_LINK_QUERY_PARAMS, PreferenceManager.getInstance().getAccountId())
                                .build();
                        url = uri.toString();
                    }
                } else {
                    url = "";
                }
                break;
            case TYPE_VW_MEMBER_COUPON:
                title = getString(R.string.coupon_title);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MEMBER_COUPON_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;
            case TYPE_VW_MEMBER_COUPON_DEEPLINK:
                title = getString(R.string.coupon_title);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_MEMBER_BENEFITS:
                title = getString(R.string.benefits_title);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MEMBER_BENEFITS_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;
            case TYPE_VW_MEMBER_PROFILE:
                title = getString(R.string.member_profile_title);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MEMBER_PROFILE_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate )
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;

            case TYPE_VW_FAB_AST_CAR:
                title = getString(R.string.maintenance_reservation);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_CAR_ESTIMATE:
                title = getString(R.string.web_view_title_car_estimate);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_SUGGEST_MAINTAIN_CAR:
                title = getString(R.string.web_view_title_suggest_maintain);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_NEWS:
                title = getIntent().getStringExtra(NEWS_TITLE);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_VW_WARRANTY:
                title = getString(R.string.web_view_title_car_warranty);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_MAINTENANCE_RECORDS:
                title = getString(R.string.web_view_title_maintenance_records);
                url = getIntent().getStringExtra(EXTRA_KEY_URL);
                break;
            case TYPE_MAINTENANCE_WARRANTY:
                title = getString(R.string.web_view_title_car_warranty);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MAINTENANCE_WARRANTY_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;

            case TYPE_MAINTENANCE_PAT:
                title = getString(R.string.car_maintenance_title);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MAINTENANCE_PAT_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;

            case TYPE_MAINTENANCE_RECALLS:
                title = getString(R.string.web_view_title_maintenance_recalls);
                if( carData != null) {
                    url = Uri.parse(Config.VW_MAINTENANCE_RECALLS_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;

            case TYPE_MAINTENANCE_BODYANDPAINT:
                title = getString(R.string.web_view_title_car_estimate);
                if( carData != null) {
                    url = Uri.parse(Config.VW_CAR_ESTIMATE_URL).buildUpon()
                            .appendQueryParameter("token", PreferenceManager.getInstance().getUserToken())
                            .appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId())
                            .appendQueryParameter("LicensePlateNumber", carData.plate)
                            .appendQueryParameter("Vin", carData.vin)
                            .build()
                            .toString();
                }
                break;

            case TYPE_VW_EMAIL_SERVICE:
                title = getString(R.string.web_view_title_vw_email_service);
                url = Config.VW_EMAIL_SERVICE;
                break;

            default:
                break;
        }

        if (title != null) {
            mTitle.setText(title);
        }

        if (url == null) {
            return;
        }

        if (url.startsWith(BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme))) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
            finish();
        } else {
            mWebView.loadUrl(url);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }

        switch (mType) {
            case TYPE_VW_LOGIN:
                FirebaseAnalytics.getInstance(this).logEvent(VwFirebaseAnalytics.Event.LOGIN_BACK, null);
                break;
            case TYPE_VW_CAR_MAINTENANCE:
                break;
        }
    }

    public void onClick(View v) {
        onBackPressed();
    }

    private String generateState() {
        String state = EncryptUtils.encrypt(UUID.randomUUID().toString());
        PreferenceManager.getInstance().saveState(state);
        return state;
    }

    @SuppressLint("MissingPermission")
    private void call() {
        startActivity(new Intent(Intent.ACTION_DIAL, mCallUri));
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == Utils.REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                if (permissions.length > 0 && permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA) && webViewPermissionRequest != null) {
                    webViewPermissionRequest.grant(webViewPermissionRequest.getResources());
                    return;
                }

                call();
            } else {
                // Permission denied.
            }
        }
    }
}
