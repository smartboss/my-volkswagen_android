package com.volkswagen.myvolkswagen.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.CruisysApiClientManager;
import com.volkswagen.myvolkswagen.controller.CruisysApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarSummary;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.CarTripsResponseData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.utils.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;

// TODO: 2024/1/14 remove it
public class CarWithAssistantFragment extends Fragment implements View.OnClickListener{
    public static final String ARG_INITIAL_VIEW = "arg_initial_view";

    public static final int INITIAL_NORMAL = 0;
    public static final int INITIAL_CAR_INFO = 1;
    public static final int INITIAL_MAINTAIN_INFO = 2;


    private TextView tvNickname;
    private ImageButton btnNotice;
    private Button btn_myCar, btnAddCar, btnBinding;
    private View viewNonOwnerInfo, viewBtnBinding, viewUnbindingInfo, viewAssistantInfo, viewTripsInfo;

    public static CarTripsResponseData carTrips;
    private ViewTripsPagerAdapter tripsAdapter;

    private TextView assistant_total_miles_value,
            assistant_current_gas_value,
            assistant_current_coolant_temp_value,
            assistant_current_intake_temp_value,
            assistant_current_voltage_value;

    public static CarWithAssistantFragment newInstance(int initialView) {
        CarWithAssistantFragment fragment = new CarWithAssistantFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_INITIAL_VIEW, initialView);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_with_assistant, container, false);
        tvNickname = view.findViewById(R.id.tv_title_nickname);
        btnNotice = view.findViewById(R.id.btn_notice);
        viewNonOwnerInfo = view.findViewById(R.id.nonOwner_info);
        viewBtnBinding = view.findViewById(R.id.vw_btn_binding);
        viewUnbindingInfo = view.findViewById(R.id.unbinding_info);
        viewAssistantInfo = view.findViewById(R.id.assistant_info);
        viewTripsInfo = view.findViewById(R.id.trips_history_info);

        if( tvNickname != null) {
            tvNickname.setText("Hi " + PreferenceManager.getInstance().getUserNickname());
        }

        btn_myCar = view.findViewById(R.id.btn_mycar);
        btn_myCar.setOnClickListener(this);

        btnAddCar = view.findViewById(R.id.add_car_btn);
        btnAddCar.setOnClickListener(this);

        btnBinding = view.findViewById(R.id.btn_binding);
        btnBinding.setOnClickListener(this);


        assistant_total_miles_value = view.findViewById(R.id.assistant_total_miles_value);
        assistant_current_gas_value = view.findViewById(R.id.assistant_current_gas_value);
        assistant_current_voltage_value = view.findViewById(R.id.assistant_current_voltage_value);
        assistant_current_coolant_temp_value = view.findViewById(R.id.assistant_current_coolant_temp_value);
        assistant_current_intake_temp_value = view.findViewById(R.id.assistant_current_intake_temp_value);

        CarSummaryResponseData carSummaryResponseData = PreferenceManager.getInstance().getCarSummary();
        CarSummary carSummary = null;
        if( carSummaryResponseData != null) {
            if(carSummaryResponseData.carSummaryDataList != null&& carSummaryResponseData.carSummaryDataList.size() > 0)
            {
                carSummary = carSummaryResponseData.carSummaryDataList.get(0);
                String mileageText = carSummary.odometer.value + "";
                assistant_total_miles_value.setText(mileageText);
                String odometerStr = carSummary.fuelLevel.value + "";
                assistant_current_gas_value.setText(odometerStr);
                String batteryStr = carSummary.controlModuleVoltage.value + "";
                assistant_current_voltage_value.setText(batteryStr);
                String coolantStr = carSummary.coolantTemperature.value + "";
                assistant_current_coolant_temp_value.setText(coolantStr);
                String intake_airStr = carSummary.intakeAirTemperature.value + "";
                assistant_current_intake_temp_value.setText(intake_airStr);
            }
        }

        //notice button
//        boolean hasNew = PreferenceManager.getInstance().getNewNoticeCount() > 0;
//        btnNotice.setImageResource(hasNew ? R.drawable.ic_notice_black_new : R.drawable.ic_notice_black);

        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
        if( btn_myCar != null && carData != null) {
            btn_myCar.setText(carData.plate);
        }

        if( PreferenceManager.getInstance().getCarTrips() != null ) {
            carTrips = PreferenceManager.getInstance().getCarTrips();
        }

        tripsAdapter = new ViewTripsPagerAdapter(carTrips, this.getContext(), new ViewTripsPagerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                WebViewActivity.goNewsLink(getActivity(), title, url);
                Toast.makeText(getContext(), "Item Clicked", Toast.LENGTH_LONG).show();
                tripsAdapter.notifyDataSetChanged();
            }
        });
        RecyclerView recyclerView = view.findViewById(R.id.trips_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(tripsAdapter);
        tripsAdapter.notifyDataSetChanged();

        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
    }

    @Override
    public void onResume() {
        super.onResume();

        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();
        String accessToken = PreferenceManager.getInstance().getCruisysAccessToken();


        if( carData != null && accessToken != null) {
            String carTripsUrl = CruisysApiClientManager.getServiceCarTrips(carData.plate);
            CruisysApiService service = CruisysApiClientManager.getClient().create(CruisysApiService.class);

            String summaryUrl = CruisysApiClientManager.getServiceCarSummary(carData.plate);
            service.service_summary(summaryUrl, "Bearer " + accessToken).enqueue(new Callback<CarSummaryResponseData>()
            {
                @Override
                public void onResponse(Call<CarSummaryResponseData> call, retrofit2.Response<CarSummaryResponseData> response) {
                    Logger.v(this, "getServiceCarSummary: " +  response.toString());
                    PreferenceManager.getInstance().saveMyCarSummary(response.body());
                    updateSummary();
                }
                @Override
                public void onFailure(Call<CarSummaryResponseData> call, Throwable t) {
                    Logger.e(this, "getServiceCarSummary: " + t.toString());
                }
            });


            service.service_car_trips(carTripsUrl, "Bearer " + accessToken).enqueue(new Callback<CarTripsResponseData>(){
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onResponse(Call<CarTripsResponseData> call, retrofit2.Response<CarTripsResponseData> response) {
                    CarTripsResponseData data = response.body();

                    if( data != null ) {
                        Logger.v(this,"getServiceCarTrips: " +  data + ", trip size:" + data.tripDataList.size());
                        PreferenceManager.getInstance().saveCarTrips(data);
                        tripsAdapter.updateTrips(data);
                        tripsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<CarTripsResponseData> call, Throwable t) {
                    Logger.v(this,"getServiceCarTrips: " +  t.toString());
                }
            });
        }

        switch (DashboardFragment.mViewState)
        {
            //非車主
            case nonOwner:
                tvNickname.setVisibility(View.VISIBLE);
                btnNotice.setVisibility(View.VISIBLE);
                viewNonOwnerInfo.setVisibility(View.VISIBLE);
                viewUnbindingInfo.setVisibility(View.GONE);
                viewAssistantInfo.setVisibility(View.GONE);
                viewTripsInfo.setVisibility(View.GONE);
                btn_myCar.setVisibility(View.GONE);
                viewBtnBinding.setVisibility(View.GONE);
                break;
            //綁車不綁助理(提醒)
            case carBoundNoAssistant_notify:
                //綁車不綁助理
            case carBoundNoAssistant:
                tvNickname.setVisibility(View.GONE);
                btnNotice.setVisibility(View.GONE);
                viewNonOwnerInfo.setVisibility(View.GONE);
                viewUnbindingInfo.setVisibility(View.VISIBLE);
                viewAssistantInfo.setVisibility(View.GONE);
                viewTripsInfo.setVisibility(View.GONE);
                btn_myCar.setVisibility(View.VISIBLE);
                viewBtnBinding.setVisibility(View.VISIBLE);
                break;

            //綁車綁助理
            case carAndAssistantBound:
                tvNickname.setVisibility(View.GONE);
                btnNotice.setVisibility(View.GONE);
                viewNonOwnerInfo.setVisibility(View.GONE);
                viewUnbindingInfo.setVisibility(View.GONE);
                viewAssistantInfo.setVisibility(View.VISIBLE);
                viewTripsInfo.setVisibility(View.VISIBLE);
                btn_myCar.setVisibility(View.VISIBLE);
                viewBtnBinding.setVisibility(View.GONE);

            default:
                break;
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_mycar:
                MyCarActivity.goMyCar(getActivity());
                break;

                // 新增愛車
            case R.id.add_car_btn:
                inAppAddCarActivity.goAddCar(this.getActivity(), PreferenceManager.getInstance().getUserNickname());
                break;

                // 綁定行車助理
            case R.id.btn_binding:
                inAppBindingActivity.goBindingCar(this.getActivity());
                break;
        }
    }

    private void updateSummary() {
        CarSummaryResponseData carSummaryResponseData = PreferenceManager.getInstance().getCarSummary();
        CarSummary carSummary = null;
        if( carSummaryResponseData != null) {
            if(carSummaryResponseData.carSummaryDataList != null&& carSummaryResponseData.carSummaryDataList.size() > 0)
            {
                carSummary = carSummaryResponseData.carSummaryDataList.get(0);
                String mileageText = carSummary.odometer.value + "";
                assistant_total_miles_value.setText(mileageText);
                String odometerStr = carSummary.fuelLevel.value + "";
                assistant_current_gas_value.setText(odometerStr);
                String batteryStr = carSummary.controlModuleVoltage.value + "";
                assistant_current_voltage_value.setText(batteryStr);
                String coolantStr = carSummary.coolantTemperature.value + "";
                assistant_current_coolant_temp_value.setText(coolantStr);
                String intake_airStr = carSummary.intakeAirTemperature.value + "";
                assistant_current_intake_temp_value.setText(intake_airStr);
            }
        }
    }

}

