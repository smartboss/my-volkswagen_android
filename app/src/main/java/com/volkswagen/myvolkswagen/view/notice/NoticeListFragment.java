package com.volkswagen.myvolkswagen.view.notice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.InboxContent;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.model.Notice;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.adapter.NewsListAdapter;
import com.volkswagen.myvolkswagen.view.adapter.NoticeListAdapter;
import com.volkswagen.myvolkswagen.view.alert.VWBaseAlertDialogFragment;
import com.volkswagen.myvolkswagen.view.bottomsheet.NoticeManagerBottomSheetDialog;
import com.volkswagen.myvolkswagen.view.setting.SettingsActivity;

import java.util.List;

public class NoticeListFragment extends Fragment implements View.OnClickListener, NoticeListAdapter.NoticeControlListener, NewsListAdapter.OnNewsClickListener, NoticeManagerBottomSheetDialog.NoticeManagerListener {
    private static final String TYPE = "type";
    private static NoticeListFragment fragment;

    private TextView tvTabNotice, tvTabNews, tvNoticeHint;
    private RecyclerView recyclerNoticeList, recyclerNewsList;
    private NoticeListAdapter noticeListAdapter;
    private NewsListAdapter newsListAdapter;
    private int type = NoticeActivity.TYPE_NOTICE;

    public static NoticeListFragment newInstance(int type) {
        if (fragment == null) {
            fragment = new NoticeListFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        fragment.setArguments(bundle);

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notice_list, container, false);
        initView(view);

        getNoticeList();
        getNewsList();
        return view;
    }

    private void initView(View view) {
        tvTabNotice = view.findViewById(R.id.tv_tab_notice);
        tvTabNotice.setOnClickListener(this);

        tvTabNews = view.findViewById(R.id.tv_tab_news);
        tvTabNews.setOnClickListener(this);

        tvNoticeHint = view.findViewById(R.id.tv_notice_hint);

        recyclerNoticeList = view.findViewById(R.id.recycler_view_notice);
        recyclerNoticeList.setLayoutManager(new LinearLayoutManager(getContext()));
        noticeListAdapter = new NoticeListAdapter();
        recyclerNoticeList.setAdapter(noticeListAdapter);
        noticeListAdapter.addNoticeControlListener(this);

        recyclerNewsList = view.findViewById(R.id.recycler_view_news);
        recyclerNewsList.setLayoutManager(new LinearLayoutManager(getContext()));
        newsListAdapter = new NewsListAdapter();
        recyclerNewsList.setAdapter(newsListAdapter);
        newsListAdapter.addNewsClickListener(this);

        type = NoticeActivity.TYPE_NOTICE;
        if (getArguments() != null && getArguments().containsKey(TYPE)) {
            type = getArguments().getInt(TYPE, NoticeActivity.TYPE_NOTICE);
        }

        switch (type) {
            case NoticeActivity.TYPE_NOTICE:
                showNoticeView();

                break;
            case NoticeActivity.TYPE_NEWS:
                showNewsView();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof NoticeActivity) {
            ((NoticeActivity) getActivity()).setTitle(getString(R.string.notice_tab_notice_title));
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            switch (type) {
                case NoticeActivity.TYPE_NOTICE:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
                    break;
                case NoticeActivity.TYPE_NEWS:
                    Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NEWS, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_notice:
                showNoticeView();
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NEWS, "車主通知", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.tv_tab_news:
                showNewsView();
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE, "最新消息", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
        }
    }

    private void getNoticeList() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().getNoticeList(3, new Repository.OnApiCallback<List<Notice>>() {
            @Override
            public void onSuccess(List<Notice> notices) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                noticeListAdapter.setData(notices);
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
            }
        });
    }

    private void getNewsList() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().getNewsList(new Repository.OnApiCallback<List<NewsData>>() {
            @Override
            public void onSuccess(List<NewsData> newsData) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                newsListAdapter.setData(newsData);
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
            }
        });
    }

    private void deleteNotice(int noticeId) {
        Repository.getInstance().deleteNotice(getContext(), noticeId, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                getNoticeList();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private void showNoticeView() {
        type = NoticeActivity.TYPE_NOTICE;
        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);

        tvTabNotice.setBackgroundResource(R.drawable.rec_white_corner_7);
        tvTabNews.setBackgroundResource(0);

        recyclerNoticeList.setVisibility(View.VISIBLE);
        tvNoticeHint.setVisibility(View.VISIBLE);
        recyclerNewsList.setVisibility(View.GONE);
    }

    private void showNewsView() {
        type = NoticeActivity.TYPE_NEWS;
        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NEWS, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);

        tvTabNews.setBackgroundResource(R.drawable.rec_white_corner_7);
        tvTabNotice.setBackgroundResource(0);

        recyclerNewsList.setVisibility(View.VISIBLE);
        tvNoticeHint.setVisibility(View.GONE);
        recyclerNoticeList.setVisibility(View.GONE);
    }

    @Override
    public void onClickMore(Notice notice) {
        if (getContext() != null) {
            NoticeManagerBottomSheetDialog dialog = new NoticeManagerBottomSheetDialog(getContext(), notice, this);
            dialog.show();
        }
    }

    @Override
    public void onSelectNotice(Notice notice) {
        Repository.getInstance().readNotice(notice.id);

        if (notice.kind == 99) {
            if (getActivity() != null && getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).goCoupon(2, "");
            }
            return;
        }

        if (notice.kind == 101) {
            if (getActivity() != null && getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).goMainCarDongle();
            }
            return;
        }

        Repository.getInstance().getInboxContent(getContext(), notice.newsId, new Repository.OnApiCallback<InboxContent>() {
            @Override
            public void onSuccess(InboxContent data) {
                goNoticeDetail(NoticeActivity.TYPE_NOTICE, notice.kind, data.photoList, data.title, data.content, data.date, data.accountId);
                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE, "車主通知詳細_" + data.noticeID, null, EventLog.EVENT_CATEGORY_BUTTON);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onClickNews(NewsData news) {
        if (news.is_link == 1) {
            if (news.link != null && !news.link.isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.link));
                startActivity(intent);
            }
        } else {
            goNoticeDetail(NoticeActivity.TYPE_NEWS, -1, news.gallery, news.title, news.content, news.published_at, "");
        }

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NEWS, "最新消息詳細_" + news.id, null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    private void goNoticeDetail(int type, int kind, List<String> imageList, String title, String content, String date, String accountId) {
        if (getActivity() != null && getActivity() instanceof NoticeActivity) {
            ((NoticeActivity) getActivity()).goNoticeDetail(type, kind, imageList, title, content, date, accountId, true);
        }
    }

    @Override
    public void onNoticeDelete(Notice notice) {
        VWBaseAlertDialogFragment dialogFragment = VWBaseAlertDialogFragment.newInstance(getString(R.string.notice_message_delete), getString(R.string.notice_delete_dialog_content), getString(R.string.confirm_button), getString(R.string.cancel_button));
        dialogFragment.show(getChildFragmentManager(), null);
        dialogFragment.addDialogListener(new VWBaseAlertDialogFragment.VWDialogListener() {
            @Override
            public void onDialogPositiveClick() {
                deleteNotice(notice.id);
            }

            @Override
            public void onDialogNegativeClick() {
            }
        });

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_NOTICE, "刪除通知", null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onNoticeSettingClick() {
        SettingsActivity.goSettings(getActivity());
    }

    public int getCurrentType() {
        return type;
    }
}
