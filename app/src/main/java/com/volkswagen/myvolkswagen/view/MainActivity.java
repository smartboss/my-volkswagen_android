package com.volkswagen.myvolkswagen.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.CruisysApiClientManager;
import com.volkswagen.myvolkswagen.controller.CruisysApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.controller.ValueCallback;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.ExtIdAuthResponse;
import com.volkswagen.myvolkswagen.model.CarLocationResponseData;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.CarTripsResponseData;
import com.volkswagen.myvolkswagen.model.CarTypeData;
import com.volkswagen.myvolkswagen.model.DongleNotificationResponseData;
import com.volkswagen.myvolkswagen.model.DongleBindingResponseData;
import com.volkswagen.myvolkswagen.model.EventBus.CarTypeEvent;
import com.volkswagen.myvolkswagen.model.EventBus.MainActivityTabEvent;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.EventBus.UpdateNoticeEvent;
import com.volkswagen.myvolkswagen.model.ExtIdAuthInput;
import com.volkswagen.myvolkswagen.model.MemberLevelInfoResponse;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.NoticeCount;
import com.volkswagen.myvolkswagen.model.QuotationsResponseData;
import com.volkswagen.myvolkswagen.model.ServiceCardResponseData;
import com.volkswagen.myvolkswagen.model.User;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends BaseActivity {
    public static final String EXTRA_TAB_ID = "extra_tab_id";
    private static final MainActivity sInstance = new MainActivity();

    public static MainActivity getInstance() {
        return sInstance;
    }

    public static final int TAB_COMMUNITY = 0;
    public static final int TAB_MYCAR = 1;
    public static final int TAB_TOOLS = 2;
    public static final int TAB_CHATBOT = 3;
    public static final int TAB_MY = 4;

    public static void goMain(final Activity activity, final int tabId) {
        final Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_TAB_ID, tabId);
        activity.startActivity(intent);
    }

    private static final int NOTICE_POLLING_INTERVAL = 60000;

    private FirebaseAnalytics mFirebaseAnalytics;
    private ApiService mService;
    private Handler mHandler;
    private String mCurrentPhotoPath;
    private int mMyCarInitialView = MyCarFragment.INITIAL_NORMAL;
    private int completedRequestsCount = 0;
    private long TIMEOUT_THRESHOLD = 5;
    private int TOTAL_REQUESTS = 8;
    private KProgressHUD hud;

    private synchronized void incrementAndCheck(int id) {
        completedRequestsCount += 1;
        Logger.e("EEE", "incrementAndCheck count:" + completedRequestsCount);
        if (completedRequestsCount >= TOTAL_REQUESTS) {
            OnDashboardView(id);
        }
    }

    private void OnDashboardView(int id) {
        SetDashboardViewState(PreferenceManager.getInstance().tryGetFirstCarData());
        selectTab(getIntent().getIntExtra(EXTRA_TAB_ID, id));
        if (hud != null) {
            hud.dismiss();
        }
    }

    private Runnable mNoticeCountChecker = new Runnable() {
        @Override
        public void run() {
            if (EventBus.getDefault().getStickyEvent(TokenReadyEvent.class) != null) {
                getNoticeCount();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mService = ApiClientManager.getClient().create(ApiService.class);
        mHandler = new Handler();


        if (PreferenceManager.getInstance().getUserToken() == null) {
            LoginActivity.goLogin(this);
            finish();
            return;
        }

        processAutoLogin(PreferenceManager.getInstance().getUserToken());
        OnDashboardNewsCheck();
        handleRemoteMessage(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();

        mHandler.removeCallbacks(mNoticeCountChecker);
        mHandler.post(mNoticeCountChecker);
        selectTab(R.id.tab_button_community);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        int tabId = intent.getIntExtra(EXTRA_TAB_ID, 0);
        if (tabId > 0) {
            selectTab(tabId);
        }

        handleRemoteMessage(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
        retrieveDataFromServer();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            // need to change to button_home
            case R.id.tab_button_community:
                if (!v.isSelected()) {
                    OnDashboardNewsCheckNew();
                    selectTab(R.id.tab_button_community);
                }
                break;
            case R.id.tab_button_mycar:
                mMyCarInitialView = MyCarFragment.INITIAL_NORMAL;
                selectTab(R.id.tab_button_mycar);
                break;
            case R.id.tab_button_tools:
                if (!v.isSelected()) {
                    selectTab(R.id.tab_button_tools);
                }
                break;
            case R.id.tab_button_chatbot:
                if (!v.isSelected()) {
                    selectTab(R.id.tab_button_chatbot);
                }
                break;
            case R.id.tab_button_my:
                if (!v.isSelected()) {
                    selectTab(R.id.tab_button_my);
                }
                break;
        }
    }

    public void showPopupFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_popup, fragment, "").addToBackStack("").commit();
        }
    }

    void goMyCar(int initialView) {
        mMyCarInitialView = initialView;
        setTabView(R.id.tab_button_mycar);
        loadFragment(R.id.tab_button_mycar, MyCarFragment.newInstance(mMyCarInitialView));
    }

    private void goMemberWithDeeplink(String deeplink) {
        setTabView(R.id.tab_button_my);
        loadFragment(R.id.tab_button_my, MemberWebViewFragment.newInstance(deeplink));
    }

    private void selectTab(int id) {
        setTabView(id);
        switch (id) {
            case R.id.tab_button_community:
                ToDashBoardView(id);
                break;
            case R.id.tab_button_mycar:
                loadFragment(id, new CarMaintenanceFragment());
                break;
            case R.id.tab_button_tools:
//                loadFragment(id, new ToolsFragment());
                loadFragment(id, new CarWithAssistantFragment());
                break;
            case R.id.tab_button_chatbot:
//                loadFragment(id, new ChatBotFragment());
                loadFragment(id, new OnlineStoreFragment());
                break;
            case R.id.tab_button_my:
//                loadFragment(id, MemberWebViewFragment.newInstance(""));
                loadFragment(id, new MemberWebViewNewFragment());
                break;
        }
    }


    private void OnProfileManagerDataCheck(int id) {
        OnCarPlateCheck(id);
    }

    private void OnDashboardNewsCheck() {
        // test getAppNews
        ApiService service = ApiClientManager.getClient().create(ApiService.class);
        service.getAppNews().enqueue(new Callback<AppNews>() {
            @Override
            public void onResponse(Call<AppNews> call, retrofit2.Response<AppNews> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                AppNews appNews = response.body();
                DashboardFragment.SetAppNews(appNews);
                OnProfileManagerDataCheck(R.id.tab_button_community);
                Logger.d(MainActivity.this, "\n" + appNews.toString());
            }

            @Override
            public void onFailure(Call<AppNews> call, Throwable t) {
                Logger.d(MainActivity.this, "Failed to get app news: " + t.getLocalizedMessage());
                OnProfileManagerDataCheck(R.id.tab_button_community);
            }
        });
    }

    private void OnDashboardNewsCheckNew() {
        // test getAppNews
        ApiService service = ApiClientManager.getClient().create(ApiService.class);
        service.getAppNews().enqueue(new Callback<AppNews>() {
            @Override
            public void onResponse(Call<AppNews> call, retrofit2.Response<AppNews> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                AppNews appNews = response.body();
                DashboardFragment.SetAppNews(appNews);
//                OnProfileManagerDataCheck(R.id.tab_button_community);
                OnCarPlateCheck(0);
                Logger.d(MainActivity.this, "\n" + appNews.toString());
            }

            @Override
            public void onFailure(Call<AppNews> call, Throwable t) {
                Logger.d(MainActivity.this, "Failed to get app news: " + t.getLocalizedMessage());
//                OnProfileManagerDataCheck(R.id.tab_button_community);
            }
        });
    }


    private void OnCarPlateCheck(int id) {

        if (hud == null) {
            hud = AlertUtils.showLoadingHud(this);
        }
        CruisysApiService service = CruisysApiClientManager.getClient().create(CruisysApiService.class);
        CarData carData = PreferenceManager.getInstance().tryGetFirstCarData();

        if (carData != null && carData.plate != null) {
            ExtIdAuthInput loginData = new ExtIdAuthInput(PreferenceManager.getInstance().getAccountId(), carData.plate);
            final ExtIdAuthResponse[] authData = new ExtIdAuthResponse[1];
            service.postExtIdAuth(loginData).enqueue(new Callback<ExtIdAuthResponse>() {
                @Override
                public void onResponse(Call<ExtIdAuthResponse> call, retrofit2.Response<ExtIdAuthResponse> response) {
                    if (!response.isSuccessful()) {
                        Logger.d(this, "Failed to login cruisys");
                        hud.dismiss();
                        return;
                    }

                    authData[0] = response.body();
                    String accessToken = authData[0].extIdAuthData.accessToken;
                    // save cruisys access token
                    PreferenceManager.getInstance().saveCruisysAccessToken(accessToken);

                    if (carData != null && carData.plate != null) {
                        String cardUrl = CruisysApiClientManager.getServiceCardUrl(carData.plate);
                        service.service_card(cardUrl, "Bearer " + accessToken).enqueue(new Callback<ServiceCardResponseData>() {
                            @Override
                            public void onResponse(Call<ServiceCardResponseData> call, retrofit2.Response<ServiceCardResponseData> response) {
                                // Handle service card response
                                Logger.e("EEE", "CarExistDataCruisys");
                                if (response != null) {
                                    PreferenceManager.getInstance().saveServiceCard(response.body());
                                }
                                incrementAndCheck(id);
                            }

                            @Override
                            public void onFailure(Call<ServiceCardResponseData> call, Throwable t) {
                                // Handle failure
                                incrementAndCheck(id);
                            }
                        });
                    }


                    String quotationsUrl = CruisysApiClientManager.getCustomersQuotationsUrl(PreferenceManager.getInstance().getAccountId());
                    service.customers_quotations(quotationsUrl, "Bearer " + accessToken).enqueue(new Callback<QuotationsResponseData>() {
                        @Override
                        public void onResponse(Call<QuotationsResponseData> call, retrofit2.Response<QuotationsResponseData> response) {
                            // Handle quotations response
                            Logger.e("EEE", "customers_quotations");
                            if (response != null) {
                                PreferenceManager.getInstance().saveQuotations(response.body());
                            }
                            incrementAndCheck(id);
                        }

                        @Override
                        public void onFailure(Call<QuotationsResponseData> call, Throwable t) {
                            // Handle failure
                            incrementAndCheck(id);
                        }
                    });

                    if (carData != null && carData.plate != null) {
//                        String reminderUrl = CruisysApiClientManager.getServiceReminderUrl(carData.plate, PreferenceManager.getInstance().getUserToken());
//                        service.getServiceReminder(reminderUrl, "Bearer " + accessToken).enqueue(new Callback<ServiceReminderResponseData>() {
//                            @Override
//                            public void onResponse(Call<ServiceReminderResponseData> call, retrofit2.Response<ServiceReminderResponseData> response) {
//                                // Handle reminder response
//                                Logger.e("EEE", "service_reminder");
//                                if (response != null) {
//                                    PreferenceManager.getInstance().saveReminder(response.body());
//                                }
//                                incrementAndCheck(id);
//                            }
//
//                            @Override
//                            public void onFailure(Call<ServiceReminderResponseData> call, Throwable t) {
//                                // Handle failure
//                                incrementAndCheck(id);
//                            }
//                        });

                        String summaryUrl = CruisysApiClientManager.getServiceCarSummary(carData.plate);
                        service.service_summary(summaryUrl, "Bearer " + accessToken).enqueue(new Callback<CarSummaryResponseData>() {
                            @Override
                            public void onResponse(Call<CarSummaryResponseData> call, retrofit2.Response<CarSummaryResponseData> response) {
                                Logger.v(this, "getServiceCarSummary: " + response.toString());
                                if (response != null) {
                                    PreferenceManager.getInstance().saveMyCarSummary(response.body());
                                }
                                incrementAndCheck(id);
                            }

                            @Override
                            public void onFailure(Call<CarSummaryResponseData> call, Throwable t) {
                                Logger.e(this, "getServiceCarSummary: " + t.toString());
                                incrementAndCheck(id);
                            }
                        });

                        String carLocationUrl = CruisysApiClientManager.getServiceCarLocation(carData.plate);
                        service.service_car_location(carLocationUrl, "Bearer " + accessToken).enqueue(new Callback<CarLocationResponseData>() {
                            @Override
                            public void onResponse(Call<CarLocationResponseData> call, retrofit2.Response<CarLocationResponseData> response) {
                                Logger.v(this, "getServiceCarLocation: " + response.toString());

                                if (response != null) {
                                    PreferenceManager.getInstance().saveCarLocation(response.body());
                                }
                                incrementAndCheck(id);
                            }

                            @Override
                            public void onFailure(Call<CarLocationResponseData> call, Throwable t) {
                                Logger.v(this, "getServiceCarLocation: " + t.toString());
                                incrementAndCheck(id);
                            }
                        });

                        String carTripsUrl = CruisysApiClientManager.getServiceCarTrips(carData.plate);
                        service.service_car_trips(carTripsUrl, "Bearer " + accessToken).enqueue(new Callback<CarTripsResponseData>() {
                            @Override
                            public void onResponse(Call<CarTripsResponseData> call, retrofit2.Response<CarTripsResponseData> response) {
                                if (response != null) {
                                    CarTripsResponseData data = response.body();

                                    if (data != null) {
                                        Logger.v(this, "getServiceCarTrips: " + data + ", trip size:" + data.tripDataList.size());
                                        PreferenceManager.getInstance().saveCarTrips(data);
                                        incrementAndCheck(id);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CarTripsResponseData> call, Throwable t) {
                                Logger.v(this, "getServiceCarTrips: " + t.toString());
                                incrementAndCheck(id);
                            }
                        });

                        String dongleNotificationUrl = CruisysApiClientManager.getServiceDongleNotification(carData.plate);
                        service.service_dongle_notification(dongleNotificationUrl, "Bearer " + accessToken).enqueue(new Callback<DongleNotificationResponseData>() {
                            @Override
                            public void onResponse(Call<DongleNotificationResponseData> call, retrofit2.Response<DongleNotificationResponseData> response) {
                                if (response != null) {
                                    Logger.v(this, "getServiceDongleNotification: " + response.toString());
                                    PreferenceManager.getInstance().saveNotification(response.body());
                                }
                                incrementAndCheck(id);
                            }

                            @Override
                            public void onFailure(Call<DongleNotificationResponseData> call, Throwable t) {
                                Logger.v(this, "getServiceDongleNotification: " + t.toString());
                                incrementAndCheck(id);
                            }
                        });

                        String Dongle_bindingUrl = CruisysApiClientManager.GET_ServiceDongle_bindingUrl(carData.plate);
                        service.service_get_dongle_bind(Dongle_bindingUrl, "Bearer " + accessToken).enqueue(new Callback<DongleBindingResponseData>() {
                            @Override
                            public void onResponse(Call<DongleBindingResponseData> call, retrofit2.Response<DongleBindingResponseData> response) {
                                if (response != null) {
                                    Logger.v(this, "GET Donglebind: " + response.toString());
                                    PreferenceManager.getInstance().saveDongle_binding(response.body());
                                }
                                incrementAndCheck(id);
                            }

                            @Override
                            public void onFailure(Call<DongleBindingResponseData> call, Throwable t) {
                                Logger.e(this, "GET Donglebind: " + t.toString());
                                incrementAndCheck(id);
                            }
                        });
                    }


                    Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            OnDashboardView(id);
                        }
                    };
                    handler.postDelayed(runnable, TIMEOUT_THRESHOLD);
                }

                @Override
                public void onFailure(Call<ExtIdAuthResponse> call, Throwable t) {
                    Logger.e(this, "onFailure: " + t.toString());
                }
            });
        } else {
            OnDashboardView(id);
        }
    }

    private void SetDashboardViewState(CarData carData) {
        if (carData == null) {
            DashboardFragment.SetDashboardViewState(DashboardFragment.DashboardViewState.nonOwner);
            selectTab(R.id.tab_button_community);
        } else {
            if (PreferenceManager.getInstance().isNeverShowDongleHint()) {
                DashboardFragment.SetDashboardViewState(DashboardFragment.DashboardViewState.carBoundNoAssistant_notify);
            } else {
                DashboardFragment.SetDashboardViewState(DashboardFragment.DashboardViewState.carBoundNoAssistant);
            }
            //DashboardFragment.SetDashboardViewState(DashboardFragment.DashboardViewState.carAndAssistantBound);
        }
    }

    private void ToDashBoardView(int id) {
        //change community view to dashboard
        loadFragment(id, new DashboardFragment());
    }

    private void setTabView(int id) {
        switch (id) {
            case R.id.tab_button_community: {
                findViewById(R.id.tab_button_community).setSelected(true);
                findViewById(R.id.tab_button_mycar).setSelected(false);
                findViewById(R.id.tab_button_tools).setSelected(false);
                findViewById(R.id.tab_button_chatbot).setSelected(false);
                findViewById(R.id.tab_button_my).setSelected(false);

                EventBus.getDefault().postSticky(new MainActivityTabEvent(TAB_COMMUNITY));
                break;
            }
            case R.id.tab_button_mycar: {
                findViewById(R.id.tab_button_community).setSelected(false);
                findViewById(R.id.tab_button_mycar).setSelected(true);
                findViewById(R.id.tab_button_tools).setSelected(false);
                findViewById(R.id.tab_button_chatbot).setSelected(false);
                findViewById(R.id.tab_button_my).setSelected(false);

                EventBus.getDefault().postSticky(new MainActivityTabEvent(TAB_MYCAR));
                break;
            }
            case R.id.tab_button_tools: {
                findViewById(R.id.tab_button_community).setSelected(false);
                findViewById(R.id.tab_button_mycar).setSelected(false);
                findViewById(R.id.tab_button_tools).setSelected(true);
                findViewById(R.id.tab_button_chatbot).setSelected(false);
                findViewById(R.id.tab_button_my).setSelected(false);

                EventBus.getDefault().postSticky(new MainActivityTabEvent(TAB_TOOLS));
                break;
            }
            case R.id.tab_button_chatbot: {
                findViewById(R.id.tab_button_community).setSelected(false);
                findViewById(R.id.tab_button_mycar).setSelected(false);
                findViewById(R.id.tab_button_tools).setSelected(false);
                findViewById(R.id.tab_button_chatbot).setSelected(true);
                findViewById(R.id.tab_button_my).setSelected(false);

                EventBus.getDefault().postSticky(new MainActivityTabEvent(TAB_CHATBOT));
                break;
            }
            case R.id.tab_button_my: {
                findViewById(R.id.tab_button_community).setSelected(false);
                findViewById(R.id.tab_button_mycar).setSelected(false);
                findViewById(R.id.tab_button_tools).setSelected(false);
                findViewById(R.id.tab_button_chatbot).setSelected(false);
                findViewById(R.id.tab_button_my).setSelected(true);

                EventBus.getDefault().postSticky(new MainActivityTabEvent(TAB_MY));
                break;
            }
        }
    }

    private void loadFragment(int id, Fragment targetFragment) {
        if (targetFragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, targetFragment, null).commitAllowingStateLoss();
        }
    }

    private void processAutoLogin(String token) {
        mService.autoLogin(token).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                if (!response.isSuccessful()) {
                    loginFailed();
                    return;
                }

                User user = response.body();
                if (!user.result) {
                    loginFailed();
                    return;
                }

                PreferenceManager.getInstance().saveUserData(
                        user.token,
                        user.data.accountId,
                        user.data.email,
                        user.data.mobile,
                        user.data.level,
                        user.data.levelName);

                EventBus.getDefault().postSticky(new TokenReadyEvent());
                if (user.data.showProfile) {
//                    ProfileActivity.goProfile(MainActivity.this, null, null);
//                    ProfileInputActivity.goProfile(MainActivity.this, null, null);
                }

                // add get nick name process
//                Utils.updateUserNickname(user.data.accountId, user.token);
//                String nickname = PreferenceManager.getInstance().getUserNickname();
//                Logger.d(MainActivity.this, "\n get user nickname:" + nickname);


                // call cu_api014 to get nickname and save it to PreferenceManager
//                ApiService service = ApiClientManager.getClient().create(ApiService.class);
//                service.getProfile( new AccountId(user.data.accountId), user.token ).enqueue(new Callback<Profile>() {
//                    @Override
//                    public void onResponse(Call<Profile> call, retrofit2.Response<Profile> response2) {
//                        Profile profile = response2.body();
//                        if (profile.result) {
//                            Logger.d(MainActivity.this, "\n get user nickname:" + profile.nickname);
//                            PreferenceManager.getInstance().saveUserNickname(profile.nickname);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<Profile> call, Throwable t) {
//
//                    }
//                });

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loginFailed();
            }
        });

    }

    private void loginFailed() {
        LoginActivity.goLogin(MainActivity.this);
    }

    private void callCustomerService() {
        AlertUtils.showAlert(
                this, getString(R.string.call_customer_service),
                getString(R.string.tools_call_number),
                getString(R.string.forget_it_button),
                getString(R.string.confirm_button),
                new DialogInterface.OnClickListener() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.tools_call_number)));
                            startActivity(intent);
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Utils.REQUEST_PERMISSIONS_CODE:
                    composePost();
                    break;
                case Utils.REQUEST_PERMISSIONS_CAMERA:
                    mCurrentPhotoPath = Utils.takePicture(MainActivity.this);
                    break;
                default:
                    break;
            }
        }
    }

    public void composePost() {
        AlertUtils.selectItemDialog(this, null, R.array.array_photo_options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        //from camera
                        if (Utils.checkPermissions(MainActivity.this, Manifest.permission.CAMERA)) {
                            mCurrentPhotoPath = Utils.takePicture(MainActivity.this);
                        } else {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CAMERA);
                        }
                        break;
                    case 1:
                        //choose photo
                        Utils.choosePhoto(MainActivity.this);
                        break;
                }
            }
        });
    }

    public void retrieveDataFromServer() {
        retrieveMyCars();
        retrieveCarModels();
        getNoticeCount();
        getMemberLevelInfo();
    }

    public void retrieveMyCars() {
        mService.getMyCarData(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<MyCarData>() {
            @Override
            public void onResponse(Call<MyCarData> call, retrofit2.Response<MyCarData> response) {
                if (ApiClientManager.handleTokenResponse(MainActivity.this, response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to get my car data");
                    return;
                }

                MyCarData data = response.body();
                Logger.d(this, "\n" + data.toString());
                if (!data.result) {
                    Logger.d(this, "Failed to get my car data: code=" + data.errorCode);
                    return;
                }

                // update mycardata to PreferenceManager
                PreferenceManager.getInstance().saveMyCars(data);
                Logger.d(this, "Car data size=" + data.cars.size());
            }

            @Override
            public void onFailure(Call<MyCarData> call, Throwable t) {
                Logger.d(this, "Failed to get my car data: " + t.getLocalizedMessage());
            }
        });
    }


    public void retrieveCarModels() {
        mService.getCarTypes(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<CarTypeData>() {
            @Override
            public void onResponse(Call<CarTypeData> call, retrofit2.Response<CarTypeData> response) {

                if (ApiClientManager.handleTokenResponse(MainActivity.this, response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    return;
                }

                CarTypeData carModels = response.body();

                if (!carModels.result) {
                    return;
                }

                PreferenceManager.getInstance().saveCarTypes(carModels.models);

                EventBus.getDefault().postSticky(new CarTypeEvent());

            }

            @Override
            public void onFailure(Call<CarTypeData> call, Throwable t) {
            }
        });
    }

    private void getNoticeCount() {
        mHandler.removeCallbacks(mNoticeCountChecker);
        mService.getNoticeCount(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<NoticeCount>() {
            @Override
            public void onResponse(Call<NoticeCount> call, retrofit2.Response<NoticeCount> response) {
                if (ApiClientManager.handleTokenResponse(MainActivity.this, response)) {
                    return;
                }

                mHandler.postDelayed(mNoticeCountChecker, NOTICE_POLLING_INTERVAL);

                if (!response.isSuccessful()) {
                    return;
                }

                NoticeCount data = response.body();
                if (!data.result) {
                    return;
                }

                PreferenceManager.getInstance().saveNewNoticeCount(data.count);
                EventBus.getDefault().post(new UpdateNoticeEvent(data.count > 0));
            }

            @Override
            public void onFailure(Call<NoticeCount> call, Throwable t) {
                mHandler.postDelayed(mNoticeCountChecker, NOTICE_POLLING_INTERVAL);
            }
        });
    }

    private void getMemberLevelInfo() {
        Repository.getInstance().getMemberLevelInfo(this, new ValueCallback<MemberLevelInfoResponse.MemberLevelInfo>() {
            @Override
            public void onSuccess(MemberLevelInfoResponse.MemberLevelInfo memberLevelInfo) {
            }
        });
    }

    private void handleRemoteMessage(Intent intent) {
        if (intent == null) return;

        String appScheme = BuildConfig.DEBUG ? getString(R.string.app_deeplink_scheme_dev) : getString(R.string.app_deeplink_scheme);
        if (intent.getData() != null && intent.getData().getScheme() != null && intent.getData().getScheme().equals(appScheme)) {
//            deeplink myvolkswagen://member/coupon
//            uri.getScheme():myvolkswagen
//            uri.getHost():member
//            uri.getPath: /coupon
            Logger.d(MainActivity.this, "got intent scheme: " + intent.getData().getScheme());
            String intentHost = intent.getData().getHost();
            String intentPath = intent.getData().getPath();
            if (intentHost == null || intentPath == null) {
                return;
            }

            if (intentHost.equals(getString(R.string.app_deeplink_host_car))) {
                //myvolkswagen://car
                //myvolkswagen://car/maintain
//                mMyCarInitialView = intentPath.equals(getString(R.string.app_deeplink_path_maintain)) ? MyCarFragment.INITIAL_MAINTAIN_INFO : MyCarFragment.INITIAL_NORMAL;
//                selectTab(R.id.tab_button_mycar);
            } else if (intentHost.equals(getString(R.string.app_deeplink_host_maintenance))) {
                //myvolkswagen://maintenance
                if (intentPath.equals(getString(R.string.app_deeplink_path_records))) {
                    WebViewActivity.goMaintenanceRecords(this, Config.VW_MAINTENANCE_RECORDS_URL);
                } else if (intentPath.equals(getString(R.string.app_deeplink_path_warranty))) {
                    WebViewActivity.goMaintenanceWarranty(this);
                } else {
                    selectTab(R.id.tab_button_mycar);
//                    OnDashboardView(TAB_MYCAR);
                    Logger.d(MainActivity.this, "got to maintenance");
                }

            } else if (intentHost.equals(getString(R.string.app_deeplink_host_member))) {
                //myvolkswagen://member/
                //myvolkswagendev://member
//                goMemberWithDeeplink(intentPath);
                if (intentPath.equals(getString(R.string.app_deeplink_path_coupon))) {
                    WebViewActivity.goVWMemberCoupon(this);
                } else if (intentPath.equals(getString(R.string.app_deeplink_path_benefits))) {
                    WebViewActivity.goVWMemberBenefits(this);
                } else if (intentPath.equals(getString(R.string.app_deeplink_path_profile))) {
                    WebViewActivity.goVWMemberProfile(this);
                }
                Logger.d(MainActivity.this, "got intent path:" + intentPath);
            } else if (intentHost.equals(getString(R.string.app_deeplink_host_main))) {
                //myvolkswagen://main/
                //myvolkswagendev://main
//                goMemberWithDeeplink(intentPath);
                OnDashboardView(TAB_COMMUNITY);

                Logger.d(MainActivity.this, "got to dashboard");
            } else {
                //do nothing
            }
        } else if (intent.getExtras() != null) {
            try {
                if (intent.getExtras().containsKey("news_id")) {
//                    int newsId = Integer.valueOf(intent.getStringExtra("news_id"));
//                    if (newsId > 0) {
//                        PostActivity.goPost(this, newsId, false);
//                    }
                } else if (intent.getExtras().containsKey("inbox_id")) {
                    int newsId = Integer.valueOf(intent.getStringExtra("inbox_id"));
                    if (newsId > 0) {
//                        OwnershipActivity.goOwnerShipActivity(this, newsId);
                    }
                }
            } catch (NumberFormatException e) {
            }
        }
    }
}
