package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.volkswagen.myvolkswagen.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AddCarStep4 extends Fragment implements View.OnClickListener{

    private Button add_car_step4_btn;
    private Button add_car_step4_btn_skip;
    private AddCarActivity baseActivity;
    private ImageButton btn_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycar_add_car_step4, container, false);
        initView(view);
        return view;
    }
    private void initView(View v) {
        baseActivity = (AddCarActivity) getActivity();

        btn_back = v.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        add_car_step4_btn = v.findViewById(R.id.add_car_step4_btn_next);
        add_car_step4_btn.setOnClickListener(this);
        add_car_step4_btn_skip = v.findViewById(R.id.add_car_step4_btn_skip);
        add_car_step4_btn_skip.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step4_btn_next:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new BindingLineFragment()).commitAllowingStateLoss();
                break;
            case  R.id.add_car_step4_btn_skip:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep5()).commitAllowingStateLoss();
                break;
            case R.id.btn_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep3()).commitAllowingStateLoss();
                break;
        }
    }
}
