package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.installations.FirebaseInstallations;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.LoginData_ID;
import com.volkswagen.myvolkswagen.model.User;

public class VerifyFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = VerifyFragment.class.getSimpleName();
    public static final int COUNT_DOWN_TIME = 60;

    public static final String PREFIX = "prefix";
    public static final String PHONE = "phone";

    private static VerifyFragment fragment;

    private String prefix = "";
    private String phone = "";

    private CountDownTimer timer = null;
    private int countDownTime = 0;

    private TextView tvResend, tvVerifyContent, tvVerifyCodeError, tvLogin;
    private EditText etVerifyCode;

    public static VerifyFragment newInstance(String prefix, String phone) {
        if (fragment == null) {
            fragment = new VerifyFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putString(PREFIX, prefix);
        bundle.putString(PHONE, phone);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verify, container, false);

        if (getArguments() != null) {
            prefix = getArguments().getString(PREFIX);
            phone = getArguments().getString(PHONE);
        }

        initView(view);
        return view;
    }

    private void initView(View view) {
        ImageButton btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        tvLogin = view.findViewById(R.id.tv_login);
        tvLogin.setOnClickListener(this);
        tvResend = view.findViewById(R.id.tv_resend);
        tvResend.setOnClickListener(this);
        tvVerifyCodeError = view.findViewById(R.id.tv_code_error);
        tvVerifyContent = view.findViewById(R.id.tv_mobile_verify_content);
        tvVerifyContent.setText(String.format(getString(R.string.msg_verify_mobile), prefix + phone));

        etVerifyCode = view.findViewById(R.id.et_verify);
        etVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String code = etVerifyCode.getText().toString();
                boolean codeCorrect = code.length() == 6;
                tvLogin.getBackground().setTint(codeCorrect ? getResources().getColor(R.color.lightblue400) : getResources().getColor(R.color.grey300));
                tvLogin.setTextColor(codeCorrect ? getResources().getColor(R.color.white2) : getResources().getColor(R.color.grey400));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        startCountDown();
    }

    private void startCountDown() {
        countDownTime = COUNT_DOWN_TIME;

        tvResend.setOnClickListener(null);
        tvResend.setTextColor(getResources().getColor(R.color.grey400));
        tvResend.setBackgroundResource(R.drawable.rec_stroke_corner_gray400);

        if (timer == null) {
            timer = new CountDownTimer(COUNT_DOWN_TIME * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    countDownTime -= 1;
                    tvResend.setText(String.format(getString(R.string.resend), Integer.toString(countDownTime)));
                }

                @Override
                public void onFinish() {
                    tvResend.setText(String.format(getString(R.string.resend), Integer.toString(COUNT_DOWN_TIME)));
                    tvResend.setOnClickListener(VerifyFragment.this);
                    tvResend.setTextColor(getResources().getColor(R.color.lightblue400));
                    tvResend.setBackgroundResource(R.drawable.rec_stroke_corner_blue);
                }
            };
        }

        timer.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_resend:
                startCountDown();
                getOtpCode(prefix, phone);
                break;
            case R.id.tv_login:
                String code = etVerifyCode.getText().toString();
                if (code.length() == 6) {
                    postLoginByOtp();
                } else {
                    Toast.makeText(getContext(), getString(R.string.hint_verify_code), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_back:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        timer.cancel();
        timer = null;
    }

    private void getOtpCode(String countryCode, String phone) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().genOtpCode(countryCode, phone, new Repository.OnApiCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                etVerifyCode.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
                tvVerifyCodeError.setText("");
                tvVerifyCodeError.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.msg_verify_code_resend), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                String message = t.getMessage();
                showErrorMessage(message);
            }
        });
    }

    private void postLoginByOtp() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        FirebaseInstallations.getInstance().getId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                return;
            }

            callLoginByTopApi(task.getResult());
        });
    }

    private void callLoginByTopApi(String firebaseId) {
        Repository.getInstance().postLoginByOtp(prefix, phone, etVerifyCode.getText().toString(), firebaseId, new Repository.OnApiCallback<User>() {
            @Override
            public void onSuccess(User user) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (getActivity() != null && getActivity() instanceof LoginActivity) {
                    ((LoginActivity) getActivity()).handleResult(user);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                String message = t.getMessage();
                showErrorMessage(message);
            }
        });
    }

    private void showErrorMessage(String message) {
        etVerifyCode.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
        tvVerifyCodeError.setVisibility(View.VISIBLE);
        if (message == null) {
            tvVerifyCodeError.setText(getString(R.string.unknown_error));
        } else {
            tvVerifyCodeError.setText(message);
        }
    }
}
