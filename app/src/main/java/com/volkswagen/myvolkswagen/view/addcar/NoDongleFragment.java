package com.volkswagen.myvolkswagen.view.addcar;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_NO_DONGLE;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;

public class NoDongleFragment extends Fragment {
    public static NoDongleFragment getInstance() {
        return new NoDongleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_no_dongle, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_NO_DONGLE);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }
}
