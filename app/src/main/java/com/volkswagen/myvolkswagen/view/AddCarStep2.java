package com.volkswagen.myvolkswagen.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarModel;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.model.MyCar;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.utils.VwFirebaseAnalytics;

public class AddCarStep2 extends Fragment implements View.OnClickListener{

    private Button add_car_step2_btn;
    private Button add_car_step2_btn_skip;
    private Spinner spinner_car_model;
    private TextView carTypeText , carPlateText;
    final String PLATE_FORMAT = "%1$s-%2$s";
    private List<CarModel> mCarModels;
    private List<String> mCarImgs;
    private List<Integer> mCarIds;
    private ImageView img_add_car;
    private AddCarActivity baseActivity;
    private ImageButton btn_back;
    private int SelectModelID;

    TextView errorText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycar_add_car_step2, container, false);

        initView(view);
        return view;
    }
    private void initView(View v) {
        baseActivity = (AddCarActivity) getActivity();

        btn_back = v.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        carTypeText = v.findViewById(R.id.add_car_step2_tv_car_type);
        carPlateText = v.findViewById(R.id.add_car_step2_tv_car_plate);
        carPlateText.setText(getArguments().getString(AddCarFragment.CAR_PLATE));

        errorText =v.findViewById(R.id.add_car_step2_tv_error);

        add_car_step2_btn = v.findViewById(R.id.add_car_step2_btn_next);
        add_car_step2_btn.setOnClickListener(this);
        add_car_step2_btn_skip = v.findViewById(R.id.add_car_step2_btn_skip);
        add_car_step2_btn_skip.setOnClickListener(this);

        spinner_car_model = v.findViewById(R.id.spinner_car_model);

        img_add_car = v.findViewById(R.id.img_add_car);

        loadImageWithPicasso("https://imgd.aeplcdn.com/664x374/n/cw/ec/144681/virtus-exterior-right-front-three-quarter-7.jpeg");

        GetCarModel();
    }

    private void GetCarModel() {
        mCarModels = (List<CarModel>) getArguments().getSerializable(AddCarFragment.CAR_MODEL);
        mCarImgs = new ArrayList<String>();
        mCarIds = new ArrayList<Integer>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
        for(CarModel carModel : mCarModels)
        {
            mCarIds.add(carModel.id);
            mCarImgs.add(carModel.image);
            adapter.add(carModel.name);
        }
        spinner_car_model.setAdapter(adapter);

        spinner_car_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // 您可以在这里处理Spinner项的选择更改
                SelectModelID = mCarIds.indexOf(position);
                String modelName = parentView.getItemAtPosition(position).toString();
                carTypeText.setText(modelName);
                // 如有必要，可以使用selectedValue或position
                loadImageWithPicasso(mCarImgs.get(position));
                baseActivity.GetCarData().modelId = GetTypeIdByCarModel(modelName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // 如果用户未选择任何项，您可以在这里处理
            }
        });
    }

    private void loadImageWithPicasso(String url) {
        Picasso.get().
                load(url).
                fit().
                centerCrop().
                into(img_add_car);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_car_step2_btn_next:
                AddCarData addCarData = baseActivity.GetCarData();
                addCarData.typeId = getArguments().getInt(AddCarFragment.CAR_TYPE_ID);
                addCar(addCarData);
                break;
            case  R.id.add_car_step2_btn_skip:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep5()).commitAllowingStateLoss();
                break;
            case R.id.btn_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep1()).commitAllowingStateLoss();
                break;
        }
    }

    private int GetTypeIdByCarModel(String _modelName)
    {
        for (CarModel carModel: mCarModels
             ) {
            if(carModel.name.equals(_modelName))
                return carModel.id;
        }
        return 0;
    }

    private void addCar(AddCarData addCarData) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(getActivity());
        baseActivity.GetApiService().addCar(addCarData, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                hud.dismiss();
                if (ApiClientManager.handleTokenResponse(getActivity(), response)) {
                    return;
                }

                if (!response.isSuccessful()) {
                    Logger.d(this, "Failed to add car");
                    errorText.setText("Failed to add car");
                    return;
                }

                Response data = response.body();
                if (!data.result) {
                    Logger.d(this, "Failed to add car: code=" + data.errorCode);
                    switch (data.errorCode)
                    {
                        case 3001:
                            errorText.setText(R.string.addcar_error_code_3001);
                            break;
                        case 3003:
                            errorText.setText(R.string.addcar_error_code_3003);
                            break;
                        case 3004:
                            errorText.setText(R.string.addcar_error_code_3004);
                            break;
                        case 3005:
                            errorText.setText(R.string.addcar_error_code_3005);
                            break;
                        case 3008:
                            errorText.setText(R.string.addcar_error_code_3008);
                            break;
                        case 3009:
                            errorText.setText(R.string.addcar_error_code_3009);
                            break;
                        case 3010:
                            errorText.setText(R.string.addcar_error_code_3010);
                            break;
                    }
                    return;
                }
                FirebaseAnalytics.getInstance(getContext()).logEvent(VwFirebaseAnalytics.Event.ADD_CAR_COMPLETE, null);

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep3()).commitAllowingStateLoss();
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                Logger.d(this, "Failed to add car: " + t.getLocalizedMessage());
                hud.dismiss();
            }
        });
    }
}
