package com.volkswagen.myvolkswagen.view.base;

import static com.volkswagen.myvolkswagen.utils.Config.VW_MEMBER_COUPON_URL;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.BaseWebViewFragment;

public class BaseWebViewActivity extends BaseActivity implements View.OnClickListener {
    private static final String TYPE = "type";
    private static final String QUERY = "query";
    private static final String TITLE = "title";
    private static final String URL = "url";

    public static final int TYPE_NONE = 0;
    public static final int TYPE_PRIVACY = 1;
    public static final int TYPE_TERMS = 2;
    public static final int TYPE_CAR_IDENTIFICATION = 3;
    public static final int TYPE_SERVICE_NEARBY = 4;
    public static final int TYPE_VW_EMAIL_SERVICE = 5;
    public static final int TYPE_SERVICE_SETTING = 6;
    public static final int TYPE_MEMBER_BENEFITS = 7;
    public static final int TYPE_MEMBER_COUPON = 8;
    public static final int TYPE_CAR_RESERVATION = 9;
    public static final int TYPE_CAR_RECORDS = 10;
    public static final int TYPE_CAR_SUGGESTION = 11;
    public static final int TYPE_CAR_ESTIMATE = 12;
    public static final int TYPE_CAR_INSURANCE = 13;
    public static final int TYPE_CAR_RECALL = 14;
    public static final int TYPE_CAR_ADDITIONAL_QUOTATION = 15;
    public static final int TYPE_LINE_BINDING = 16;
    public static final int TYPE_OPEN_HUB = 17;
    public static final int TYPE_ANY = 18;

    private WebView webView;
    private TextView tvTitle;
    private ImageButton btnBack;
    private String title = "";
    private String url = "";

    public static void goWebView(final Activity activity, int type) {
        final Intent intent = new Intent(activity, BaseWebViewActivity.class);
        intent.putExtra(TYPE, type);
        activity.startActivity(intent);
    }

    public static void goWebView(final Activity activity, int type, String query) {
        final Intent intent = new Intent(activity, BaseWebViewActivity.class);
        intent.putExtra(TYPE, type);
        intent.putExtra(QUERY, query);
        activity.startActivity(intent);
    }

    public static void goWebView(final Activity activity, int type, String title, String url) {
        final Intent intent = new Intent(activity, BaseWebViewActivity.class);
        intent.putExtra(TYPE, type);
        intent.putExtra(TITLE, title);
        intent.putExtra(URL, url);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_base);

        tvTitle = findViewById(R.id.tv_view_title);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        int type = getIntent().getIntExtra(TYPE, TYPE_NONE);
        getTitleAndUrlByType(type);

        BaseWebViewFragment fragment = BaseWebViewFragment.newInstance(url);
        webView = fragment.getWebView();
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, fragment, "").commitAllowingStateLoss();
    }

    private void getTitleAndUrlByType(int type) {
        switch (type) {
            case TYPE_PRIVACY:
                title = getString(R.string.settings_privacy);
                url = Config.VW_PRIVACY_URL;
                break;
            case TYPE_TERMS:
                title = getString(R.string.settings_terms_of_service);
                url = Config.VW_TERMS_URL;
                break;
            case TYPE_CAR_IDENTIFICATION:
                title = getString(R.string.title_online_view);
                url = Config.VW_MY_CAR_IDENTIFICATION_URL;
                break;
            case TYPE_SERVICE_NEARBY:
                title = getString(R.string.web_view_title_vw_service);
                url = Config.VW_STORES_URL;
                break;
            case TYPE_VW_EMAIL_SERVICE:
                title = getString(R.string.web_view_title_vw_email_service);
                url = Config.VW_EMAIL_SERVICE;
                break;
            case TYPE_SERVICE_SETTING:
                title = getString(R.string.member_profile_title);
                url = Config.VW_MEMBER_PROFILE_URL;
                break;
            case TYPE_MEMBER_BENEFITS:
                title = getString(R.string.benefits_title);
                url = Config.VW_MEMBER_BENEFITS_URL;
                break;
            case TYPE_MEMBER_COUPON:
                title = getString(R.string.coupon_title);
                url = VW_MEMBER_COUPON_URL;
                break;
            case TYPE_CAR_RESERVATION:
                title = getString(R.string.maintenance_reservation);
                url = Config.VW_MAINTENANCE_RESERVATION_URL;
                break;
            case TYPE_CAR_RECORDS:
                title = getString(R.string.web_view_title_maintenance_records);
                url = Config.VW_MAINTENANCE_RECORDS_URL;
                break;
            case TYPE_CAR_SUGGESTION:
                title = getString(R.string.car_maintenance_title);
                url = Config.VW_MAINTENANCE_PAT_URL;
                break;
            case TYPE_CAR_ESTIMATE:
                title = getString(R.string.web_view_title_car_estimate);
                url = Config.VW_MAINTENANCE_BODY_AND_PAINT_URL;
                break;
            case TYPE_CAR_INSURANCE:
                title = getString(R.string.dashboard_car_insurance_title);
                url = Config.VW_MAINTENANCE_WARRANTY_URL;
                break;
            case TYPE_CAR_RECALL:
                title = getString(R.string.web_view_title_maintenance_recalls);
                url = Config.VW_MAINTENANCE_RECALL_URL;
                break;
            case TYPE_CAR_ADDITIONAL_QUOTATION:
                title = getString(R.string.web_view_title_car_additional_quotation);
                url = Config.VW_MAINTENANCE_ADDITIONAL_QUOTATION_URL;
                break;
            case TYPE_LINE_BINDING:
                title = getString(R.string.msg_binding_line);
                url = String.format(Config.VW_LINE_LIFF_URL, PreferenceManager.getInstance().getUserToken());
                break;
            case TYPE_OPEN_HUB:
                title = "";
                url = getIntent().getStringExtra(URL);
                break;
            case TYPE_ANY:
                title = getIntent().getStringExtra(TITLE);
                url = getIntent().getStringExtra(URL);
                break;
        }

        if (title.trim().replaceAll(" ", "").isEmpty()) {
            hideTitleBarUI();
        } else {
            showTitleBarUI();
            tvTitle.setText(title);
        }

        if (type == TYPE_ANY) {
            return;
        }

        String query = getIntent().getStringExtra(QUERY);

        if (query != null && !query.isEmpty()) {
            url = url + "?" + query;
        }

        //when
        // (1) query is empty
        // (2) query contain utm_xxx params
        // (3) url contain coupon
        // need add four params token / AccountId / PlateNumber / VIN
        // others needless add additional params
        if (query == null || query.isEmpty() || query.contains("utm_") || url.contains(VW_MEMBER_COUPON_URL)) {
            url = Utils.addDefaultParamsToUrl(url);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
        }
    }

    private void showTitleBarUI() {
        tvTitle.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
    }

    private void hideTitleBarUI() {
        tvTitle.setVisibility(View.GONE);
        btnBack.setVisibility(View.GONE);
    }
}
