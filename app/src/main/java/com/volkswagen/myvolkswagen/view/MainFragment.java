package com.volkswagen.myvolkswagen.view;

import static com.volkswagen.myvolkswagen.model.CarData.DONGLE_STATUS_USER_MAPPED;
import static com.volkswagen.myvolkswagen.model.CarData.DONGLE_STATUS_VIN_MAPPED;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarSummary;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.model.EventBus.TokenReadyEvent;
import com.volkswagen.myvolkswagen.model.ExtIdAuthResponse;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.model.NotificationData;
import com.volkswagen.myvolkswagen.model.NotificationItem;
import com.volkswagen.myvolkswagen.model.Profile;
import com.volkswagen.myvolkswagen.model.ServiceReminder;
import com.volkswagen.myvolkswagen.model.UserData;
import com.volkswagen.myvolkswagen.model.WarrantyData;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.adapter.CarMaintenanceAdapter;
import com.volkswagen.myvolkswagen.view.adapter.NewsHorizontalListAdapter;
import com.volkswagen.myvolkswagen.view.adapter.NewsListAdapter;
import com.volkswagen.myvolkswagen.view.bottomsheet.DongleNotificationBottomSheetDialog;
import com.volkswagen.myvolkswagen.view.notice.NoticeActivity;
import com.volkswagen.myvolkswagen.view.widget.GifView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment implements View.OnClickListener, NewsListAdapter.OnNewsClickListener, CarMaintenanceAdapter.OnMaintenanceItemClick, DongleNotificationBottomSheetDialog.DongleNotificationItemClickListener {
    private static final long DURATION_DONGLE_SWITCHER_TIME = 5000;
    private static final long DURATION_NOTIFICATION_SWITCH_TIME = 5000;

    private static MainFragment fragment;
    private ConstraintLayout layoutDongle;
    private Group groupHasCar, groupEmptyCar, groupDongleInfo, groupNoDongle, groupDongleNotification;
    private TextView tvNickName, tvCarType, tvCarTypeInfo, tvCarVin, tvCarVinDate, tvCarMaintainDate,
            tvCarMaintainCheck, tvCarIdentification, tvMyService, tvMyServiceNearby, tvAddCar,
            tvOdoMeter, tvOdoMeterUnit, tvGasPump, tvBattery, tvBatteryUnit, tvNoDongleHint;
    private TextSwitcher tvSwitcherNotification, tvSwitcherNotificationTime;
    private ImageView imgMemberLevel, imgMemberLevelWhiteGolden, imgCar, imgServiceHint;
    private GifView viewCarMaintenanceLoading, viewCarMaintenanceDateLoading;
    private View bgMemberLevel;
    private ViewPager pagerCarMaintenance;
    private TabLayout carMaintenanceIndicator;
    private CarMaintenanceAdapter carMaintenanceAdapter;
    private NewsHorizontalListAdapter newsHorizontalListAdapter;

    //notification text switcher
    private int currentIndex = -1;
    private Handler notificationHandler;
    private Runnable notificationRunnable;

    //maintain view pager
    private Handler pageChangeHandler;
    private Runnable pageChangeRunnable;

    private boolean isNoticeCountApiFinish = false;
    private boolean isMemberProfileApiFinish = false;
    private boolean isGetMyCarDataApiFinish = false;
    private boolean isGetNewsListFinish = false;

    private boolean userHasCar = false;

    public static MainFragment newInstance() {
        fragment = new MainFragment();

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, EventLog.EVENT_NAME_PAGE_VIEW, null, EventLog.EVENT_CATEGORY_PAGE_VIEW);
    }

    private void initView(View view) {
        layoutDongle = view.findViewById(R.id.layout_dongle_info);

        groupHasCar = view.findViewById(R.id.group_car_info);
        groupEmptyCar = view.findViewById(R.id.group_empty_car);
        groupDongleInfo = view.findViewById(R.id.group_dongle_info);
        groupNoDongle = view.findViewById(R.id.group_no_dongle);
        groupDongleNotification = view.findViewById(R.id.group_dongle_notification);

        TextView tvNoticeNews = view.findViewById(R.id.tv_notice_more_news);
        tvNoticeNews.setOnClickListener(this);

        RecyclerView recyclerNews = view.findViewById(R.id.recycler_view_news);
        recyclerNews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        newsHorizontalListAdapter = new NewsHorizontalListAdapter();
        recyclerNews.setAdapter(newsHorizontalListAdapter);
        newsHorizontalListAdapter.addNewsClickListener(this);

        View viewContactUs = view.findViewById(R.id.view_contact_us);
        viewContactUs.setOnClickListener(this);

        TextView tvContactUs = view.findViewById(R.id.tv_contact_us);
        tvContactUs.setOnClickListener(this);

        tvNickName = view.findViewById(R.id.tv_nickname_dashboard);
        tvCarType = view.findViewById(R.id.tv_car_type);
        tvCarTypeInfo = view.findViewById(R.id.tv_car_type_info);
        tvCarVin = view.findViewById(R.id.tv_car_vin);
        tvCarVinDate = view.findViewById(R.id.tv_car_vin_date);
        tvCarMaintainDate = view.findViewById(R.id.tv_car_maintain_date);
        tvCarMaintainCheck = view.findViewById(R.id.tv_car_maintain_check);
        tvCarMaintainCheck.setOnClickListener(this);
        tvCarIdentification = view.findViewById(R.id.tv_car_identification);
        tvMyService = view.findViewById(R.id.tv_my_service);
        tvMyServiceNearby = view.findViewById(R.id.tv_service_nearby);
        tvMyServiceNearby.setOnClickListener(this);
        tvAddCar = view.findViewById(R.id.btn_add_car);
        tvAddCar.setOnClickListener(this);
        tvOdoMeter = view.findViewById(R.id.tv_total_odometer_title);
        tvOdoMeterUnit = view.findViewById(R.id.tv_total_odometer_unit);
        tvGasPump = view.findViewById(R.id.tv_gas_pump_title);
        tvBattery = view.findViewById(R.id.tv_battery_title);
        tvBatteryUnit = view.findViewById(R.id.tv_battery_unit);
        tvNoDongleHint = view.findViewById(R.id.btn_no_dongle_hint);
        tvNoDongleHint.setOnClickListener(this);

        tvSwitcherNotification = view.findViewById(R.id.tv_switcher_notification);
        tvSwitcherNotificationTime = view.findViewById(R.id.tv_switcher_notification_time);

        tvSwitcherNotification.setFactory(() -> {
            TextView textView = new TextView(getContext());
            textView.setTextSize(15);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setTextColor(getContext().getResources().getColor(R.color.grey700));
            return textView;
        });

        tvSwitcherNotificationTime.setFactory(() -> {
            TextView textView = new TextView(getContext());
            textView.setTextSize(14);
            textView.setTextColor(getContext().getResources().getColor(R.color.grey500));
            return textView;
        });

        tvSwitcherNotification.setInAnimation(getContext(), R.anim.slide_in);
        tvSwitcherNotificationTime.setInAnimation(getContext(), R.anim.slide_in);
        tvSwitcherNotification.setOutAnimation(getContext(), R.anim.slide_out);
        tvSwitcherNotificationTime.setOutAnimation(getContext(), R.anim.slide_out);

        imgMemberLevel = view.findViewById(R.id.img_member_level);
        imgMemberLevelWhiteGolden = view.findViewById(R.id.img_empty_level_white_golden);

        imgCar = view.findViewById(R.id.img_car);
        imgCar.setOnClickListener(this);
        imgServiceHint = view.findViewById(R.id.img_service_hint);
        imgServiceHint.setOnClickListener(this);
        viewCarMaintenanceLoading = view.findViewById(R.id.gif_loading_car_maintenance);
        viewCarMaintenanceDateLoading = view.findViewById(R.id.gif_loading_maintain_date);

        bgMemberLevel = view.findViewById(R.id.bg_member_level);
        pagerCarMaintenance = view.findViewById(R.id.pager_car_maintenance);
        carMaintenanceIndicator = view.findViewById(R.id.layout_indicator);
        carMaintenanceAdapter = new CarMaintenanceAdapter(getContext());
        carMaintenanceAdapter.setOnMaintenanceItemClick(this);
        pagerCarMaintenance.setAdapter(carMaintenanceAdapter);
    }

    @Subscribe(sticky = true)
    public void onTokenReadyEvent(TokenReadyEvent event) {
        //api step
        //step1: getNoticeCount,getMemberProfile,getMyCarData,getNewsList <= show full screen loading
        //step2: postGetCruisysToken (after get myCarData finish)
        //step3: getDongleBinding, getServiceReminder, getWarranty, getCarSummary
        initMemberLevel();
        startCallStep1Api();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (notificationRunnable != null && notificationHandler != null) {
            notificationHandler.removeCallbacks(notificationRunnable);
            notificationRunnable = null;
        }

        if (pageChangeRunnable != null && pageChangeHandler != null) {
            pageChangeHandler.removeCallbacks(pageChangeRunnable);
            pageChangeRunnable = null;
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initMemberLevel() {
        int level = PreferenceManager.getInstance().getLevel();
        switch (level) {
            case UserData.MEMBER_LEVEL_SILVER:
                bgMemberLevel.setBackgroundResource(R.drawable.bg_member_level_silver);
                imgMemberLevel.setImageResource(R.drawable.ic_new_membership_sliver);
                break;
            case UserData.MEMBER_LEVEL_GOLD:
                bgMemberLevel.setBackgroundResource(R.drawable.bg_member_level_gold);
                imgMemberLevel.setImageResource(R.drawable.ic_new_membership_golden);
                break;
            case UserData.MEMBER_LEVEL_WHITE_GOLDEN:
                bgMemberLevel.setBackgroundResource(R.drawable.bg_member_level_white_golden);
                imgMemberLevel.setImageResource(R.drawable.ic_new_membership_platinum);
                break;
            case UserData.MEMBER_LEVEL_BLUE:
            default:
                bgMemberLevel.setBackgroundResource(R.drawable.bg_member_level_blue);
                imgMemberLevel.setImageResource(R.drawable.ic_new_membership_blue);
                break;
        }
    }

    public void startCallStep1Api() {
        isNoticeCountApiFinish = false;
        isMemberProfileApiFinish = false;
        isGetMyCarDataApiFinish = false;
        isGetNewsListFinish = false;

        //hide dongle info default, if state is USER_MAPPED or VIN_MAPPED will visible after step3 finish
        if (layoutDongle != null) {
            layoutDongle.setVisibility(View.GONE);
        }

        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        getNoticeCount();
        getMemberProfile();
        getMyCarData();
        getNewsList();
    }

    private void getNoticeCount() {
        Repository.getInstance().getNoticeCount(new Repository.OnApiCallback<Integer>() {
            @Override
            public void onSuccess(Integer integer) {
                isNoticeCountApiFinish = true;

                if (!isAdded()) {
                    return;
                }

                if (getActivity() != null && getActivity() instanceof MainNewActivity) {
                    ((MainNewActivity) getActivity()).setNotice(integer > 0);
                }

                finishCallStep1Api();
            }

            @Override
            public void onFailure(Throwable t) {
                isNoticeCountApiFinish = true;
                finishCallStep1Api();
            }
        });
    }

    private void getMemberProfile() {
        Repository.getInstance().getMemberProfile(new Repository.OnApiCallback<Profile>() {
            @Override
            public void onSuccess(Profile profile) {
                isMemberProfileApiFinish = true;
                if (!isAdded()) {
                    return;
                }

                finishCallStep1Api();

                tvNickName.setText(String.format(getString(R.string.dashboard_welcome), PreferenceManager.getInstance().getUserNickname()));
            }

            @Override
            public void onFailure(Throwable t) {
                isMemberProfileApiFinish = true;
                finishCallStep1Api();
            }
        });
    }

    private void getMyCarData() {
        Repository.getInstance().getMyCarDataList(new Repository.OnApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> carDataList) {
                isGetMyCarDataApiFinish = true;
                if (!isAdded()) {
                    return;
                }

                finishCallStep1Api();

                userHasCar = carDataList.size() > 0;
                groupHasCar.setVisibility(userHasCar ? View.VISIBLE : View.GONE);
                groupEmptyCar.setVisibility(userHasCar ? View.GONE : View.VISIBLE);

                int userMemberLevel = PreferenceManager.getInstance().getLevel();
                imgMemberLevelWhiteGolden.setVisibility(!userHasCar && userMemberLevel == UserData.MEMBER_LEVEL_WHITE_GOLDEN ? View.VISIBLE : View.GONE);

                for (CarData carData : carDataList) {
                    String mainCar = PreferenceManager.getInstance().getMainCarPlate();
                    if (carData.plate.equalsIgnoreCase(mainCar)) {
                        Picasso.get().load(carData.image).fit().centerInside().into(imgCar);

                        tvCarType.setText(carData.type);
                        tvCarTypeInfo.setText(carData.model);
                        tvCarVin.setText(carData.vin);
                        tvCarVinDate.setText(carData.ownerDate);
                        tvMyService.setText(carData.dealerPlantName);
                        tvCarIdentification.setOnClickListener(view -> {
                            if (getActivity() != null && getActivity() instanceof BaseActivity) {
                                ((BaseActivity) getActivity()).goCarIdentification(carData.plate, carData.vin, "");
                            }

                            Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "立即鑑價", null, EventLog.EVENT_CATEGORY_BUTTON);
                        });
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                isGetMyCarDataApiFinish = true;
                finishCallStep1Api();
            }
        });
    }

    private void getNewsList() {
        Repository.getInstance().getNewsList(new Repository.OnApiCallback<List<NewsData>>() {
            @Override
            public void onSuccess(List<NewsData> newsData) {
                isGetNewsListFinish = true;
                if (!isAdded()) {
                    return;
                }

                finishCallStep1Api();

                newsHorizontalListAdapter.setData(newsData);
            }

            @Override
            public void onFailure(Throwable t) {
                isGetNewsListFinish = true;
                finishCallStep1Api();
            }
        });
    }

    private void finishCallStep1Api() {
        if (isNoticeCountApiFinish && isMemberProfileApiFinish && isGetMyCarDataApiFinish && isGetNewsListFinish) {
            if (getActivity() != null && getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).hideLoading();
            }

            if (getActivity() != null && getActivity() instanceof MainNewActivity) {
                ((MainNewActivity) getActivity()).showNameAndNotice();
            }

            startCallStep2Api();
        }
    }

    private void startCallStep2Api() {
        Repository.getInstance().postGetCruisysToken(new Repository.OnApiCallback<ExtIdAuthResponse>() {
            @Override
            public void onSuccess(ExtIdAuthResponse extIdAuthResponse) {
                startCallStep3Api();
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    private void startCallStep3Api() {
        if (!userHasCar) {
            return;
        }

        viewCarMaintenanceLoading.setVisibility(View.VISIBLE);
        viewCarMaintenanceDateLoading.setVisibility(View.VISIBLE);

        Repository.getInstance().getDongleBinding(PreferenceManager.getInstance().getMainCarPlate(), new Repository.OnApiCallback<DongleBindingCarData>() {
            @Override
            public void onSuccess(DongleBindingCarData data) {
                if (!isAdded()) {
                    return;
                }

                switch (data.state) {
                    case DONGLE_STATUS_USER_MAPPED:
                        layoutDongle.setVisibility(View.VISIBLE);
                        groupDongleInfo.setVisibility(View.VISIBLE);
                        groupNoDongle.setVisibility(View.GONE);
                        break;
                    case DONGLE_STATUS_VIN_MAPPED:
                        layoutDongle.setVisibility(View.VISIBLE);
                        groupDongleInfo.setVisibility(View.GONE);
                        groupNoDongle.setVisibility(PreferenceManager.getInstance().isNeverShowDongleHint() ? View.GONE : View.VISIBLE);
                        break;
                    default:
                        layoutDongle.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (!isAdded()) {
                    return;
                }
                layoutDongle.setVisibility(View.GONE);
            }
        });

        Repository.getInstance().getServiceReminder(new Repository.OnApiCallback<List<ServiceReminder>>() {
            @Override
            public void onSuccess(List<ServiceReminder> serviceReminders) {
                if (!isAdded()) {
                    return;
                }

                viewCarMaintenanceLoading.setVisibility(View.GONE);
                carMaintenanceAdapter.setData(serviceReminders);
                if (serviceReminders.size() > 1) {
                    carMaintenanceIndicator.setVisibility(View.VISIBLE);
                    setupAutoPageChange();
                } else {
                    carMaintenanceIndicator.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        Repository.getInstance().getWarranty(new Repository.OnApiCallback<WarrantyData>() {
            @Override
            public void onSuccess(WarrantyData warrantyData) {
                if (!isAdded()) {
                    return;
                }

                viewCarMaintenanceDateLoading.setVisibility(View.GONE);
                tvCarMaintainDate.setText(warrantyData.homeExpirationDate);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        Repository.getInstance().getCarSummary(new Repository.OnApiCallback<CarSummary>() {
            @Override
            public void onSuccess(CarSummary summary) {
                if (!isAdded()) {
                    return;
                }

                NumberFormat format = NumberFormat.getNumberInstance();
                tvOdoMeter.setText(String.format(getString(R.string.dashboard_total_odometer_title), format.format(Math.round(summary.odometer.value))));
                tvOdoMeterUnit.setText(summary.odometer.unit);
                tvGasPump.setText(String.valueOf(Math.round(summary.fuelLevel.value)));
                tvBattery.setText(String.valueOf(Math.round(summary.controlModuleVoltage.value)));
                tvBatteryUnit.setText(summary.controlModuleVoltage.unit);
            }

            @Override
            public void onFailure(Throwable t) {
                if (!isAdded()) {
                    return;
                }

                tvOdoMeter.setText(String.format(getString(R.string.dashboard_total_odometer_title), "0"));
                tvOdoMeterUnit.setText("km");
                tvGasPump.setText("0");
                tvBattery.setText("0");
                tvBatteryUnit.setText("V");
            }
        });

        Repository.getInstance().getDongleNotification(new Repository.OnApiCallback<NotificationData>() {
            @Override
            public void onSuccess(NotificationData data) {
                if (!isAdded()) {
                    return;
                }

                if (data.notificationList != null && !data.notificationList.isEmpty()) {
                    setupAutoNotificationSwitcher(data.notificationList);
                    groupDongleNotification.setVisibility(View.VISIBLE);
                } else {
                    groupDongleNotification.setVisibility(View.GONE);
                }

                if (data.notificationDialogList != null && !data.notificationDialogList.isEmpty() && getContext() != null) {
                    DongleNotificationBottomSheetDialog dialog = new DongleNotificationBottomSheetDialog(getContext(), data.notificationDialogList, MainFragment.this);
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (!isAdded()) {
                    return;
                }

                groupDongleNotification.setVisibility(View.GONE);
            }
        });
    }

    private void setupAutoNotificationSwitcher(List<NotificationItem> list) {
        notificationHandler = new Handler();
        notificationRunnable = new Runnable() {
            @Override
            public void run() {
                currentIndex = (currentIndex + 1) % list.size();
                tvSwitcherNotification.setText(list.get(currentIndex).description);
                tvSwitcherNotificationTime.setText(Utils.timeAgo(list.get(currentIndex).eventTime, "yyyy/MM/dd HH:mm"));
                notificationHandler.postDelayed(this, DURATION_DONGLE_SWITCHER_TIME);
            }
        };

        notificationHandler.postDelayed(notificationRunnable, DURATION_DONGLE_SWITCHER_TIME);
    }

    private void setupAutoPageChange() {
        pageChangeHandler = new Handler();
        pageChangeRunnable = new Runnable() {
            @Override
            public void run() {
                int nextPage = pagerCarMaintenance.getCurrentItem() + 1;
                if (nextPage >= carMaintenanceAdapter.getCount()) {
                    nextPage = 0;
                }
                pagerCarMaintenance.setCurrentItem(nextPage, true);
                pageChangeHandler.postDelayed(this, DURATION_NOTIFICATION_SWITCH_TIME);
            }
        };

        pageChangeHandler.postDelayed(pageChangeRunnable, DURATION_NOTIFICATION_SWITCH_TIME);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_notice_more_news:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goNotice(NoticeActivity.TYPE_NEWS);
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "最新消息查看更多", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.img_service_hint:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).showServiceCenterBottomSheetDialog();
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "我的服務中心介紹", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.tv_service_nearby:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goMyServiceNearby("");
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "鄰近服務中心", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.view_contact_us:
            case R.id.tv_contact_us:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).showContactUsBottomSheetDialog();
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "聯絡我們", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.tv_car_maintain_check:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goCarInsurance("");
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "愛車保固", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.btn_add_car:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goAddCar();
                }
                break;
            case R.id.img_car:
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).goMainCarDongle();
                }

                Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "查看車圖", null, EventLog.EVENT_CATEGORY_BUTTON);
                break;
            case R.id.btn_no_dongle_hint:
                PreferenceManager.getInstance().neverShowDongleHint();
                layoutDongle.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClickNews(NewsData news) {
        if (news.is_link == 1) {
            if (news.link != null && !news.link.isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.link));
                startActivity(intent);
            }
        } else {
            NoticeActivity.goNewsDetailDirect(getActivity(), new ArrayList<>(news.gallery), news.title, news.content, news.published_at);
        }

        Repository.getInstance().sendPatEventLog(EventLog.EVENT_PV_HOME, "最新消息詳細_" + news.id, null, EventLog.EVENT_CATEGORY_BUTTON);
    }

    @Override
    public void onButtonClick(ServiceReminder reminder) {
        if (reminder.serviceName != null && reminder.serviceName.length() > 0) {
            Repository.getInstance().sendPatEventLog(reminder.serviceName, reminder.eventName, null, EventLog.EVENT_CATEGORY_BUTTON);
        }

        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).handleDeeplink(reminder.btnUrl);
        }
    }

    @Override
    public void onDongleNotificationButtonClick(String deeplink) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).handleDeeplink(deeplink);
        }
    }
}
