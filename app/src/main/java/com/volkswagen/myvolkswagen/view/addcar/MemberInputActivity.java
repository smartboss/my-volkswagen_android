package com.volkswagen.myvolkswagen.view.addcar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.BindingLineFragment;
import com.volkswagen.myvolkswagen.view.MainNewActivity;

public class MemberInputActivity extends BaseActivity implements View.OnClickListener {
    public static final int STATE_NONE = -1;
    public static final int STATE_MEMBER_INFO = 0;
    public static final int STATE_ADD_CAR = 1;
    public static final int STATE_ADD_CAR_TYPE = 2;
    public static final int STATE_DONGLE_BIND = 3;
    public static final int STATE_NO_DONGLE = 4;
    public static final int STATE_BIND_LINE = 5;

    private int currentState = STATE_MEMBER_INFO;

    private TextView tvTitle;
    private ImageView imgState;
    private Button btnNext, btnSkip;
    private ScrollView scrollView;

    public static void goMemberInput(final Activity activity) {
        Intent intent = new Intent(activity, MemberInputActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getData() != null) {
            String url = intent.getData().toString();
            intent.setData(null);
            handleDeeplink(url);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_input);

        tvTitle = findViewById(R.id.tv_title);
        imgState = findViewById(R.id.img_state);
        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
        btnSkip = findViewById(R.id.btn_skip);
        btnSkip.setOnClickListener(this);
        ImageButton btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        scrollView = findViewById(R.id.scroll_view);

        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, MemberInputFragment.getInstance()).commitAllowingStateLoss();
    }

    public void setCurrentState(int state) {
        currentState = state;

        switch (state) {
            case STATE_MEMBER_INFO:
                tvTitle.setText(getString(R.string.member_profile_title));
                imgState.setImageResource(R.drawable.ic_auth_step2);
                btnNext.setText(getString(R.string.btn_next));
                btnSkip.setVisibility(View.GONE);
                break;
            case STATE_ADD_CAR:
            case STATE_ADD_CAR_TYPE:
            case STATE_DONGLE_BIND:
                tvTitle.setText(getString(R.string.mycar_storage_msg_add_car));
                imgState.setImageResource(R.drawable.ic_auth_step3);
                btnNext.setText(getString(R.string.btn_next));
                btnSkip.setVisibility(View.VISIBLE);
                break;
            case STATE_NO_DONGLE:
                tvTitle.setText(getString(R.string.mycar_storage_msg_add_car));
                imgState.setImageResource(R.drawable.ic_auth_step3);
                btnNext.setText(getString(R.string.btn_next));
                btnSkip.setVisibility(View.GONE);
                break;
            case STATE_BIND_LINE:
                tvTitle.setText(getString(R.string.msg_binding_account_title));
                imgState.setImageResource(R.drawable.ic_auth_step4);
                btnNext.setText(getString(R.string.msg_binding_line));
                btnSkip.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void scrollToTop() {
        scrollView.scrollTo(0, 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_next:
                switch (currentState) {
                    case STATE_MEMBER_INFO:
                    case STATE_ADD_CAR:
                    case STATE_ADD_CAR_TYPE:
                    case STATE_DONGLE_BIND:
                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);
                        if (fragment instanceof AddCarControlListener) {
                            ((AddCarControlListener) fragment).onNextClick();
                        }
                        break;
                    case STATE_NO_DONGLE:
                        goLineBindingFragment();
                        break;
                    case STATE_BIND_LINE:
                        goLineBinding();
                        break;
                }

                break;
            case R.id.btn_skip:
                //when skip dongle binding,need go bind line
                //else go Main
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);
                if (fragment instanceof DongleBindingFragment) {
                    goLineBindingFragment();
                    return;
                }

                MainNewActivity.goMain(MemberInputActivity.this);
                break;
        }
    }

    public void goLineBindingFragment() {
        getSupportFragmentManager()
                .beginTransaction().replace(R.id.layout_frame, BindingLineFragment.getInstance(), "")
                .addToBackStack("")
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (currentState == STATE_NONE || currentState == STATE_MEMBER_INFO) {
            return;
        }

        super.onBackPressed();
    }
}
