package com.volkswagen.myvolkswagen.view;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.CarTrips;
import com.volkswagen.myvolkswagen.model.CarTripsResponseData;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.model.TripData;
import com.volkswagen.myvolkswagen.utils.Logger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

public class ViewTripsPagerAdapter extends RecyclerView.Adapter<ViewTripsPagerAdapter.ViewHolder> {
    private Context context;
    private List<TripData> tripDataList;

    private final OnItemClickListener listener;
    public ViewTripsPagerAdapter(CarTripsResponseData tripsData, Context c, OnItemClickListener listener) {
        if(this.tripDataList == null) {
            this.tripDataList = new ArrayList<>();
        }
        if( tripsData != null) {
            for( int i=0; i<tripsData.tripDataList.size(); i++) {
                this.tripDataList.add( tripsData.tripDataList.get(i));
            }
        }
        this.listener = listener;
        this.context = c;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_trip_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if( this.tripDataList == null) {
            return;
        }
//        Logger.v(this, "ViewTripsPagerAdapter onBindViewHolder, position: " +  position);

        TripData data = this.tripDataList.get(position);
        int miles = 0;
        try {
            miles = Integer.parseInt(data.distance);
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        String milesString = String.valueOf(miles/1000 );
        holder.text_distance.setText(milesString);
        holder.text_start_time.setText(data.start_time);
        if( holder.hideStatus > 0 ) {
            holder.viewStartLocation.setVisibility(View.GONE);
            holder.viewDivision.setVisibility(View.GONE);
            holder.viewEndLocation.setVisibility(View.GONE);
            holder.viewSpeedDriveTime.setVisibility(View.GONE);
        }

        holder.bind( data, position, listener, this.context );

//        if( holder.hideStatus > 0 ) {
//            holder.viewStartLocation.setVisibility(View.GONE);
//            holder.viewDivision.setVisibility(View.GONE);
//            holder.viewEndLocation.setVisibility(View.GONE);
//            holder.viewSpeedDriveTime.setVisibility(View.GONE);
//            return;
//        } else {
//            holder.viewStartLocation.setVisibility(View.VISIBLE);
//            holder.viewDivision.setVisibility(View.VISIBLE);
//            holder.viewEndLocation.setVisibility(View.VISIBLE);
//            holder.viewSpeedDriveTime.setVisibility(View.VISIBLE);
//
//
//            Geocoder geocoder = new Geocoder(this.context, Locale.getDefault());
//
//            String startLocation = data.start_location.event_time;
//            try {
//                List<Address> addresses = geocoder.getFromLocation(data.start_location.latitude, data.start_location.longitude, 1);
//                if (addresses != null && addresses.size() > 0) {
//                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                    startLocation = data.start_location.event_time + "  " + address;
//                }
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//
//            holder.text_start_location_time_address.setText(startLocation);
//
//            String endLocation = data.end_location.event_time;
//            try {
//                List<Address> addresses = geocoder.getFromLocation(data.end_location.latitude, data.end_location.longitude, 1);
//                if (addresses != null && addresses.size() > 0) {
//                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                    endLocation = data.end_location.event_time + "  " + address;
//                }
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//
//            holder.text_end_location_time_address.setText(endLocation);
//
//            String maxSpeed = "最高速度 " + data.max_speed + " kmph";
//            holder.text_max_speed.setText(maxSpeed);
//
//            String driveTime = "行駛時間 " + data.driving_time + " 分鐘";
//            holder.text_drive_time.setText(driveTime);
//        }
    }

    @Override
    public int getItemCount() {
        if( this.tripDataList != null ) {
//            Logger.v(this, "ViewTripsPagerAdapter getItemCount: " +  this.tripDataList.size());
            return this.tripDataList.size();
        } else {
            return 0;
        }
    }

    public void updateTrips(CarTripsResponseData tripsData) {
        if(this.tripDataList == null) {
            this.tripDataList = new ArrayList<>();
        } else {
            this.tripDataList.clear();
        }
        for( int i=0; i<tripsData.tripDataList.size(); i++) {
            this.tripDataList.add( tripsData.tripDataList.get(i));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_start_time;
        TextView text_distance;
        TextView text_start_location_time_address;
        TextView text_end_location_time_address;
        TextView text_max_speed;
        TextView text_drive_time;
        View viewStartLocation, viewEndLocation, viewDivision, viewSpeedDriveTime;
        ImageButton btnStatusIcon;
        int hideStatus = 1;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_start_time = itemView.findViewById(R.id.trip_info_start_time);
            text_distance = itemView.findViewById(R.id.view_holder_trip_info_distance);
            text_start_location_time_address = itemView.findViewById(R.id.trip_info_start_location_time_address);
            text_end_location_time_address = itemView.findViewById(R.id.trip_info_end_location_time_address);
            text_max_speed = itemView.findViewById(R.id.trip_info_max_speed);
            text_drive_time = itemView.findViewById(R.id.trip_info_drive_time);
            viewStartLocation = itemView.findViewById(R.id.trip_info_start_location);
            viewEndLocation = itemView.findViewById(R.id.trip_info_end_location);
            viewDivision = itemView.findViewById(R.id.trip_info_location_division);
            viewSpeedDriveTime = itemView.findViewById(R.id.trip_info_speed_drivetime);
            btnStatusIcon = itemView.findViewById(R.id.trip_info_status_icon);
        }


        public void bind(TripData data, int position, ViewTripsPagerAdapter.OnItemClickListener listen, Context context ) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Logger.v(this, "hideStatus: " +  hideStatus + ",position:" + position);
                    hideStatus = 1 - hideStatus;
                    Logger.v(this, "hideStatus2: " +  hideStatus + ",position:" + position);
//                    listen.onItemClick(position);
//                    notifyDataSetChanged();
                    if( hideStatus > 0 ) {
                        viewStartLocation.setVisibility(View.GONE);
                        viewDivision.setVisibility(View.GONE);
                        viewEndLocation.setVisibility(View.GONE);
                        viewSpeedDriveTime.setVisibility(View.GONE);
                        btnStatusIcon.setImageResource(R.drawable.ic_new_down);
                    } else {
                        viewStartLocation.setVisibility(View.VISIBLE);
                        viewDivision.setVisibility(View.VISIBLE);
                        viewEndLocation.setVisibility(View.VISIBLE);
                        viewSpeedDriveTime.setVisibility(View.VISIBLE);
                        btnStatusIcon.setImageResource(R.drawable.ic_new_up);


                        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                        String startLocation = data.start_location.event_time;
                        try {
                            List<Address> addresses = geocoder.getFromLocation(data.start_location.latitude, data.start_location.longitude, 1);
                            if (addresses != null && addresses.size() > 0) {
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                startLocation = data.start_location.event_time + "  " + address;
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        text_start_location_time_address.setText(startLocation);

                        String endLocation = data.end_location.event_time;
                        try {
                            List<Address> addresses = geocoder.getFromLocation(data.end_location.latitude, data.end_location.longitude, 1);
                            if (addresses != null && addresses.size() > 0) {
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                endLocation = data.end_location.event_time + "  " + address;
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        text_end_location_time_address.setText(endLocation);

                        String maxSpeed = "最高速度 " + data.max_speed + " kmph";
                        text_max_speed.setText(maxSpeed);

                        String driveTime = "行駛時間 " + data.driving_time + " 分鐘";
                        text_drive_time.setText(driveTime);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
