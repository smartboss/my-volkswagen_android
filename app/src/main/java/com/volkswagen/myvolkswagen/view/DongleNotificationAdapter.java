package com.volkswagen.myvolkswagen.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.NotificationDialogItem;

import java.util.ArrayList;
import java.util.List;

public class DongleNotificationAdapter extends PagerAdapter {
    private Context context;
    private List<NotificationDialogItem> list = new ArrayList<>();

    public DongleNotificationAdapter(Context context, List<NotificationDialogItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pager_dongle_notification, null);

        TextView tvDescription = view.findViewById(R.id.tv_notification_description);
        tvDescription.setText(list.get(position).description);
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}