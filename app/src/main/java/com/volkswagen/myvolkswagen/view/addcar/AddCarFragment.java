package com.volkswagen.myvolkswagen.view.addcar;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_ADD_CAR;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.view.BaseActivity;
import com.volkswagen.myvolkswagen.view.bottomsheet.VinHintBottomSheetDialog;

public class AddCarFragment extends Fragment implements AddCarControlListener, View.OnClickListener {
    public static final String SHOW_TOP_HINT_WORDING = "show_top_hint_wording";

    private EditText etCarOwner, etCarPlateFront, etCarPlateEnd, etCarVin;
    private TextView tvHint, tvPlateError, tvVinError;
    private ImageView imgVinHint;

    public static AddCarFragment getInstance(boolean showHintWording) {
        AddCarFragment fragment = new AddCarFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_TOP_HINT_WORDING, showHintWording);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_car, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        etCarOwner = view.findViewById(R.id.et_car_owner);
        String carOwnerName = PreferenceManager.getInstance().getCarOwnerName();
        String nickname = PreferenceManager.getInstance().getUserNickname();

        if (carOwnerName != null && !carOwnerName.isEmpty()) {
            etCarOwner.setText(carOwnerName);
            etCarOwner.setKeyListener(null);
        } else {
            etCarOwner.setText(nickname);
        }

        etCarPlateFront = view.findViewById(R.id.et_car_plate_front);
        etCarPlateFront.setFilters(new InputFilter[]{
                new InputFilter.AllCaps()
        });

        etCarPlateEnd = view.findViewById(R.id.et_car_plate_end);
        etCarPlateEnd.setFilters(new InputFilter[]{
                new InputFilter.AllCaps()
        });

        etCarVin = view.findViewById(R.id.et_vin);

        tvHint = view.findViewById(R.id.tv_hint);
        if (getArguments() != null && getArguments().containsKey(SHOW_TOP_HINT_WORDING)) {
            tvHint.setVisibility(getArguments().getBoolean(SHOW_TOP_HINT_WORDING, true) ? View.VISIBLE : View.GONE);
        }

        tvPlateError = view.findViewById(R.id.add_car_plate_error);
        tvVinError = view.findViewById(R.id.add_car_vin_error);
        imgVinHint = view.findViewById(R.id.img_vin_hint);
        imgVinHint.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_ADD_CAR);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }

    @Override
    public void onNextClick() {
        if (isInputValid()) {
            String ownerName = etCarOwner.getText().toString().replaceAll(" ", "");
            String carPlateFront = etCarPlateFront.getText().toString().replaceAll(" ", "");
            String carPlateEnd = etCarPlateEnd.getText().toString().replaceAll(" ", "");
            String carVin = etCarVin.getText().toString().replaceAll(" ", "");

            checkCarExist(ownerName, carPlateFront + "-" + carPlateEnd, carVin);
        }
    }

    private void checkCarExist(String name, String plate, String vin) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().postGetCarExist(name, plate, vin, new Repository.OnApiCallback<CarExistData>() {
            @Override
            public void onSuccess(CarExistData data) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                if (data.result) {
                    if (getParentFragment() != null) {
                        getParentFragment()
                                .getChildFragmentManager()
                                .beginTransaction()
                                .replace(R.id.layout_child_frame, AddCarTypeFragment.getInstance(name, plate, vin, data.id, data.name), "")
                                .addToBackStack("")
                                .commitAllowingStateLoss();
                    }
                } else {
                    tvPlateError.setVisibility(View.VISIBLE);
                    switch (data.errorCode) {
                        case 3001:
                            tvPlateError.setText(R.string.add_car_step1_error_3001);
                            break;
                        case 3003:
                            tvPlateError.setText(R.string.add_car_step1_error_3003);
                            break;
                        case 3004:
                            tvPlateError.setText(R.string.add_car_step1_error_3004);
                            break;
                        case 3005:
                        case 3009:
                            tvPlateError.setText(R.string.add_car_step1_error_3005_3009);
                            break;
                        case 3010:
                            tvPlateError.setText(R.string.add_car_step1_error_3010);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                tvPlateError.setVisibility(View.VISIBLE);
                tvPlateError.setText(t.getMessage());
            }
        });
    }

    private boolean isInputValid() {
        String ownerName = etCarOwner.getText().toString().replaceAll(" ", "");
        String carPlateFront = etCarPlateFront.getText().toString().replaceAll(" ", "");
        String carPlateEnd = etCarPlateEnd.getText().toString().replaceAll(" ", "");
        String carVin = etCarVin.getText().toString().replaceAll(" ", "");

        if (ownerName.isEmpty()) {
            etCarOwner.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
            Toast.makeText(getContext(), getString(R.string.add_car_step1_owner_hint), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            etCarOwner.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
        }

        if (carPlateFront.isEmpty()) {
            etCarPlateFront.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
            tvPlateError.setVisibility(View.VISIBLE);
            tvPlateError.setText(getString(R.string.msg_car_plate_error));
            return false;
        } else {
            etCarPlateFront.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
            tvPlateError.setVisibility(View.GONE);
            tvPlateError.setText("");
        }

        if (carPlateEnd.isEmpty()) {
            etCarPlateEnd.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
            tvPlateError.setVisibility(View.VISIBLE);
            tvPlateError.setText(getString(R.string.msg_car_plate_error));
            return false;
        } else {
            etCarPlateEnd.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
            tvPlateError.setVisibility(View.GONE);
            tvPlateError.setText("");
        }

        if (carVin.isEmpty() || carVin.length() < 5) {
            etCarVin.setBackgroundResource(R.drawable.rec_white_corner_with_red50_stroke_8);
            tvVinError.setVisibility(View.VISIBLE);
            tvVinError.setText(getString(R.string.msg_car_vin_error));
            return false;
        } else {
            etCarVin.setBackgroundResource(R.drawable.rec_white_corner_with_gray_stroke_8);
            tvVinError.setVisibility(View.GONE);
            tvVinError.setText("");
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_vin_hint:
                if (getActivity() != null) {
                    VinHintBottomSheetDialog bottomSheetDialog = new VinHintBottomSheetDialog(getActivity());
                    bottomSheetDialog.show();
                }
                break;
        }
    }
}
