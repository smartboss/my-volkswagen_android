package com.volkswagen.myvolkswagen.view.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.Utils;

public class OptionsLinearLayout extends LinearLayout {

    LayoutInflater mInflater;
    Context mContext;

    public OptionsLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mInflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addOption(String text, OnClickListener callback) {

        int interval = Utils.dp2px(getResources(), 5);
        int paddingV = Utils.dp2px(getResources(), 6);
        int paddingH = Utils.dp2px(getResources(), 10);
        int screenWidth = Utils.getScreenWidth();
        int screenHeight = Utils.getScreenHeight();

//        CardView option = (CardView) mInflater.inflate(R.layout.layout_chatbot_option_card,null);
//        ((LinearLayout.LayoutParams)option.getLayoutParams()).setMargins(0, interval, 0, interval);
//
//        TextView option_text = option.findViewById(R.id.chatbot_option_textview);
//        option_text.setText(text);

//        addView(option);

        TextView option = new TextView(mContext);
        option.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ((LinearLayout.LayoutParams)option.getLayoutParams()).setMargins(0, interval, 0, interval);
        option.setPadding(paddingH, paddingV, paddingH, paddingV);
        option.setTextSize(15);
        option.setText(text);
        option.setOnClickListener(callback);
        option.setTextColor(getResources().getColor(R.color.black1));
        option.setGravity(Gravity.CENTER);
        option.measure(screenWidth, screenHeight);
        option.setBackgroundResource(R.drawable.chatbot_option_background);
//        textv.setTag(item.id);

        addView(option);
    }

}
