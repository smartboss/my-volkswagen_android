package com.volkswagen.myvolkswagen.view;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import com.volkswagen.myvolkswagen.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SplashFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        Button btnNext = view.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(view1 -> {
            if (getActivity() != null) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new OnBoardingFragment()).commitAllowingStateLoss();
            }
        });

        Uri uri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.home);
        VideoView videoView = view.findViewById(R.id.videoView);
        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(mp -> mp.setVolume(0, 0));
        videoView.start();

        videoView.setOnCompletionListener(mediaPlayer -> {
            // 当视频播放结束时重新开始
            videoView.start();
        });

    }
}
