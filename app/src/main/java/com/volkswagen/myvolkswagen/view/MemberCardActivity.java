package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import com.google.android.material.tabs.TabLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;
import com.volkswagen.myvolkswagen.model.RedeemCodeResponse;
import com.volkswagen.myvolkswagen.model.RedeemData;
import com.volkswagen.myvolkswagen.model.RedeemInput;
import com.volkswagen.myvolkswagen.model.RedeemList;
import com.volkswagen.myvolkswagen.model.RedeemPointListResponse;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.Config;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class MemberCardActivity extends BaseActivity implements LineRedeemBottomSheet.LineRedeemPresenter {
    public static final String TAG = MemberCardActivity.class.getSimpleName();

    public static final int TAB_LEVEL = 0;
    public static final int TAB_TASK_LIST = 1;
    public static final int TAB_REDEEM = 2;

    private int point = 0;

    private TabLayout tabLayout;
    private RecyclerView recyclerTasks, recyclerPoints;
    private TextView tvTotalPoint;
    private LineRedeemBottomSheet lineRedeemBottomSheet;

    private TasksRecyclerAdapter taskAdapter;
    private PointsRecyclerAdapter pointAdapter;

    private ApiService mApiService;

    public static void goMemberCard(final Activity activity, final int tab) {
        final Intent intent = new Intent(activity, MemberCardActivity.class);
        intent.putExtra(TAG, tab);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_card);
        initView();

        int tab = getIntent().getIntExtra(TAG, TAB_LEVEL);
        tabLayout.getTabAt(tab).select();
    }

    private void initView() {
        //api service
        mApiService = ApiClientManager.getClient().create(ApiService.class);

        //toolbar view
        tabLayout = findViewById(R.id.member_tab);
        tabLayout.addOnTabSelectedListener(onTabSelectedListener);

        //member level view
        TextView tvMemberCardId = findViewById(R.id.member_card_id);
        tvMemberCardId.setText(PreferenceManager.getInstance().getAccountId());

        TextView tvMemberCardLevel = findViewById(R.id.member_card_level);
        tvMemberCardLevel.setText(PreferenceManager.getInstance().getLevelName());

        ImageView imgMemberCard = findViewById(R.id.member_card_image);
        int level = PreferenceManager.getInstance().getLevel();

        if (level <= 0) {
            imgMemberCard.setVisibility(View.INVISIBLE);
        } else if (level == 1) {
            imgMemberCard.setImageResource(R.drawable.member_card_1);
        } else if (level == 2) {
            imgMemberCard.setImageResource(R.drawable.member_card_2);
        } else {
            imgMemberCard.setImageResource(R.drawable.member_card_3);
        }

        //tasks list
        recyclerTasks = findViewById(R.id.recycler_task_list);
        recyclerTasks.setLayoutManager(new LinearLayoutManager(this));
        recyclerTasks.setHasFixedSize(true);
        taskAdapter = new TasksRecyclerAdapter();
        recyclerTasks.setAdapter(taskAdapter);

        //points list
        tvTotalPoint = findViewById(R.id.tv_point);
        setPoint(0);
        recyclerPoints = findViewById(R.id.recycler_points);
        recyclerPoints.setLayoutManager(new LinearLayoutManager(this));
        recyclerPoints.setHasFixedSize(true);
        pointAdapter = new PointsRecyclerAdapter();
        recyclerPoints.setAdapter(pointAdapter);
    }

    private void setPoint(int point) {
        this.point = point;
        tvTotalPoint.setText(Integer.toString(point));
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch (tab.getPosition()) {
                case 0:
                    setTabView(TAB_LEVEL);
                    break;
                case 1:
                    setTabView(TAB_TASK_LIST);
                    break;
                case 2:
                    setTabView(TAB_REDEEM);
                    break;
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void setTabView(int tabType) {
        Group groupLevel = findViewById(R.id.group_member_level);
        Group groupPoint = findViewById(R.id.group_points);

        switch (tabType) {
            case TAB_LEVEL:
                groupLevel.setVisibility(View.VISIBLE);
                recyclerTasks.setVisibility(View.GONE);
                groupPoint.setVisibility(View.GONE);
                break;
            case TAB_TASK_LIST:
                groupLevel.setVisibility(View.GONE);
                recyclerTasks.setVisibility(View.VISIBLE);
                recyclerTasks.scrollToPosition(0);
                groupPoint.setVisibility(View.GONE);
                getRedeemRecords();
                break;
            case TAB_REDEEM:
                groupLevel.setVisibility(View.GONE);
                recyclerTasks.setVisibility(View.GONE);
                groupPoint.setVisibility(View.VISIBLE);
                recyclerPoints.scrollToPosition(0);
                getRedeemPoints();
                break;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.member_card_level_container:
                WebViewActivity.goVWMemberLevel(this);
                break;
            case R.id.member_card_faq_container:
                WebViewActivity.goVWMemberFaq(this);
                break;
            case R.id.member_card_back_btn:
                onBackPressed();
                break;
            case R.id.tv_point_info:
                WebViewActivity.goVWLinePointInfo(this);
                break;
        }
    }

    @Override
    public void onExchange(RedeemData redeemData) {
        lineRedeemBottomSheet.dismiss();
        exchangeRedeem(redeemData);
    }

    @Override
    public void onCopy(RedeemData redeemData, String code) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", code);
        clipboard.setPrimaryClip(clipData);
        copyRedeemCode(redeemData);
    }

    @Override
    public void goLine() {
        lineRedeemBottomSheet.dismiss();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Config.VW_LINE_POINT_REDEEM));
        startActivity(intent);
    }

    private void getRedeemRecords() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        mApiService.getRedeemRecords(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<RedeemList>() {
            @Override
            public void onResponse(Call<RedeemList> call, retrofit2.Response<RedeemList> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    taskAdapter.setList(response.body().redeems);
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<RedeemList> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void exchangeRedeem(RedeemData data) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        mApiService.redeemExchange(new RedeemInput(data.redeemID), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Response>() {

            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    Toast.makeText(MemberCardActivity.this, "已兌換", Toast.LENGTH_SHORT).show();
                    tabLayout.getTabAt(TAB_REDEEM).select();
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                hud.dismiss();
                Toast.makeText(MemberCardActivity.this, "兌換失敗", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getRedeemPoints() {
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        mApiService.getRedeemPointList(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<RedeemPointListResponse>() {
            @Override
            public void onResponse(Call<RedeemPointListResponse> call, retrofit2.Response<RedeemPointListResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    setPoint(response.body().totalPoints);
                    pointAdapter.setList(response.body().redeems, new ValueCallback<RedeemData>() {
                        @Override
                        public void onReceiveValue(RedeemData value) {
                            generateRedeemLineCode(value);
                        }
                    });
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<RedeemPointListResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void generateRedeemLineCode(final RedeemData redeemData) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        mApiService.redeemGetLineCode(new RedeemInput(redeemData.redeemID), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<RedeemCodeResponse>() {
            @Override
            public void onResponse(Call<RedeemCodeResponse> call, retrofit2.Response<RedeemCodeResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    if (redeemData.canGetPoint()) {
                        redeemData.status = 2;
                        setPoint(point + Integer.parseInt(redeemData.point));
                    }

                    String code = "";
                    lineRedeemBottomSheet = LineRedeemBottomSheet.newInstance(LineRedeemBottomSheet.TYPE_COPY_CODE);
                    lineRedeemBottomSheet.setRedeemData(redeemData, code);
                    lineRedeemBottomSheet.addPresenter(MemberCardActivity.this);
                    lineRedeemBottomSheet.show(getSupportFragmentManager(), null);
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<RedeemCodeResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void copyRedeemCode(RedeemData redeemData) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(this);
        mApiService.redeemCopy(new RedeemInput(redeemData.redeemID), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    Toast.makeText(MemberCardActivity.this, "代碼已複製", Toast.LENGTH_SHORT).show();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    class TasksRecyclerAdapter extends RecyclerView.Adapter<TasksRecyclerAdapter.TasksRecordHolder> {
        private ArrayList<RedeemData> list = new ArrayList<>();

        @NonNull
        @Override
        public TasksRecordHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task_record, parent, false);
            return new TasksRecordHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull TasksRecordHolder holder, int position) {
            final RedeemData data = list.get(position);
            holder.setRedeem(data);
            holder.btnExchange.setOnClickListener(data.canExchange() ? new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lineRedeemBottomSheet = LineRedeemBottomSheet.newInstance(LineRedeemBottomSheet.TYPE_EXCHANGE);
                    lineRedeemBottomSheet.setRedeemData(data);
                    lineRedeemBottomSheet.addPresenter(MemberCardActivity.this);
                    lineRedeemBottomSheet.show(getSupportFragmentManager(), null);
                }
            } : null);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void setList(ArrayList<RedeemData> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        class TasksRecordHolder extends RecyclerView.ViewHolder {
            private TextView tvTitle, tvTime, tvDeadLine;
            ImageButton btnExchange;

            TasksRecordHolder(@NonNull View itemView) {
                super(itemView);
                tvTitle = itemView.findViewById(R.id.tv_title);
                tvTime = itemView.findViewById(R.id.tv_time);
                tvDeadLine = itemView.findViewById(R.id.tv_deadline);
                btnExchange = itemView.findViewById(R.id.btn_exchange);
            }

            void setRedeem(RedeemData redeem) {
                tvTitle.setText(redeem.name);
                tvTime.setText(redeem.getTime);
                tvDeadLine.setText(String.format(itemView.getResources().getString(R.string.exchange_deadline), redeem.redeemExpired));

                if (redeem.canExchange()) {
                    tvTitle.setTextColor(getResources().getColor(R.color.black1));
                    tvTime.setTextColor(getResources().getColor(R.color.brown_grey));
                    tvDeadLine.setTextColor(getResources().getColor(R.color.brown_grey));
                    btnExchange.setImageResource(R.drawable.btn_exchange_small);
                } else {
                    btnExchange.setImageResource(R.drawable.already_exchange);
                    tvTitle.setTextColor(getResources().getColor(R.color.black3));
                    tvTime.setTextColor(getResources().getColor(R.color.brown_grey6));
                    tvDeadLine.setTextColor(getResources().getColor(R.color.brown_grey6));
                }
            }
        }
    }

    class PointsRecyclerAdapter extends RecyclerView.Adapter<PointsRecyclerAdapter.PointsRecordHolder> {
        private ArrayList<RedeemData> list = new ArrayList<>();
        private ValueCallback<RedeemData> onPointClick;

        @NonNull
        @Override
        public PointsRecordHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_member_point, parent, false);
            return new PointsRecordHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final PointsRecordHolder holder, final int position) {
            final RedeemData data = list.get(position);
            holder.setRedeem(data);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onPointClick != null) {
                        holder.setIsChangePointView();
                        onPointClick.onReceiveValue(data);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void setList(ArrayList<RedeemData> list, ValueCallback<RedeemData> onPointClick) {
            this.list = list;
            this.onPointClick = onPointClick;
            notifyDataSetChanged();
        }

        class PointsRecordHolder extends RecyclerView.ViewHolder {
            private TextView tvTitle, tvTime, tvDeadLine, tvPoint;

            PointsRecordHolder(@NonNull View itemView) {
                super(itemView);
                tvTitle = itemView.findViewById(R.id.tv_title);
                tvTime = itemView.findViewById(R.id.tv_time);
                tvDeadLine = itemView.findViewById(R.id.tv_deadline);
                tvPoint = itemView.findViewById(R.id.tv_point);
            }

            void setRedeem(RedeemData redeem) {
                tvTitle.setText(redeem.name);
                tvTime.setText(redeem.getTime);
                tvDeadLine.setText(String.format(itemView.getResources().getString(R.string.point_deadline), redeem.receiveExpired));
                tvPoint.setText(redeem.point);

                if (redeem.canGetPoint()) {
                    setUnChangePointView();
                } else {
                    setIsChangePointView();
                }
            }

            void setUnChangePointView() {
                tvTitle.setTextColor(getResources().getColor(R.color.black1));
                tvTime.setTextColor(getResources().getColor(R.color.brown_grey));
                tvDeadLine.setTextColor(getResources().getColor(R.color.brown_grey));
                tvPoint.setTextColor(getResources().getColor(R.color.black1));
                tvPoint.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_point, 0, 0, 0);
            }

            void setIsChangePointView() {
                tvTitle.setTextColor(getResources().getColor(R.color.black3));
                tvTime.setTextColor(getResources().getColor(R.color.brown_grey6));
                tvDeadLine.setTextColor(getResources().getColor(R.color.brown_grey6));
                tvPoint.setTextColor(getResources().getColor(R.color.brown_grey));
                tvPoint.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_point_grey, 0, 0, 0);
            }
        }
    }
}
