package com.volkswagen.myvolkswagen.view.notice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.view.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class NoticeActivity extends BaseActivity implements View.OnClickListener {
    private static final String TYPE = "type";
    public static final int TYPE_NOTICE = 0;
    public static final int TYPE_NEWS = 1;
    public static final int TYPE_NEWS_DIRECT = 2;

    private static String LIST = "list";
    private static String TITLE = "title";
    private static String CONTENT = "content";
    private static String DATE = "date";

    private TextView tvTitle;

    public static void goNotice(final Activity activity, int type) {
        Intent intent = new Intent(activity, NoticeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void goNewsDetailDirect(final Activity activity, ArrayList<String> imageList, String title, String content, String date) {
        Intent intent = new Intent(activity, NoticeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, TYPE_NEWS_DIRECT);
        bundle.putStringArrayList(LIST, imageList);
        bundle.putString(TITLE, title);
        bundle.putString(CONTENT, content);
        bundle.putString(DATE, date);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_back);
        tvTitle = findViewById(R.id.tv_view_title);

        ImageButton btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        int type = TYPE_NOTICE;
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(TYPE)) {
            type = getIntent().getExtras().getInt(TYPE, TYPE_NOTICE);
        }

        if (type == TYPE_NEWS_DIRECT) {
            goNoticeDetail(getIntent().getExtras());
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, NoticeListFragment.newInstance(type), null).commitAllowingStateLoss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);
                if (currentFragment instanceof NoticeDetailFragment) {
                    int type = ((NoticeDetailFragment) currentFragment).getCurrentType();
                    Repository.getInstance().sendPatEventLog(type == TYPE_NOTICE ? EventLog.EVENT_PV_NOTICE_DETAIL : EventLog.EVENT_PV_NEWS_DETAIL, "上一頁", null, EventLog.EVENT_CATEGORY_BUTTON);
                } else if (currentFragment instanceof NoticeListFragment) {
                    int type = ((NoticeListFragment) currentFragment).getCurrentType();
                    Repository.getInstance().sendPatEventLog(type == TYPE_NOTICE ? EventLog.EVENT_PV_NOTICE : EventLog.EVENT_PV_NEWS, "上一頁", null, EventLog.EVENT_CATEGORY_BUTTON);
                }

                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment != null) {
            transaction.show(currentFragment);
            setTitle(getString(R.string.notice_tab_notice_title));
        }
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void goNoticeDetail(int type, int kind, List<String> imageList, String title, String content, String date, String accountId, boolean needAddBackStack) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.layout_frame);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        transaction.add(R.id.layout_frame, NoticeDetailFragment.newInstance(type, kind, new ArrayList<>(imageList), title, content, date, accountId), null);

        if (needAddBackStack) {
            //if go news detail direct, needless to add back stack
            transaction.addToBackStack("");
        }
        transaction.commitAllowingStateLoss();
    }

    private void goNoticeDetail(Bundle bundle) {
        ArrayList<String> imageList = new ArrayList<>();
        String title = "";
        String content = "";
        String date = "";

        if (bundle != null) {
            if (bundle.containsKey(LIST)) {
                imageList = bundle.getStringArrayList(LIST);
            }

            if (bundle.containsKey(TITLE)) {
                title = bundle.getString(TITLE, "");
            }

            if (bundle.containsKey(CONTENT)) {
                content = bundle.getString(CONTENT, "");
            }

            if (bundle.containsKey(DATE)) {
                date = bundle.getString(DATE, "");
            }
        }

        goNoticeDetail(TYPE_NEWS, -1, imageList, title, content, date, "", false);
    }
}
