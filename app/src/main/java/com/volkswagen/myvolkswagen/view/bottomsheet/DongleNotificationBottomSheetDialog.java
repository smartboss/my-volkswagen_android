package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.NotificationDialogItem;
import com.volkswagen.myvolkswagen.view.DongleNotificationAdapter;

import java.util.ArrayList;
import java.util.List;

public class DongleNotificationBottomSheetDialog extends BottomSheetDialog {
    private List<NotificationDialogItem> list = new ArrayList<>();
    private final DongleNotificationItemClickListener listener;

    public DongleNotificationBottomSheetDialog(@NonNull Context context, List<NotificationDialogItem> list, DongleNotificationItemClickListener listener) {
        super(context, R.style.BaseBottomSheetDialog);
        this.list = list;
        this.listener = listener;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_dongle_notification, null);
        setContentView(view);

        ViewPager viewPager = view.findViewById(R.id.view_pager_dongle_notification);
        DongleNotificationAdapter adapter = new DongleNotificationAdapter(getContext(), list);
        viewPager.setAdapter(adapter);

        TabLayout indicator = view.findViewById(R.id.layout_indicator);
        indicator.setupWithViewPager(viewPager);

        ImageView btnClose = view.findViewById(R.id.img_close);
        btnClose.setOnClickListener(view1 -> dismiss());

        TextView btnDongleNotification = view.findViewById(R.id.btn_dongle_notification);
        btnDongleNotification.setOnClickListener(view12 -> {
            if (listener != null) {
                int position = viewPager.getCurrentItem();
                listener.onDongleNotificationButtonClick(list.get(position).btnUrl);
                dismiss();
            }
        });
    }

    public interface DongleNotificationItemClickListener {
        void onDongleNotificationButtonClick(String deeplink);
    }
}