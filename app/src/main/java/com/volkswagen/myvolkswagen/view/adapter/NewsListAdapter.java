package com.volkswagen.myvolkswagen.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsListViewHolder> {
    private List<NewsData> list = new ArrayList<>();
    private OnNewsClickListener listener;

    @NonNull
    @Override
    public NewsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_news, parent, false);
        return new NewsListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsListViewHolder holder, int position) {
        NewsData data = list.get(position);

        holder.tvTitle.setText(data.title);
        holder.tvContent.setText(data.description);
        holder.tvTime.setText(Utils.formatVWDate(data.published_at));
        if (!data.cover_image.isEmpty()) {
            Picasso.get().load(data.cover_image).fit().centerInside().into(holder.imgBanner);
        } else {
            holder.imgBanner.setImageResource(0);
        }

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onClickNews(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addNewsClickListener(OnNewsClickListener listener) {
        this.listener = listener;
    }

    public void setData(List<NewsData> newsList) {
        list.clear();
        list.addAll(newsList);
        notifyDataSetChanged();
    }

    public static class NewsListViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvContent, tvTime;
        private ImageView imgBanner;

        public NewsListViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_view_holder_title);
            tvContent = itemView.findViewById(R.id.tv_view_holder_content);
            tvTime = itemView.findViewById(R.id.tv_view_holder_time);
            imgBanner = itemView.findViewById(R.id.view_holder_image);
        }
    }

    public interface OnNewsClickListener {
        void onClickNews(NewsData news);
    }
}
