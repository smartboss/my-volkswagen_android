package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volkswagen.myvolkswagen.R;

public class ServiceCenterBottomSheetDialog extends BottomSheetDialog {
    private final ServiceCenterListener serviceCenterListener;

    public ServiceCenterBottomSheetDialog(@NonNull Context context, ServiceCenterListener serviceCenterListener) {
        super(context, R.style.BaseBottomSheetDialog);
        this.serviceCenterListener = serviceCenterListener;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_service_center, null);
        setContentView(view);

        FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        TextView tvSettingServiceCenter = view.findViewById(R.id.tv_setting_service_center);
        tvSettingServiceCenter.setOnClickListener(view1 -> {
            if (serviceCenterListener != null) {
                serviceCenterListener.onSettingServiceCenterClick();
            }

            dismiss();
        });
    }

    public interface ServiceCenterListener {
        void onSettingServiceCenterClick();
    }
}