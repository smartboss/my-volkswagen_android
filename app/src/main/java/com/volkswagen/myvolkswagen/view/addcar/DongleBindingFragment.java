package com.volkswagen.myvolkswagen.view.addcar;

import static com.volkswagen.myvolkswagen.view.addcar.MemberInputActivity.STATE_DONGLE_BIND;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.utils.Utils;
import com.volkswagen.myvolkswagen.view.BaseActivity;

public class DongleBindingFragment extends Fragment implements AddCarControlListener, View.OnClickListener {
    public static final String CONST_PLATE_NO = "plateNo";
    public static final String CONST_MODEL = "model";

    private String plateNo = "";
    private String model = "";

    private OnDongleBindingListener dongleBindingListener;
    private EditText etImei;
    private Button btnScanQRCode;

    public static DongleBindingFragment getInstance(String plateNo, String model) {
        DongleBindingFragment fragment = new DongleBindingFragment();

        Bundle bundle = new Bundle();
        bundle.putString(CONST_PLATE_NO, plateNo);
        bundle.putString(CONST_MODEL, model);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dongle_binding, container, false);
        initView(view);

        if (getArguments() != null && getArguments().containsKey(CONST_PLATE_NO)) {
            plateNo = getArguments().getString(CONST_PLATE_NO, "");
        }

        if (getArguments() != null && getArguments().containsKey(CONST_MODEL)) {
            model = getArguments().getString(CONST_MODEL, "");
        }
        return view;
    }

    private void initView(View view) {
        etImei = view.findViewById(R.id.et_imei);
        btnScanQRCode = view.findViewById(R.id.btn_scan_imei);
        btnScanQRCode.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
            ((MemberInputActivity) getActivity()).setCurrentState(STATE_DONGLE_BIND);
            ((MemberInputActivity) getActivity()).scrollToTop();
        }
    }

    public void setDongleBindingListener(OnDongleBindingListener dongleBindingListener) {
        this.dongleBindingListener = dongleBindingListener;
    }

    @Override
    public void onNextClick() {
        String imei = etImei.getText().toString();
        if (imei.length() == 15) {
            bindCarDongle(plateNo, imei, model);
        } else {
            Toast.makeText(getContext(), getString(R.string.msg_imei_length_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void bindCarDongle(String carPlateNo, String imei, String model) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoading();
        }

        Repository.getInstance().postDongleBinding(carPlateNo, imei, model, new Repository.OnApiCallback<DongleBindingCarData>() {
            @Override
            public void onSuccess(DongleBindingCarData dongleBindingCarData) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                //login flow
                if (getActivity() != null && getActivity() instanceof MemberInputActivity) {
                    ((MemberInputActivity) getActivity()).goLineBindingFragment();
                    return;
                }

                //Popup flow in MyCarActivity
                if (dongleBindingListener != null) {
                    dongleBindingListener.onDongleBinding();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null && getActivity() instanceof BaseActivity) {
                    ((BaseActivity) getActivity()).hideLoading();
                }

                Toast.makeText(getContext(), getString(R.string.msg_binding_error), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_scan_imei:
                if (Utils.checkPermissions(getContext(), Manifest.permission.CAMERA)) {
                    openScanQRCode();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Utils.REQUEST_PERMISSIONS_CAMERA);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Utils.REQUEST_PERMISSIONS_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openScanQRCode();
                } else {
                    Toast.makeText(getContext(), getString(R.string.camera_permission_denied), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void openScanQRCode() {
        if (getContext() == null) {
            return;
        }

        GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder().setBarcodeFormats(Barcode.FORMAT_QR_CODE, Barcode.FORMAT_AZTEC).build();
        GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(getContext(), options);
        scanner.startScan().addOnSuccessListener(barcode -> {
            String result = barcode.getRawValue();
            etImei.setText(result);
        });
    }

    public interface OnDongleBindingListener {
        void onDongleBinding();
    }
}
