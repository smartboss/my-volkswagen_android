package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.os.Bundle;

public class ErrorDialogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ErrorDialogBox errorDialog = new ErrorDialogBox(this);

        String errorDetails = getIntent().getStringExtra("error_details");
        if (errorDetails == null) {
            errorDetails = "未知錯誤";
        }

        errorDialog.setTitle("錯誤");
        errorDialog.setDescription(errorDetails);
        errorDialog.show();
    }
}
