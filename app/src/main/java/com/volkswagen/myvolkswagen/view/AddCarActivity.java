package com.volkswagen.myvolkswagen.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.ApiClientManager;
import com.volkswagen.myvolkswagen.controller.ApiService;
import com.volkswagen.myvolkswagen.model.AddCarData;

public class AddCarActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_NICKNAME = "nick_name";

    public static final String BACK_MY_CAR_VIEW = "BACK_MY_CAR_VIEW";
    private static final String ARG_PENDING_DATA = "arg_pending_data";

    private AddCarData mData;
    private ApiService mApiService;

    public AddCarData GetCarData() { return mData; }

    public ApiService GetApiService(){ return  mApiService; }

    public final static void goAddCar(final Activity activity, final String nickname, boolean backMyCar) {
        final Intent intent = new Intent(activity, AddCarActivity.class);
        intent.putExtra(KEY_NICKNAME, nickname);
        intent.putExtra(BACK_MY_CAR_VIEW, backMyCar);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mApiService = ApiClientManager.getClient().create(ApiService.class);
        mData = new AddCarData();
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_frame, new AddCarStep1()).commitAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {

    }
}
