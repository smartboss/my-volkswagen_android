package com.volkswagen.myvolkswagen.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.volkswagen.myvolkswagen.R;

public class CarPlantMessageBottomSheet extends BottomSheetDialogFragment {
    private static CarPlantMessageBottomSheet fragment;
    private BottomSheetDialog mDialog;

    @Override
    public int getTheme() {
        return R.style.AppTheme_BottomSheetDialog;
    }

    public static CarPlantMessageBottomSheet newInstance() {
        if (fragment == null) {
            fragment = new CarPlantMessageBottomSheet();
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialog = (BottomSheetDialog) getDialog();
        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(mDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet));
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_HIDDEN);
                                break;
                            case BottomSheetBehavior.STATE_HIDDEN:
                                mDialog.cancel();
                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            }
        });

        return inflater.inflate(R.layout.bottom_sheet_car_plant_message, container, false);
    }
}