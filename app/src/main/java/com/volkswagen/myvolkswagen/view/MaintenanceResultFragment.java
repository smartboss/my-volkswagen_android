package com.volkswagen.myvolkswagen.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.Repository;
import com.volkswagen.myvolkswagen.controller.ValueCallback;
import com.volkswagen.myvolkswagen.list.PatMaintenanceResultAdapter;
import com.volkswagen.myvolkswagen.model.MaintenancePlant;
import com.volkswagen.myvolkswagen.model.PatContent;

// TODO: 2024/1/14 remove it
public class MaintenanceResultFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = MaintenanceResultFragment.class.getSimpleName();

    private Button btnLastStep, btnSave, btnReservation;
    private RecyclerView recyclerView;
    private PatMaintenanceResultAdapter adapter;
    private PatContent patContent;

    public static MaintenanceResultFragment getInstance(PatContent content) {
        MaintenanceResultFragment fragment = new MaintenanceResultFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PatContent.class.getSimpleName(), content);
        fragment.setArguments(bundle);

        return fragment;
    }

    private MaintenanceResultFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_result, container, false);
        if (getArguments() != null && getArguments().containsKey(PatContent.class.getSimpleName())) {
            patContent = (PatContent) getArguments().getSerializable(PatContent.class.getSimpleName());
            initView(view);
        }

        return view;
    }

    private void initView(View view) {
        btnLastStep = view.findViewById(R.id.btn_last);
        btnLastStep.setOnClickListener(this);
        btnSave = view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnReservation = view.findViewById(R.id.btn_reservation);
        btnReservation.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PatMaintenanceResultAdapter();
        adapter.setList(patContent.data);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_last:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
            case R.id.btn_save:
                Repository.getInstance().sendPatNotice(getContext(), patContent.patId, new ValueCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        new AlertDialog.Builder(getContext())
                                .setMessage(R.string.maintenance_notice_send_hint)
                                .setPositiveButton(R.string.close_button, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }
                });
                break;
            case R.id.btn_reservation:
                Repository.getInstance().getMemberCarMaintenancePlant(getContext(), patContent.vin, patContent.carNumber, new ValueCallback<MaintenancePlant>() {
                    @Override
                    public void onSuccess(MaintenancePlant maintenancePlant) {
                        if (maintenancePlant.plantId >= 0 && !maintenancePlant.address.isEmpty() && getActivity() != null) {
                            ((CarMaintenanceActivity) getActivity()).goMaintenanceAreaFragment(maintenancePlant);
                        } else {
                            WebViewActivity.goVWCarMaintenance(getActivity(), maintenancePlant.url);
                        }
                    }
                });
                break;
        }
    }
}
