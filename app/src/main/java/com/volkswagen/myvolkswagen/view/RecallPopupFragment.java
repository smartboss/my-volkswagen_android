package com.volkswagen.myvolkswagen.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.utils.Const;

public class RecallPopupFragment extends Fragment implements View.OnClickListener {
    private static RecallPopupFragment fragment;
    private ImageButton btnClose;
    private WebView webView;

    public static RecallPopupFragment getInstance(String url) {
        if (fragment == null) {
            fragment = new RecallPopupFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putString(Const.URL, url);
        fragment.setArguments(bundle);

        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_recall_popup, container, false);
        btnClose = view.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);
        view.findViewById(R.id.view_top).setOnClickListener(this);
        view.findViewById(R.id.view_bottom).setOnClickListener(this);

        webView = view.findViewById(R.id.webview);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());

        if (getArguments() != null && getArguments().containsKey(Const.URL)) {
            webView.loadUrl(getArguments().getString(Const.URL, ""));
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                break;
            default:
                break;
        }
    }
}
