package com.volkswagen.myvolkswagen.view.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.Notice;

public class NoticeManagerBottomSheetDialog extends BottomSheetDialog {
    private Notice notice;
    private final NoticeManagerListener noticeManagerListener;

    public NoticeManagerBottomSheetDialog(@NonNull Context context, Notice notice, NoticeManagerListener noticeManagerListener) {
        super(context, R.style.BaseBottomSheetDialog);
        this.notice = notice;
        this.noticeManagerListener = noticeManagerListener;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.sheetdialog_notice_manager, null);
        setContentView(view);

        TextView tvNoticeDelete = view.findViewById(R.id.btn_notice_delete);
        TextView tvNoticeSetting = view.findViewById(R.id.btn_notice_setting);

        tvNoticeDelete.setOnClickListener(view1 -> {
            if (noticeManagerListener != null) {
                noticeManagerListener.onNoticeDelete(notice);
            }

            dismiss();
        });

        tvNoticeSetting.setOnClickListener(view1 -> {
            if (noticeManagerListener != null) {
                noticeManagerListener.onNoticeSettingClick();
            }

            dismiss();
        });
    }

    public interface NoticeManagerListener {
        void onNoticeDelete(Notice notice);

        void onNoticeSettingClick();
    }
}