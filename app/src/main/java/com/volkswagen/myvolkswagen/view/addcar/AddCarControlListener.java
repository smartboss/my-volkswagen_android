package com.volkswagen.myvolkswagen.view.addcar;

public interface AddCarControlListener {
    void onNextClick();
}
