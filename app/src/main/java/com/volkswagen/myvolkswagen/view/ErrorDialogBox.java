package com.volkswagen.myvolkswagen.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.volkswagen.myvolkswagen.R;

public class ErrorDialogBox extends Dialog {

    private TextView titleTextView;
    private TextView descriptionTextView;
    private Button confirmButton;

    public ErrorDialogBox(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.error_dialogbox);

        titleTextView = findViewById(R.id.tv_title);
        descriptionTextView = findViewById(R.id.tv_description);
        confirmButton = findViewById(R.id.btn_confirm);

        setupListeners();
    }

    private void setupListeners() {
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle the confirm button click here
                dismiss();
            }
        });
    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    public void setDescription(String description) {
        descriptionTextView.setText(description);
    }
}
