package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ReportNewsInput {

    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("ReportItem")
    public String reportType;

    public ReportNewsInput(int newsId, String reportType) {
        this.newsId = newsId;
        this.reportType = reportType;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** ReportNewsInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("reportType=" + reportType + "\n")
                .append("********************")
                .toString();
    }
}
