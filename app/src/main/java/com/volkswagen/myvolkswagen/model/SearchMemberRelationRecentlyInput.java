package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchMemberRelationRecentlyInput {
    @SerializedName("RecentAccountIDList")
    public List<String> list;

    public SearchMemberRelationRecentlyInput(List<String> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** SearchMemberRelationRecentlyInput *****\n")
                .append("list=" + list + "\n")
                .append("********************")
                .toString();
    }
}
