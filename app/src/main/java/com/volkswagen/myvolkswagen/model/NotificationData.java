package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationData {
    @SerializedName("notification_list")
    public List<NotificationItem> notificationList;

    @SerializedName("notification_dialog_list")
    public List<NotificationDialogItem> notificationDialogList;
}
