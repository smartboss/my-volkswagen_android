package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class PushData extends Response {
    @SerializedName("Device")
    public String device;
    @SerializedName("PushToken")
    public String token;

    public PushData(String device, String token) {
        this.device = device;
        this.token = token;
    }
}
