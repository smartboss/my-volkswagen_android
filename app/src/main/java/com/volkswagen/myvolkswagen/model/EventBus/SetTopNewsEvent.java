package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

public class SetTopNewsEvent {

    private int clubId;

    public SetTopNewsEvent(@NonNull int clubId) {
        this.clubId = clubId;
    }

    public int getClubId() {
        return clubId;
    }
}
