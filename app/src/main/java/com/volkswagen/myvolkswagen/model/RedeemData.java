package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class RedeemData {
    @SerializedName("RedeemID")
    public int redeemID = 0;
    @SerializedName("Name")
    public String name = "";
    @SerializedName("Point")
    public String point = "";
    @SerializedName("GetTime")
    public String getTime = "";
    @SerializedName("RedeemExpired")
    public String redeemExpired = "";
    @SerializedName("ReceiveExpired")
    public String receiveExpired = "";
    @SerializedName("Status")
    public int status = 0;

    public boolean canExchange() {
        return status == 1;
    }

    public boolean canGetPoint() {
        return status == 1;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** RedeemData *****\n")
                .append("redeemID=" + redeemID + "\n")
                .append("name=" + name + "\n")
                .append("getTime=" + getTime + "\n")
                .append("redeemExpired=" + redeemExpired + "\n")
                .append("status=" + status + "\n")
                .append("********************")
                .toString();
    }
}
