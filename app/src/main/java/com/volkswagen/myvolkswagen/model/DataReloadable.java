package com.volkswagen.myvolkswagen.model;

public interface DataReloadable {
    public void loadData();
}
