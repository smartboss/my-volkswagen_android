package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Profile extends Response {
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Level")
    public int level;
    @SerializedName("LevelName")
    public String levelName;
    @SerializedName("OwnModel")
    public List<String> carModels;
    @SerializedName("Nickname")
    public String nickname;
    @SerializedName("Sex")
    public String gender;
    @SerializedName("Brief")
    public String brief;
    @SerializedName("OTPFlag")
    public int otpFlag;
    @SerializedName("EMail")
    public String email;
    @SerializedName("Cellphone")
    public String mobile;
    @SerializedName("Birthday")
    public String birthday;
    @SerializedName("StickerURL")
    public String headUrl;
    @SerializedName("CoverURL")
    public String coverUrl;
    @SerializedName("FollowAmount")
    public int followingNum;
    @SerializedName("FollowerAmount")
    public int followerNum;
    @SerializedName("Offical")
    public int official;
    @SerializedName("DealerAreaId")
    public String dealerAreaId;
    @SerializedName("DealerAreaName")
    public String dealerAreaName;
    @SerializedName("DealerPlantId")
    public String dealerPlantId;
    @SerializedName("DealerPlantName")
    public String dealerPlantName;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** Proflie *****\n")
                .append("accountId=" + accountId + "\n")
                .append("level=" + level + "\n")
                .append("levelName=" + levelName + "\n")
                .append("carModels=" + carModels + "\n")
                .append("nickname=" + nickname + "\n")
                .append("gender=" + gender + "\n")
                .append("brief=" + brief + "\n")
                .append("email=" + email + "\n")
                .append("mobile=" + mobile + "\n")
                .append("birthday=" + birthday + "\n")
                .append("headUrl=" + headUrl + "\n")
                .append("coverUrl=" + coverUrl + "\n")
                .append("followNum=" + followingNum + "\n")
                .append("followerNum=" + followerNum + "\n")
                .append("official=" + official + "\n")
                .append("dealerAreaId=" + dealerAreaId + "\n")
                .append("dealerAreaName=" + dealerAreaName + "\n")
                .append("dealerPlantId=" + dealerPlantId + "\n")
                .append("dealerPlantName=" + dealerPlantName + "\n")
                .append("********************")
                .toString();
    }
}
