package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class InboxContentInput {
    @SerializedName("NewsID")
    public int newsId;

    public InboxContentInput(int newsId) {
        this.newsId = newsId;
    }
}
