package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WarrantyData {
    @SerializedName("data")
    public List<WarrantyProduct> productList;

    @SerializedName("home_expiration_date")
    public String homeExpirationDate;
}
