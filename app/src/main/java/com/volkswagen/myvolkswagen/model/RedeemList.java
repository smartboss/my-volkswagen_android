package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RedeemList extends Response {
    @SerializedName("DataList")
    public ArrayList<RedeemData> redeems;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** RedeemList *****\n")
                .append("redeems=" + redeems + "\n")
                .append("********************")
                .toString();
    }
}
