package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyCar {
    @SerializedName("VIN")
    public String vin;
    @SerializedName("CarData")
    public List<CarData> carData;
    @SerializedName("CarMaintainData")
    public List<List<CarMaintainData>> maintainData;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder().append("***** MyCar *****\n");
        if (vin != null) {
            sb.append(vin);
        }
        if (carData != null) {
            sb.append(carData).append("\n");
        }
        if (maintainData != null) {
            sb.append(maintainData).append("\n");
        }
        sb.append("********************");
        return sb.toString();
    }
}
