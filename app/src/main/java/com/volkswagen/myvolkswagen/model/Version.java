package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class Version extends Response {
    @SerializedName("Version")
    public String version;
    @SerializedName("IsUpdate")
    public boolean isUpdate;
    @SerializedName("Message")
    public String message;
    @SerializedName("URL")
    public String url;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** Version *****\n")
                .append("version=" + version + "\n")
                .append("isUpdate=" + isUpdate + "\n")
                .append("message=" + message + "\n")
                .append("url=" + url + "\n")
                .append("********************")
                .toString();
    }
}
