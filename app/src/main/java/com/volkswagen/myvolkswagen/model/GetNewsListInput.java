package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetNewsListInput {
    public static final String TYPE_ALL = "1";
    public static final String TYPE_FOLLOW = "2";

    @SerializedName("Kind")
    public String kind;
    @SerializedName("ModelList")
    public String modelList;
    @SerializedName("Page")
    public int page;

    public GetNewsListInput(String kind, String modelList, int page) {
        this.kind = kind;
        this.modelList = modelList;
        this.page = page;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** GetNewsListInput *****\n")
                .append("kind=" + kind + "\n")
                .append("modelList=" + modelList + "\n")
                .append("page=" + page + "\n")
                .append("********************")
                .toString();
    }
}
