package com.volkswagen.myvolkswagen.model.EventBus;

import com.volkswagen.myvolkswagen.model.PendingAddCarData;

public class CancelAddCarEvent {

    private PendingAddCarData mData;

    public CancelAddCarEvent(PendingAddCarData data) {
        mData = data;
    }

    public PendingAddCarData getData() {
        return mData;
    }
}
