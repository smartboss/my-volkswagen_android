package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

/*
* this event is used to notify postFragment WHO_SELF + KIND_SELF_COLLECTION
* that bookmark is updated
* */

public class UpdateBookmarkStatusEvent {

    private int mNewsId;
    private boolean mBookmarkStatus;

    public UpdateBookmarkStatusEvent(@NonNull int newsId, boolean bookmarkStatus)
    {
        mNewsId = newsId;
        mBookmarkStatus = bookmarkStatus;
    }
    public int getNewsId() {
        return mNewsId;
    }

    public boolean getBookMarkStatus() {
        return mBookmarkStatus;
    }
}
