package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ProfileRedeem extends Response {
    @SerializedName("RedeemTaskCount")
    public int redeemTaskCount;
    @SerializedName("RedeemTotalPoints")
    public int redeemTotalPoints;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** ProfileRedeem *****\n")
                .append("RedeemTaskCount=" + redeemTaskCount + "\n")
                .append("RedeemTotalPoints=" + redeemTotalPoints + "\n")
                .append("********************")
                .toString();
    }
}
