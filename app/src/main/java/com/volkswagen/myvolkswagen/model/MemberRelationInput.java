package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class MemberRelationInput {
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Who")
    public String who;
    @SerializedName("Kind")
    public String kind;

    public MemberRelationInput(String accountId, String who, String kind) {
        this.accountId = accountId;
        this.who = who;
        this.kind = kind;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** MemberRelationInput *****\n")
                .append("accountId=" + accountId + "\n")
                .append("who=" + who + "\n")
                .append("kind=" + kind + "\n")
                .append("********************")
                .toString();
    }
}
