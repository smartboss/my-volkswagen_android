package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarModelData extends Response {
    @SerializedName("CarModel")
    public List<CarModel> models;
}
