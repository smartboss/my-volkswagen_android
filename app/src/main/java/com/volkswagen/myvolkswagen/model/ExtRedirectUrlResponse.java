package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ExtRedirectUrlResponse {
    @SerializedName("data")
    public ExtRedirectUrlData extRedirectUrlData;
}
