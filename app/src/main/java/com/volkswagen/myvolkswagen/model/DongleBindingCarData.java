package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DongleBindingCarData extends Response {
    @SerializedName("car_id")
    public String carId = "";

    @SerializedName("vin")
    public String vin = "";

    @SerializedName("imei")
    public String imei = "";

    @SerializedName("model")
    public String model = "";

    @SerializedName("state")
    public String state = "";

    @SerializedName("email")
    public String email = "";

    @SerializedName("first_name")
    public String firstName = "";

    @SerializedName("id")
    public String id = "";

    @SerializedName("last_name")
    public String lastName = "";

    @SerializedName("phone")
    public String phone = "";

    @SerializedName("user_mapped_time")
    public String userMappedTime = "";

    @SerializedName("redirect_url")
    public String redirectUrl = "";
}
