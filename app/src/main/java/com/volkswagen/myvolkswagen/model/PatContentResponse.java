package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class PatContentResponse extends Response{
    @SerializedName("PAT")
    public PatContent patContent;
}
