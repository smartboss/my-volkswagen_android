package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class Comment {
    @SerializedName("CommentID")
    public int commentId;
    @SerializedName("Content")
    public String content;
    @SerializedName("PDate")
    public String date;
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Level")
    public int level;
    @SerializedName("Nickname")
    public String nickename;
    @SerializedName("StickerURL")
    public String stickerURL;
    @SerializedName("Offical")
    public int official;
    @SerializedName("IsFollower")
    public boolean isFollower;
    @SerializedName("IsFollow")
    public boolean isFollow;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** Comment *****\n")
                .append("commentId=" + commentId + "\n")
                .append("content=" + content + "\n")
                .append("date=" + date + "\n")
                .append("accountId=" + accountId + "\n")
                .append("level=" + level + "\n")
                .append("nickename=" + nickename + "\n")
                .append("stickerURL=" + stickerURL + "\n")
                .append("official=" + official + "\n")
                .append("isFollower=" + isFollower + "\n")
                .append("isFollow=" + isFollow + "\n")
                .append("********************")
                .toString();
    }
}
