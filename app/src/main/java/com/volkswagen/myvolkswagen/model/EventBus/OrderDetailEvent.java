package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

import com.volkswagen.myvolkswagen.model.CarMaintainData;

public class OrderDetailEvent {
    private CarMaintainData maintainData;

    public OrderDetailEvent(@NonNull CarMaintainData maintainData) {
        this.maintainData = maintainData;
    }

    public CarMaintainData getMaintainData() {
        return maintainData;
    }
}
