package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class NotificationItem {
    @SerializedName("event_time")
    public String eventTime;

    @SerializedName("description")
    public String description;
}
