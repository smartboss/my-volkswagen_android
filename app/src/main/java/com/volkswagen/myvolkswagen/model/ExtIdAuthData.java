package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExtIdAuthData {
    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("account_id")
    public String accountId;
    @SerializedName("name")
    public String name;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("email")
    public String email;
    @SerializedName("gender")
    public String gender;
    @SerializedName("level")
    public int level;
    @SerializedName("preferred_workshop")
    public MaintenancePlantCruisys workshop;
    @SerializedName("cars")
    public List<CarDataCruisys> carList;
    @SerializedName("default_available_booking_days")
    public int bookingDays;
}
