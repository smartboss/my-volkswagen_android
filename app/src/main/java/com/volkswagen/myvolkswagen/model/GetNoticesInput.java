package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class GetNoticesInput {
    @SerializedName("Kind")
    public String kind;

    public GetNoticesInput(String kind) {
        this.kind = kind;
    }
}
