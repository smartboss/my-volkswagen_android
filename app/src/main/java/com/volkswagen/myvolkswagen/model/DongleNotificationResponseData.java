package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DongleNotificationResponseData {
    @SerializedName("data")
    public NotificationData data;
}
