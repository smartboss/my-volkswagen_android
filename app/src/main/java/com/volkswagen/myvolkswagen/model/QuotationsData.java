package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class QuotationsData {
    @SerializedName("_id")
    public String _id;
    @SerializedName("service_ticket_id")
    public String service_ticket_id;
    @SerializedName("send_time")
    public String send_time;
    @SerializedName("checked")
    public Boolean checked;
    @SerializedName("price")
    public int price;
    @SerializedName("quotation_url")
    public String quotation_url;
    @SerializedName("quotation_state")
    public String quotation_state;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** QuotationsData *****\n")
                .append("_id=" + _id + "\n")
                .append("service_ticket_id=" + service_ticket_id + "\n")
                .append("send_time=" + send_time + "\n")
                .append("checked=" + checked + "\n")
                .append("price" + price + "\n")
                .append("quotation_url=" + quotation_url + "\n")
                .append("quotation_state=" + quotation_state + "\n")
                .append("********************")
                .toString();
    }
}

