package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class WarrantyResponseData {
    @SerializedName("data")
    public WarrantyData data;
}
