package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyCarData extends Response {
    @SerializedName("datas")
    public List<MyCar> cars;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString() + "\n")
                .append("***** MyCarData *****\n");
        if (cars != null) {
            for (int i = 0; i < cars.size(); i++) {
                sb.append(cars.get(i) + "\n");
            }
        }
        sb.append("********************");
        return sb.toString();
    }

    public ArrayList<CarVinAndPlantData> getCarVinAndPlantData() {
        ArrayList<CarVinAndPlantData> list = new ArrayList<>();
        for (MyCar car : cars) {
            list.add(new CarVinAndPlantData(car.vin, car.carData.get(0).plate));
        }

        return list;
    }

    public static class CarVinAndPlantData implements Parcelable {
        public String vin = "";
        public String plant = "";

        public CarVinAndPlantData() {
            vin = "";
            plant = "";
        }

        public CarVinAndPlantData(String vin, String plant) {
            this.vin = vin;
            this.plant = plant;
        }

        protected CarVinAndPlantData(Parcel in) {
            vin = in.readString();
            plant = in.readString();
        }

        public static final Creator<CarVinAndPlantData> CREATOR = new Creator<CarVinAndPlantData>() {
            @Override
            public CarVinAndPlantData createFromParcel(Parcel in) {
                return new CarVinAndPlantData(in);
            }

            @Override
            public CarVinAndPlantData[] newArray(int size) {
                return new CarVinAndPlantData[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(vin);
            dest.writeString(plant);
        }
    }
}
