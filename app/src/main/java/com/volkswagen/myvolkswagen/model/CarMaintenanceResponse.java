package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarMaintenanceResponse extends Response{
    @SerializedName("Data")
    public List<Area> data;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** CarMaintenanceResponse *****\n")
                .append("data=" + data + "\n")
                .append("********************")
                .toString();
    }
}
