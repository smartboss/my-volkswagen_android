package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class CarDataCruisys extends Response {
    @SerializedName("model")
    public String model;
    @SerializedName("plate_no")
    public String plate;
    @SerializedName("vin")
    public String vin;
}
