package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InboxContent {
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("NoticeID")
    public int noticeID;
    @SerializedName("Level")
    public int level;
    @SerializedName("LevelName")
    public String levelName;
    @SerializedName("Nickname")
    public String nickName;
    @SerializedName("StickerURL")
    public String stickerUrl;
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("PDate")
    public String date;
    @SerializedName("title")
    public String title;
    @SerializedName("Content")
    public String content;
    @SerializedName("PhotoURL")
    public String photoUrl;
    @SerializedName("PhotoList")
    public List<String> photoList;

    @Override
    public String toString() {
        return new StringBuilder("***** InboxContent *****\n")
                .append("accountId=" + accountId + "\n")
                .append("noticeID=" + noticeID + "\n")
                .append("level=" + level + "\n")
                .append("levelName=" + levelName + "\n")
                .append("nickName=" + nickName + "\n")
                .append("stickerUrl=" + stickerUrl + "\n")
                .append("newsId=" + newsId + "\n")
                .append("date=" + date + "\n")
                .append("title=" + title + "\n")
                .append("content=" + content + "\n")
                .append("photoUrl=" + photoUrl + "\n")
                .append("photoList=" + photoList + "\n")
                .append("********************")
                .toString();
    }
}
