package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MemberLevelInfoResponse extends Response {
    @SerializedName("Data")
    public MemberLevelInfo data;

    public static class MemberLevelInfo {
        @SerializedName("LevelInfo")
        public LevelInfo levelInfo;

        @SerializedName("LevelPrivilege")
        public ArrayList<LevelPrivilege> levelPrivileges;
    }

    public static class LevelInfo {
        @SerializedName("AccountID")
        public String accountID = "";

        @SerializedName("Level")
        public int level = 0;

        @SerializedName("LevelName")
        public String levelName = "";

        @SerializedName("LevelExpiredFrom")
        public String levelExpiredFrom = "";

        @SerializedName("LevelExpiredTo")
        public String levelExpiredTo = "";

        @SerializedName("LevelStatus")
        public LevelStatus levelStatus = new LevelStatus();
    }

    public static class LevelStatus {
        @SerializedName("Maintain")
        public String maintain = "";

        @SerializedName("UpgradeSilver")
        public String upgradeSilver = "";

        @SerializedName("UpgradeGold")
        public String upgradeGold = "";
    }

    public static class LevelPrivilege {
        @SerializedName("Level")
        public int level = 0;

        @SerializedName("LevelName")
        public String levelName = "";

        @SerializedName("Rule")
        public String rule = "";
    }
}
