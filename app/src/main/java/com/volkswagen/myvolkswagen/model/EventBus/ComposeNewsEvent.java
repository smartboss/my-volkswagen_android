package com.volkswagen.myvolkswagen.model.EventBus;

public class ComposeNewsEvent {

    private boolean isClubNews;

    public ComposeNewsEvent() {
        isClubNews = false;
    }

    public ComposeNewsEvent(boolean isClubNews) {
        this.isClubNews = isClubNews;
    }

    public boolean isClubNews() {
        return isClubNews;
    }

}
