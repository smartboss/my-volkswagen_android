package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

public class DeleteNewsEvent {

    private boolean isClubNews;
    private int newsId;

    public DeleteNewsEvent(@NonNull int newsId) {
        isClubNews = false;
        this.newsId = newsId;
    }

    public DeleteNewsEvent(@NonNull int newsId, boolean isClubNews) {
        this.isClubNews = isClubNews;
        this.newsId = newsId;
    }
    public int getNewsId() {
        return newsId;
    }

    public boolean isClubNews() {
        return isClubNews;
    }

}
