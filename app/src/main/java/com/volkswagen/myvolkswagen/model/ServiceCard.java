package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ServiceCard {
    @SerializedName("description")
    public String description;
    @SerializedName("header")
    public String header;
    @SerializedName("id")
    public String id;
    @SerializedName("title")
    public String title;
    @SerializedName("btns")
    public ServiceCardButton buttons;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** ServiceCard *****\n")
                .append("description=" + description + "\n")
                .append("header=" + header + "\n")
                .append("id=" + id + "\n")
                .append("title=" + title + "\n")
                .append("buttons" + buttons + "\n")
                .append("********************")
                .toString();
    }
}

