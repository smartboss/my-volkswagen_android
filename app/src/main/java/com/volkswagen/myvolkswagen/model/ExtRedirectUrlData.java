package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ExtRedirectUrlData {
    @SerializedName("redirect_url")
    public String redirectUrl = "";
}
