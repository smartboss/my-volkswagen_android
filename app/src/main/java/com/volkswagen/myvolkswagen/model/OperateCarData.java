package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class OperateCarData {
    @SerializedName("Name")
    public String name;
    @SerializedName("VIN")
    public String vin;
    @SerializedName("LicensePlateNumber")
    public String plate;

    public OperateCarData() {
    }

    public OperateCarData(String name, String plate, String vin) {
        this.name = name;
        this.plate = plate;
        this.vin = vin;
    }
}
