package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class MemberListInBehaviorInput {
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("Behavior")
    public String behavior;

    public MemberListInBehaviorInput(int newsId, String behavior) {
        this.newsId = newsId;
        this.behavior = behavior;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** MemberListInBehaviorInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("behavior=" + behavior + "\n")
                .append("********************")
                .toString();
    }
}
