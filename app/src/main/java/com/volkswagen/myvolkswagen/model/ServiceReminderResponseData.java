package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceReminderResponseData {
    @SerializedName("data")
    public List<ServiceReminder> serviceReminderList;
}
