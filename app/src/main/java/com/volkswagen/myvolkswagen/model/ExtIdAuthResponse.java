package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ExtIdAuthResponse {
    @SerializedName("data")
    public ExtIdAuthData extIdAuthData;
}
