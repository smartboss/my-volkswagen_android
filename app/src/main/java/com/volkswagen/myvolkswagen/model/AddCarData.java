package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class AddCarData extends OperateCarData {
    @SerializedName("CarTypeID")
    public int typeId;
    @SerializedName("CarModelID")
    public int modelId;

    public void clearAll() {
        name = vin = plate = null;
        typeId = modelId = 0;
    }
}
