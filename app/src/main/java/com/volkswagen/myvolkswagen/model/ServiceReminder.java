package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ServiceReminder {
    @SerializedName("id")
    public String id;
    @SerializedName("type")
    public String type;
    @SerializedName("description")
    public String description;
    @SerializedName("btn_url")
    public String btnUrl;
    @SerializedName("btn_name")
    public String btnName;
    @SerializedName("service_name")
    public String serviceName;
    @SerializedName("event_name")
    public String eventName;
    @SerializedName("event_json")
    public EventJSON eventJSON;
}

