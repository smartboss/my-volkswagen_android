package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarTripsResponseData extends Response{
    @SerializedName("data")
    public List<TripData> tripDataList;
}
