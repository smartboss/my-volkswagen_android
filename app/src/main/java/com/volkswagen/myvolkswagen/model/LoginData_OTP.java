package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class LoginData_OTP {
    @SerializedName("mobile_prefix")
    public String prefix;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("code")
    public String code;
    @SerializedName("device_id")
    public String deviceId;

    public LoginData_OTP(String prefix, String mobile, String code, String id) {
        this.prefix = prefix;
        this.mobile = mobile;
        this.code = code;
        this.deviceId = id;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** LoginData_OTP *****\n")
                .append("mobile_prefix=" + prefix + "\n")
                .append("mobile=" + mobile + "\n")
                .append("code=" + code + "\n")
                .append("deviceId=" + deviceId + "\n")
                .append("********************")
                .toString();
    }
}

