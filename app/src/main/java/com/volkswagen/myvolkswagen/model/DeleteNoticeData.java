package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DeleteNoticeData extends Response {
    @SerializedName("notice_id")
    public int noticeId;

    public DeleteNoticeData(int noticeId) {
        this.noticeId = noticeId;
    }
}
