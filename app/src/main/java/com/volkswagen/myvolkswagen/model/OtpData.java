package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;
import com.volkswagen.myvolkswagen.utils.EncryptUtils;

public class OtpData {
    @SerializedName("mobile_prefix")
    public String prefix;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("hash_mobile")
    public String hash;

    public OtpData(String prefix, String mobile) {
        this.prefix = prefix;
        this.mobile = mobile;
        this.hash = EncryptUtils.encrypt(prefix + this.mobile, "VolkswagenApp").toUpperCase();
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** OtpData *****\n")
                .append("mobile_prefix=" + prefix + "\n")
                .append("mobile=" + mobile + "\n")
                .append("hash_mobile=" + hash + "\n")
                .append("********************")
                .toString();
    }
}
