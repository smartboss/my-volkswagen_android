package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class UserData {
    public static final int MEMBER_LEVEL_NORMAL = 0;
    public static final int MEMBER_LEVEL_BLUE = 1;
    public static final int MEMBER_LEVEL_SILVER = 2;
    public static final int MEMBER_LEVEL_GOLD = 3;
    public static final int MEMBER_LEVEL_WHITE_GOLDEN = 4;

    @SerializedName("account_id")
    public String accountId;
    @SerializedName("vw_email")
    public String email;
    @SerializedName("vw_mobile")
    public String mobile;
    @SerializedName("level")
    public int level;
    @SerializedName("level_name")
    public String levelName;
    @SerializedName("show_profile")
    public boolean showProfile;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** UserData *****\n")
                .append("accountId=" + accountId + "\n")
                .append("email=" + email + "\n")
                .append("mobile=" + mobile + "\n")
                .append("level=" + level + "\n")
                .append("levelName=" + levelName + "\n")
                .append("showProfile=" + showProfile + "\n")
                .append("********************")
                .toString();
    }
}
