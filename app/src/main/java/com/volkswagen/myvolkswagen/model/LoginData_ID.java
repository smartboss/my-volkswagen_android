package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class LoginData_ID {
    @SerializedName("auth_code")
    public String code;
    @SerializedName("id_token")
    public String token;
    @SerializedName("device_id")
    public String deviceId;

    public LoginData_ID(String code, String token, String id) {
        this.code = code;
        this.token = token;
        this.deviceId = id;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** LoginData *****\n")
                .append("code=" + code + "\n")
                .append("token=" + token + "\n")
                .append("deviceId=" + deviceId + "\n")
                .append("********************")
                .toString();
    }
}
