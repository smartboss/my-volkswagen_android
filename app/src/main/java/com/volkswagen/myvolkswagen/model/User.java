package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class User extends Response {
    @SerializedName("UserToken")
    public String token;
    @SerializedName("UserData")
    public UserData data;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** User *****\n")
                .append("token=" + token + "\n")
                .append("data=" + data + "\n")
                .append("********************")
                .toString();
    }
}
