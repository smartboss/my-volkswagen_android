package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class RedeemCodeResponse extends Response {
    @SerializedName("Code")
    public String code;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** RedeemCodeResponse *****\n")
                .append("code=" + code + "\n")
                .append("********************")
                .toString();
    }
}
