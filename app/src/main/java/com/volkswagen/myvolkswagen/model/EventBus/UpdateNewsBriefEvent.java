package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

import com.volkswagen.myvolkswagen.model.NewsBrief;

public class UpdateNewsBriefEvent {

    private NewsBrief mNewsBrief;

    public UpdateNewsBriefEvent(@NonNull NewsBrief newsBrief)
    {
        mNewsBrief = newsBrief;
    }
    public NewsBrief getNewsBrief() {
        return mNewsBrief;
    }

}
