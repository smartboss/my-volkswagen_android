package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class MemberContentInput {
    @SerializedName("AccountID")
    public String accountID;
    @SerializedName("Who")
    public String who;
    @SerializedName("Kind")
    public String kind;

    public MemberContentInput(String accountID, String who, String kind) {
        this.accountID = accountID;
        this.who = who;
        this.kind = kind;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** MemberContentInput *****\n")
                .append("accountID=" + accountID + "\n")
                .append("who=" + who + "\n")
                .append("kind=" + kind + "\n")
                .append("********************")
                .toString();
    }
}
