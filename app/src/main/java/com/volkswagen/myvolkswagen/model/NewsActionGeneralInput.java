package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class NewsActionGeneralInput {
    public static final String TYPE_LIKE = "1";
    public static final String TYPE_UNLIKE = "0";

    public static final String TYPE_BOOKMARK = "1";
    public static final String TYPE_UNBOOKMARK = "0";

    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("Action")
    public String action;

    public NewsActionGeneralInput(int newsId, String action) {
        this.newsId = newsId;
        this.action = action;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** NewsActionGeneralInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("action=" + action + "\n")
                .append("********************")
                .toString();
    }
}
