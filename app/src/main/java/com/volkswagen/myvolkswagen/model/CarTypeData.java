package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarTypeData extends Response {
    @SerializedName("ModelList")
    public List<CarModel> models;
    @SerializedName("Count")
    public int count;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** CarTypeData *****\n")
                .append("models=" + models + "\n")
                .append("count=" + count + "\n")
                .append("********************")
                .toString();
    }
}
