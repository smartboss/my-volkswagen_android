package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MemberRelation {
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Offical")
    public int official;
    @SerializedName("Level")
    public int level;
    @SerializedName("Nickname")
    public String nickname;
    @SerializedName("StickerURL")
    public String stickerUrl;
    @SerializedName("OwnModel")
    public List<String> carModels;
    @SerializedName("IsFollower")
    public boolean isFollower;
    @SerializedName("IsFollow")
    public boolean isFollow;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** MemberRelation *****\n")
                .append("accountId=" + accountId + "\n")
                .append("official=" + official + "\n")
                .append("level=" + level + "\n")
                .append("nickname=" + nickname + "\n")
                .append("stickerUrl=" + stickerUrl + "\n")
                .append("carModels=" + carModels + "\n")
                .append("isFollower=" + isFollower + "\n")
                .append("isFollow=" + isFollow + "\n")
                .append("********************")
                .toString();
    }
}
