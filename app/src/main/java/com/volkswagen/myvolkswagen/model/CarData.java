package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class CarData {
    public static final String DONGLE_STATUS_USER_MAPPED = "USER_MAPPED";
    public static final String DONGLE_STATUS_VIN_MAPPED = "VIN_MAPPED";
    public static final String DONGLE_STATUS_ACTIVATED = "ACTIVATED";
    public static final String DONGLE_STATUS_DISCARDED = "DISCARDED";
    public static final String DONGLE_STATUS_EXPIRED = "EXPIRED";
    public static final String DONGLE_STATUS_NONE = "NONE";

    @SerializedName("Name")
    public String name = "";
    @SerializedName("VIN")
    public String vin = "";
    @SerializedName("LicensePlateNumber")
    public String plate = "";
    @SerializedName("CarTypeID")
    public String typeId = "";
    @SerializedName("CarTypeName")
    public String type = "";
    @SerializedName("CarModelID")
    public String modelId = "";
    @SerializedName("CarModelName")
    public String model = "";
    @SerializedName("CarModelImage")
    public String image = "";
    @SerializedName("CarDate")
    public String date = "";
    @SerializedName("OwnerDate")
    public String ownerDate = "";
    @SerializedName("Mileage")
    public String mileage = "";
    @SerializedName("Dealer")
    public String dealer = "";
    @SerializedName("DealerAreaId")
    public String dealerAreaId = "";
    @SerializedName("DealerAreaName")
    public String dealerAreaName = "";
    @SerializedName("DealerPlantId")
    public String dealerPlantId = "";
    @SerializedName("DealerPlantName")
    public String dealerPlantName = "";

    public String dongleStatus = DONGLE_STATUS_NONE;
}
