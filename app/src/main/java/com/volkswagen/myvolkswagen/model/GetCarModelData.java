package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class GetCarModelData {
    @SerializedName("CarTypeID")
    public int id;

    public GetCarModelData(int id) {
        this.id = id;
    }
}
