package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class AccountBindingData {
    @SerializedName("token")
    public String jwtToken;
    @SerializedName("auth_code")
    public String code;
    @SerializedName("id_token")
    public String idToken;
    @SerializedName("device_id")
    public String deviceId;

    public AccountBindingData(String jwtToken, String code, String idToken, String deviceId) {
        this.jwtToken = jwtToken;
        this.code = code;
        this.idToken = idToken;
        this.deviceId = deviceId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** AccountBindingData *****\n")
                .append("token=" + jwtToken + "\n")
                .append("auth_code=" + code + "\n")
                .append("id_token=" + idToken + "\n")
                .append("device_id=" + deviceId + "\n")
                .append("********************")
                .toString();
    }
}
