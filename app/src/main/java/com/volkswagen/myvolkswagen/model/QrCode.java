package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class QrCode extends Response {
    @SerializedName("QRcode")
    public String image;
}
