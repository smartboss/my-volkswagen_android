package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class MyCarButton {
    public static final int BUTTON_STATUS_INVISIBLE = 0;
    public static final int BUTTON_STATUS_VISIBLE = 1;
    public static final int BUTTON_STATUS_MANUAL = 2;

    public static final String BUTTON_CODE_ADD_CAR = "AddCar";
    public static final String BUTTON_CODE_PAT = "PAT";
    public static final String BUTTON_CODE_BODY_AND_PAINT = "BodyAndPaint";
    public static final String BUTTON_CODE_CAR_MAINTENANCE = "CarMaintenance";
    public static final String BUTTON_CODE_RECALL = "Recall";

    @SerializedName("BtnCode")
    public String buttonCode;
    @SerializedName("BtnName")
    public String buttonName;
    @SerializedName("Order")
    public int order;
    @SerializedName("Status")
    public int status;
    @SerializedName("Url")
    public String url;

    public boolean needVisible() {
        return status == BUTTON_STATUS_VISIBLE;
    }

    public static class MyCarButtonComparator implements Comparator<MyCarButton>
    {
        public int compare(MyCarButton left, MyCarButton right) {
            return left.order - right.order;
        }
    }
}
