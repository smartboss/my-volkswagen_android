package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class BindingData {
    public static final String TYPE_BIND = "B";
    public static final String TYPE_UNBIND = "R";

    @SerializedName("FBID")
    public String fbId;
    @SerializedName("Type")
    public String type;

    public BindingData(String fbId, String type) {
        this.fbId = fbId;
        this.type = type;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** BindingData *****\n")
                .append("fbId=" + fbId + "\n")
                .append("type=" + type + "\n")
                .append("********************")
                .toString();
    }
}
