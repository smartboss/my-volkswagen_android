package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class CarMaintainOrderDetail implements Parcelable {
    @SerializedName("EXTERNAL")
    public String external;
    @SerializedName("WARRANTY")
    public String warranty;
    @SerializedName("INSURANCE")
    public String insurance;

    protected CarMaintainOrderDetail(Parcel in) {
        external = in.readString();
        warranty = in.readString();
        insurance = in.readString();
    }

    public static final Creator<CarMaintainOrderDetail> CREATOR = new Creator<CarMaintainOrderDetail>() {
        @Override
        public CarMaintainOrderDetail createFromParcel(Parcel in) {
            return new CarMaintainOrderDetail(in);
        }

        @Override
        public CarMaintainOrderDetail[] newArray(int size) {
            return new CarMaintainOrderDetail[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return new StringBuilder("***** CarMaintainOrderDetail *****\n")
                .append("EXTERNAL=").append(external).append("\n")
                .append("WARRANTY=").append(warranty).append("\n")
                .append("INSURANCE=").append(insurance).append("\n")
                .append("********************")
                .toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(external);
        dest.writeString(warranty);
        dest.writeString(insurance);
    }
}
