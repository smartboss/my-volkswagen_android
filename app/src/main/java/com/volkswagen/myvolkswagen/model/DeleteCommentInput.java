package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DeleteCommentInput {

    @SerializedName("CommentID")
    public int commentId;

    public DeleteCommentInput(int commentId) {
        this.commentId = commentId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** DeleteCommentInput *****\n")
                .append("commentId=" + commentId + "\n")
                .append("********************")
                .toString();
    }
}
