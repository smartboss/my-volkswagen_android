package com.volkswagen.myvolkswagen.model;

import java.io.Serializable;

public class CarModel implements Serializable {
    public int id;
    public String name;
    public String image;
}
