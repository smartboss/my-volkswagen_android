package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class InboxContentData extends Response {
    @SerializedName("Count")
    public int count;
    @SerializedName("News")
    public InboxContent inboxContent;
}
