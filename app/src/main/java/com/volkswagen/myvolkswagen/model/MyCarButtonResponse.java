package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyCarButtonResponse extends Response {
    @SerializedName("Data")
    public MyCarData data;


    public static class MyCarData {
        @SerializedName("Buttons")
        public ArrayList<MyCarButton> buttons;
    }
}
