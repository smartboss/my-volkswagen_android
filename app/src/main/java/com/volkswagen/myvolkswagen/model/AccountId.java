package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class AccountId {

    @SerializedName("AccountID")
    public String accountId;

    public AccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** AccountId *****\n")
                .append("accountId=" + accountId + "\n")
                .append("********************")
                .toString();
    }
}
