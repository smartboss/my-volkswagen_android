package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarSummaryResponseData extends Response {
    @SerializedName("data")
    public List<CarSummary> carSummaryDataList;
}
