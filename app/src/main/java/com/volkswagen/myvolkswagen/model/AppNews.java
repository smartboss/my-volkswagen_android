package com.volkswagen.myvolkswagen.model;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class AppNews extends Response {
    @SerializedName("Data")
    public List<NewsData> newsDataList;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** App/News *****\n")
                .append("datas=" + newsDataList + "\n")
                .append("********************")
                .toString();
    }
}
