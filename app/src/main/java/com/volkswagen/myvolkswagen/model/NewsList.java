package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsList extends Response {
    @SerializedName("NewsList")
    public List<NewsBrief> newsBriefList;
    @SerializedName("Count")
    public int count;


    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** NewsList *****\n")
                .append("count=" + count + "\n")
                .append("newsBriefList=" + newsBriefList + "\n")
                .append("********************")
                .toString();
    }
}
