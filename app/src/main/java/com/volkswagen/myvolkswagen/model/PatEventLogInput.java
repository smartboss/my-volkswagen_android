package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class PatEventLogInput {
    @SerializedName("AccountId")
    public String accountId;
    @SerializedName("Level")
    public int level;
    @SerializedName("ServiceName")
    public String serviceName;
    @SerializedName("EventName")
    public String eventName;
    @SerializedName("UniqueId")
    public String uniqueId;
    @SerializedName("InboxId")
    public Integer inboxId;
    @SerializedName("Timestamp")
    public String timeStamp;
    @SerializedName("EventJSON")
    public EventJSON eventJSON;

    public PatEventLogInput(String accountId, int level, String serviceName, String eventName, String uniqueId, Integer inboxId, String timeStamp, EventJSON eventJSON) {
        this.accountId = accountId;
        this.level = level;
        this.serviceName = serviceName;
        this.eventName = eventName;
        this.uniqueId = uniqueId;
        this.inboxId = inboxId;
        this.timeStamp = timeStamp;
        this.eventJSON = eventJSON;
    }
}
