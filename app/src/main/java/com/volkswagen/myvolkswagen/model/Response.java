package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class Response {
    @SerializedName("Result")
    public boolean result;
    @SerializedName("ErrorCode")
    public int errorCode;
    @SerializedName("ErrorMsg")
    public String errorMsg;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** Response *****\n")
                .append("result=" + result + "\n")
                .append("errorCode=" + errorCode + "\n")
                .append("errorMsg=" + errorMsg + "\n")
                .append("********************")
                .toString();
    }
}
