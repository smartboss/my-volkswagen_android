package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuotationsResponseData extends Response {
    @SerializedName("data")
    public List<QuotationsData> quotationsDataList;
}
