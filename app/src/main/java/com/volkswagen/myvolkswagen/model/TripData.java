package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TripData {
    @SerializedName("start_time")
    public String start_time;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("distance")
    public String distance;

    @SerializedName("driving_time")
    public String driving_time;

    @SerializedName("max_speed")
    public String max_speed;

    @SerializedName("average_fuel_consumption")
    public String average_fuel_consumption;

    @SerializedName("start_location")
    public CarTrips.Location start_location;

    @SerializedName("end_location")
    public CarTrips.Location end_location;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** TripData *****\n")
                .append("start_time=" + start_time + "\n")
                .append("end_time=" + end_time + "\n")
                .append("distance=" + distance + "\n")
                .append("driving_time=" + driving_time + "\n")
                .append("max_speed=" + max_speed + "\n")
                .append("average_fuel_consumption=" + average_fuel_consumption + "\n")
                .append("start_location=" + start_location + "\n")
                .append("end_location=" + end_location + "\n")
                .append("********************")
                .toString();
    }
}
