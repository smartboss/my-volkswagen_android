package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class UpdateNewsInput {
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("Content")
    public String content;
    @SerializedName("ModelList")
    public String modelList;

    public UpdateNewsInput(int newsId, String content, String modelList) {
        this.newsId = newsId;
        this.content = content;
        this.modelList = modelList;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** UpdateNewsInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("content=" + content + "\n")
                .append("modelList=" + modelList + "\n")
                .append("********************")
                .toString();
    }
}
