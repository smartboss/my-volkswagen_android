package com.volkswagen.myvolkswagen.model;

import java.util.List;

public class PendingAddCarData {
    public int step;
    public String name;
    public String plate1;
    public String plate2;
    public String vin;
    public String type;
    public int typeId;
    public List<CarModel> models;
    public int modelId;

    public PendingAddCarData(int step, String name, String plate1, String plate2, String vin, String type, int typeId, List<CarModel> models, int modelId) {
        this.step = step;
        this.name = name;
        this.plate1 = plate1;
        this.plate2 = plate2;
        this.vin = vin;
        this.type = type;
        this.typeId = typeId;
        this.models = models;
        this.modelId = modelId;
        this.modelId = modelId;
    }
}
