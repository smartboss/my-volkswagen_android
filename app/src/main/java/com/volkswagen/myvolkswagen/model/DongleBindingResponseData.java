package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DongleBindingResponseData extends Response {
    @SerializedName("data")
    public List<DongleBindingCarData> dongleBindingCarDataList;

    @SerializedName("return_code")
    public List<DongleBindingReturnCode> returnCodeList;
}
