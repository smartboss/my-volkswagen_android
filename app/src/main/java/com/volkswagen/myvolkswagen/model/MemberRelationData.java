package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MemberRelationData extends Response {
    @SerializedName("MemberRelationList")
    public List<MemberRelation> memberRelations;
    @SerializedName("Count")
    public int count;


    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** MemberRelationData *****\n")
                .append("count=" + count + "\n")
                .append("memberRelation=" + memberRelations + "\n")
                .append("********************")
                .toString();
    }
}
