package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class Plant {
    @SerializedName("PlantId")
    public String plantId;
    @SerializedName("PlantName")
    public String plantName;

    public Plant() {
        plantId = "";
        plantName = "";
    }

    public Plant(String plantId, String plantName) {
        this.plantId = plantId;
        this.plantName = plantName;
    }

    @Override
    public String toString() {
        return plantName;
    }
}
