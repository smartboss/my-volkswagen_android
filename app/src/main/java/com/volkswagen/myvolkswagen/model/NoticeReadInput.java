package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class NoticeReadInput {
    @SerializedName("NoticeID")
    public int noticeID;

    public NoticeReadInput(int noticeID) {
        this.noticeID = noticeID;
    }
}
