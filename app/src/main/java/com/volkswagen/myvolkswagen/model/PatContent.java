package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PatContent implements Serializable {
    @SerializedName("PAT_ID")
    public String patId;
    @SerializedName("VIN")
    public String vin;
    @SerializedName("CarNumber")
    public String carNumber;
    @SerializedName("CurrentMileage")
    public int currentMileage;
    @SerializedName("AnnualMileage")
    public int annualMileage;
    @SerializedName("CycleDay")
    public int cycleDay;
    @SerializedName("Data")
    public List<PatContentData> data;

    public static class PatContentData implements Serializable {
        @SerializedName("Service")
        public List<String> service;
        @SerializedName("Additional")
        public List<String> additional;
        @SerializedName("PredictedServiceDate")
        public String predictedServiceDate;
        @SerializedName("PredictedServiceMileage")
        public int predictedServiceMileage;

        public String getAllService() {
            StringBuilder builder = new StringBuilder();
            for (String s : service) {
                builder.append(s).append("\n");
            }

            return builder.toString();
        }

        public String getAllAdditional() {
            StringBuilder builder = new StringBuilder();
            for (String s : additional) {
                builder.append(s).append("\n");
            }

            return builder.toString();
        }
    }
}
