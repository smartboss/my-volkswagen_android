package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class NewsData extends Response {

    @SerializedName("id")
    public int id;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("link")
    public String link;
    @SerializedName("content")
    public String content;
    @SerializedName("is_link")
    public int is_link;
    @SerializedName("cover_image")
    public String cover_image;
    @SerializedName("thumb_cover_image")
    public String thumb_cover_image;
    @SerializedName("published_at")
    public String published_at;
    @SerializedName("gallery")
    public List<String> gallery;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** News *****\n")
                .append("id=" + id + "\n")
                .append("title=" + title + "\n")
                .append("description=" + description + "\n")
                .append("link=" + link + "\n")
                .append("content=" + content + "\n")
                .append("is_link=" + is_link + "\n")
                .append("cover_image=" + cover_image + "\n")
                .append("thumb_cover_image=" + thumb_cover_image + "\n")
                .append("published_at=" + published_at + "\n")
                .append("gallery=" + gallery + "\n")
                .append("********************")
                .toString();
    }
}

