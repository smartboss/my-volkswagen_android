package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DongleBindingInput {
    @SerializedName("imei")
    public String imei = "";
    @SerializedName("brand")
    public String brand = "Volkswagen Personal";
    @SerializedName("model")
    public String model = "";
    @SerializedName("power_type")
    public String powerType = "fuel";

    public DongleBindingInput(String imei, String model) {
        this.imei = imei;
        this.model = model;
    }
}
