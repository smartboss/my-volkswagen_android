package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class CarLocation {
    @SerializedName("latitude")
    public double latitude;

    @SerializedName("longitude")
    public double longitude;

    @SerializedName("elevation")
    public int elevation;

    @SerializedName("heading")
    public int heading;

    @SerializedName("speed")
    public int speed;

    @SerializedName("event_time")
    public String event_time;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** LocationData *****\n")
                .append("latitude=" + latitude + "\n")
                .append("longitude=" + longitude + "\n")
                .append("elevation=" + elevation + "\n")
                .append("heading=" + heading + "\n")
                .append("speed=" + speed + "\n")
                .append("event_time=" + event_time + "\n")
                .append("********************")
                .toString();
    }
}