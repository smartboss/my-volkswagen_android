package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class WarrantyProduct {
    @SerializedName("product_name")
    public String productName;
    @SerializedName("effective_date")
    public String effectiveDate;
    @SerializedName("expiration_date")
    public String expirationDate;
    @SerializedName("org_name")
    public String orgName;
    @SerializedName("product_type")
    public String productType;
}

