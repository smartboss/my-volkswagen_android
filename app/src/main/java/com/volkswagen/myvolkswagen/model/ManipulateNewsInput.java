package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ManipulateNewsInput {

    @SerializedName("NewsID")
    public int newsId;

    public ManipulateNewsInput(int newsId) {
        this.newsId = newsId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** ManipulateNewsInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("********************")
                .toString();
    }
}
