package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceCardResponseData extends Response {
    @SerializedName("data")
    public List<ServiceCard> serviceCardList;
}
