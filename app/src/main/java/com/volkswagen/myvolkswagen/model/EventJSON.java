package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class EventJSON {
    @SerializedName("event_category")
    public String eventCategory = "";

    @SerializedName("pageName")
    public String pageName = "";

    @SerializedName("eventLabel")
    public String eventLabel = "";

    @SerializedName("utm_source")
    public String utmSource = "";

    @SerializedName("utm_medium")
    public String utmMedium = "";

    @SerializedName("utm_campaign")
    public String utmCampaign = "";

    public EventJSON(String eventCategory, String pageName, String eventLabel, String utmSource, String utmMedium, String utmCampaign) {
        this.eventCategory = eventCategory;
        this.pageName = pageName;
        this.eventLabel = eventLabel;
        this.utmSource = utmSource;
        this.utmMedium = utmMedium;
        this.utmCampaign = utmCampaign;
    }
}
