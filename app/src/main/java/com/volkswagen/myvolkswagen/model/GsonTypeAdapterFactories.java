package com.volkswagen.myvolkswagen.model;

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public final class GsonTypeAdapterFactories {

    private GsonTypeAdapterFactories() {
        throw new UnsupportedOperationException();
    }

    public static final TypeAdapter<Number> VW_INTEGER = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            JsonToken jsonToken = in.peek();
            switch (jsonToken) {
                case NUMBER:
                case STRING:
                    String s = in.nextString();
                    try {
                        return Integer.parseInt(s);
                    } catch (NumberFormatException ignored) {
                    }
                    try {
                        return (int)Double.parseDouble(s);
                    } catch (NumberFormatException ignored) {
                    }
                    return null;
                case NULL:
                    in.nextNull();
                    return null;
                case BOOLEAN:
                    in.nextBoolean();
                    return null;
                default:
                    throw new JsonSyntaxException("Expecting number, got: " + jsonToken);
            }
        }

        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };

    public static final TypeAdapterFactory VW_INTEGER_FACTORY
            = TypeAdapters.newFactory(int.class, Integer.class, VW_INTEGER);

    public static final TypeAdapter<Boolean> VW_BOOLEAN = new TypeAdapter<Boolean>() {
        @Override
        public Boolean read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.STRING) {
                String s = in.nextString();
                if (s.equalsIgnoreCase("y") || s.equalsIgnoreCase("1")) {
                    return true;
                } else if (s.equalsIgnoreCase("n") || s.equalsIgnoreCase("0")) {
                    return false;
                } else {
                    return Boolean.parseBoolean(s);
                }
            } else if (in.peek() == JsonToken.NUMBER) {
                int num = in.nextInt();
                if(num == 0) {
                    return false;
                } else {
                    return true;
                }
            }
            return in.nextBoolean();
        }
        @Override
        public void write(JsonWriter out, Boolean value) throws IOException {
            out.value(value);
        }
    };

    public static final TypeAdapterFactory VW_BOOLEAN_FACTORY
            = TypeAdapters.newFactory(boolean.class, Boolean.class, VW_BOOLEAN);
}
