package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyMemberLevelInfo extends Response {
    @SerializedName("Level")
    public int level;
    @SerializedName("LevelName")
    public String levelName;
    @SerializedName("LevelIntro")
    public String levelIntro;

    @Override
    public String toString() {
        return new StringBuilder("***** MyMemberLevelInfo *****\n")
                .append("level=" + level + "\n")
                .append("levelName=" + levelName + "\n")
                .append("levelIntro=" + levelIntro + "\n")
                .toString();
    }
}
