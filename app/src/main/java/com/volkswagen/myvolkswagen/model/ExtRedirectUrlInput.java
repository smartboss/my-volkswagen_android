package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ExtRedirectUrlInput {
    public static final String OPEN_HUB = "openhub";
    public static final String ESHOP = "eshop";
    @SerializedName("type")
    public String type = "";
    @SerializedName("token")
    public String token = "";
    @SerializedName("vin")
    public String vin = "";
    @SerializedName("plate_no")
    public String plateNo = "";
    @SerializedName("account_id")
    public String accountId = "";

    public ExtRedirectUrlInput(String type, String token, String vin, String plateNo, String accountId) {
        this.type = type;
        this.token = token;
        this.vin = vin;
        this.plateNo = plateNo;
        this.accountId = accountId;
    }
}
