package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class DongleBindingReturnCode {
    @SerializedName("code")
    public String code = "";

    @SerializedName("data")
    public String data = "";
}
