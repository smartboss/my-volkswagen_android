package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class RedeemInput {

    @SerializedName("RedeemID")
    public int redeemId;

    public RedeemInput(int redeemId) {
        this.redeemId = redeemId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** RedeemInput *****\n")
                .append("redeemId=" + redeemId + "\n")
                .append("********************")
                .toString();
    }
}
