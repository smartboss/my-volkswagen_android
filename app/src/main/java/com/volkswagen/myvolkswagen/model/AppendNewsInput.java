package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppendNewsInput {
    @SerializedName("Content")
    public String content;
    @SerializedName("PhotoStreamArray")
    public List<String> photoStreamArray;
    @SerializedName("ModelList")
    public String modelList;

    public AppendNewsInput(String content, List<String> photoStreamArray, String modelList) {
        this.content = content;
        this.photoStreamArray = photoStreamArray;
        this.modelList = modelList;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** AppendNewsInput *****\n")
                .append("content=" + content + "\n")
                .append("modelList=" + modelList + "\n")
                .append("photoStreamArray=" + photoStreamArray + "\n")
                .append("********************")
                .toString();
    }
}
