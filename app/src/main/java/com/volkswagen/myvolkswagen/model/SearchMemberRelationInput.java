package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class SearchMemberRelationInput {
    @SerializedName("Keyword")
    public String keyword;

    public SearchMemberRelationInput(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** SearchMemberRelationInput *****\n")
                .append("keyword=" + keyword + "\n")
                .append("********************")
                .toString();
    }
}
