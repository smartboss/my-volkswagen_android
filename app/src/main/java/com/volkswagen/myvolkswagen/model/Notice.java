package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Notice {
    @SerializedName("Kind")
    public int kind;
    @SerializedName("NoticeID")
    public int id;
    @SerializedName("Content")
    public String content;
    @SerializedName("description")
    public String description;
    @SerializedName("StickerURL")
    public String stickerUrl;
    @SerializedName("Read")
    public boolean isRead;
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Level")
    public int level;
    @SerializedName("LevelName")
    public String levelName;
    @SerializedName("Nickname")
    public String nickName;
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("PDate")
    public String date;
    @SerializedName("CommentContent")
    public String commentContent;
    @SerializedName("Offical")
    public int official;
    @SerializedName("IsFollower")
    public boolean isFollower;
    @SerializedName("IsFollow")
    public boolean isFollow;
    @SerializedName("SVID")
    public String svid;
    @SerializedName("SVURL")
    public String svurl;

    @Override
    public String toString() {
        return new StringBuilder("***** Notice *****\n")
                .append("kind=" + kind + "\n")
                .append("id=" + id + "\n")
                .append("content=" + content + "\n")
                .append("description=" + description + "\n")
                .append("stickerUrl=" + stickerUrl + "\n")
                .append("isRead=" + isRead + "\n")
                .append("accountId=" + accountId + "\n")
                .append("level=" + level + "\n")
                .append("levelName=" + levelName + "\n")
                .append("nickName=" + nickName + "\n")
                .append("newsId=" + newsId + "\n")
                .append("date=" + date + "\n")
                .append("commentContent=" + commentContent + "\n")
                .append("official=" + official + "\n")
                .append("isFollower=" + isFollower + "\n")
                .append("isFollow=" + isFollow + "\n")
                .append("svid=" + svid + "\n")
                .append("svurl=" + svurl + "\n")
                .append("********************")
                .toString();
    }
}
