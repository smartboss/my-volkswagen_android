package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoticeData extends Response {
    @SerializedName("Count")
    public int count;
    @SerializedName("NoticeList")
    public List<Notice> noticeList;
}
