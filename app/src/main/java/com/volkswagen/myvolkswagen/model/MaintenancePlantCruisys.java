package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MaintenancePlantCruisys  {
    @SerializedName("display_name")
    public String displayName;
    @SerializedName("name")
    public String plantsName;


    protected MaintenancePlantCruisys(String display_name, String name) {
        displayName = display_name;
        plantsName = name;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** MaintenancePlantCruisys *****\n")
                .append("displayName=" + displayName + "\n")
                .append("plantsName=" + plantsName + "\n")
                .append("********************")
                .toString();
    }

}
