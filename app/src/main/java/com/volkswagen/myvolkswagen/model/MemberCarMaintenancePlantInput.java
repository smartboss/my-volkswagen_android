package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;

public class MemberCarMaintenancePlantInput {
    @SerializedName("AccountId")
    public String accountId;
    @SerializedName("VIN")
    public String vin;
    @SerializedName("LicensePlateNumber")
    public String licensePlateNumber;

    public MemberCarMaintenancePlantInput(String vin, String licensePlateNumber) {
        accountId =  PreferenceManager.getInstance().getAccountId();
        this.vin = vin;
        this.licensePlateNumber = licensePlateNumber;
    }
}
