package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class PatContentInput {
    @SerializedName("VIN")
    public String vin;

    @SerializedName("CarNumber")
    public String carNumber;

    @SerializedName("CurrentMileage")
    public int currentMileage;

    public PatContentInput(String vin, String carNumber, int currentMileage) {
        this.vin = vin;
        this.carNumber = carNumber;
        this.currentMileage = currentMileage;
    }
}
