package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class CarSummary {
    @SerializedName("odometer")
    public MeasurementData odometer;

    @SerializedName("coolant_temperature")
    public MeasurementData coolantTemperature;

    @SerializedName("intake_air_temperature")
    public MeasurementData intakeAirTemperature;

    @SerializedName("control_module_voltage")
    public MeasurementData controlModuleVoltage;

    @SerializedName("fuel_level")
    public MeasurementData fuelLevel;

    @SerializedName("engine")
    public String engine;

    @SerializedName("speed")
    public MeasurementData speed;

    @SerializedName("state")
    public State state;

    public static class MeasurementData {
        @SerializedName("value")
        public double value;

        @SerializedName("unit")
        public String unit;
    }

    static class State {
        @SerializedName("code")
        public String code;

        @SerializedName("name")
        public String name;
    }
}
