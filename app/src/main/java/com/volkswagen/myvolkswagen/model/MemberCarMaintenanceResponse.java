package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class MemberCarMaintenanceResponse extends Response {
    @SerializedName("Data")
    public MemberCarMaintenanceResponse.Data data;

    public static class Data {
        @SerializedName("MaintenancePlant")
        public MaintenancePlant maintenancePlant;
    }
}
