package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarLocationResponseData extends Response{
    @SerializedName("data")
    public List<CarLocation> CarLocationResponseDataList;
}
