package com.volkswagen.myvolkswagen.model.EventBus;

public class UpdateNoticeEvent {

    public boolean hasNew;

    public UpdateNoticeEvent(boolean hasNew) {
        this.hasNew = hasNew;
    }
}
