package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class FollowMemberInput {
    @SerializedName("FollowAccountID")
    public String accountId;
    @SerializedName("Action")
    public String action;

    public FollowMemberInput(String accountId, String action) {
        this.accountId = accountId;
        this.action = action;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** FollowMemberInput *****\n")
                .append("accountId=" + accountId + "\n")
                .append("action=" + action + "\n")
                .append("********************")
                .toString();
    }
}
