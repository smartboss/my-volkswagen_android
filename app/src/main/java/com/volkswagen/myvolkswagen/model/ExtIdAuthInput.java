package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ExtIdAuthInput {
    @SerializedName("account_id")
    public String account_id;
    @SerializedName("plate_no")
    public String plate_no;

    public ExtIdAuthInput(String account, String plate) {
        this.account_id = account;
        this.plate_no = plate;
    }
}

