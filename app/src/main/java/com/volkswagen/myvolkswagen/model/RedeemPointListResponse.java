package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RedeemPointListResponse extends Response {
    @SerializedName("TotalPoints")
    public int totalPoints;
    @SerializedName("DataList")
    public ArrayList<RedeemData> redeems;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** RedeemPointListResponse *****\n")
                .append("totalPoints=" + totalPoints + "\n")
                .append("redeems=" + redeems + "\n")
                .append("********************")
                .toString();
    }
}
