package com.volkswagen.myvolkswagen.model.EventBus;

import androidx.annotation.NonNull;

public class MainActivityTabEvent {

    private int mTab;

    public MainActivityTabEvent(@NonNull int tab)
    {
        mTab = tab;
    }
    public int getTab() {
        return mTab;
    }

}
