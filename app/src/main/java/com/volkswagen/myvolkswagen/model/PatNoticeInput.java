package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class PatNoticeInput {
    @SerializedName("PAT_ID")
    public String patId;

    public PatNoticeInput(String patId) {
        this.patId = patId;
    }
}
