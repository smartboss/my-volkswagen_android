package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileInput {

    @SerializedName("Nickname")
    public String nickname;
    @SerializedName("Sex")
    public String gender;
    @SerializedName("Brief")
    public String brief;
    @SerializedName("EMail")
    public String email;
    @SerializedName("Cellphone")
    public String mobile;
    @SerializedName("Birthday")
    public String birthday;
    @SerializedName("StickerStream")
    public String headbase64;
    @SerializedName("CoverStream")
    public String coverbase64;
    @SerializedName("AreaId")
    public String areaId;
    @SerializedName("PlantId")
    public String plantId;
    @SerializedName("address")
    public String address;

    public UpdateProfileInput(String nickname, String gender, String brief, String email, String mobile, String birthday, String headbase64, String coverbase64, String areaId, String plantId, String address) {
        this.nickname = nickname;
        this.gender = gender;
        this.brief = brief;
        this.email = email;
        this.mobile = mobile;
        this.birthday = birthday;
        this.headbase64 = headbase64;
        this.coverbase64 = coverbase64;
        this.areaId = areaId;
        this.plantId = plantId;
        this.address = address;
    }
}
