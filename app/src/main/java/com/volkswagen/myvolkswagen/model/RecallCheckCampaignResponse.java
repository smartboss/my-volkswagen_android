package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class RecallCheckCampaignResponse extends Response {
    @SerializedName("Data")
    public RecallCheckCampaignData data;

    public static class RecallCheckCampaignData {
        @SerializedName("ShowNoticeBoard")
        public boolean showNoticeBoard;

        @SerializedName("RecallUrl")
        public String recallUrl;
    }
}