package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class ServiceCardButton {
    @SerializedName("default_btn")
    public String default_btn;
    @SerializedName("default_btn_url")
    public String default_btn_url;
    @SerializedName("primary_btn")
    public String primary_btn;
    @SerializedName("primary_btn_url")
    public String primary_btn_url;
    @SerializedName("secondary_btn")
    public String secondary_btn;
    @SerializedName("secondary_btn_url")
    public String secondary_btn_url;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** ServiceCardButton *****\n")
                .append("default_btn=" + default_btn + "\n")
                .append("default_btn_url=" + default_btn_url + "\n")
                .append("primary_btn=" + primary_btn + "\n")
                .append("primary_btn_url=" + primary_btn_url + "\n")
                .append("secondary_btn=" + secondary_btn + "\n")
                .append("secondary_btn_url=" + secondary_btn_url + "\n")
                .append("********************")
                .toString();
    }
}

