package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class NoticeCount extends Response {
    @SerializedName("Count")
    public int count;
}
