package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Area {
    @SerializedName("AreaId")
    public String areaId;
    @SerializedName("AreaName")
    public String areaName;
    @SerializedName("Plants")
    public List<Plant> plants;

    public Area() {
        areaId = "";
        areaName = "";
        plants = new ArrayList<>();
    }

    public Area(String areaId, String areaName, List<Plant> plants) {
        this.areaId = areaId;
        this.areaName = areaName;
        this.plants = plants;
    }

    @Override
    public String toString() {
        return areaName;
    }
}
