package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class News extends Response {
    @SerializedName("News")
    public NewsBrief newsBrief;
    @SerializedName("CommentLists")
    public List<Comment> comments;
    @SerializedName("Count")
    public int count;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** News *****\n")
                .append("newsBrief=" + newsBrief + "\n")
                .append("comments=" + comments + "\n")
                .append("count=" + count + "\n")
                .append("********************")
                .toString();
    }
}
