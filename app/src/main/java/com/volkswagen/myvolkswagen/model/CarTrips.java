package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarTrips {
    @SerializedName("data")
    public List<TripData> tripDataList;

    @SerializedName("pagination")
    public Pagination pagination;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n***** CarTrips *****\n")
                .append("data=" + tripDataList + "\n")
                .append("pagination=" + pagination + "\n")
                .append("********************")
                .toString();
    }



    public static class Location {
        @SerializedName("latitude")
        public double latitude;

        @SerializedName("longitude")
        public double longitude;

        @SerializedName("elevation")
        public int elevation;

        @SerializedName("heading")
        public int heading;

        @SerializedName("speed")
        public int speed;

        @SerializedName("event_time")
        public String event_time;

        @Override
        public String toString() {
            return "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", elevation=" + elevation +
                    ", heading=" + heading +
                    ", speed=" + speed +
                    ", event_time=" + event_time;
        }
    }

    static class Pagination {
        @SerializedName("amount")
        public int amount;

        @SerializedName("pp")
        public int pp;

        @SerializedName("tp")
        public int tp;

        @Override
        public String toString() {
            return "amount=" + amount +
                    ", pp=" + pp +
                    ", tp=" + tp;
        }
    }
}
