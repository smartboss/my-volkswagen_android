package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarMaintainData implements Parcelable{
    @SerializedName("CloseDate")
    public String closeDate;
    @SerializedName("Mileage")
    public String mileage;
    @SerializedName("ExternalCharge")
    public String externalCharge;
    @SerializedName("Type")
    public ArrayList<String> type;
    @SerializedName("OrderDetails")
    public CarMaintainOrderDetail orderDetail;

    protected CarMaintainData(Parcel in) {
        closeDate = in.readString();
        mileage = in.readString();
        externalCharge = in.readString();
        type = in.createStringArrayList();
        orderDetail = in.readParcelable(CarMaintainOrderDetail.class.getClassLoader());
    }

    public static final Creator<CarMaintainData> CREATOR = new Creator<CarMaintainData>() {
        @Override
        public CarMaintainData createFromParcel(Parcel in) {
            return new CarMaintainData(in);
        }

        @Override
        public CarMaintainData[] newArray(int size) {
            return new CarMaintainData[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("***** CarMaintainData *****\n");
        builder.append("CloseDate=").append(closeDate).append("\n")
                .append("Mileage=").append(mileage).append("\n")
                .append("ExternalCharge=").append(externalCharge).append("\n")
                .append("OrderDetails=").append(orderDetail).append("\n")
                .append("********************");
        return builder.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(closeDate);
        dest.writeString(mileage);
        dest.writeString(externalCharge);
        dest.writeStringList(type);
        dest.writeParcelable(orderDetail, flags);
    }
}
