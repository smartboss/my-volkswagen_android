package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class AppendCommentInput {
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("Content")
    public String content;

    public AppendCommentInput(int newsId, String content) {
        this.newsId = newsId;
        this.content = content;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("***** AppendCommentInput *****\n")
                .append("newsId=" + newsId + "\n")
                .append("content=" + content + "\n")
                .append("********************")
                .toString();
    }
}
