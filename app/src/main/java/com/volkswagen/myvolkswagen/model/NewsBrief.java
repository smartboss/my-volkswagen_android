package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsBrief {
    @SerializedName("AccountID")
    public String accountId;
    @SerializedName("Level")
    public int level;
    @SerializedName("Nickname")
    public String nickname;
    @SerializedName("StickerURL")
    public String stickerUrl;
    @SerializedName("NewsID")
    public int newsId;
    @SerializedName("PDate")
    public String date;
    @SerializedName("Content")
    public String content;
    @SerializedName("PhotoList")
    public List<String> photoList;
    @SerializedName("Offical")
    public int official;
    @SerializedName("LikeAmount")
    public int likeAmount;
    @SerializedName("LikeStatus")
    public boolean likeStatus;
    @SerializedName("BookmarkAmount")
    public int bookmarkAmount;
    @SerializedName("BookmarkStatus")
    public boolean bookmarkStatus;
    @SerializedName("ReportStatus")
    public boolean reportStatus;
    @SerializedName("CommentAmount")
    public int commentAmount;
    @SerializedName("CommentStatus")
    public boolean commentStatus;
    @SerializedName("IsFollower")
    public boolean isFollower;
    @SerializedName("IsFollow")
    public boolean isFollow;
    @SerializedName("IsTop")
    public boolean isTop;
    @SerializedName("ModelList")
    public List<Integer> modelList;
    //isSelf is not retrieved from server
    public boolean isSelf;

    public String getMainPhoto() {
        if (photoList != null && photoList.get(0) != null) {
            return photoList.get(0);
        }

        return "";
    }

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** NewsBrief *****\n")
                .append("accountId=" + accountId + "\n")
                .append("level=" + level + "\n")
                .append("nickname=" + nickname + "\n")
                .append("stickerUrl=" + stickerUrl + "\n")
                .append("newsId=" + newsId + "\n")
                .append("date=" + date + "\n")
                .append("content=" + content + "\n")
                .append("photoList=" + photoList + "\n")
                .append("official=" + official + "\n")
                .append("likeAmount=" + likeAmount + "\n")
                .append("likeStatus=" + likeStatus + "\n")
                .append("bookmarkAmount=" + bookmarkAmount + "\n")
                .append("bookmarkStatus=" + bookmarkStatus + "\n")
                .append("reportStatus=" + reportStatus + "\n")
                .append("commentAmount=" + commentAmount + "\n")
                .append("commentStatus=" + commentStatus + "\n")
                .append("isFollower=" + isFollower + "\n")
                .append("isFollow=" + isFollow + "\n")
                .append("isTop=" + isTop + "\n")
                .append("modelList=" + modelList + "\n")
                .append("********************")
                .toString();
    }
}
