package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class NotificationDialogItem {
    @SerializedName("event_time")
    public String eventTime;

    @SerializedName("description")
    public String description;

    @SerializedName("btn_url")
    public String btnUrl;
}
