package com.volkswagen.myvolkswagen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MaintenancePlant implements Parcelable {
    @SerializedName("PlantId")
    public int plantId;
    @SerializedName("PlantsName")
    public String plantsName;
    @SerializedName("Address")
    public String address;
    @SerializedName("Phone")
    public String phone;
    @SerializedName("Url")
    public String url;

    protected MaintenancePlant(Parcel in) {
        plantId = in.readInt();
        plantsName = in.readString();
        address = in.readString();
        phone = in.readString();
        url = in.readString();
    }

    public static final Creator<MaintenancePlant> CREATOR = new Creator<MaintenancePlant>() {
        @Override
        public MaintenancePlant createFromParcel(Parcel in) {
            return new MaintenancePlant(in);
        }

        @Override
        public MaintenancePlant[] newArray(int size) {
            return new MaintenancePlant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(plantId);
        dest.writeString(plantsName);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(url);
    }
}
