package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class CarExistData extends Response {
    @SerializedName("CarTypeID")
    public int id;
    @SerializedName("CarTypeName")
    public String name;
}
