package com.volkswagen.myvolkswagen.model;

import com.google.gson.annotations.SerializedName;

public class AppendNewsResponse extends Response {

    @SerializedName("NewsID")
    public int newsId;

    @Override
    public String toString() {
        return new StringBuilder(super.toString() + "\n")
                .append("***** AppendNewsResponse *****\n")
                .append("newsId=" + newsId + "\n")
                .append("********************")
                .toString();
    }
}
