package com.volkswagen.myvolkswagen.utils;

import com.volkswagen.myvolkswagen.BuildConfig;

public class Config {
    public static final String LINE_CHANNEL_ID = BuildConfig.DEBUG ? "1655984739" : "1655984733";
    public static final String WEB_HOST = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/" : "https://vwapp.volkswagentaiwan.com.tw/";
    public static final String API_HOST = WEB_HOST + "api/";
    public static final String VW_CONTACT_US_URL = "https://consulting-service-pre.volkswagentaiwan.com.tw/join?category_id=3";

    private static final String VWID_HOST = BuildConfig.DEBUG ? "https://identity-sandbox.vwgroup.io/oidc/v1/authorize" : "https://identity.vwgroup.io/oidc/v1/authorize";
    private static final String VWID_CLIENT_ID = BuildConfig.DEBUG ? "35f670e0-f120-44db-99fe-f4082f8470cb@apps_vw-dilab_com" : "e3ed3ac1-453e-46cd-b1ef-49bde85350ff@apps_vw-dilab_com";
    private static final String VWID_SCOPE = "openid profile";
    private static final String VWID_RESPONSE_TYPE = "code id_token token";
    private static final String VWID_NONCE = BuildConfig.DEBUG ? "8jYKt0sTiPS7eeTx9KeRg6Wq12eOppa3" : "joza7h9au82wlcfKzqeE1O2J1Ki3p86Y";

    public static final String VWID_LOGIN_URL = VWID_HOST + "?client_id=" + VWID_CLIENT_ID + "&scope=" + VWID_SCOPE + "&response_type=" + VWID_RESPONSE_TYPE + "&nonce=" + VWID_NONCE + "&redirect_uri=%s://&state=%s";

    public static final String VW_LINE_LIFF_URL = BuildConfig.DEBUG ? "https://liff.line.me/1655984739-AabB5xZq?token=%s&source=new" : "https://liff.line.me/1655984733-vm1KoyD4?token=%s&source=new";

    public static final String VW_IDENTITY_CAR_URL = BuildConfig.DEBUG ? "https://myvolkswagenapp.page.link/uLaQ" : "https://myvolkswagenapp.page.link/HExF";
    public static final String VW_WEBSITE_URL = "https://www.volkswagen.com.tw/zh.html";
    //    public static final String VW_STORES_URL = "https://www.volkswagen.com.tw/zh/find-a-dealer.html?lat-app=23.77479&lng-app=120.9428&zoom-app=7";
    public static final String VW_STORES_URL = "https://www.volkswagen.com.tw/zh/find-a-dealer.html?adchan=VW&campaign=VW_VWAPP_homepage_carinformation__os&adgroup=VWAPP&publisher=homepage&utm_source=homepage&utm_medium=carinformation&utm_campaign=VWAPP";
    public static final String ETAG_URL = "https://www.fetc.net.tw";
    public static final String GOV_URL = "https://www.mvdis.gov.tw/m3-emv-vil/vil/penaltyQueryPay";
    public static final String TEST_DRIVE_URL = "https://www.volkswagen.com.tw/app/testdrive/default.aspx";
    public static final String VW_TERMS_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/terms" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/terms";
    public static final String VW_PRIVACY_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/privacy-Statement" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/privacy-Statement";
    public static final String VW_CAR_ESTIMATE_URL = BuildConfig.DEBUG ? "https://sit.vw-bodyandpaint.com/vw/" : "https://vw-bodyandpaint.com/vw/";
    public static final String VW_MY_CAR_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/myCar.html" : "https://vwapp.volkswagentaiwan.com.tw/webview/myCar.html";
    public static final String VW_MY_CAR_IDENTIFICATION_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/connectToCPO.html" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/connectToCPO.html";
    public static final String VW_MEMBER_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/member.html" : "https://vwapp.volkswagentaiwan.com.tw/webview/member.html";
    public static final String VW_MEMBER_NEW_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/user" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/user";
    public static final String VW_MEMBER_COUPON_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/coupon" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/coupon";
    public static final String VW_MEMBER_BENEFITS_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/member-benefits" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/member-benefits";
    public static final String VW_MEMBER_PROFILE_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/user/edit" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/user/edit";
    public static final String VW_FAB_AST_CAR = BuildConfig.DEBUG ? "https://sit-ast.volkswagen-service.com/ast/" : "https://ast.volkswagen-service.com/ast/";
    public static final String VW_SUGGEST_MAINTAIN_CAR = BuildConfig.DEBUG ? "https://sit-service-cam.volkswagen-service.com" : "https://service-cam.volkswagen-service.com";
    public static final String VW_ONLINE_STORE_URL = "https://www.volkswagen-eshop.com/";
    public static final String VW_MAINTENANCE_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/maintenance" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/maintenance";

    public static final String VW_MEMBER_LEVEL = WEB_HOST + "member_terms/level";
    public static final String VW_MEMBER_FAQ = WEB_HOST + "member_terms/faq";

    public static final String VW_SV_NORMAL_LINK_QUERY_PARAMS = "aka_account_id";
    public static final String VW_SV_NEW_CAR_LINK_QUERY_PARAMS = "AccountId";

    public static final String VW_LINE_POINT_INFO = WEB_HOST + "member_terms/redeem";
    public static final String VW_LINE_POINT_REDEEM = "https://points.line.me/pointcode/#/pointcode/form";

    public static final String API_DEFAUL_TIMEZONE = "Asia/Taipei";

    public static final String AES_SECRET_KEY = BuildConfig.DEBUG ? "TzDma3z9MwYAsLjaPqxsMmE3G8ENpFvx" : "pROqtn1AeuHL3Z8k8ZsN5ndyQedO39Ea";
    public static final String AES_IV_KEY = BuildConfig.DEBUG ? "em8DRutVJ6yFJSYa" : "kTwGp9aLh8Qozf5U";

    public static final String CRUISYS_HOST = BuildConfig.DEBUG ? "https://sit-api.vw-bodyandpaint.com:25443/api/volkswagen/" : "https://api.vw-bodyandpaint.com/api/volkswagen/";

    public static final String VW_MAINTENANCE_RESERVATION_URL = BuildConfig.DEBUG ? "https://sit-ast.volkswagen-service.com/ast/" : "https://ast.volkswagen-service.com/ast/";
    public static final String VW_MAINTENANCE_RECORDS_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/maintenance/record" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/maintenance/record";
    public static final String VW_MAINTENANCE_PAT_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/pat" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/pat";
    public static final String VW_MAINTENANCE_BODY_AND_PAINT_URL = BuildConfig.DEBUG ? "https://sit.vw-bodyandpaint.com/vw/" : "https://vw-bodyandpaint.com/vw/";
    public static final String VW_MAINTENANCE_WARRANTY_URL = BuildConfig.DEBUG ? "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/warranty" : "https://vwapp.volkswagentaiwan.com.tw/webview/new/warranty";
    public static final String VW_MAINTENANCE_RECALL_URL = BuildConfig.DEBUG ? "https://sit-recall.vw-bodyandpaint.com/vw/r" : "https://recall.vw-bodyandpaint.com/vw/r";
    public static final String VW_MAINTENANCE_ADDITIONAL_QUOTATION_URL = BuildConfig.DEBUG ? "https://sit-service-cam.volkswagen-service.com/" : "https://service-cam.volkswagen-service.com/";

    public static final String VW_MAINTENANCE_RECALLS_URL = "https://sit-recall.vw-bodyandpaint.com/vw/r";
    public static final String VW_EMAIL_SERVICE = "https://www.volkswagen.com.tw/app/contactus/index.aspx?utm_source=VWAPP&utm_medium=member&utm_campaign=contactus";


}