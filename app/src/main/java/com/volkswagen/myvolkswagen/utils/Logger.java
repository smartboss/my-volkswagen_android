package com.volkswagen.myvolkswagen.utils;

import android.util.Log;

import com.volkswagen.myvolkswagen.BuildConfig;

public class Logger {
    public static boolean DEBUG = BuildConfig.DEBUG;

    public static String TAG = "MyVolkswagen";

    public static void v(Object object, String message) {
        if (DEBUG)
            Log.v(TAG, "[" + object.getClass().getSimpleName() + "] " + message);
    }

    public static void d(Object object, String message) {
        if (DEBUG)
            Log.d(TAG, "[" + object.getClass().getSimpleName() + "] " + message);
    }

    public static void i(Object object, String message) {
        if (DEBUG)
            Log.i(TAG, "[" + object.getClass().getSimpleName() + "] " + message);
    }

    public static void w(Object object, String message) {
        if (DEBUG)
            Log.w(TAG, "[" + object.getClass().getSimpleName() + "] " + message);
    }

    public static void e(Object object, String message) {
        if (DEBUG)
            Log.e(TAG, "[" + object.getClass().getSimpleName() + "] " + message);
    }
}
