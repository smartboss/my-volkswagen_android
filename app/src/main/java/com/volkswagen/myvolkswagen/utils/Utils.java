package com.volkswagen.myvolkswagen.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.util.Base64;
import android.webkit.CookieManager;
import android.webkit.MimeTypeMap;

import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.controller.PreferenceManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

public class Utils {

    public static final int REQUEST_PERMISSIONS_CODE = 100;
    public static final int REQUEST_PERMISSIONS_CAMERA = 102;
    public static final int REQ_CODE_CHOOSE_PHOTO = 10;
    public static final int REQ_CODE_TAKE_PICTURE = 11;
    public static final int REQ_LOGIN = 13;

    public static final int MAX_CORP_SIZE = 1242;

    public static String addDefaultParamsToUrl(String url) {
        Uri parsedUri = Uri.parse(url);
        Set<String> parameterNames = parsedUri.getQueryParameterNames();
        Uri.Builder builder = parsedUri.buildUpon();

        String carPlate = PreferenceManager.getInstance().getMainCarPlate();
        String carVin = PreferenceManager.getInstance().getMainCarVin();
        if (!carPlate.isEmpty() && !carVin.isEmpty()) {
            if (!parameterNames.contains("token")) {
                builder.appendQueryParameter("token", PreferenceManager.getInstance().getUserToken());
            }

            if (!parameterNames.contains("AccountId")) {
                builder.appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId());
            }

            if (!parameterNames.contains("LicensePlateNumber")) {
                builder.appendQueryParameter("LicensePlateNumber", carPlate);
            }

            if (!parameterNames.contains("VIN")) {
                builder.appendQueryParameter("VIN", carVin);
            }
        } else {
            if (!parameterNames.contains("token")) {
                builder.appendQueryParameter("token", PreferenceManager.getInstance().getUserToken());
            }

            if (!parameterNames.contains("AccountId")) {
                builder.appendQueryParameter("AccountId", PreferenceManager.getInstance().getAccountId());
            }
        }

        return builder.build().toString();
    }

    public static String formatVWDate(String originDateString) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat originalFormat2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            Date date = originalFormat.parse(originDateString);
            return newFormat.format(date);
        } catch (ParseException e) {
            try {
                Date date = originalFormat2.parse(originDateString);
                return newFormat.format(date);
            } catch (ParseException e2) {
                e2.printStackTrace();
                return "";
            }
        }
    }

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
//            if(phone.length() < 6 || phone.length() > 13) {
            if (phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static int dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return (int) (sp * scale);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidMobile(String prefix, String target) {
        if (target == null || prefix.isEmpty()) {
            return false;
        } else {
            if (prefix.equals("+886")) {
                return (target.matches("09[0-9]{8}") || target.matches("[0-9]{9}"));
            } else {
                return true;
            }
        }
    }

    public final static boolean checkPermissions(Context context, String permission) {
        int permissionState = ActivityCompat.checkSelfPermission(context, permission);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public final static String takePicture(@NonNull Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String currentPhotoPath = null;
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                currentPhotoPath = "file://" + photoFile.getPath();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile));
                activity.startActivityForResult(takePictureIntent, REQ_CODE_TAKE_PICTURE);
            }
        }
        return currentPhotoPath;
    }

    public final static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "/JPEG_" + timeStamp;
        String storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();
        File image = new File(storageDir + imageFileName + ".jpg");
        return image;
    }

    public final static void choosePhoto(@NonNull Activity activity) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(pickPhoto, REQ_CODE_CHOOSE_PHOTO);
    }

    public static String getMimeType(String uri) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(uri);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }


    public static String getBase64FromImageUri(Context context, Uri uri) {

        if (uri != null && uri.getPath() != null) {
            File file = new File(uri.getPath());

            int size = (int) file.length();

            if (size > 0) {

                byte[] bytes = new byte[size];

                try {
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                    buf.read(bytes, 0, bytes.length);
                    buf.close();

                    return context.getResources().getString(R.string.profile_img_base64,
                            Utils.getMimeType(uri.toString()),
                            Base64.encodeToString(bytes, Base64.DEFAULT));

                } catch (FileNotFoundException e) {
                    Logger.d(context, "update coverimg FileNotFoundException: " + e.getLocalizedMessage());
                } catch (IOException e) {
                    Logger.d(context, "update coverimg IOException: " + e.getLocalizedMessage());
                }
            }
        }

        return null;
    }

    public static void deleteFileWithUri(Uri uri) {
        if (uri != null && uri.getPath() != null) {
            File file = new File(uri.getPath());
            file.delete();
        }
    }

    public static void deleteWebViewCookiesForDomain(String domain) {
        CookieManager cookieManager = CookieManager.getInstance();
        if (domain == null) {
            cookieManager.removeAllCookies(null);
        } else {
            String cookieGlob = cookieManager.getCookie(domain);
            if (cookieGlob != null) {
                String[] cookies = cookieGlob.split(";");
                for (String cookieTuple : cookies) {
                    String[] cookieParts = cookieTuple.split("=");
                    cookieManager.setCookie(domain, cookieParts[0] + "=");
                }
            }
        }
    }

    public static String formatPlateNoToApiPath(String plate) {
        String s = plate.toUpperCase();
        return s.replaceAll("[^a-zA-Z0-9]*", "");
    }

    public static String timeAgo(String dateString, String formatPattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatPattern, Locale.getDefault());
        Calendar dateCalendar = Calendar.getInstance();
        try {
            dateCalendar.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
            return "日期解析錯誤";
        }

        Calendar currentCalendar = Calendar.getInstance();
        long diffInMillis = currentCalendar.getTimeInMillis() - dateCalendar.getTimeInMillis();

        long years = diffInMillis / (365 * 24 * 60 * 60 * 1000L);
        if (years > 0) {
            return years + "年前";
        }

        long months = diffInMillis / (30 * 24 * 60 * 60 * 1000L);
        if (months > 0) {
            return months + "月前";
        }

        long days = diffInMillis / (24 * 60 * 60 * 1000L);
        if (days > 0) {
            return days + "天前";
        }

        long hours = diffInMillis / (60 * 60 * 1000L);
        if (hours > 0) {
            return hours + "小時前";
        }

        long minutes = diffInMillis / (60 * 1000L);
        if (minutes > 0) {
            return minutes + "分鐘前";
        }

        return "剛剛";
    }
}

