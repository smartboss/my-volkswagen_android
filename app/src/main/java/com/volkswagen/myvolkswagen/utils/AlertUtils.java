package com.volkswagen.myvolkswagen.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;

public class AlertUtils {

    public static final KProgressHUD showLoadingHud(final Context context) {
        return KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static final void showAlert(final Activity activity, final String title, final String message, final String negative, final String positive, final DialogInterface.OnClickListener listener) {
        if (activity == null) {
            return;
        }

        if (TextUtils.isEmpty(negative) && TextUtils.isEmpty(positive)) {
            Toast.makeText(activity, title + "\n" + message, Toast.LENGTH_SHORT).show();
        } else {
            showDialog(activity, title, message, negative, positive, listener);
        }
    }

    private final static void showDialog(final Context context, final String title, final String message, final String negative, final String positive, final DialogInterface.OnClickListener listener) {
        if (TextUtils.isEmpty(message)) {
            return;
        }

        if (TextUtils.isEmpty(positive)) {
            return;
        }

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);


        // set title
        if (!TextUtils.isEmpty(title)) {
            alertDialogBuilder.setTitle(title);
        }

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positive, listener);

        // set negative
        if (!TextUtils.isEmpty(negative)) {
            alertDialogBuilder.setNegativeButton(negative, listener);
        }

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static final void selectItemDialog(final Activity activity, final String title, int itemsId, final DialogInterface.OnClickListener listener) {
        if (activity == null) {
            return;
        }

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // set title
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        // add a list
        builder.setItems(itemsId, listener);


        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static final void selectItemDialog(final Activity activity, final String title, CharSequence[] items, final DialogInterface.OnClickListener listener) {
        if (activity == null) {
            return;
        }

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // set title
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        // add a list
        builder.setItems(items, listener);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
