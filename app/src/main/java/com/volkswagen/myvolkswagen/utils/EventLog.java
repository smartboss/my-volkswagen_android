package com.volkswagen.myvolkswagen.utils;

public class EventLog {
    public static final String EVENT_HEADER_MENU = "header_menu";
    public static final String EVENT_PV_HOME = "pv_home";
    public static final String EVENT_PV_VEHICLE = "pv_vehicle";
    public static final String EVENT_PV_NOTICE = "pv_notice";
    public static final String EVENT_PV_NOTICE_DETAIL = "pv_notice_detail";
    public static final String EVENT_PV_NEWS = "pv_news";
    public static final String EVENT_PV_NEWS_DETAIL = "pv_news_detail";
    public static final String EVENT_FOOTER_MENU = "footer_menu";

    public static final String EVENT_NAME_PAGE_VIEW = "pageview";

    public static final String EVENT_CATEGORY_PAGE_VIEW = "pageview";
    public static final String EVENT_CATEGORY_BUTTON = "button";

    public static String generateUniqueId() {
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 36; i++) {
            int idx = (int) (Math.random() * str.length());
            sb.append(str.charAt(idx));
        }
        return sb.toString();
    }
}
