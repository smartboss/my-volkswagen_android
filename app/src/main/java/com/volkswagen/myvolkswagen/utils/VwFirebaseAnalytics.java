package com.volkswagen.myvolkswagen.utils;

public class VwFirebaseAnalytics {

    public static class Event {
        public static final String LOGIN_BACK = "login_back";
        public static final String LOGIN_SKIP_FB = "login_skip_fb";
        public static final String CONNECT_FB = "connect_fb";
        public static final String ADD_CAR = "add_car";
        public static final String ADD_CAR_FIRST = "add_car_first";
        public static final String ADD_CAR_COMPLETE = "add_car_complete";
        public static final String ADD_POST = "add_post";
        public static final String ADD_POST_TAG = "add_post_tag";
        public static final String ADD_POST_COMPLETE = "add_post_complete";
        public static final String POST_SHARE = "post_share";
        public static final String POST_LIKE = "post_like";
        public static final String POST_COMMENT = "post_comment";
        public static final String POST_SAVE = "post_save";
        public static final String SEARCH = "search";
        public static final String VIEW_SEARCH_RESULTS = "view_search_results";
        public static final String VIEW_NEWSFEED_ALL = "view_newsfeed_all";
        public static final String VIEW_NEWSFEED_FOLLOWING = "view_newsfeed_following";
        public static final String VIEW_CAR = "view_car";
        public static final String VIEW_TOOL = "view_tool";
        public static final String VIEW_MY = "view_my";
        public static final String VIEW_VW = "view_vw";
        public static final String VIEW_NOTIFICATION = "view_notification";
        public static final String VIEW_OTHER_PROFILE = "view_other_profile";
        public static final String VIEW_VW_PROFILE = "view_vw_profile";
        public static final String VIEW_VW_NEWS = "view_vw_news";
        public static final String VIEW_VW_EVENT = "view_vw_event";
        public static final String VIEW_FOLLOW_LIST = "view_follow_list";
        public static final String VIEW_FOLLOWING_LIST = "view_following_list";
        public static final String NEWSFEED_TAG = "newsfeed_tag";
        public static final String VW_TEST_DRIVE = "vw_test_drive";
        public static final String VW_360 = "vw_360";
        public static final String VW_HELP_CALL = "vw_help_call";
        public static final String VW_CENTER = "vw_center";
        public static final String VW_OFFICIAL_SITE = "vw_official_site";
        public static final String FOLLOW = "follow";
        public static final String UNFOLLOW = "unfollow";
        public static final String CAR_RESERVATION = "car_reservation";
        public static final String TOOL_PARKING = "tool_parking";
        public static final String TOOL_GAS = "tool_gas";
        public static final String TOOL_VW_CENTER = "tool_vw_center";
        public static final String TOOL_HELP_CALL = "tool_help_call";
        public static final String TOOL_SOP = "tool_sop";
        public static final String TOOL_ETAG = "tool_etag";
        public static final String TOOL_BILL = "tool_bill";
        public static final String QR_CODE = "qr_code";
        public static final String SETTING = "setting";
    }

    public static class Param {
        public static final String SCREEN_NAME = "screen_name";
        public static final String TAG_NAME = "tag_name";
    }

    public static class Value {
        public static final String SCREEN_LOGIN = "screen_login";
        public static final String SCREEN_SETTING = "screen_setting";
        public static final String SCREEN_CAR = "screen_car";
        public static final String SCREEN_NEWSFEED_ALL = "screen_newsfeed_all";
        public static final String SCREEN_NEWSFEED_FOLLOWING = "screen_newsfeed_following";
        public static final String SCREEN_MY = "screen_my";
        public static final String TAG_FORMAT = "tag_%s";
    }
}
