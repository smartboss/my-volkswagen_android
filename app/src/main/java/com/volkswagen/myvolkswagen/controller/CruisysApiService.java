package com.volkswagen.myvolkswagen.controller;

import com.volkswagen.myvolkswagen.model.DongleBindingInput;
import com.volkswagen.myvolkswagen.model.ExtIdAuthResponse;
import com.volkswagen.myvolkswagen.model.CarLocationResponseData;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.CarTripsResponseData;
import com.volkswagen.myvolkswagen.model.DongleBindingResponseData;
import com.volkswagen.myvolkswagen.model.DongleNotificationResponseData;
import com.volkswagen.myvolkswagen.model.ExtIdAuthInput;
import com.volkswagen.myvolkswagen.model.ExtRedirectUrlInput;
import com.volkswagen.myvolkswagen.model.ExtRedirectUrlResponse;
import com.volkswagen.myvolkswagen.model.QuotationsResponseData;
import com.volkswagen.myvolkswagen.model.ServiceCardResponseData;
import com.volkswagen.myvolkswagen.model.ServiceReminderResponseData;
import com.volkswagen.myvolkswagen.model.WarrantyResponseData;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface CruisysApiService {
    @Headers("Content-Type: application/json")
    @POST("service_go/ext_id_auth")
    Call<ExtIdAuthResponse> postExtIdAuth(@Body ExtIdAuthInput data);

    @Headers("Content-Type: application/json")
    @GET("service_go/cars/{plateNo}/service_reminder/{userToken}")
    Call<ServiceReminderResponseData> getServiceReminder(@Path("plateNo") String plateNo, @Path("userToken") String userToken, @Header("Authorization") String cruisysToken);

    @Headers("Content-Type: application/json")
    @GET("service_go/cars/{plateNo}/warranty")
    Call<WarrantyResponseData> getWarranty(@Path("plateNo") String plateNo, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("service_go/cars/{plateNo}/dongle_binding")
    Call<DongleBindingResponseData> getDongleBinding(@Path("plateNo") String plateNo, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("service_go/cars/{plateNo}/dongle_binding")
    Call<DongleBindingResponseData> postDongleBinding(@Path("plateNo") String plateNo, @Body DongleBindingInput data, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @DELETE("service_go/cars/{plateNo}/dongle_binding")
    Call<Object> deleteDongleBinding(@Path("plateNo") String plateNo, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("service_go/cars/{plateNo}/summary")
    Call<CarSummaryResponseData> getSummary(@Path("plateNo") String plateNo, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET("service_go/cars/{plateNo}/dongle_notification")
    Call<DongleNotificationResponseData> getDongleNotification(@Path("plateNo") String plateNo, @Header("Authorization") String token);
    @Headers("Content-Type: application/json")
    @POST("service_go/ext_redirect_url")
    Call<ExtRedirectUrlResponse> postExtRedirectUrl(@Body ExtRedirectUrlInput data, @Header("Authorization") String token);
    //<------- before -------->
    // API 3
    @Headers("Content-Type: application/json")
    @GET()
    Call<ServiceCardResponseData> service_card(@Url String url, @Header("Authorization") String token);

    // API 5
    @Headers("Content-Type: application/json")
    @GET()
    Call<QuotationsResponseData> customers_quotations(@Url String url, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET()
    Call<CarSummaryResponseData> service_summary(@Url String url, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET()
    Call<CarTripsResponseData> service_car_trips(@Url String url, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET()
    Call<CarLocationResponseData> service_car_location(@Url String url, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET()
    Call<DongleNotificationResponseData> service_dongle_notification(@Url String url, @Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @GET()
    Call<DongleBindingResponseData> service_get_dongle_bind(@Url String url, @Header("Authorization") String token);
}