package com.volkswagen.myvolkswagen.controller;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

import android.app.Activity;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.model.GsonTypeAdapterFactories;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.view.LoginActivity;

import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientManager {

    private static ApiClientManager sInstance = new ApiClientManager();

    private Retrofit mRetrofit;

    private ApiClientManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }
        OkHttpClient client = builder.build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactories.VW_INTEGER_FACTORY)
                .registerTypeAdapterFactory(GsonTypeAdapterFactories.VW_BOOLEAN_FACTORY)
                .create();

//        mRetrofit = new Retrofit.Builder()
//                .baseUrl(Config.API_HOST)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
        OkHttpClient mOkHttpClient = ApiClientManager.getUnsafeOkHttpClient();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(Config.API_HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(mOkHttpClient)
                .build();
    }

    public static Retrofit getClient() {
        return sInstance.mRetrofit;
    }

    public static boolean handleTokenResponse(final Activity activity, final retrofit2.Response response) {
        Response data = (Response) response.body();
        if (data != null && data.errorCode == 1001) {
            if (activity != null) {
                LoginActivity.goLogin(activity);
            }
            return true;
        }

        // update token if necessary
        String auth = response.headers().get("Authorization");
        if (!TextUtils.isEmpty(auth)) {
            PreferenceManager.getInstance().saveUserToken(auth.replaceFirst("\\s*Bearer\\s+", ""));
        }
        return false;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
