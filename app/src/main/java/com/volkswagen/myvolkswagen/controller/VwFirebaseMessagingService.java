package com.volkswagen.myvolkswagen.controller;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.volkswagen.myvolkswagen.R;
import com.volkswagen.myvolkswagen.model.PushData;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.Logger;
import com.volkswagen.myvolkswagen.view.MainNewActivity;

import java.security.SecureRandom;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class VwFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(final String token) {
        String userToken = PreferenceManager.getInstance().getUserToken();
        if (TextUtils.isEmpty(userToken)) {
            return;
        }

        ApiService service = ApiClientManager.getClient().create(ApiService.class);
        service.updatePushToken(new PushData("android", token), userToken)
                .enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (!response.isSuccessful()) {
                            Logger.d(VwFirebaseMessagingService.this, "Failed to update push token");
                            return;
                        }

                        Response data = response.body();
                        if (!data.result) {
                            Logger.d(VwFirebaseMessagingService.this, "Failed to update push token: code=" + data.errorCode);
                            return;
                        }

                        PreferenceManager.getInstance().savePushToken(token);
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        Logger.d(VwFirebaseMessagingService.this, "Failed to update push token: " + t.getLocalizedMessage());
                    }
                });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sendNotification(remoteMessage);
    }

    private void sendNotification(RemoteMessage remoteMessage) {
        SecureRandom ranGen = new SecureRandom();

        Intent intent = new Intent(this, MainNewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            intent.putExtra(entry.getKey(), entry.getValue());
        }

        int reqCode = ranGen.nextInt(Integer.MAX_VALUE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, reqCode, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "News Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        int pushId = ranGen.nextInt();
        notificationManager.notify(pushId, notificationBuilder.build());
    }
}
