package com.volkswagen.myvolkswagen.controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarLocationResponseData;
import com.volkswagen.myvolkswagen.model.CarModel;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.CarTripsResponseData;
import com.volkswagen.myvolkswagen.model.DongleNotificationResponseData;
import com.volkswagen.myvolkswagen.model.DongleBindingResponseData;
import com.volkswagen.myvolkswagen.model.MyCar;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.QuotationsResponseData;
import com.volkswagen.myvolkswagen.model.ServiceCardResponseData;
import com.volkswagen.myvolkswagen.model.ServiceReminderResponseData;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PreferenceManager {
    private static final String PREFERENCE_APP = "pref_app";
    private static final String KEY_STATE = "key_state";
    private static final String KEY_USER_TOKEN = "key_user_token";
    private static final String KEY_CRUISYS_ACCESS_TOKEN = "key_cruisys_assess_token";
    private static final String KEY_ACCOUNT_ID = "key_account_id";
    private static final String KEY_EMAIL = "key_email";
    private static final String KEY_MOBILE = "key_mobile";
    private static final String KEY_LEVEL = "key_level";
    private static final String KEY_LEVEL_NAME = "key_level_name";
    private static final String KEY_CAR_TYPES = "key_car_types";
    private static final String KEY_RECENTSEARCH = "key_recent_search";
    private static final String KEY_PUSH_TOKEN = "key_push_token";
    private static final String KEY_NEW_NOTICE_COUNT = "key_new_notice_count";
    private static final String KEY_NICKNAME = "key_nickname";
    private static final String KEY_CAR_OWNER_NAME = "key_car_owner_name";
    private static final String KEY_MY_CARS = "key_my_cars";
    private static final String KEY_CAR_SUMMARY = "key_car_summary";
    private static final String KEY_QUOTATIONS = "key_quotations";
    private static final String KEY_REMINDER = "key_reminder";
    private static final String KEY_SERVICE_CARD = "key_service_card";
    private static final String KEY_CAR_LOCATION = "key_car_location";
    private static final String KEY_CAR_TRIPS = "key_car_trips";
    private static final String KEY_DONGLE_NOTIFICATION = "key_dongle_notification";
    private static final String KEY_DONGLE_BIND = "key_dongle_bind";
    private static final String KEY_NEVER_SHOW_DONGLE_HINT = "key_never_show_dongle";
    private static final String KEY_MAIN_CAR_PLATE = "key_main_car_plate";
    private static final String KEY_MAIN_CAR_VIN = "key_main_car_vin";
    private static final PreferenceManager sInstance = new PreferenceManager();
    private static Context sContext;
    private static SharedPreferences sAppPreference;


    public static PreferenceManager getInstance() {
        return sInstance;
    }

    public void init(Context context) {
        sContext = context;
        if (sContext != null) {
            sAppPreference = sContext.getSharedPreferences(PREFERENCE_APP, Context.MODE_PRIVATE);
        }
    }

    public void saveState(String state) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_STATE, state);
            editor.commit();
        }
    }

    public String getState() {
        String state = null;
        if (sAppPreference != null) {
            state = sAppPreference.getString(KEY_STATE, null);
        }
        return state;
    }

    public void saveUserData(String token, String accountId, String email, String mobile, int level, String levelName) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_USER_TOKEN, token);
            editor.putString(KEY_ACCOUNT_ID, accountId);
            editor.putString(KEY_EMAIL, email);
            editor.putString(KEY_MOBILE, mobile);
            editor.putInt(KEY_LEVEL, level);
            editor.putString(KEY_LEVEL_NAME, levelName);
            editor.commit();
        }
    }

    public void saveUserToken(String token) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_USER_TOKEN, token);
            editor.commit();
        }
    }

    public String getUserToken() {
        String token = null;
        if (sAppPreference != null) {
            token = sAppPreference.getString(KEY_USER_TOKEN, null);
        }
        return token;
    }

    public void deleteUserToken() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_USER_TOKEN);
            editor.commit();
        }
    }

    public void saveCruisysAccessToken(String token) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_CRUISYS_ACCESS_TOKEN, token);
            editor.commit();
        }
    }

    public String getCruisysAccessToken() {
        String token = null;
        if (sAppPreference != null) {
            token = sAppPreference.getString(KEY_CRUISYS_ACCESS_TOKEN, null);
        }
        return token;
    }

    public void deleteCruisysAccessToken() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_CRUISYS_ACCESS_TOKEN);
            editor.commit();
        }
    }

    public String getAccountId() {
        String accountId = null;
        if (sAppPreference != null) {
            accountId = sAppPreference.getString(KEY_ACCOUNT_ID, null);
        }
        return accountId;
    }

    public String getEmail() {
        String email = null;
        if (sAppPreference != null) {
            email = sAppPreference.getString(KEY_EMAIL, null);
        }
        return email;
    }

    public String getMobile() {
        String mobile = null;
        if (sAppPreference != null) {
            mobile = sAppPreference.getString(KEY_MOBILE, null);
        }
        return mobile;
    }

    public void saveLevel(int level) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putInt(KEY_LEVEL, level);
            editor.commit();
        }
    }

    public int getLevel() {
        int level = 0;
        if (sAppPreference != null) {
            level = sAppPreference.getInt(KEY_LEVEL, 0);
        }
        return level;
    }

    public void saveLevelName(String levelName) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_LEVEL_NAME, levelName);
            editor.commit();
        }
    }

    public String getLevelName() {
        String level_name = null;
        if (sAppPreference != null) {
            level_name = sAppPreference.getString(KEY_LEVEL_NAME, null);
        }
        return level_name;
    }

    public void deleteUserData() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_USER_TOKEN);
            editor.remove(KEY_ACCOUNT_ID);
            editor.remove(KEY_EMAIL);
            editor.remove(KEY_MOBILE);
            editor.remove(KEY_LEVEL);
            editor.remove(KEY_LEVEL_NAME);
            editor.remove(KEY_PUSH_TOKEN);
            editor.commit();
        }
    }

    public void saveCarTypes(List<CarModel> carTypes) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(carTypes);
            editor.putString(KEY_CAR_TYPES, json);
            editor.commit();
        }
    }

    public List<CarModel> getCarTypes() {

        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_CAR_TYPES, null);
            Gson gson = new Gson();
            Type type = new TypeToken<List<CarModel>>() {
            }.getType();
            return gson.fromJson(json, type);
        }

        return null;
    }

    public void savePushToken(String token) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_PUSH_TOKEN, token);
            editor.apply();
        }
    }

    public String getPushToken() {
        String token = null;
        if (sAppPreference != null) {
            token = sAppPreference.getString(KEY_PUSH_TOKEN, null);
        }
        return token;
    }

    public void saveNewNoticeCount(int count) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putInt(KEY_NEW_NOTICE_COUNT, count);
            editor.apply();
        }
    }

    public void saveRecentSearch(List<String> accountIds) {

        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(accountIds);
            editor.putString(KEY_RECENTSEARCH, json);
            editor.apply();
        }
    }

    public List<String> getRecentSearch() {

        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_RECENTSEARCH, null);
            Gson gson = new Gson();
            Type type = new TypeToken<List<String>>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void deleteRecentSearch() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_RECENTSEARCH);
            editor.commit();
        }
    }

    public int getNewNoticeCount() {
        int count = 0;
        if (sAppPreference != null) {
            count = sAppPreference.getInt(KEY_NEW_NOTICE_COUNT, 0);
        }
        return count;
    }

    public void saveUserNickname(String nickname) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_NICKNAME, nickname);
            editor.commit();
        }
    }

    public String getUserNickname() {
        String nickname = null;
        if (sAppPreference != null) {
            nickname = sAppPreference.getString(KEY_NICKNAME, null);
        }
        return nickname;
    }

    public void deleteUserNickName() {
        SharedPreferences.Editor editor = sAppPreference.edit();
        editor.remove(KEY_NICKNAME);
        editor.commit();
    }

    public void saveMyCars(MyCarData carData) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(carData);
            editor.putString(KEY_MY_CARS, json);
            editor.commit();
        }
    }

    public MyCarData getMyCars() {

        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_MY_CARS, null);
            Gson gson = new Gson();
            Type type = new TypeToken<MyCarData>() {
            }.getType();
            return gson.fromJson(json, type);
        }

        return null;
    }

    public void deleteMyCars() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_MY_CARS);
            editor.commit();
        }
    }


    public void saveMyCarSummary(CarSummaryResponseData summary) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(summary);
            editor.putString(KEY_CAR_SUMMARY, json);
            editor.commit();
        }
    }

    // TODO: 2023/12/30 remove it
    public CarData tryGetFirstCarData() {
        MyCarData myCarData = getMyCars();
        String carPlate = getMainCarPlate();
        CarData carData = null;

        if (myCarData == null || myCarData.cars == null || myCarData.cars.size() == 0) {
            return new CarData();
        }

        for (MyCar mycar : myCarData.cars) {
            if (mycar.carData.size() > 0 && mycar.carData.get(0).plate != null && mycar.carData.get(0).plate.equalsIgnoreCase(carPlate)) {
                return mycar.carData.get(0);
            }
        }

        return new CarData();
    }


    public CarSummaryResponseData getCarSummary() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_CAR_SUMMARY, null);
            Gson gson = new Gson();
            Type type = new TypeToken<CarSummaryResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveQuotations(QuotationsResponseData questions) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(questions);
            editor.putString(KEY_QUOTATIONS, json);
            editor.commit();
        }
    }

    public QuotationsResponseData getQuestions() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_QUOTATIONS, null);
            Gson gson = new Gson();
            Type type = new TypeToken<QuotationsResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveReminder(ServiceReminderResponseData reminder) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(reminder);
            editor.putString(KEY_REMINDER, json);
            editor.commit();
        }
    }

    public ServiceReminderResponseData getReminder() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_REMINDER, null);
            Gson gson = new Gson();
            Type type = new TypeToken<ServiceReminderResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveServiceCard(ServiceCardResponseData serviceCard) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(serviceCard);
            editor.putString(KEY_SERVICE_CARD, json);
            editor.commit();
        }
    }

    public ServiceCardResponseData getCarService() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_SERVICE_CARD, null);
            Gson gson = new Gson();
            Type type = new TypeToken<ServiceCardResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveCarLocation(CarLocationResponseData location) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(location);
            editor.putString(KEY_CAR_LOCATION, json);
            editor.commit();
        }
    }

    public CarLocationResponseData getCarLocation() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_CAR_LOCATION, null);
            Gson gson = new Gson();
            Type type = new TypeToken<CarLocationResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveCarTrips(CarTripsResponseData carTrips) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(carTrips);
            editor.putString(KEY_CAR_TRIPS, json);
            editor.commit();
        }
    }

    public CarTripsResponseData getCarTrips() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_CAR_TRIPS, null);
            Gson gson = new Gson();
            Type type = new TypeToken<CarTripsResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveNotification(DongleNotificationResponseData dongleNotification) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(dongleNotification);
            editor.putString(KEY_DONGLE_NOTIFICATION, json);
            editor.commit();
        }
    }

    public DongleNotificationResponseData getDongleNotification() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_DONGLE_NOTIFICATION, null);
            Gson gson = new Gson();
            Type type = new TypeToken<DongleNotificationResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void saveDongle_binding(DongleBindingResponseData bind) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            Gson gson = new Gson();
            String json = gson.toJson(bind);
            editor.putString(KEY_DONGLE_BIND, json);
            editor.commit();
        }
    }

    public DongleBindingResponseData getDongleBind() {
        if (sAppPreference != null) {
            String json = sAppPreference.getString(KEY_DONGLE_BIND, null);
            Gson gson = new Gson();
            Type type = new TypeToken<DongleBindingResponseData>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        return null;
    }

    public void neverShowDongleHint() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putBoolean(KEY_NEVER_SHOW_DONGLE_HINT, true);
            editor.commit();
        }
    }

    public boolean isNeverShowDongleHint() {
        if (sAppPreference != null) {
            return sAppPreference.getBoolean(KEY_NEVER_SHOW_DONGLE_HINT, false);
        }
        return true;
    }

    public void saveMainCar(String carOwnerName, String carPlate, String carVin) {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.putString(KEY_CAR_OWNER_NAME, carOwnerName);
            editor.putString(KEY_MAIN_CAR_PLATE, carPlate);
            editor.putString(KEY_MAIN_CAR_VIN, carVin);
            editor.commit();
        }
    }

    public void deleteMainCar() {
        if (sAppPreference != null) {
            SharedPreferences.Editor editor = sAppPreference.edit();
            editor.remove(KEY_CAR_OWNER_NAME);
            editor.remove(KEY_MAIN_CAR_PLATE);
            editor.remove(KEY_MAIN_CAR_VIN);
            editor.commit();
        }
    }

    public String getCarOwnerName() {
        String name = null;
        if (sAppPreference != null) {
            name = sAppPreference.getString(KEY_CAR_OWNER_NAME, null);
        }
        return name;
    }

    public String getMainCarPlate() {
        if (sAppPreference != null) {
            return sAppPreference.getString(KEY_MAIN_CAR_PLATE, "");
        }
        return "";
    }

    public String getMainCarVin() {
        if (sAppPreference != null) {
            return sAppPreference.getString(KEY_MAIN_CAR_VIN, "");
        }
        return "";
    }

    public ArrayList<String> getCarPlateList() {
        ArrayList<String> carplateList = new ArrayList<String>();
        List<MyCar> mycars = getMyCars().cars;
        for (MyCar mycar : mycars) {
            List<CarData> carDatas = mycar.carData;
            for (CarData cardata : carDatas) {
                carplateList.add(cardata.plate);
            }
        }
        return carplateList;
    }
}
