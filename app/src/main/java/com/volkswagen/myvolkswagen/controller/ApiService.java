package com.volkswagen.myvolkswagen.controller;

import com.volkswagen.myvolkswagen.model.AccountBindingData;
import com.volkswagen.myvolkswagen.model.AccountId;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.AppendCommentInput;
import com.volkswagen.myvolkswagen.model.AppendNewsInput;
import com.volkswagen.myvolkswagen.model.AppendNewsResponse;
import com.volkswagen.myvolkswagen.model.BindingData;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.model.CarMaintenanceResponse;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.CarTypeData;
import com.volkswagen.myvolkswagen.model.DeleteCommentInput;
import com.volkswagen.myvolkswagen.model.FollowMemberInput;
import com.volkswagen.myvolkswagen.model.GetCarModelData;
import com.volkswagen.myvolkswagen.model.InboxContentInput;
import com.volkswagen.myvolkswagen.model.GetNewsListInput;
import com.volkswagen.myvolkswagen.model.GetNoticesInput;
import com.volkswagen.myvolkswagen.model.InboxContentData;
import com.volkswagen.myvolkswagen.model.LoginData_ID;
import com.volkswagen.myvolkswagen.model.LoginData_OTP;
import com.volkswagen.myvolkswagen.model.ManipulateNewsInput;
import com.volkswagen.myvolkswagen.model.MemberCarMaintenancePlantInput;
import com.volkswagen.myvolkswagen.model.MemberCarMaintenanceResponse;
import com.volkswagen.myvolkswagen.model.MemberContentInput;
import com.volkswagen.myvolkswagen.model.MemberLevelInfoResponse;
import com.volkswagen.myvolkswagen.model.MemberListInBehaviorInput;
import com.volkswagen.myvolkswagen.model.MemberRelationData;
import com.volkswagen.myvolkswagen.model.MemberRelationInput;
import com.volkswagen.myvolkswagen.model.MyCarButtonResponse;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.MyMemberLevelInfo;
import com.volkswagen.myvolkswagen.model.News;
import com.volkswagen.myvolkswagen.model.NewsActionGeneralInput;
import com.volkswagen.myvolkswagen.model.NewsList;
import com.volkswagen.myvolkswagen.model.NoticeCount;
import com.volkswagen.myvolkswagen.model.NoticeData;
import com.volkswagen.myvolkswagen.model.NoticeReadInput;
import com.volkswagen.myvolkswagen.model.OperateCarData;
import com.volkswagen.myvolkswagen.model.OtpData;
import com.volkswagen.myvolkswagen.model.PatContentInput;
import com.volkswagen.myvolkswagen.model.PatContentResponse;
import com.volkswagen.myvolkswagen.model.PatEventLogInput;
import com.volkswagen.myvolkswagen.model.PatNoticeInput;
import com.volkswagen.myvolkswagen.model.Profile;
import com.volkswagen.myvolkswagen.model.ProfileRedeem;
import com.volkswagen.myvolkswagen.model.PushData;
import com.volkswagen.myvolkswagen.model.QrCode;
import com.volkswagen.myvolkswagen.model.RecallCheckCampaignResponse;
import com.volkswagen.myvolkswagen.model.RedeemCodeResponse;
import com.volkswagen.myvolkswagen.model.RedeemInput;
import com.volkswagen.myvolkswagen.model.RedeemList;
import com.volkswagen.myvolkswagen.model.RedeemPointListResponse;
import com.volkswagen.myvolkswagen.model.ReportNewsInput;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.model.SearchMemberRelationInput;
import com.volkswagen.myvolkswagen.model.SearchMemberRelationRecentlyInput;
import com.volkswagen.myvolkswagen.model.UpdateNewsInput;
import com.volkswagen.myvolkswagen.model.UpdateProfileInput;
import com.volkswagen.myvolkswagen.model.User;
import com.volkswagen.myvolkswagen.model.Version;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    // member api
    // Member API001
    @GET("APP_Version?Device=android")
    Call<Version> getVersion();

    @POST("APP_VWID_Login")
    Call<User> loginById(@Body LoginData_ID data);

    // Member API020
    @POST("APP_Phone_Send_Code")
    Call<Response> genOtpCode(@Body OtpData data);

    // Member API021
    @POST("APP_Phone_Login")
    Call<User> loginByOtp(@Body LoginData_OTP data);

    // Member API022
    @POST("APP_Bind_VWID")
    Call<User> bindVWidToOTP(@Body AccountBindingData data);

    @POST("APP_FBID")
    Call<Response> bindFb(@Body BindingData data, @Query("token") String token);

    // Member API003
    @POST("APP_AutoLogin")
    Call<User> autoLogin(@Query("token") String token);

    @POST("APP_Logout")
    Call<Response> logout(@Query("token") String token);

    // Member API013
    @GET("APP_Get_MemberLevel_Info")
    Call<MyMemberLevelInfo> getMemberLevelInfo(@Query("token") String token);

    // Member API005
    @POST("APP_Get_MyCar_Data")
    Call<MyCarData> getMyCarData(@Query("token") String token);

    // Member API008
    @POST("APP_Get_CarExist")
    Call<CarExistData> getCarExist(@Body OperateCarData data, @Query("token") String token);

    // Member API009
    @POST("APP_Get_CarModel_Data")
    Call<CarModelData> getCarModelData(@Body GetCarModelData data, @Query("token") String token);

    // Member API006
    @POST("APP_Add_Car_Data")
    Call<Response> addCar(@Body AddCarData data, @Query("token") String token);

    // Member API007
    @POST("APP_Del_Car_Data")
    Call<Response> deleteCar(@Body OperateCarData data, @Query("token") String token);

    // Member API010
    @POST("APP_Push_Token")
    Call<Response> updatePushToken(@Body PushData data, @Query("token") String token);

    // Member API011
    @GET("APP_Get_Member_Qrcode")
    Call<QrCode> getQrCode(@Query("token") String token);

    // Member API036
    @GET("app/news")
    Call<AppNews> getAppNews();

    // Member API038
    @DELETE("app/notice/{noticeId}")
    Call<Response> deleteNotice(@Path("noticeId") String noticeId, @Header("Authorization") String token);

    // community api CU_API14
    @POST("APP_CU_Get_Member_Profile")
    Call<Profile> getProfile(@Body AccountId id, @Query("token") String token);

    // Member API023
    @GET("APP_Get_Maintenance_Plant")
    Call<CarMaintenanceResponse> getMaintenancePlant(@Query("token") String token);

    @GET("APP_Redeem_Get_Stat")
    Call<ProfileRedeem> getProfileRedeem(@Query("token") String token);

    @POST("APP_CU_Update_Member_Profile")
    Call<Response> updateProfile(@Body UpdateProfileInput input, @Query("token") String token);

    // CU_API01
    @GET("APP_CU_Get_Car_Model")
    Call<CarTypeData> getCarTypes(@Query("token") String token);

    @POST("APP_CU_Get_News_List")
    Call<NewsList> getNewsList(@Body GetNewsListInput input, @Query("token") String token);

    @POST("APP_CU_News_Like")
    Call<Response> likeNews(@Body NewsActionGeneralInput input, @Query("token") String token);

    @POST("APP_CU_News_Bookmark")
    Call<Response> bookmarkNews(@Body NewsActionGeneralInput input, @Query("token") String token);

    @POST("APP_CU_Get_News")
    Call<News> getNews(@Body ManipulateNewsInput input, @Query("token") String token);

    @POST("APP_CU_Append_News")
    Call<AppendNewsResponse> appendNews(@Body AppendNewsInput input, @Query("token") String token);

    @POST("APP_CU_Update_News")
    Call<Response> updateNews(@Body UpdateNewsInput input, @Query("token") String token);

    @POST("APP_CU_News_Delete")
    Call<Response> deleteNews(@Body ManipulateNewsInput input, @Query("token") String token);

    @POST("APP_CU_News_Report")
    Call<Response> reportNews(@Body ReportNewsInput input, @Query("token") String token);

    @POST("APP_CU_Append_News_Comment")
    Call<Response> appendComment(@Body AppendCommentInput input, @Query("token") String token);

    @POST("APP_CU_Comment_Delete")
    Call<Response> deleteComment(@Body DeleteCommentInput input, @Query("token") String token);

    @POST("APP_CU_Member_Main_Content")
    Call<NewsList> getMemberContent(@Body MemberContentInput input, @Query("token") String token);

    @POST("APP_CU_MemberList_In_Behavior")
    Call<MemberRelationData> getMemberListInBehavior(@Body MemberListInBehaviorInput input, @Query("token") String token);

    @POST("APP_CU_Get_MemberRelation")
    Call<MemberRelationData> getMemberRelation(@Body MemberRelationInput input, @Query("token") String token);

    @POST("APP_CU_Search_MemberRelation")
    Call<MemberRelationData> searchMemberRelation(@Body SearchMemberRelationInput input, @Query("token") String token);

    @POST("APP_CU_MemberRelation_Recently")
    Call<MemberRelationData> searchMemberRelationRecently(@Body SearchMemberRelationRecentlyInput input, @Query("token") String token);

    @POST("APP_CU_Member_Follow")
    Call<Response> followMember(@Body FollowMemberInput input, @Query("token") String token);

    // CU_API10
    @POST("APP_CU_Get_Notice_Count")
    Call<NoticeCount> getNoticeCount(@Query("token") String token);

    // CU_API11
    @POST("APP_CU_Get_Notice_List")
    Call<NoticeData> getNoticeList(@Body GetNoticesInput input, @Query("token") String token);

    // CU_API12
    @POST("APP_CU_Notice_Read")
    Call<Response> setNoticeRead(@Body NoticeReadInput input, @Query("token") String token);

    // CU_API13
    @POST("APP_CU_Get_Notice_History")
    Call<NoticeData> getPastNoticeList(@Body GetNoticesInput input, @Query("token") String token);

    @POST("APP_CU_Get_Inbox_Content")
    Call<InboxContentData> getInboxContent(@Body InboxContentInput input, @Query("token") String token);

    // CU_API26
    @POST("APP_CU_Get_Notice_Ring_Close")
    Call<Response> clearNoticeIndication(@Query("token") String token);

//    @POST("APP_CU_Club_Append")
//    Call<ClubAppendNewResponse> appendNewClub(@Body ClubAppendNewInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Update")
//    Call<Response> updateClub(@Body ClubUpdateInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_Relation")
//    Call<ClubMemberRelationData> getClubMemberRelation(@Body ClubMemberRelationInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Add_Member_Invite")
//    Call<Response> addClubMemberInvitation(@Body ClubMemberInviteInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_My_JoinCount")
//    Call<MyClubCount> getMyClubCount(@Query("token") String token);
//
//    @POST("APP_CU_Club_Get_Member_List")
//    Call<ClubMemberRelationData2> getClubMemberList(@Body ClubMemberGetListInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_Guest_Page")
//    Call<CoverClubData> getCoverClubs(@Query("token") String token);
//
//    @POST("APP_CU_Club_Get_Guest_All_List")
//    Call<ClubList> getClubList(@Body GetClubListInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Member_Add")
//    Call<Response> joinClub(@Body ClubId id, @Query("token") String token);
//
//    @POST("APP_CU_Club_Leave")
//    Call<Response> leaveClub(@Body ClubLeaveInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Member_Discard")
//    Call<Response> discardClubMember(@Body ClubDiscardMemberInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_My_ClubList")
//    Call<MyClubListData> getMyClubList(@Body MyClubListInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_Single_Page")
//    Call<ClubSinglePageData> getClubSinglePage(@Body ClubGetSinglePageInput input, @Query("token") String token);

    @POST("APP_CU_Club_News_Like")
    Call<Response> clubLikeNews(@Body NewsActionGeneralInput input, @Query("token") String token);

    @POST("APP_CU_Club_News_Bookmark")
    Call<Response> clubBookmarkNews(@Body NewsActionGeneralInput input, @Query("token") String token);

//    @POST("APP_CU_Club_Append_News")
//    Call<AppendNewsResponse> clubAppendNews(@Body ClubAppendNewsInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_My_Page")
//    Call<MyClubPageData> getMyClubPage(@Body ClubGetMyPageInput input, @Query("token") String token);

    @POST("APP_CU_Club_Update_News")
    Call<Response> clubUpdateNews(@Body UpdateNewsInput input, @Query("token") String token);

    @POST("APP_CU_Club_Delete_News")
    Call<Response> clubDeleteNews(@Body ManipulateNewsInput input, @Query("token") String token);

//    @POST("APP_CU_Club_Set_TopNews")
//    Call<Response> clubNewsSetTop(@Body ClubSetTopNewsInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Get_News")
//    Call<ClubNews> getClubNews(@Body ManipulateNewsInput input, @Query("token") String token);

//    @POST("APP_CU_Club_Comment_Delete")
//    Call<Response> deleteClubComment(@Body DeleteCommentInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_Append_News_Comment")
//    Call<Response> appendClubComment(@Body AppendCommentInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_News_Like")
//    Call<Response> likeClubNews(@Body NewsActionGeneralInput input, @Query("token") String token);
//
//    @POST("APP_CU_Club_MemberList_In_Behavior")
//    Call<MemberRelationData> getClubMemberListInBehavior(@Body MemberListInBehaviorInput input, @Query("token") String token);

//    @POST("APP_CU_Club_Search")
//    Call<ClubSearchResult> searchClub(@Body ClubSearchInput input, @Query("token") String token);

    @GET("APP_Redeem_Get_Records")
    Call<RedeemList> getRedeemRecords(@Query("token") String token);

    @POST("APP_Redeem_Redeem")
    Call<Response> redeemExchange(@Body RedeemInput input, @Query("token") String token);

    @GET("APP_Redeem_Get_List")
    Call<RedeemPointListResponse> getRedeemPointList(@Query("token") String token);

    @POST("APP_Redeem_Receive")
    Call<RedeemCodeResponse> redeemGetLineCode(@Body RedeemInput input, @Query("token") String token);

    @POST("APP_Redeem_Copied")
    Call<Response> redeemCopy(@Body RedeemInput input, @Query("token") String token);

    @POST("APP_CU_PAT_Get_Content")
    Call<PatContentResponse> getPatContent(@Body PatContentInput input, @Query("token") String token);

    @POST("APP_CU_PAT_Send")
    Call<Response> sendPatNotice(@Body PatNoticeInput input, @Query("token") String token);

    @POST("APP_Add_App_Event_Log")
    Call<Response> sendPatEventLog(@Body PatEventLogInput input, @Query("token") String token);

    @GET("APP_Get_MyCar_Buttons")
    Call<MyCarButtonResponse> getMyCarButtons(@Query("token") String token, @Query("VIN") String vin, @Query("LicensePlateNumber") String licenseNumber);

    @POST("APP_Get_MemberCar_Maintenance_Plant")
    Call<MemberCarMaintenanceResponse> getMemberCarMaintenancePlant(@Body MemberCarMaintenancePlantInput input, @Query("token") String token);

    @GET("APP_Recall_Check_Campaign")
    Call<RecallCheckCampaignResponse> getCheckCarRecallCampaign(@Query("token") String token);

    @POST("APP_Recall_Read_NoticeBoard")
    Call<Response> updateRecallNoticeCount(@Query("token") String token);

    @GET("APP_Get_MemberLevel_Info_Webview")
    Call<MemberLevelInfoResponse> getMemberLevelInfoWebView(@Query("token") String token);

    @POST("APP_Del_Account")
    Call<Response> postDeleteAccount(@Query("token") String token);

    @POST("APP_Unbind_LineUID")
    Call<Response> postUnbindLine(@Query("token") String token);
}