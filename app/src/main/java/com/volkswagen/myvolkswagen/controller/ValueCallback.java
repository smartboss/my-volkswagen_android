package com.volkswagen.myvolkswagen.controller;

public interface ValueCallback<T> {
    void onSuccess(T t);
}
