package com.volkswagen.myvolkswagen.controller;

import android.app.Activity;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volkswagen.myvolkswagen.BuildConfig;
import com.volkswagen.myvolkswagen.model.GsonTypeAdapterFactories;
import com.volkswagen.myvolkswagen.model.Response;
import com.volkswagen.myvolkswagen.utils.Config;
import com.volkswagen.myvolkswagen.view.LoginActivity;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CruisysApiClientManager {

    private static CruisysApiClientManager sInstance = new CruisysApiClientManager();

    private Retrofit mRetrofit;

    private CruisysApiClientManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(loggingInterceptor);
        }
        OkHttpClient client = builder.build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactories.VW_INTEGER_FACTORY)
                .registerTypeAdapterFactory(GsonTypeAdapterFactories.VW_BOOLEAN_FACTORY)
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(Config.CRUISYS_HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    public static Retrofit getClient() {
        return sInstance.mRetrofit;
    }

    public static boolean handleTokenResponse(final Activity activity, final retrofit2.Response response) {
        Response data = (Response) response.body();
        if (data != null && data.errorCode == 1001) {
            if (activity != null) {
                LoginActivity.goLogin(activity);
            }
            return true;
        }

        // update token if necessary
        String auth = response.headers().get("Authorization");
        if (!TextUtils.isEmpty(auth)) {
            PreferenceManager.getInstance().saveUserToken(auth.replaceFirst("\\s*Bearer\\s+", ""));
        }
        return false;
    }

    private static String checkPlate(String plate) {
        String s = plate.toUpperCase();
        String plateNew = s.replaceAll("[^a-zA-Z0-9]*", "");
        return plateNew;
    }

    // 這邊的車牌不能有一槓，英文字母的車牌要全部大寫
    public static String getServiceReminderUrl(String plate, String userToken) {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/service_reminder/" + userToken;
        return url;
    }
    public static String getServiceCardUrl(String plate) {
        String plateNew = checkPlate(plate);
        String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/service_card";
        return url;
    }

    public static String getServiceWarrantyUrl(String plate) {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/warranty";
        return url;
    }

    public static String getCustomersQuotationsUrl(String accountId) {
        final String url = Config.CRUISYS_HOST + "customers/" + accountId + "/quotations";
        return url;
    }

    public static String GET_ServiceDongle_bindingUrl(String plate)
    {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/dongle_binding";
        return url;
    }

    public static String getServiceCarSummary(String plate)
    {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/summary";
        return url;
    }

    public static String getServiceCarLocation(String plate)
    {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/location";
        return url;
    }

    public static String getServiceCarTrips(String plate)
    {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/trips";
        return url;
    }

    public static String getServiceDongleNotification(String plate)
    {
        String plateNew = checkPlate(plate);
        final String url = Config.CRUISYS_HOST + "service_go/cars/" + plateNew + "/dongle_notification";
        return url;
    }
}
