package com.volkswagen.myvolkswagen.controller;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.volkswagen.myvolkswagen.model.AccountId;
import com.volkswagen.myvolkswagen.model.AddCarData;
import com.volkswagen.myvolkswagen.model.AppNews;
import com.volkswagen.myvolkswagen.model.Area;
import com.volkswagen.myvolkswagen.model.CarData;
import com.volkswagen.myvolkswagen.model.CarExistData;
import com.volkswagen.myvolkswagen.model.CarMaintenanceResponse;
import com.volkswagen.myvolkswagen.model.CarModelData;
import com.volkswagen.myvolkswagen.model.CarSummary;
import com.volkswagen.myvolkswagen.model.CarSummaryResponseData;
import com.volkswagen.myvolkswagen.model.DongleBindingCarData;
import com.volkswagen.myvolkswagen.model.DongleBindingInput;
import com.volkswagen.myvolkswagen.model.DongleBindingResponseData;
import com.volkswagen.myvolkswagen.model.DongleNotificationResponseData;
import com.volkswagen.myvolkswagen.model.EventJSON;
import com.volkswagen.myvolkswagen.model.ExtIdAuthResponse;
import com.volkswagen.myvolkswagen.model.ExtRedirectUrlInput;
import com.volkswagen.myvolkswagen.model.ExtRedirectUrlResponse;
import com.volkswagen.myvolkswagen.model.GetCarModelData;
import com.volkswagen.myvolkswagen.model.GetNoticesInput;
import com.volkswagen.myvolkswagen.model.InboxContent;
import com.volkswagen.myvolkswagen.model.InboxContentData;
import com.volkswagen.myvolkswagen.model.InboxContentInput;
import com.volkswagen.myvolkswagen.model.ExtIdAuthInput;
import com.volkswagen.myvolkswagen.model.LoginData_OTP;
import com.volkswagen.myvolkswagen.model.MaintenancePlant;
import com.volkswagen.myvolkswagen.model.MemberCarMaintenancePlantInput;
import com.volkswagen.myvolkswagen.model.MemberCarMaintenanceResponse;
import com.volkswagen.myvolkswagen.model.MemberLevelInfoResponse;
import com.volkswagen.myvolkswagen.model.MyCar;
import com.volkswagen.myvolkswagen.model.MyCarData;
import com.volkswagen.myvolkswagen.model.NewsData;
import com.volkswagen.myvolkswagen.model.Notice;
import com.volkswagen.myvolkswagen.model.NoticeCount;
import com.volkswagen.myvolkswagen.model.NoticeData;
import com.volkswagen.myvolkswagen.model.NoticeReadInput;
import com.volkswagen.myvolkswagen.model.NotificationData;
import com.volkswagen.myvolkswagen.model.OperateCarData;
import com.volkswagen.myvolkswagen.model.OtpData;
import com.volkswagen.myvolkswagen.model.PatContent;
import com.volkswagen.myvolkswagen.model.PatContentInput;
import com.volkswagen.myvolkswagen.model.PatContentResponse;
import com.volkswagen.myvolkswagen.model.PatEventLogInput;
import com.volkswagen.myvolkswagen.model.PatNoticeInput;
import com.volkswagen.myvolkswagen.model.Profile;
import com.volkswagen.myvolkswagen.model.PushData;
import com.volkswagen.myvolkswagen.model.RecallCheckCampaignResponse;
import com.volkswagen.myvolkswagen.model.ServiceReminder;
import com.volkswagen.myvolkswagen.model.ServiceReminderResponseData;
import com.volkswagen.myvolkswagen.model.UpdateProfileInput;
import com.volkswagen.myvolkswagen.model.User;
import com.volkswagen.myvolkswagen.model.Version;
import com.volkswagen.myvolkswagen.model.WarrantyData;
import com.volkswagen.myvolkswagen.model.WarrantyResponseData;
import com.volkswagen.myvolkswagen.utils.AlertUtils;
import com.volkswagen.myvolkswagen.utils.EventLog;
import com.volkswagen.myvolkswagen.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    public static final String TAG = Repository.class.getSimpleName();
    private static Repository repository;
    private ApiService apiService;
    private CruisysApiService cruisysApiService;

    public static Repository getInstance() {
        if (repository == null) {
            repository = new Repository();
        }

        return repository;
    }

    private Repository() {
        apiService = ApiClientManager.getClient().create(ApiService.class);
        cruisysApiService = CruisysApiClientManager.getClient().create(CruisysApiService.class);
    }

    public void getVersion(OnApiCallback<Version> onApiCallback) {
        apiService.getVersion().enqueue(new Callback<Version>() {
            @Override
            public void onResponse(Call<Version> call, Response<Version> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    onApiCallback.onSuccess(response.body());
                } else {
                    onApiCallback.onFailure(new Throwable(""));
                }
            }

            @Override
            public void onFailure(Call<Version> call, Throwable t) {
                onApiCallback.onFailure(t);
            }
        });
    }

    public void genOtpCode(String countryCode, String phone, OnApiCallback<Boolean> onApiCallback) {
        apiService.genOtpCode(new OtpData(countryCode, phone)).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().result) {
                        onApiCallback.onSuccess(true);
                    } else {
                        try {
                            JSONObject responseObject = new JSONObject(response.body().errorMsg);
                            String msg = responseObject.optString("msg", "");
                            onApiCallback.onFailure(new Throwable(msg));
                        } catch (JSONException e) {
                            onApiCallback.onFailure(new Throwable(""));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                onApiCallback.onFailure(t);
            }
        });
    }

    public void postLoginByOtp(String prefix, String phone, String code, String firebaseId, OnApiCallback<User> apiCallback) {
        LoginData_OTP data = new LoginData_OTP(prefix, phone, code, firebaseId);
        apiService.loginByOtp(data).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    User user = response.body();
                    PreferenceManager.getInstance().saveUserData(user.token, user.data.accountId, user.data.email, user.data.mobile, user.data.level, user.data.levelName);
                    apiCallback.onSuccess(user);
                } else if (response.body() != null) {
                    try {
                        JSONObject responseObject = new JSONObject(response.body().errorMsg);
                        String msg = responseObject.optString("msg", "");
                        apiCallback.onFailure(new Throwable(msg));
                    } catch (Exception e) {
                        apiCallback.onFailure(new Throwable("unknown error"));
                    }
                } else {
                    apiCallback.onFailure(new Throwable("unknown error"));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                apiCallback.onFailure(t);
            }
        });
    }

    public void autoLogin(OnApiCallback<Boolean> apiCallback) {
        apiService.autoLogin(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    updateFirebaseToken(PreferenceManager.getInstance().getPushToken());
                    User user = response.body();
                    PreferenceManager.getInstance().saveUserData(user.token, user.data.accountId, user.data.email, user.data.mobile, user.data.level, user.data.levelName);
                    apiCallback.onSuccess(true);
                } else if (response.body() != null) {
                    try {
                        JSONObject responseObject = new JSONObject(response.body().errorMsg);
                        String msg = responseObject.optString("msg", "");
                        apiCallback.onFailure(new Throwable(msg));
                    } catch (Exception e) {
                        apiCallback.onFailure(new Throwable("unknown error"));
                    }
                } else {
                    apiCallback.onFailure(new Throwable("unknown error"));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                apiCallback.onFailure(t);
            }
        });
    }

    public void postGetCruisysToken(OnApiCallback<ExtIdAuthResponse> callback) {
        String accountId = PreferenceManager.getInstance().getAccountId();
        String plateNo = PreferenceManager.getInstance().getMainCarPlate();
        ExtIdAuthInput loginData = new ExtIdAuthInput(accountId, plateNo);

        cruisysApiService.postExtIdAuth(loginData).enqueue(new Callback<ExtIdAuthResponse>() {
            @Override
            public void onResponse(Call<ExtIdAuthResponse> call, Response<ExtIdAuthResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().extIdAuthData != null) {
                    PreferenceManager.getInstance().saveCruisysAccessToken(response.body().extIdAuthData.accessToken);
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<ExtIdAuthResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getServiceReminder(OnApiCallback<List<ServiceReminder>> callback) {
        String plateNo = Utils.formatPlateNoToApiPath(PreferenceManager.getInstance().getMainCarPlate());
        String userToken = PreferenceManager.getInstance().getUserToken();
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.getServiceReminder(plateNo, userToken, cruisysToken).enqueue(new Callback<ServiceReminderResponseData>() {
            @Override
            public void onResponse(Call<ServiceReminderResponseData> call, Response<ServiceReminderResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().serviceReminderList != null) {
                    callback.onSuccess(response.body().serviceReminderList);
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<ServiceReminderResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getWarranty(OnApiCallback<WarrantyData> callback) {
        String plateNo = Utils.formatPlateNoToApiPath(PreferenceManager.getInstance().getMainCarPlate());
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.getWarranty(plateNo, cruisysToken).enqueue(new Callback<WarrantyResponseData>() {
            @Override
            public void onResponse(Call<WarrantyResponseData> call, Response<WarrantyResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().data != null) {
                    callback.onSuccess(response.body().data);
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<WarrantyResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getDongleBinding(String plateNo, OnApiCallback<DongleBindingCarData> callback) {
        String formatPlateNo = Utils.formatPlateNoToApiPath(plateNo);
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.getDongleBinding(formatPlateNo, cruisysToken).enqueue(new Callback<DongleBindingResponseData>() {
            @Override
            public void onResponse(Call<DongleBindingResponseData> call, Response<DongleBindingResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().dongleBindingCarDataList != null && response.body().dongleBindingCarDataList.size() > 0) {
                    callback.onSuccess(response.body().dongleBindingCarDataList.get(0));
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<DongleBindingResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postDongleBinding(String plateNo, String imei, String model, OnApiCallback<DongleBindingCarData> callback) {
        String formatPlateNo = Utils.formatPlateNoToApiPath(plateNo);
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        DongleBindingInput input = new DongleBindingInput(imei, model);
        cruisysApiService.postDongleBinding(formatPlateNo, input, cruisysToken).enqueue(new Callback<DongleBindingResponseData>() {
            @Override
            public void onResponse(Call<DongleBindingResponseData> call, Response<DongleBindingResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().dongleBindingCarDataList != null && response.body().dongleBindingCarDataList.size() > 0) {
                    callback.onSuccess(response.body().dongleBindingCarDataList.get(0));
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<DongleBindingResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getPatContent(final Context context, String vin, String carId, int currentMile, final ValueCallback<PatContent> valueCallback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);

        PatContentInput patContentInput = new PatContentInput(vin, carId, currentMile);
        apiService.getPatContent(patContentInput, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<PatContentResponse>() {
            @Override
            public void onResponse(Call<PatContentResponse> call, Response<PatContentResponse> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    valueCallback.onSuccess(response.body().patContent);
                } else {
                    Toast.makeText(context, response.body() == null ? "發生錯誤" : response.body().errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PatContentResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void sendPatNotice(final Context context, String patId, final ValueCallback<Boolean> valueCallback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);

        apiService.sendPatNotice(new PatNoticeInput(patId), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    valueCallback.onSuccess(true);
                } else {
                    Toast.makeText(context, response.body() == null ? "發生錯誤" : response.body().errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void sendPatEventLog(String serviceName, String eventName, Integer inboxId, String eventCategory) {
        String accountId = PreferenceManager.getInstance().getAccountId();
        int level = PreferenceManager.getInstance().getLevel();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        Date date = new Date();
        date.setTime(System.currentTimeMillis());
        String timeStamp = format.format(date);

        String eventLabel = eventCategory.equalsIgnoreCase(EventLog.EVENT_CATEGORY_BUTTON) ? eventName : "";
        EventJSON eventJSON = new EventJSON(eventCategory, serviceName, eventLabel, "", "", "");

        PatEventLogInput input = new PatEventLogInput(accountId, level, serviceName, eventName, null, inboxId, timeStamp, eventJSON);
        apiService.sendPatEventLog(input, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
            }
        });
    }

    public void getMemberCarMaintenancePlant(final Context context, String vin, String licensePlateNumber, final ValueCallback<MaintenancePlant> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);

        apiService.getMemberCarMaintenancePlant(new MemberCarMaintenancePlantInput(vin, licensePlateNumber), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<MemberCarMaintenanceResponse>() {
            @Override
            public void onResponse(Call<MemberCarMaintenanceResponse> call, Response<MemberCarMaintenanceResponse> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result && response.body().data != null) {
                    callback.onSuccess(response.body().data.maintenancePlant);
                } else {
                    Toast.makeText(context, response.body() == null ? "發生錯誤" : response.body().errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MemberCarMaintenanceResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void checkCarHasRecallCampaign(final Context context, final ValueCallback<RecallCheckCampaignResponse.RecallCheckCampaignData> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);
        apiService.getCheckCarRecallCampaign(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<RecallCheckCampaignResponse>() {
            @Override
            public void onResponse(Call<RecallCheckCampaignResponse> call, Response<RecallCheckCampaignResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(response.body().data);
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<RecallCheckCampaignResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void getMemberLevelInfo(final Context context, final ValueCallback<MemberLevelInfoResponse.MemberLevelInfo> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);
        apiService.getMemberLevelInfoWebView(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<MemberLevelInfoResponse>() {
            @Override
            public void onResponse(Call<MemberLevelInfoResponse> call, Response<MemberLevelInfoResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(response.body().data);
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<MemberLevelInfoResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void getMyCarDataList(final OnApiCallback<List<CarData>> callback) {
        apiService.getMyCarData(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<MyCarData>() {
            @Override
            public void onResponse(Call<MyCarData> call, Response<MyCarData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result && response.body().cars != null) {
                    List<CarData> dataList = new ArrayList<>();

                    for (MyCar myCar : response.body().cars) {
                        dataList.addAll(myCar.carData);
                    }

                    if (dataList.size() > 0 && (PreferenceManager.getInstance().getMainCarPlate().isEmpty() || PreferenceManager.getInstance().getMainCarPlate().isEmpty())) {
                        //if no main car in app, save the first one car
                        PreferenceManager.getInstance().saveMainCar(dataList.get(0).name, dataList.get(0).plate, dataList.get(0).vin);
                    }

                    callback.onSuccess(dataList);
                } else {
                    callback.onFailure(null);
                }
            }

            @Override
            public void onFailure(Call<MyCarData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void removeCar(String name, String plate, String vin, final OnApiCallback<Boolean> callback) {
        apiService.deleteCar(new OperateCarData(name, plate, vin), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    boolean result = response.body().result;
                    if (result) {
                        String currentMainCarPlate = PreferenceManager.getInstance().getMainCarPlate();
                        if (plate.equalsIgnoreCase(currentMainCarPlate)) {
                            PreferenceManager.getInstance().deleteMainCar();
                        }

                        callback.onSuccess(true);
                    } else {
                        callback.onFailure(new Throwable(response.body().errorMsg));
                    }
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void removeCarDongle(String plateNo, final OnApiCallback<Boolean> callback) {
        String formatPlateNo = Utils.formatPlateNoToApiPath(plateNo);
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.deleteDongleBinding(formatPlateNo, cruisysToken).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (callback == null) return;

                if (response.isSuccessful()) {
                    callback.onSuccess(true);
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getCarSummary(OnApiCallback<CarSummary> callback) {
        String plateNo = Utils.formatPlateNoToApiPath(PreferenceManager.getInstance().getMainCarPlate());
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.getSummary(plateNo, cruisysToken).enqueue(new Callback<CarSummaryResponseData>() {
            @Override
            public void onResponse(Call<CarSummaryResponseData> call, Response<CarSummaryResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().carSummaryDataList != null && response.body().carSummaryDataList.size() > 0) {
                    callback.onSuccess(response.body().carSummaryDataList.get(0));
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<CarSummaryResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getDongleNotification(OnApiCallback<NotificationData> callback) {
        String plateNo = Utils.formatPlateNoToApiPath(PreferenceManager.getInstance().getMainCarPlate());
        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        cruisysApiService.getDongleNotification(plateNo, cruisysToken).enqueue(new Callback<DongleNotificationResponseData>() {
            @Override
            public void onResponse(Call<DongleNotificationResponseData> call, Response<DongleNotificationResponseData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().data != null) {
                    callback.onSuccess(response.body().data);
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<DongleNotificationResponseData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postGetExtRedirectUrl(String type, OnApiCallback<String> callback) {
        String accountId = PreferenceManager.getInstance().getAccountId();
        String plateNo = PreferenceManager.getInstance().getMainCarPlate();
        String vin = PreferenceManager.getInstance().getMainCarVin();

        String cruisysToken = "Bearer " + PreferenceManager.getInstance().getCruisysAccessToken();
        ExtRedirectUrlInput input = new ExtRedirectUrlInput(type, PreferenceManager.getInstance().getUserToken(), vin, plateNo, accountId);
        cruisysApiService.postExtRedirectUrl(input, cruisysToken).enqueue(new Callback<ExtRedirectUrlResponse>() {
            @Override
            public void onResponse(Call<ExtRedirectUrlResponse> call, Response<ExtRedirectUrlResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().extRedirectUrlData != null) {
                    callback.onSuccess(response.body().extRedirectUrlData.redirectUrl);
                } else {
                    callback.onFailure(new Throwable("未知錯誤"));
                }
            }

            @Override
            public void onFailure(Call<ExtRedirectUrlResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getNoticeCount(OnApiCallback<Integer> callback) {
        apiService.getNoticeCount(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<NoticeCount>() {
            @Override
            public void onResponse(Call<NoticeCount> call, Response<NoticeCount> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(response.body().count);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<NoticeCount> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getNoticeList(int kind, OnApiCallback<List<Notice>> callback) {
        apiService.getNoticeList(new GetNoticesInput(Integer.toString(kind)), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<NoticeData>() {
            @Override
            public void onResponse(Call<NoticeData> call, retrofit2.Response<NoticeData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result && response.body().noticeList != null) {
                    callback.onSuccess(response.body().noticeList);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<NoticeData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getNewsList(OnApiCallback<List<NewsData>> callback) {
        apiService.getAppNews().enqueue(new Callback<AppNews>() {
            @Override
            public void onResponse(Call<AppNews> call, retrofit2.Response<AppNews> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    List<NewsData> list = new ArrayList<>();
                    if (response.body().newsDataList != null) {
                        list.addAll(response.body().newsDataList);
                    }
                    callback.onSuccess(list);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<AppNews> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void readNotice(int noticeID) {
        apiService.setNoticeRead(new NoticeReadInput(noticeID), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
            }
        });
    }

    public void getInboxContent(Context context, int newsId, OnApiCallback<InboxContent> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);
        apiService.getInboxContent(new InboxContentInput(newsId), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<InboxContentData>() {
            @Override
            public void onResponse(Call<InboxContentData> call, retrofit2.Response<InboxContentData> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(response.body().inboxContent);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<InboxContentData> call, Throwable t) {
                hud.dismiss();
                callback.onFailure(t);
            }
        });
    }

    public void deleteNotice(final Context context, int noticeId, OnApiCallback<Boolean> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);
        apiService.deleteNotice(Integer.toString(noticeId), "Bearer " + PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
                hud.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(true);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                hud.dismiss();
                callback.onFailure(t);
            }
        });
    }

    public void getMemberProfile(OnApiCallback<Profile> callback) {
        apiService.getProfile(new AccountId(PreferenceManager.getInstance().getAccountId()), PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, retrofit2.Response<Profile> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    PreferenceManager.getInstance().saveUserNickname(response.body().nickname);
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getMaintenancePlant(OnApiCallback<List<Area>> callback) {
        apiService.getMaintenancePlant(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<CarMaintenanceResponse>() {
            @Override
            public void onResponse(Call<CarMaintenanceResponse> call, Response<CarMaintenanceResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result && response.body().data != null) {
                    callback.onSuccess(response.body().data);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<CarMaintenanceResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postUpdateMemberProfile(String nickname, String email, String phone, String address, String birth, String gender, String areaId, String plantId, OnApiCallback<Boolean> callback) {
        UpdateProfileInput input = new UpdateProfileInput(nickname, gender, "", email, phone, birth, "", "", areaId, plantId, address);
        apiService.updateProfile(input, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(true);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postGetCarExist(String name, String plate, String vin, OnApiCallback<CarExistData> callback) {
        OperateCarData data = new OperateCarData();
        data.name = name;
        data.vin = vin;
        data.plate = plate;

        apiService.getCarExist(data, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<CarExistData>() {
            @Override
            public void onResponse(Call<CarExistData> call, Response<CarExistData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<CarExistData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getCarModelData(int carTypeId, OnApiCallback<CarModelData> callback) {
        GetCarModelData data = new GetCarModelData(carTypeId);
        apiService.getCarModelData(data, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<CarModelData>() {
            @Override
            public void onResponse(Call<CarModelData> call, Response<CarModelData> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<CarModelData> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postAddCar(String name, String plate, String vin, int carTypeId, int carModelId, OnApiCallback<com.volkswagen.myvolkswagen.model.Response> callback) {
        AddCarData data = new AddCarData();
        data.name = name;
        data.plate = plate;
        data.vin = vin;
        data.typeId = carTypeId;
        data.modelId = carModelId;

        apiService.addCar(data, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void logout(final Context context, final OnApiCallback<Boolean> callback) {
        final KProgressHUD hud = AlertUtils.showLoadingHud(context);
        apiService.logout(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
                hud.dismiss();
                PreferenceManager.getInstance().deleteUserToken();
                PreferenceManager.getInstance().deleteCruisysAccessToken();
                callback.onSuccess(true);
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                hud.dismiss();
                PreferenceManager.getInstance().deleteUserToken();
                PreferenceManager.getInstance().deleteCruisysAccessToken();
                callback.onSuccess(false);
            }
        });
    }

    public void updateFirebaseToken(String firebaseToken) {
        PushData data = new PushData("android", firebaseToken);
        apiService.updatePushToken(data, PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, Response<com.volkswagen.myvolkswagen.model.Response> response) {
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
            }
        });
    }

    public void postUnbindLine(final OnApiCallback<Boolean> callback) {
        apiService.postUnbindLine(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(true);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void postDeleteAccount(final OnApiCallback<Boolean> callback) {
        apiService.postDeleteAccount(PreferenceManager.getInstance().getUserToken()).enqueue(new Callback<com.volkswagen.myvolkswagen.model.Response>() {
            @Override
            public void onResponse(Call<com.volkswagen.myvolkswagen.model.Response> call, retrofit2.Response<com.volkswagen.myvolkswagen.model.Response> response) {
                if (response.isSuccessful() && response.body() != null && response.body().result) {
                    callback.onSuccess(true);
                } else {
                    callback.onFailure(new Throwable("unKnown"));
                }
            }

            @Override
            public void onFailure(Call<com.volkswagen.myvolkswagen.model.Response> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void requestFirebaseToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    String token = task.getResult();
                    PreferenceManager.getInstance().savePushToken(token);
                });
    }

    public interface OnApiCallback<T> extends ValueCallback<T> {
        void onFailure(Throwable t);
    }
}
